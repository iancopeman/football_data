//
//  MatchTests.m
//  FootballDataTests
//
//  Created by Ian Copeman on 17/02/2025.
//

#import <XCTest/XCTest.h>
#import "FootballModel.h"
#import "FileManager.h"
#import "NSString+CSVParser.h"
#import "APIFootballParser.h"

@interface MatchTests : XCTestCase

@end

@implementation MatchTests {
    FootballModel* model;
    FileManager* fileManager;
    NSArray* competitionArray;
    NSUInteger competitionArrayCount;
    NSUInteger competitionArrayEntrySize;
}

- (void)setUp {
    model = [FootballModel getModel];
    fileManager = [[FileManager alloc] init];
    
    [model loadTeams];
    [model loadFootballers];
    
    competitionArray = @[
       /*
        @(CompetitionPremierLeague), @(1992), @(0),
        @(CompetitionFACup), @(1872), @(0),
        @(CompetitionLeagueCup), @(1960), @(0),
        @(CompetitionEuropeanCup), @(1955), @(0),
        @(CompetitionEuropeanCupWinnersCup), @(1960), @(1998),
        @(CompetitionEuropaLeague), @(2009), @(0),
        @(CompetitionCharityShield), @(1921), @(0),
        @(CompetitionUEFACup), @(1971), @(2008),
        @(CompetitionChampionship), @(2004), @(0),
        @(CompetitionEuropaConferenceLeague), @(2021), @(0),
*/
        @(CompetitionFirstDivision), @(1888), @(1991),
      //  CompetitionFirstDivision

/*
        
        @(CompetitionEuropaLeague), @(3), // First season 2014
*/
    ];
    competitionArrayEntrySize = 3;
    competitionArrayCount = competitionArray.count / competitionArrayEntrySize;
    
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}


#pragma mark - Matches

//
// Check matches in .csv files maych those with json files
#define FilePath (@"/Devlex/Matches/")

- (void)testMatchCoverage {
    int seasonInt = 1975
    ;
    int decade = seasonInt / 10 * 10;
    int year = seasonInt % 10;
    int season = decade + year;
    int nextSeason = (decade + year + 1) % 100;
    
    //
    // Directories
    NSArray* subDirectories = @[ @"Complete", @"Partial", @"Basic"];
    
    NSMutableDictionary* matchArrayDictionary = [NSMutableDictionary dictionaryWithCapacity:10];
    NSString* seasonString = [NSString stringWithFormat:@"%ds/%d-%02d/", decade, decade + year, nextSeason];
    NSString* dataPath  = [[NSUserDefaults standardUserDefaults] objectForKey:@"dataPath"];
    NSString* directoryPath = [dataPath stringByAppendingPathComponent:FilePath];
    NSURL* directoryURL = [NSURL URLWithString:[directoryPath  stringByAppendingPathComponent:seasonString]];
    
    NSError* error;
    NSArray * dirs = [[NSFileManager defaultManager]  contentsOfDirectoryAtURL:directoryURL
                                                    includingPropertiesForKeys:@[]
                                                                       options:NSDirectoryEnumerationSkipsSubdirectoryDescendants | NSDirectoryEnumerationSkipsHiddenFiles
                                                                         error:&error];
    //
    // Enumerate csv files
    [dirs enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSURL* filename = (NSURL *)obj;
        if (filename.hasDirectoryPath) {
            int ian = 10;
        }
        else {
            NSString *lastPathComponent = [filename lastPathComponent];
            int ian = 10;
            CompetitionType competitionType = [Match getCompetitionFromShortName:lastPathComponent];
            NSArray* matchList = [self getMatchListFromCSVFile:filename];
            if (matchList != nil) {
                [matchArrayDictionary setObject:matchList forKey:@(competitionType)];
            }
        }
    }];
    
    //
    // Enumerate JSON files
    [dirs enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSURL* filename = (NSURL *)obj;
        if (filename.hasDirectoryPath) {
            NSString *lastPathComponent = [filename lastPathComponent];
            CompetitionType competitionType = [Match getCompetitionFromShortName:lastPathComponent];
            if (competitionType != CompetitionUnknown) {
                //
                // Directories are loaded in order of subDirectories array
                for (NSString* subDirectoryString in subDirectories) {
                    NSURL* subDirectory = [filename URLByAppendingPathComponent:subDirectoryString];
                    NSDictionary* matchListJSON = [self checkMatchesFromURL:subDirectory];
                    if (matchListJSON.count != 0) {
                        NSArray* matchListCSV = [matchArrayDictionary objectForKey:@(competitionType)];
                        if (![self checkJSONList:matchListJSON csvList:matchListCSV]) {
                            XCTFail(@"%d: Failed to validate %@ match lists in %@", season, lastPathComponent, subDirectoryString);
                        }
                        else {
                            NSLog(@"%d: Validated %@ match lists in %@", season, lastPathComponent, subDirectoryString);
                        }
                    }
                    int ian = 10;
                    // [self loadMatchesFromFile:filename competitionType:competitionType season:season];
                }
            }
            
            int ian = 10;
        }
    }];
    
    
}

- (void)testMatchNames {
    int seasonStart = 1890;
    int seasonEnd = 2024;
    for (int i = seasonStart; i <= seasonEnd; i++) {
        [self testMatchNames:i];
    }
    
}


- (void)testMatchNames:(int)seasonInt {
    ;
    int decade = seasonInt / 10 * 10;
    int year = seasonInt % 10;
    int season = decade + year;
    int nextSeason = (decade + year + 1) % 100;
    
    //
    // Directories
    NSArray* subDirectories = @[ @"Complete", @"Partial", @"Basic"];
    
    NSMutableDictionary* matchArrayDictionary = [NSMutableDictionary dictionaryWithCapacity:10];
    NSString* seasonString = [NSString stringWithFormat:@"%ds/%d-%02d/", decade, decade + year, nextSeason];
    NSString* dataPath  = [[NSUserDefaults standardUserDefaults] objectForKey:@"dataPath"];
    NSString* directoryPath = [dataPath stringByAppendingPathComponent:FilePath];
    NSURL* directoryURL = [NSURL URLWithString:[directoryPath  stringByAppendingPathComponent:seasonString]];
    
    NSError* error;
    NSArray * dirs = [[NSFileManager defaultManager]  contentsOfDirectoryAtURL:directoryURL
                                                    includingPropertiesForKeys:@[]
                                                                       options:NSDirectoryEnumerationSkipsSubdirectoryDescendants | NSDirectoryEnumerationSkipsHiddenFiles
                                                                         error:&error];
    //
    // Enumerate csv files
    [dirs enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSURL* filename = (NSURL *)obj;
        if (filename.hasDirectoryPath) {
            int ian = 10;
        }
        else {
            NSString *lastPathComponent = [filename lastPathComponent];
            int ian = 10;
            CompetitionType competitionType = [Match getCompetitionFromShortName:lastPathComponent];
            NSArray* matchList = [self getMatchListFromCSVFile:filename];
            if (matchList != nil) {
                [matchArrayDictionary setObject:matchList forKey:@(competitionType)];
            }
        }
    }];
    
    //
    // Enumerate JSON files
    [dirs enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSURL* filename = (NSURL *)obj;
        if (filename.hasDirectoryPath) {
            NSString *lastPathComponent = [filename lastPathComponent];
            CompetitionType competitionType = [Match getCompetitionFromShortName:lastPathComponent];
            if (competitionType != CompetitionUnknown) {
                //
                // Directories are loaded in order of subDirectories array
                for (NSString* subDirectoryString in subDirectories) {
                    NSURL* subDirectory = [filename URLByAppendingPathComponent:subDirectoryString];
                    NSDictionary* matchListJSON = [self checkMatchesFromURL:subDirectory];
                    if (matchListJSON.count != 0) {
                        NSArray* matchListKeys = [matchListJSON allKeys];
                        //
                        // Keys should contain the year
                        for (NSString* matchNameJSON in matchListKeys) {
                            NSString* yearString = [matchNameJSON substringToIndex:4];
                            int year = [yearString intValue];
                            if (seasonInt != year && seasonInt + 1 != year) {
                                NSLog(@"Match has incorrect year: %@, %d", matchNameJSON, seasonInt);
                            }
                        }
                    }
                    int ian = 10;
                    // [self loadMatchesFromFile:filename competitionType:competitionType season:season];
                }
            }
            
            int ian = 10;
        }
    }];
    
    
}


- (bool)checkJSONList:(NSDictionary*)matchDictionaryJSON csvList:(NSArray*)matchListCSV {
    //
    // Every JSOM file must be in CSV list
    bool returnValue = true;
    NSArray* matchListJSON = [matchDictionaryJSON allKeys];
    for (NSString* matchNameJSON in matchListJSON) {
        bool found = false;
        for (NSString* matchNameCSV in matchListCSV) {
            if ([matchNameJSON compare:matchNameCSV] == NSOrderedSame) {
                found = true;
                break;
            }
        }
        if (!found) {
            NSURL* matchURL = [matchDictionaryJSON objectForKey:matchNameJSON];
            NSURL* pathNoExtension = [matchURL URLByDeletingPathExtension];
            NSString *matchID = [pathNoExtension lastPathComponent];
            
            NSLog(@"Failed to find match: %@, %@", matchNameJSON, matchID);
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            [fileManager removeItemAtURL:matchURL  error:NULL];
            returnValue = false;
        }
    }
    return returnValue;
    
}


- (NSDictionary*)checkMatchesFromURL:(NSURL*)directoryURL {
    NSMutableDictionary* matchDictionary = [NSMutableDictionary dictionaryWithCapacity:100];
    NSError* error;
    NSArray * dirs = [[NSFileManager defaultManager]  contentsOfDirectoryAtURL:directoryURL
                                                    includingPropertiesForKeys:@[]
                                                                       options:NSDirectoryEnumerationSkipsSubdirectoryDescendants | NSDirectoryEnumerationSkipsHiddenFiles
                                                                         error:&error];
    [dirs enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSURL* filename = (NSURL *)obj;
        if (!filename.hasDirectoryPath) {
            if ([self->fileManager doesFileExist:filename]) {
                // 19990124_Bradford City_Newcastle United.json
                
                NSURL* pathNoExtension = [filename URLByDeletingPathExtension];
                NSString *lastPathComponent = [pathNoExtension lastPathComponent];
                NSArray* stringParts = [lastPathComponent componentsSeparatedByString:@"_"];
                NSString* dateString = stringParts[0];
                NSString* team1String = stringParts[1];
                NSString* team2String = stringParts[2];
                
                int matchDateNumber = [dateString intValue];
                int team1ID = [model getTeamID:team1String];
                int team2ID = [model getTeamID:team2String];
                NSString* matchKey = [Match makeMatchKeyFromDateNumber:matchDateNumber team1:team1ID team2:team2ID];
                [matchDictionary setObject:filename forKey:matchKey];
            }
        }
    }];
    return matchDictionary;
    
}



- (NSArray*)getMatchListFromCSVFile:(NSURL*)fileURL {
    NSString* fileString = [fileManager loadPersistedString:fileURL];
    if (fileString == nil) {
        return nil;
    }
    NSMutableArray* matchArray = [NSMutableArray arrayWithCapacity:100];
    NSArray* rows = [fileString csvRows];
    int count = 0;
    for (NSArray *row in rows){
        if (row.count != 8) {
            break;
        }
        NSString* dateString = row[0];
        if ([dateString containsString:@"Date"]) continue;
        NSString* team1String = row[1];
        NSString* team2String = row[3];
        
        int matchDateNumber = [dateString intValue];
        int team1ID = [model getTeamID:team1String];
        int team2ID = [model getTeamID:team2String];
        NSString* matchKey = [Match makeMatchKeyFromDateNumber:matchDateNumber team1:team1ID team2:team2ID];
        [matchArray addObject:matchKey];
    }
    return matchArray;
}

- (void)testMatchDate {
    int ian = 10;

    for (int season = 2022; season < 2023; season++) {
        NSArray* matchArray = [model getAllMatchesInSeason:season   competition:CompetitionPremierLeague];
        for (Match* match in matchArray) {
            if (match.dateNumber == 20041114) {
                int ian = 10;
            }
            //
            // Check match's events
            NSArray* events = @[match.team1Events, match.team2Events];
            for (NSArray* eventsTeam in events) {
                for (NSNumber* eventNumber in eventsTeam) {
                    unsigned long long event = [eventNumber unsignedLongLongValue];
                    EventHeader* eventHeader = (EventHeader*)&event;
                    if (eventHeader->eventType == EventGoal) {
                        if (eventHeader->playerID == 0) {
                            NSLog(@"Goal without player: %d: ", match.dateNumber);
                            //  XCTFail(@"Failed to validate event");
                        }
                    }
                }
            }
        }
    }
    
}

- (void)testMatches {
    int ian = 10;
    
    NSArray* matchArray = [model getAllMatchesInSeason:2023 competition:CompetitionPremierLeague];
    NSMutableData* buffer = [NSMutableData dataWithCapacity:5000];
    for (Match* match in matchArray) {
        if (match.dateNumber == 20230813) {
            int ina = 10;
        }
        //  [match encodeToBlob:buffer];
    }
    
}


- (void)testMatchDuplicates {
    
    for (int season = 1990; season <= 2023; season++) {
        [model loadMatchesSeason:season];
        NSMutableDictionary* matchDictionary = [NSMutableDictionary dictionaryWithCapacity:100];
        
        NSArray* matchArray = [model getAllMatchesInSeason:season competition:CompetitionEuropeanCup];
        bool foundFinal = false;
        for (Match* match in matchArray) {
            NSString* key = [NSString stringWithFormat:@"%d_%d_%ld", match.getTeam1ID, match.getTeam2ID, (long)match.competitionFlag];
            Match* duplicate = [matchDictionary objectForKey:key];
            if (duplicate != nil) {
                NSLog(@"Duplicate matches: %@, %@", match.matchKey, duplicate.matchKey);
            }
            [matchDictionary setObject:match forKey:key];
        }
    }
    
    int ian = 10;
}

    

- (void)testMatchRoundInfo {
    
    for (int season = 2020; season <= 2023; season++) {
        [model loadMatchesSeason:season];

        NSArray* matchArray = [model getAllMatchesInSeason:season competition:CompetitionEuropaLeague];
        bool foundFinal = false;
        for (Match* match in matchArray) {
            if (match.competitionFlag == COMPETITION_FINAL_FLAG) {
                foundFinal = true;
            }
            //  [match encodeToBlob:buffer];
        }
        if (!foundFinal) {
            NSLog(@"Year without Final: %d", season);
            NSLog(@"without Final: %d", season);
           int ian = 10;

        }
    }
    
    int ian = 10;
}

- (void)testMatchCompetitonNumber {
    
    for (int season = 2000; season < 2001; season++) {
        [model loadMatchesSeason:season];

        NSArray* matchArray = [model getAllMatchesInSeason:season competition:CompetitionCharityShield];
        if (matchArray.count != 1) {
            NSLog(@"Incorrect match count: %d - %d", season, matchArray.count);
           int ian = 10;
        }
    }
    
    int ian = 10;
}


- (void)testMatchYear {
    int season = 1890;
    [model loadMatchesSeason:season];
    NSArray* matchArray = [model getAllMatchesInSeason:season competition:CompetitionFirstDivision];

    int ian;

}


#pragma mark - Match Number

- (void)testMatchNumber {
    int matchErrorCountGlobal = 0;
    NSMutableSet* seasonErrorArray = [NSMutableSet setWithCapacity:25];

    for (int season = 1870; season <= 2024; season++) {
        for (int i = 0; i < competitionArrayCount; i++) {
            int competition = [competitionArray[i * competitionArrayEntrySize] intValue];
            if (!IS_LEAGUE_COMPETITION(competition)) {
                continue;
            }
            NSMutableArray* competitionProcessArray = [NSMutableArray arrayWithCapacity:10];
            int startSeason = [competitionArray[i * competitionArrayEntrySize + 1] intValue];
            int endSeason = [competitionArray[i * competitionArrayEntrySize + 2] intValue];
            if (startSeason <= season && (endSeason >= season || endSeason == 0)) {
                [competitionProcessArray addObject:competitionArray[i * competitionArrayEntrySize]];
            }
            
            if (competitionProcessArray.count > 0) {
                [model loadMatchesSeason:season];
                for (NSNumber* competitionNumber in competitionProcessArray) {
                    NSArray* matchArray = [model getAllMatchesInSeason:season competition:competition];
                    NSMutableSet* teamArray = [NSMutableSet setWithCapacity:25];
                    int matchCount = 0;
                    for (Match* match in matchArray) {
                        if (match.getTeam2ID == 0) {
                            continue;
                        }
                        matchCount++;
                        [teamArray addObject:@(match.getTeam1ID)];
                        [teamArray addObject:@(match.getTeam2ID)];
                    }
                    if (matchCount != teamArray.count * teamArray.count - teamArray.count) {
                        NSLog(@"Season with incorrect match number: %d", season);
                        [seasonErrorArray addObject:@(season)];
                        matchErrorCountGlobal++;
                    }
                }
            }
        }

    }
    if (matchErrorCountGlobal != 0) {
        NSLog(@"Seasons with incorrect MatchNumbers: %d", matchErrorCountGlobal);
        for (NSNumber* seasonNumber in seasonErrorArray) {
            NSLog(@"--- Season:%@", seasonNumber);
        }
    }
    else {
        NSLog(@"All Seasons Correct Match Number");

    }

    int ian2 = 10;
}

- (void)testMatchNumberInSeasonCompetition {
    int season = 2024;
    CompetitionType competition = CompetitionPremierLeague;
    [model loadMatchesSeason:season];
    NSArray* matchArray = [model getAllMatchesInSeason:season competition:competition];
    NSMutableSet* teamArray = [NSMutableSet setWithCapacity:25];
    int matchCount = 0;
    for (Match* match in matchArray) {
        if (match.getTeam2ID == 0) {
            continue;
        }
        matchCount++;
        [teamArray addObject:@(match.getTeam1ID)];
        [teamArray addObject:@(match.getTeam2ID)];
    }
    if (matchCount != teamArray.count * teamArray.count - teamArray.count) {
        NSLog(@"Season with incorrect match number: %d", season);
    }

}

#pragma mark - GoalMask


- (void)testGoalMask {
    int matchErrorCountGlobal = 0;
    NSMutableSet* seasonErrorArray = [NSMutableSet setWithCapacity:25];

    for (int season = 1870; season <= 2024; season++) {
        for (int i = 0; i < competitionArrayCount; i++) {
            NSMutableArray* competitionProcessArray = [NSMutableArray arrayWithCapacity:10];
            int startSeason = [competitionArray[i * competitionArrayEntrySize + 1] intValue];
            int endSeason = [competitionArray[i * competitionArrayEntrySize + 2] intValue];
            if (startSeason <= season && (endSeason >= season || endSeason == 0)) {
                [competitionProcessArray addObject:competitionArray[i * competitionArrayEntrySize]];
            }
            
            if (competitionProcessArray.count > 0) {
                [model loadMatchesSeason:season];
                for (NSNumber* competitionNumber in competitionProcessArray) {
                    CompetitionType competition = [competitionNumber intValue];
                    NSArray* matchArray = [model getAllMatchesInSeason:season competition:competition];
                    NSLog(@"%d: %@ - %lu matches", season, [Match getCompetitionShortName:competition], (unsigned long)matchArray.count);
                    int matchCount = 0;
                    int matchErrorCount = 0;
                    for (Match* match in matchArray) {
                        if ([match hasEvents] && ![match goalMaskCorrect]) {
                            NSLog(@"Match with incorrect match GoalMask: %d", season);
                            matchErrorCount++;
                        }
                        matchCount++;
                    }
                    if (matchErrorCount != 0) {
                        NSLog(@"Season with incorrect GoalMasks: %d/%d", matchErrorCount, matchCount);
                        [seasonErrorArray addObject:@(season)];
                    }
                    else {
                        NSLog(@"Season with all correct GoalMasks: %d", season);
                    }
                    matchErrorCountGlobal += matchErrorCount;
                }
            }
        }

    }
    if (matchErrorCountGlobal != 0) {
        NSLog(@"Matches with incorrect GoalMasks: %d/%d", matchErrorCountGlobal);
        for (NSNumber* seasonNumber in seasonErrorArray) {
            NSLog(@"--- Season:%@", seasonNumber);
        }
    }
    else {
        NSLog(@"All Seasons Correct GoalMasks");

    }

    int ian2 = 10;
}

- (void)testGoalMaskInSeasonCompetition {
    int season = 2017;
    CompetitionType competition = CompetitionFACup;
    
    [model loadMatchesSeason:season];
    NSArray* matchArray = [model getAllMatchesInSeason:season competition:competition];
    NSLog(@"%d: %@ - %lu matches", season, [Match getCompetitionShortName:competition], (unsigned long)matchArray.count);
    NSMutableSet* matchErrorArray = [NSMutableSet setWithCapacity:25];
    int matchCount = 0;
    int matchErrorCount = 0;
    for (Match* match in matchArray) {
        if ([match hasEvents] && ![match goalMaskCorrect]) {
            NSLog(@"Match with incorrect match GoalMask: %d", season);
            [match goalMaskCorrect];
            [matchErrorArray addObject:match];
            matchErrorCount++;
        }
        matchCount++;
    }
    if (matchErrorCount != 0) {
        NSLog(@"Season with incorrect GoalMasks: %d/%d", matchErrorCount, matchCount);
    }
    else {
        NSLog(@"Season with all correct GoalMasks: %d", season);
    }

}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
