//
//  FootballDataTests.m
//  FootballDataTests
//
//  Created by Ian Copeman on 16/10/2020.
//

#import <XCTest/XCTest.h>
#import "FootballModel.h"
#import "FileManager.h"
#import "NSString+CSVParser.h"
#import "APIFootballParser.h"

@interface FootballDataTests : XCTestCase

@end

@implementation FootballDataTests {
    FootballModel* model;
    FileManager* fileManager;
    
}

- (void)setUp {
    // Put setup code here. This method is called before the invocation of each test method in the class.
    model = [FootballModel getModel];
    fileManager = [[FileManager alloc] init];
    
    [model loadTeams];
    [model loadFootballers];
 //   [model loadMatchesTest];
//    [model loadMatches];
    
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}

#pragma mark - Matches

#pragma mark - Footballers

#define BlobsFilePath (@"/Blobs/")
- (void)testPlayerNumber {
    NSUInteger beforeCount = [model getFootballerCount];
    
    
    //
    // Footballers
    FileManager* fileManager = [[FileManager alloc] init];
    NSMutableDictionary* footballerDictionary = [NSMutableDictionary dictionaryWithCapacity:1000];
    int count = 0;
    NSString* dataPath  = [[NSUserDefaults standardUserDefaults] objectForKey:@"blobPath"];
    {
        NSData* buffer = [fileManager loadPersistedData:dataPath dataName:BlobsFilePath dataType:nil dataKey:[Footballer getBlobName]];
        int index = 0;
        const char* bufferPointer = buffer.bytes;
        while (index < buffer.length) {
            Footballer* footballer = [[Footballer alloc] init];
            
            index += [footballer initFromBlob:(bufferPointer + index)];
            count++;
            if (footballer.parseIndexTM != 0) {
                NSNumber* key = @(footballer.parseIndexTM);
                [footballerDictionary setObject:footballer forKey:key];
            }
            else {
                NSString* key = [footballer generateKey];
                Footballer* test = [footballerDictionary objectForKey:key];
                if (test != nil) {
                    int ian = 10;
                }
                else {
                    [footballerDictionary setObject:footballer forKey:key];
                }
            }
            if ([footballer.dictionaryKey containsString:@"Ward_19361005"]) {
                int ian = 10;
            }
            if (footballer.index == 15205) {
                int ian = 10;
            }
        }
    }
    NSUInteger afterCount = footballerDictionary.count;
    
    int ian = 10;
    
    
    
}

- (void)testPlayerDateNumber {
    int count = 0;
    NSArray* footballers = [model getFootballersTest];
    for (int i = 0 ; i < footballers.count; i++) {
        Footballer* footballer = footballers[i];
        if (footballer.birthDateNumber < 18000101) {
            if (footballer.birthDateNumber > 1000001 && footballer.birthDateNumber < 10000000) {
                int year = footballer.birthDateNumber / 1000;
                int month = (footballer.birthDateNumber % 1000) / 100;
                int day = (footballer.birthDateNumber % 10);
                if (month == 0) month = 1;
                if (day == 0) day = 1;
                int newDateNumber = DATE_NUMBER_CREATE(year, month, day);
                if (newDateNumber < 18500101 || newDateNumber > 20100101) {
                    int error = 1;
                }
                int ian = 10;
            }
            else {
                count++;
            }
        }
    }
    int ian = count;
}

- (void)testPlayerIdentNumber {
    NSArray* footballers = [model getFootballersTest];
    for (int i = 0 ; i < footballers.count; i++) {
        Footballer* footballer = footballers[i];
        if (footballer.parseIndexAPIFootballer != 0) {
            for (int j = 0 ; j < footballers.count; j++) {
                Footballer* footballerOther = footballers[j];
                if (footballer.parseIndexAPIFootballer == footballerOther.parseIndexAPIFootballer && i != j) {
                    NSLog(@"Duplicate Footballer: %@, %@ (%d)", footballer.dictionaryKey, footballerOther.dictionaryKey, footballer.parseIndexAPIFootballer);

                    int ina = 10;
                }
            }
        }
    }
    int ian = 10;
}


- (void)testPlayerDates {
    [model loadMatches];

    NSArray* footballers = [model getFootballersTest];
    for (int i = 0 ; i < footballers.count; i++) {
        Footballer* footballer = footballers[i];
        if (footballer.index == 15620) {
            int ian = 10;
        }
        if ([footballer getEndYear] - [footballer getStartYear] > 20) {
            NSLog(@"Long career footballer Footballer: %@ - %d, %d-%d", footballer.name, [footballer getEndYear] - [footballer getStartYear], [footballer getStartYear], [footballer getEndYear]);
        }
    }
    int ian = 10;
}


- (void)testPlayerID {
    int findID = 15620;
    //
    // Footballers
    FileManager* fileManager = [[FileManager alloc] init];
    NSMutableDictionary* footballerDictionary = [NSMutableDictionary dictionaryWithCapacity:1000];
    int count = 0;
    NSString* dataPath  = [[NSUserDefaults standardUserDefaults] objectForKey:@"blobPath"];
    {
        NSData* buffer = [fileManager loadPersistedData:dataPath dataName:BlobsFilePath dataType:nil dataKey:[Footballer getBlobName]];
        int index = 0;
        const char* bufferPointer = buffer.bytes;
        while (index < buffer.length) {
            Footballer* footballer = [[Footballer alloc] init];
            
            index += [footballer initFromBlob:(bufferPointer + index)];
            count++;
            
            if (footballer.index == findID) {
                NSLog(@"Found Footballer: %@ (%d)", footballer.name, findID);
            }
        }
    }
    
}


- (void)testPlayerKeys {
    int beforeCount = [model getFootballerCount];
    
    //
    // Footballers
    FileManager* fileManager = [[FileManager alloc] init];
    NSMutableDictionary* footballerDictionary = [NSMutableDictionary dictionaryWithCapacity:1000];
    int count = 0;
    NSString* dataPath  = [[NSUserDefaults standardUserDefaults] objectForKey:@"blobPath"];
    {
        NSData* buffer = [fileManager loadPersistedData:dataPath dataName:BlobsFilePath dataType:nil dataKey:[Footballer getBlobName]];
        int index = 0;
        const char* bufferPointer = buffer.bytes;
        while (index < buffer.length) {
            Footballer* footballer = [[Footballer alloc] init];
            
            index += [footballer initFromBlob:(bufferPointer + index)];
            count++;
            
            NSString* key = [footballer generateKey];
            Footballer* test = [footballerDictionary objectForKey:key];
            if (test != nil) {
                int ian = 10;
            }
            else {
                [footballerDictionary setObject:footballer forKey:key];
            }
        }
    }
    int afterCount = footballerDictionary.count;
    
    XCTAssert(beforeCount == afterCount);
    
    int ian = 10;
    
    
    
}

- (void)testPlayerIDs {
    int season = 2023;
    [model loadMatchesSeason:season];
    NSArray* matchArray = [model getAllMatchesInSeason:season competition:CompetitionPremierLeague];
    NSArray* teams = [self getTeamsInMatchArray:matchArray];
    __block bool updateFootballers = false;
    int count = 0;
    int skipCount = 7;

    for (NSNumber* teamNumber in teams) {
        if (++count <= skipCount) continue;
        int teamID = [teamNumber intValue];
        NSString* teamName = [model getTeamName:teamID];
        NSLog(@"Checking Footballers in Team: %@ (%d)", teamName, teamID);

        NSDictionary* devlexPlayers = [self getPlayersForTeam:teamID inMatchArray:matchArray];
        NSArray* devlexFootballers = [devlexPlayers allValues];
        for (Footballer* footballer in devlexFootballers) {
            NSLog(@" = %@: %d", footballer.name, footballer.parseIndexAPIFootballer);

        }
        //
        // Parse player
        APIFootballParser* footballParser = [[APIFootballParser alloc] init];
        NSDictionary* apifPlayers = [footballParser getAPIPlayersForTeam:teamID];
        
        //
        // Check Devlex DB
        [devlexPlayers enumerateKeysAndObjectsUsingBlock:^(NSNumber* key, Footballer* footballer, BOOL* stop) {
            NSNumber* apifIndex = @(footballer.parseIndexAPIFootballer);
            if (footballer.parseIndexAPIFootballer == 0 /*!apifPlayers[apifIndex]*/) {
                NSLog(@"Failed to find devlex footballer with API key: %@: %d", footballer.name, footballer.parseIndexAPIFootballer);
            }
        }];
        
        //
        // Check APIF DB
        [apifPlayers enumerateKeysAndObjectsUsingBlock:^(NSNumber* key, NSString* footballerName, BOOL* stop) {
            int apifID = [key intValue];
            bool foundFootballer = false;
            for (Footballer* footballer in devlexFootballers) {
                if (footballer.parseIndexAPIFootballer == apifID) {
                    foundFootballer = true;
                    break;
                }
            }
            if (!foundFootballer) {
                NSLog(@" - Failed to find API footballer with API key: %@: %@", footballerName, key);
                NSString* lastName = [footballerName componentsSeparatedByString:@" "].lastObject;
                if (!lastName) {
                    NSLog(@" -- Failed to find Last Name: %@", footballerName);
                }
                else {
                    for (Footballer* footballer in devlexFootballers) {
                        if ([footballer.name containsString:lastName]) {
                            if (footballer.parseIndexAPIFootballer != 0) {
                                NSLog(@" ** Found footballer with other ID: %@: %@ (%d)", footballer.name, footballerName, footballer.parseIndexAPIFootballer);
                            }
                            else {
                                NSLog(@" * Adding ID to footballer: %@: %@", footballer.name, footballerName);
                                footballer.parseIndexAPIFootballer = apifID;
                                foundFootballer = true;
                                updateFootballers = true;
                            }
                            break;
                        }
                    }
                    if (!foundFootballer) {
                        NSLog(@" -- Failed to find suitable footballer: %@", footballerName);
                    }
                }
            }
        }];
        
        NSLog(@"** Team: %d\n", teamID);
        if (count == 8) {
          // break;
        }
    }
    
    if (updateFootballers) {
        [model exportCSVPlayers:NO];
    }
    
    int ian = 10;
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

- (NSArray*)getTeamsInMatchArray:(NSArray*)matchArray {
    NSMutableDictionary* teamDictionary = [NSMutableDictionary dictionaryWithCapacity:30];
    for (Match* match in matchArray) {
        int teamID1 = [match getTeam1ID];
        int teamID2 = [match getTeam2ID];
        if (teamID1 == 0 || teamID2 == 0) continue;
        NSNumber* teamNumber = @(teamID1);
        if (!teamDictionary[teamNumber]) {
            [teamDictionary setObject:match forKey:teamNumber];
        }
        teamNumber = @(teamID2);
        if (!teamDictionary[teamNumber]) {
            [teamDictionary setObject:teamNumber forKey:teamNumber];
        }
    }
    return [teamDictionary allKeys];
}

- (NSDictionary*)getPlayersForTeam:(int)teamID inMatchArray:(NSArray*)matchArray {
    NSMutableDictionary* teamFootballers = [NSMutableDictionary dictionaryWithCapacity:50];
    for (Match* match in matchArray) {
        if ([match getTeam1ID] == teamID || [match getTeam2ID] == teamID) {
            for (Footballer* footballer in [match getFootballers:teamID]) {
                NSNumber* footballerID = @(footballer.index);
                if (!teamFootballers[footballerID]) {
                    [teamFootballers setObject:footballer forKey:footballerID];
                }
            }
        }
    }
    return teamFootballers;
}



#pragma mark - Teams


- (void)testTeams {
    /*
     Team* team = [model getTeam:@"Notts County" identInt:0 identKey:@""];
     NSLog(@"%@: %d", @"Notts County", team.index);
     */
    
    NSArray* matchArray = [model getAllMatchesInSeason:2022 competition:CompetitionLeagueTwo];
    NSMutableDictionary* teamDictionary = [NSMutableDictionary dictionaryWithCapacity:30];
    for (Match* match in matchArray) {
        int teamID = [match getTeam1ID];
        {
            NSNumber* teamNumberID = @(teamID);
            NSString* teamString = [teamDictionary objectForKey:teamNumberID];
            if (teamString == nil) {
                teamString = [model getTeamName:teamID];
                [teamDictionary setObject:teamString forKey:teamNumberID];
            }
        }
        teamID = [match getTeam2ID];
        {
            NSNumber* teamNumberID = @(teamID);
            NSString* teamString = [teamDictionary objectForKey:teamNumberID];
            if (teamString == nil) {
                teamString = [model getTeamName:teamID];
                [teamDictionary setObject:teamString forKey:teamNumberID];
            }
        }
    }
    
    for(id key in teamDictionary) {
        NSString* teamString = [teamDictionary objectForKey:key];
        NSLog(@"%@: %@", teamString, key);
    }
    
    
    
}

- (void)testTeamID {
    NSArray* teamArray = @[
        @"RealMadrid", @(1123),
        @"QarabagFK", @(1782),
        @"         // Al-Hilal", @(1853),
    ];
    for (int i = 0; i < teamArray.count / 2; i++) {
        NSString* name = teamArray[i * 2];
        int teamID = [teamArray[i * 2 + 1] intValue];
        Team* team = [model getTeam:teamID];
        if (team != nil) {
             NSLog(@"%@ - %@", name, team.name);
        }
        else {
            NSLog(@"Can't find team:  %@", name);
        }
    }

    
}


- (void)testFindTeamID {
    //
    // Teams from APIFootall
    /*
    NSArray* teamArray = @[
        @"1. FC Heidenheim", @(180),
        @"Larne", @(5354),
        @"Pafos", @(3403),
    ];
    */
    NSArray* teamArray = @[
        @"FC Heidenheim", @(180),
        @"Larne", @(5354),
        @"Pafos", @(3403),
    ];

    for (int i = 0; i < teamArray.count / 2; i++) {
        NSString* name = teamArray[i * 2];
        Team* team = [model getTeamFromName:name];
        if (team != nil) {
             NSLog(@"%@(%d, %@),", team.name, team.index, teamArray[i * 2 + 1]);
        }
        else {
            NSLog(@"Can't find team:  %@", name);
        }
    }
}

- (void)testTeamColours {
    int season = 2024;
    [model loadMatchesSeason:season];
    NSArray* matchArray = [model getAllMatchesInSeason:season competition:CompetitionEuropaConferenceLeague];
    NSMutableDictionary* teamDictionary = [NSMutableDictionary dictionaryWithCapacity:30];
    int errorCount = 0;
    for (Match* match in matchArray) {
        int teamID = [match getTeam1ID];
        {
            NSNumber* teamNumberID = @(teamID);
            Team* team = [teamDictionary objectForKey:teamNumberID];
            if (team == nil) {
                team = [model getTeam:teamID];
                [teamDictionary setObject:team forKey:teamNumberID];
                if (team.colour == 0) {
                    NSLog(@"@\"%@\", UIColorFromRGB(0xXXX),", team.name);
                    errorCount++;
                    Team* team2 = [model getTeam:match.getTeam2ID];
                //    NSLog(@"%@ v %@", team.name, team2.name);

                }
            }
        }
        teamID = [match getTeam2ID];
        {
            NSNumber* teamNumberID = @(teamID);
            Team* team = [teamDictionary objectForKey:teamNumberID];
            if (team == nil) {
                team = [model getTeam:teamID];
                [teamDictionary setObject:team forKey:teamNumberID];
                if (team.colour == 0) {
                    NSLog(@"@\"%@\", UIColorFromRGB(0xXXX),", team.name);
                    errorCount++;
                }
            }
        }
    }

    if (errorCount == 0) {
        NSLog(@"Found all team colours");
    }
    else {
        NSLog(@"%d missing Team colours", errorCount);
    }
    
}



#pragma mark - Events


- (void)testMatchSubSub {

    for (int season = 2024; season <= 2024; season++) {
        [model loadMatchesSeason:season];
        NSArray* matchArray = [model getAllMatchesInSeason:season   competition:CompetitionPremierLeague];
        for (Match* match in matchArray) {
            if (match.dateNumber == 20041114) {
                int ian = 10;
            }
            //
            // Check match's events
            NSArray* events = @[match.team1Events, match.team2Events];
            for (NSArray* eventsTeam in events) {
                for (NSNumber* eventNumber in eventsTeam) {
                    unsigned long long event = [eventNumber unsignedLongLongValue];
                    EventHeader* eventHeader = (EventHeader*)&event;
                    int eventType = GetEventType(eventHeader->eventType);
                    if (eventType == EventSub) {
                        if (eventHeader->playerID == 0 || eventHeader->player2ID == 0) {
                            NSLog(@"Sub with missing players: %d: ", match.dateNumber);
                            XCTFail(@"Failed to validate event");
                        }
                        //
                        // Check that player 1, the sub comming on, did not start
                        for (NSNumber* eventNumber2 in eventsTeam) {
                            unsigned long long event2 = [eventNumber2 unsignedLongLongValue];
                            EventHeader* eventHeader2 = (EventHeader*)&event2;
                            int eventType2 = GetEventType(eventHeader2->eventType);
                            if (eventType2 == EventApperance || eventType2 == EventGoalkeeperApperance) {
                                if (eventHeader2->playerID == eventHeader->playerID) {
                                    NSLog(@"Sub with starting player: %d: ", match.dateNumber);
                                    XCTFail(@"Failed to validate event");
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
}

    - (void)testMatchGoalEvent {
    
    for (int season = 1950  ; season < 1980; season++) {
        [model loadMatchesSeason:season];
        NSArray* matchArray = [model getAllMatchesInSeason:season   competition:CompetitionPremierLeague];
        for (Match* match in matchArray) {
            if (match.matchParseState !=  MatchParseStateEventsFull &&
                match.matchParseState !=  MatchParseStateEventsPartial) {
                continue;
            }
            if (match.dateNumber == 20041114) {
                int ian = 10;
            }
            //
            // Check match's events
            bool firstEvents = YES;
            NSArray* events = @[match.team1Events, match.team2Events];
            for (NSArray* eventsTeam in events) {
                int eventIndex = 0;
                for (NSNumber* eventNumber in eventsTeam) {
                    unsigned long long event = [eventNumber unsignedLongLongValue];
                    EventHeader* eventHeader = (EventHeader*)&event;
                    int eventType = GetEventType(eventHeader->eventType);
                    if (eventType == EventGoal) {
                        if (eventHeader->playerID == 0) {
                            NSLog(@"Goal without player: %d: ", match.dateNumber);
                            NSLog(@"%@", match.fileURL);
                            //  XCTFail(@"Failed to validate event");
                        }
                    }
                    eventIndex++;
                }
                firstEvents = NO;
            }
        }
    }
}


- (void)testEvent {
    UInt32 yest;
    UInt64 yest2;
    UInt16 playerID = 23837;
    UInt16 playerAssistID = 34271;
    int time = 10;

    unsigned long long1 = 4 << 24 | time << 16;
    unsigned long long2 = (unsigned long long)playerAssistID << 16 | (UInt16)playerID;
    unsigned long long event2 = long1 << 32 | long2;
    EventHeader* eventHeader2 = (EventHeader*)&event2;

    
    NSNumber* eventNumber2 = [Event createEventGoalNumber:playerID playerAssistID:playerAssistID time:time];
    unsigned long long event = [eventNumber2 unsignedLongLongValue];
    EventHeader* eventHeader = (EventHeader*)&event;
    int ian = 10;

    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    
    
}



- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}




@end
