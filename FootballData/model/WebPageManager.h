//
//  WebPageManager.h
//  FootballData
//
//  Created by Ian Copeman on 05/11/2020.
//

#import <Foundation/Foundation.h>
#import "WebPage.h"

NS_ASSUME_NONNULL_BEGIN




@interface WebPageManager : NSObject

@property UInt16 index;

+ (WebPageManager*)getWebPageManager;

- (NSString*)getWebPage:(NSString*)urlString;


@end

NS_ASSUME_NONNULL_END
