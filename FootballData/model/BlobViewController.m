//
//  DataViewController.m
//  FootballData
//
//  Created by Ian Copeman on 09/08/2021.
//

#import "BlobViewController.h"
#import "FootballModel.h"

@interface BlobViewController ()

@end

@implementation BlobViewController {
    FootballModel* model;

    __weak IBOutlet NSTextField *labelFootballerNumber;
    __weak IBOutlet NSTextField *labelLocationNumber;
    __weak IBOutlet NSTextField *labelMatchNumber;
    __weak IBOutlet NSTextField *labelTeamNumber;
    __weak IBOutlet NSTextField *labelManagerNumber;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    model = [FootballModel getModel];
    
    
}

- (void)viewWillAppear {
    [super viewWillAppear];
    [self updateView];
}

- (IBAction)buttonLoadBlobsPushed:(id)sender {
    //
    // Load blobs
    FootballModel* model = [FootballModel getModel];
    [model fromBlobs];
    
    [self updateView];
}

- (IBAction)buttonSaveBlobsPushed:(id)sender {
    //
    // Create blobs
    FootballModel* model = [FootballModel getModel];
    [model toBlobs];

    [self updateView];
}

- (void)updateView {
    NSUInteger teamCount = [model getTeamCount];
    labelTeamNumber.stringValue = [NSString stringWithFormat:@"%lu", (unsigned long)teamCount];
    NSUInteger matchCount = [model getMatchCount];
    labelMatchNumber.stringValue = [NSString stringWithFormat:@"%lu", (unsigned long)matchCount];
    NSUInteger locationCount = [model getLocationCount];
    labelLocationNumber.stringValue = [NSString stringWithFormat:@"%lu", (unsigned long)locationCount];
    NSUInteger footballerCount = [model getFootballerCount];
    labelFootballerNumber.stringValue = [NSString stringWithFormat:@"%lu", (unsigned long)footballerCount];
    NSUInteger managerCount = [model getManagerCount];
    labelManagerNumber.stringValue = [NSString stringWithFormat:@"%lu", (unsigned long)managerCount];
}

@end
