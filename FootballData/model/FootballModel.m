//
//  FootballModel.m
//  FootballData
//
//  Created by Ian Copeman on 11/11/2020.
//

#import "FootballModel.h"
#import "Team.h"
#import "Event.h"
#import "FileManager.h"
#import "Location.h"
#import "TeamParser.h"
#import "PlayerParser.h"
#import "MatchParser.h"
#import "FootballDataConstants.h"
#import "APIFootballParser.h"

#define BlobsFilePath (@"/Blobs/")
#define ExportMatchDirectory (@"Devlex/Matches")
#define ExportPlayerDirectory (@"Devlex/Players")
#define ExportManagerDirectory (@"Devlex/Managers")


@implementation FootballModel {
    NSInteger currentYear;
    NSInteger currentSeason;
    NSInteger currentDateNumber;
    NSMutableDictionary* teamDictionary;
    NSMutableDictionary* locationDictionary;
    NSMutableDictionary* footballerDictionary;
    NSMutableDictionary* managerDictionary;
    NSMutableDictionary* teamFootballerDictionary;
    NSMutableDictionary* eventDictionary;
    NSMutableDictionary* matchDictionary;
    NSMutableDictionary* matchStatsDictionary;
    NSMutableSet* modifiedSeasonSet;
    bool modifiedTeams;
    bool modifiedFootballers;
    bool modifiedManagers;
    bool modifiedMatches;
    bool modifiedMatchStats;
    bool modifiedLocations;
    FileManager* fileManager;
    
    NSMutableArray* missingIDArray1;
    NSMutableArray* missingIDArray;
    NSMutableArray* newIDArray;
    
    NSMutableDictionary* clubFootballerDictionary;

}

//
// Static singleton
static FootballModel* sharedSingleton;

+ (void)initialize {
    static BOOL initialized = NO;
    if(!initialized)
    {
        initialized = YES;
        sharedSingleton = [[FootballModel alloc] init];
        sharedSingleton->teamDictionary = [NSMutableDictionary dictionaryWithCapacity:200];
        sharedSingleton->locationDictionary = [NSMutableDictionary dictionaryWithCapacity:200];
        sharedSingleton->footballerDictionary = [NSMutableDictionary dictionaryWithCapacity:2000];
        sharedSingleton->clubFootballerDictionary = [NSMutableDictionary dictionaryWithCapacity:20];
        
        sharedSingleton->managerDictionary = [NSMutableDictionary dictionaryWithCapacity:2000];
        sharedSingleton->teamFootballerDictionary = [NSMutableDictionary dictionaryWithCapacity:200];
        sharedSingleton->matchDictionary = [NSMutableDictionary dictionaryWithCapacity:1000];
        sharedSingleton->matchStatsDictionary = [NSMutableDictionary dictionaryWithCapacity:1000];
        sharedSingleton->eventDictionary = [NSMutableDictionary dictionaryWithCapacity:1000];
        
        sharedSingleton->fileManager = [[FileManager alloc] init];
        
        //
        // Get current year
        NSDate* today = [NSDate date];
        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitYear | NSCalendarUnitMonth fromDate:today];
        sharedSingleton->currentDateNumber = DATE_NUMBER_CREATE([components year], [components month],[components day]);

        sharedSingleton->currentYear = [components year];
        NSInteger month = [components month]; // 1 based index
        sharedSingleton->currentSeason = month < 7 ? sharedSingleton->currentYear - 1 : sharedSingleton->currentYear;
        
        
        /*
         //
         // Add Liverpool to team database so it's team ID 1 (LIVERPOOL_TEAM_ID)
         [sharedSingleton addTeam:@"Liverpool"];
         [sharedSingleton addLocation:@"Anfield"];
         */
        
        
#if TARGET_IPHONE_SIMULATOR
#endif
    }
}

+ (FootballModel*)getModel {
    return sharedSingleton;
}

- (NSInteger) getCurrentSeason {
    return currentSeason;
}

- (NSInteger) getCurrentDateNumber {
    return currentDateNumber;
}


- (void)clean {
    [teamDictionary removeAllObjects];
    [locationDictionary removeAllObjects];
    [footballerDictionary removeAllObjects];
    [matchDictionary removeAllObjects];
    [matchStatsDictionary removeAllObjects];
}


#pragma mark - Teams
- (void)loadTeams {
    TeamParser* teamParser = [[TeamParser alloc] init];
    [teamParser loadTeamFile];
}

- (int)addTeamUEFA:(NSString*)teamName parseUEFA:(int)parseUEFA season:(int)season countryCode:(NSString*)countryCode {
    //
    // Lookup team ID
    __block int foundIndex = 0;
    [teamDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        Team* team = (Team*)obj;
        if (team.parseUEFA == parseUEFA) {
            foundIndex = team.index;
            *stop = YES;
        }
        
        // NSLog(@"%@->%@",key,obj);
        // Set stop to YES when you wanted to break the iteration.
    }];
    if (foundIndex != 0) return foundIndex;
    
    //
    // Try to add by name
    Team* team = [teamDictionary objectForKey:teamName];
    if (team != nil) {
        NSLog(@"Found team with missing UEFA ID: %@ -> %d",teamName, parseUEFA);
        team.parseUEFA = parseUEFA;
        modifiedTeams = YES;
        if (missingIDArray == nil) {
            missingIDArray = [NSMutableArray arrayWithCapacity:10];
            missingIDArray1 = [NSMutableArray arrayWithCapacity:10];
        }
        if (team.countryID == 1) {
            [missingIDArray1 addObject:[NSString stringWithFormat:@"%@, %d, 0, 0, %d",teamName, parseUEFA, team.countryID]];
        }
        else {
            [missingIDArray addObject:[NSString stringWithFormat:@"%@, %d, 0, 0, %d",teamName, parseUEFA, team.countryID]];
        }
        return team.index;
    }
    /*
     if ([teamName containsString:@"Derry"]) {
     int ian = 10;
     }
     Team* tst = [teamDictionary objectForKey:@(parseUEFA)];
     */
    NSLog(@"New team with missing UEFA ID: %@ -> %d",teamName, parseUEFA);
    
    
    int countryID = countryCode == nil ? 0 : [Team getCountryID:countryCode];
    
    //
    // Add team
    team = [[Team alloc] init];
    team.index = (int)teamDictionary.count + 1;
    team.countryID = countryID;
    team.name = teamName;
    team.parseUEFA = parseUEFA;
    [teamDictionary setObject:team forKey:teamName];
    modifiedTeams = YES;
    NSLog(@"Added new team: %@", teamName);
    
    if (newIDArray == nil) {
        newIDArray = [NSMutableArray arrayWithCapacity:10];
    }
    [newIDArray addObject:[NSString stringWithFormat:@"%@, %d, %d, 0, 0, %d",teamName, parseUEFA, season, countryID]];
    
    
    return true;
    
}

//
// International teams
- (int)addTeam:(NSString*)teamName countryString:(NSString*)countryString {
    //    teamName = [Team modifyTeamName:teamName];
    int countryID = [Team getCountryID:countryString];
    return [self addTeam:teamName countryID:countryID];
}

//
// English teams
- (int)addTeam:(NSString*)teamName teamID:(int)teamID shortName:(NSString*)shortName vShortName:(NSString*)vShortName parseUEFA:(int)parseUEFA indexAPI:(int)indexAPI  parseTM:(int)parseTM latitudeString:(NSString*)latitudeString longitudeString:(NSString*)longitudeString countryID:(unsigned int)countryID colour:(int)colour {
    Team* team = [teamDictionary objectForKey:teamName];
    if (team != nil) {
        return team.index;
    }
    
    team = [[Team alloc] init];
    if (teamID == 0) {
        team.index = (int)teamDictionary.count + 1;
        NSLog(@"Creating ID for team: %@ (%d)", teamName, team.index);
    }
    else {
        team.index = teamID;
    }
    team.countryID = countryID;
    team.name = teamName;
    team.nameShort = shortName;
    team.nameVShort = vShortName;
    team.parseIndexAPI = indexAPI;
    team.parseUEFA = parseUEFA;
    team.parseIndexTM = parseTM;
    team.latitudeString = latitudeString;
    team.longitudeString = longitudeString;
    team.colour = colour;
    [teamDictionary setObject:team forKey:teamName];
    modifiedTeams = YES;
    NSLog(@"Added new team: %@", teamName);
    if ([teamName compare:@"Chelsea"] == NSOrderedSame) {
        int ian = 10;
    }
    
    return team.index;
}


- (int)addTeam:(NSString*)teamName {
    //  teamName = [Team modifyTeamName:teamName];
    return [self addTeam:teamName countryID:CountryEngland];
}

- (int)addTeam:(NSString*)teamName countryID:(int)countryID {
    //
    // Modify team name
    teamName = [self modifyTeamName:teamName];
    
    Team* team = [teamDictionary objectForKey:teamName];
    if (team != nil) {
        return team.index;
    }
    static int newTeamCount = 0;
    team = [[Team alloc] init];
    team.index = (int)teamDictionary.count + 1;
    team.countryID = countryID;
    team.name = teamName;
    [teamDictionary setObject:team forKey:teamName];
    modifiedTeams = YES;
    NSLog(@"Added new team: ,%@,0,0,0,0,%d", teamName, countryID);
    NSLog(@"New Teams: %d", ++newTeamCount);

    return team.index;
}

- (int)addTeam:(NSString*)teamName season:(int)season {
    //
    // Modify team name
    teamName = [self modifyTeamName:teamName];

    Team* team = [teamDictionary objectForKey:teamName];
    if (team != nil) {
        return team.index;
    }
    
    team = [[Team alloc] init];
    team.index = (int)teamDictionary.count + 1;
    team.countryID = CountryUnknown;
    team.name = teamName;
    [teamDictionary setObject:team forKey:teamName];
    modifiedTeams = YES;
    NSLog(@"Added new team: ,%@,0,0,0,0,%d", teamName, team.countryID);
    return team.index;
    
}

- (int)getTeamID:(NSString*)teamName {
    Team* team = [teamDictionary objectForKey:teamName];
    if (team == nil) return 0;
    return team.index;
}


- (NSUInteger)getTeamCount {
    return teamDictionary.count;
}

- (void)reportNewTeams {
    if (missingIDArray1 != nil) {
        NSLog(@"English Teams with new IDs");
        printf("\n");
        for (NSString* teamRow in missingIDArray1) {
            printf("%s\n", [teamRow UTF8String]);
        }
        printf("\n");
    }
    if (missingIDArray != nil) {
        NSLog(@"Teams with new IDs");
        printf("\n");
        for (NSString* teamRow in missingIDArray) {
            printf("%s\n", [teamRow UTF8String]);
        }
        printf("\n");
    }
    if (newIDArray != nil) {
        NSLog(@"New Teams");
        printf("\n");
        for (NSString* teamRow in newIDArray) {
            printf("%s\n", [teamRow UTF8String]);
        }
    }
}

- (NSString*)getTeamName:(int)teamID {
    for(id key in teamDictionary) {
        Team* team = [teamDictionary objectForKey:key];
        if (team.index == teamID) {
            return team.name;
        }
    }
    return nil;
}

- (NSString*)modifyTeamName:(NSString*)originalName {
    //
    // Common team name modifications
    originalName = [originalName stringByReplacingOccurrencesOfString:@" And " withString:@" & "];
    originalName = [originalName stringByReplacingOccurrencesOfString:@" and " withString:@" & "];

    
    /*
    if ([originalName compare:@"Yeovil Town"] == NSOrderedSame) return @"Yeovil";
    if ([originalName compare:@"Cheltenham Town"] == NSOrderedSame) return @"Cheltenham";
    if ([originalName compare:@"Chester"] == NSOrderedSame) return @"Chester City";
    if ([originalName compare:@"Bayer 04 Leverkusen"] == NSOrderedSame) return @"Bayer Leverkusen";
    if ([originalName compare:@"FC Basel 1893"] == NSOrderedSame) return @"Basel";
    if ([originalName compare:@"FC Girondins Bordeaux"] == NSOrderedSame) return @"Bordeaux";
    if ([originalName compare:@"Hereford"] == NSOrderedSame) return @"Hereford Town";
    if ([originalName compare:@"FC Merthyr Town"] == NSOrderedSame) return @"Merthyr Town";
    if ([originalName compare:@"Newport (IOW)"] == NSOrderedSame) return @"Newport IOW";
    if ([originalName compare:@"Stevenage Borough"] == NSOrderedSame) return @"Stevenage";
    if ([originalName compare:@"Runcorn Halton"] == NSOrderedSame) return @"Runcorn";
    if ([originalName compare:@"Östers IF"] == NSOrderedSame) return @"Osters IF";
    if ([originalName compare:@"Bohemian Football Club"] == NSOrderedSame) return @"Bohemians";
    if ([originalName compare:@"LASK"] == NSOrderedSame) return @"Linzer ASK";
    if ([originalName compare:@"Pogon Szczecin"] == NSOrderedSame) return @"Pogoń";
    if ([originalName compare:@"Evenwood Town (- 2005)"] == NSOrderedSame) return @"Evenwood Town";
*/
    return originalName;
}

- (int)addTeam:(NSString*)teamName identInt:(int)identInt identKey:(NSString*)identKey {
    teamName = [self modifyTeamName:teamName];
    
    //
    // First search team name
    Team* team = [self getTeam:teamName identInt:identInt identKey:identKey];
    if (team != nil) {
        return team.index;
    }
    return 0;
}

- (Team*)getTeam:(int)teamID {
    for(id key in teamDictionary) {
        Team* team = [teamDictionary objectForKey:key];
        if (team.index == teamID) {
            return team;
        }
    }
    return nil;
}
- (Team*)getTeamFromName:(NSString*)teamName {
    Team* team = [teamDictionary objectForKey:teamName];
    if (team != nil) {
        return team;
    }
    return nil;
}

- (Team*)getTeam:(NSString*)teamName identInt:(int)identInt identKey:(NSString*)identKey {
    teamName = [self modifyTeamName:teamName];
    //
    // First search team name
    Team* team = [teamDictionary objectForKey:teamName];
    if (team != nil) {
        return team;
    }
    
    //
    // Search by ident
    NSPredicate* filter = [NSPredicate predicateWithFormat:@"SELF.%@ == %d",
                           identKey, identInt];
    NSArray* filteredTeams = [[teamDictionary allValues] filteredArrayUsingPredicate:filter];
    if (filteredTeams.count == 0) {
        NSLog(@"Can't find team: %@: %d", teamName, identInt);
        Team* team = [teamDictionary objectForKey:@"Crusaders"];
        
        return nil;
    }
    
    if (filteredTeams.count != 1) {
        //
        // Multiple entries
        for (Team* team in filteredTeams) {
            NSLog(@"Duplicate Team: %@", team.name);
        }
        return nil;
    }
    
    team = filteredTeams[0];
    return team;
}

- (Team*)getTeam:(int)identInt identKey:(NSString*)identKey {
    //
    // Search by ident
    NSPredicate* filter = [NSPredicate predicateWithFormat:@"SELF.%@ == %d",
                           identKey, identInt];
    NSArray* filteredTeams = [[teamDictionary allValues] filteredArrayUsingPredicate:filter];
    if (filteredTeams.count == 0) {
        NSLog(@"Can't find team: %d", identInt);
        return nil;
    }
    
    if (filteredTeams.count != 1) {
        //
        // Multiple entries
        NSLog(@"Duplicate Team: %d", identInt);
        return nil;
    }
    
    return filteredTeams[0];

}

#pragma mark - Locations

- (int)addLocation:(NSString*)locationName {
    if ([locationName containsString:@"\""]) {
        locationName = [locationName stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    }
    
    Location* location = [locationDictionary objectForKey:locationName];
    if (location != nil) {
        return location.index;
    }
    
    location = [[Location alloc] init];
    location.index = (int)locationDictionary.count + 1;
    location.name = locationName;
    [locationDictionary setObject:location forKey:locationName];
    modifiedLocations = YES;
    
    return location.index;
}

- (NSUInteger)getLocationCount {
    return locationDictionary.count;
}

#pragma mark - Managers


- (Manager*)checkManager:(int)identInt identKey:(NSString*)identKey teamID:(int)teamID season:(int)season {
    if (identInt == 0) {
        return nil;
    }
    
    //
    // Create cache
    static NSMutableDictionary* cacheDictionary = nil;
    if (cacheDictionary == nil) {
        cacheDictionary = [NSMutableDictionary dictionaryWithCapacity:100];
    }
    
    //
    // Look up in cache
    NSNumber* identNumber = [NSNumber numberWithInt:identInt];
    Manager* manager = [cacheDictionary objectForKey:identNumber];
    if (manager) {
        
        //
        // Check season and team
        [manager checkSeason:season teamID:teamID];
        return manager;
    }
    
    //
    // Check all managers for the ident
    NSPredicate* filter = [NSPredicate predicateWithFormat:@"SELF.%@ == %d",
                           identKey, identInt];
    NSArray* filteredManagers = [[managerDictionary allValues] filteredArrayUsingPredicate:filter];
    if (filteredManagers.count == 0) return nil;
    
    if (filteredManagers.count != 1) {
        //
        // Multiple entries
        for (Manager* manager in filteredManagers) {
            NSLog(@"Duplicate manager: %@", manager.name);
        }
        return nil;
    }
    
    //
    // Check season and team
    [filteredManagers[0] checkSeason:season teamID:teamID];
    
    //
    // Add to cache
    [cacheDictionary setObject:filteredManagers[0] forKey:identNumber];
    
    return filteredManagers[0];
    
}

- (Manager*)checkManagerWithKey:(NSString*)footballerKey season:(int)season teamID:(int)teamID parseID:(int)parseID  {
    Manager* existing = [managerDictionary objectForKey:footballerKey];
    if (existing != nil) {
        existing.parseIndexAPIFootballer = parseID;
        if ([existing checkSeason:season teamID:teamID]) modifiedFootballers = YES;
    }
    return existing;
}


- (Manager*)addManager:(Manager*)manager season:(int)season  {
    //
    // Check if existing manager exists, perhaps from a different parsing route
    NSString* managerKey = [manager generateKey];
    Manager* existing = [managerDictionary objectForKey:managerKey];
    
    if (existing != nil) {
        //
        // Check for a duplicate parse ID - would happen if 2 players have same surname and date of birth (twins and unlikely coincidences)
        if ([existing checkDifferentParseID:manager]) {
            //
            // Duplicate keys
            if (manager.parseIndexTM != 0) {
                if (manager.parseIndexTM == 8915) managerKey = @"HoldsworthDa_19681108";
                else if (manager.parseIndexTM == 3101) managerKey = @"HoldsworthDe_19681108";
                else {
                    NSLog(@"Duplicate manager %@, Key: %@, ID:%d (other ID:%d)", manager.name, managerKey, manager.parseIndexTM, existing.parseIndexTM);
                    int ian = 10;
                }
            }
            if (manager.parseIndexLFC != 0) {
                //
                // LFC lists players separately when they play for Liverpool and other clubs
            }
        }
        else {
            //
            // Update footballer if needed
            if ([existing compareAndUpdate:manager]) modifiedManagers = YES;
            if ([existing checkSeason:season teamID:manager.teamID]) modifiedManagers = YES;
            return existing;
            
        }
    }
    
    //
    // Create new footballer
    manager.index = (int)managerDictionary.count + 1;
    [manager checkSeason:season teamID:manager.teamID];
    modifiedManagers = YES;
    [managerDictionary setObject:manager forKey:managerKey];
    
    return manager;
}

- (NSUInteger)getManagerCount {
    return managerDictionary.count;
}

- (Manager*)getManager:(NSString*)managerKey  {
    Manager* existing = [managerDictionary objectForKey:managerKey];
    if (existing == nil) {
        NSLog(@"Failed to find expected manager: %@", managerKey);
        return nil;
    }
    return existing;
}

- (void)addManagerComplete:(Manager*)manager  {
    NSString* managerKey = [manager generateKey];
    Manager* existing = [managerDictionary objectForKey:managerKey];
    if (existing != nil) {
        NSLog(@"Duplicate manager: %@", manager.name);
        return;
    }
    
    //
    // Create new manager
    manager.index = (int)managerDictionary.count + 1;
    // modifiedFootballers = YES;
    [managerDictionary setObject:manager forKey:managerKey];
    
}

#pragma mark - Competitions

- (CompetitionType)addCompetition:(NSString*)competitionName {
    //
    // Minor competitions
    if ([competitionName localizedCaseInsensitiveContainsString:@"Lancashire League"]) {
        return CompetitionMinor;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"FA Cup Sup. round"]) {
        return CompetitionMinor;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"Sheriff of London Charity Shield"]) {
        return CompetitionMinor;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"Qualifier"]) {
        return CompetitionMinor;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"European Cup Pre-R"]) {
        return CompetitionMinor;
    }
    
    //
    // Known competitions
    if ([competitionName localizedCaseInsensitiveContainsString:@"UEFA Cup"]) {
        return CompetitionUEFACup;
        //return CompetitionUEFACup;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"FA Cup"]) {
        return CompetitionFACup;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"1st Division"]) {
        return CompetitionFirstDivision;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"2nd Division"]) {
        return CompetitionSecondDivision;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"League One"]) {
        return CompetitionLeagueOne;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"League Two"]) {
        return CompetitionLeagueTwo;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"League Cup"]) {
        return CompetitionLeagueCup;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"Charity Shield"] ||
        [competitionName localizedCaseInsensitiveContainsString:@"Community Shield"]) {
        return CompetitionCharityShield;
        // return CompetitionCharityShield;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"European Cup"] ||
        [competitionName localizedCaseInsensitiveContainsString:@"Eur. Cup"]) {
        return CompetitionEuropeanCup;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"ECW Cup"]) {
        return CompetitionEuropeanCupWinnersCup;
        //return CompetitionEuropeanCupWinnersCup;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"EFC"]) {
        return CompetitionMinor;
        // return CompetitionInterCitiesFairsCup;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"European Super Cup"]) {
        return CompetitionEuropeanSuperCup;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"World Club Championship"] ||
        [competitionName localizedCaseInsensitiveContainsString:@"WCC "]) {
        return CompetitionMinor;
        // return CompetitionWorldClubChampionship;
    }
    
    if ([competitionName localizedCaseInsensitiveContainsString:@"SSSC"]) {
        return CompetitionMinor;
        //return CompetitionFootballLeagueSuperCup;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"Centenary T"]) {
        return CompetitionMinor;
        //return CompetitionFootballLeagueCentenaryTrophy;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"Premier League"]) {
        return CompetitionPremierLeague;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"League Championship"]) {
        return CompetitionChampionship;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"Champions L"] ||
        [competitionName localizedCaseInsensitiveContainsString:@"CL "]) {
        return CompetitionEuropeanCup;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"Test Match"]) {
        return CompetitionLeagueTestMatch;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"Europa League"]) {
        return CompetitionEuropaLeague;
        //return CompetitionEuropaLeague;
    }
    
    
    
    
    
    
    NSLog(@"Unknown Competition: %@", competitionName);
    
    return CompetitionUnknown;
    
}

#pragma mark - Footballers
- (void)loadFootballers {
    PlayerParser* parser = [[PlayerParser alloc] init];
    [parser parseAll];
    
}


- (Footballer*)checkFootballer:(int)identInt identKey:(NSString*)identKey teamID:(int)teamID season:(int)season {
    if (identInt == 0) {
        return nil;
    }
    
    //
    // Create cache
    static NSMutableDictionary* cacheDictionary = nil;
    if (cacheDictionary == nil) {
        cacheDictionary = [NSMutableDictionary dictionaryWithCapacity:100];
    }
    
    //
    // Look up in cache
    NSNumber* identNumber = [NSNumber numberWithInt:identInt];
    Footballer* footballer = [cacheDictionary objectForKey:identNumber];
    if (footballer) {
        
        //
        // Check season and team
        [footballer checkSeason:season teamID:teamID];
        return footballer;
    }
    
    //
    // Check all footballers for the ident
    NSPredicate* filter = [NSPredicate predicateWithFormat:@"SELF.%@ == %d",
                           identKey, identInt];
    NSArray* filteredFootballers = [[footballerDictionary allValues] filteredArrayUsingPredicate:filter];
    if (filteredFootballers.count == 0) return nil;
    
    if (filteredFootballers.count != 1) {
        //
        // Multiple entries
        for (Footballer* footballer in filteredFootballers) {
            NSLog(@"Duplicate footballer: %@", footballer.name);
        }
        return nil;
    }
    
    //
    // Check season and team
    [filteredFootballers[0] checkSeason:season teamID:teamID];
    
    //
    // Add to cache
    [cacheDictionary setObject:filteredFootballers[0] forKey:identNumber];
    
    return filteredFootballers[0];
    
}

- (Footballer*)checkFootballerName:(NSString*)name teamID:(int)teamID {
    if (name == 0) {
        return nil;
    }
    
    //
    // Check to see if a footballer exists
    NSArray* teamArray = [footballerDictionary objectForKey:[NSNumber numberWithInt:teamID]];
    if (teamArray == nil) return nil;
    
    NSPredicate* filter = [NSPredicate predicateWithFormat:@"(name contains[cd] %@)", name];
    NSArray* filteredFootballers = [teamArray filteredArrayUsingPredicate:filter];
    if (filteredFootballers.count == 0) return nil;
    
    if (filteredFootballers.count != 1) {
        //
        // Multiple entries
        for (Footballer* footballer in filteredFootballers) {
            NSLog(@"Duplicate footballer: %@", footballer.name);
        }
        return nil;
    }
    return filteredFootballers[0];
    
    
    return nil;
    
}

//
// Only called from APIFootballParser, so sets parseIndexAPIFootballer
- (Footballer*)checkFootballerWithKey:(NSString*)footballerKey season:(int)season teamID:(int)teamID parseID:(int)parseID  {
    Footballer* existing = [footballerDictionary objectForKey:footballerKey];
    if (existing != nil) {
        existing.parseIndexAPIFootballer = parseID;
        if ([existing checkSeason:season teamID:teamID]) modifiedFootballers = YES;
    }
    
    return existing;
}


- (Footballer*)addFootballer:(Footballer*)footballer season:(int)season  {
    /*
     //
     // Get team dictionary
     // footballer.name = [FootballModel checkEncoding:footballer.name];
     NSNumber* numberTeam = [NSNumber numberWithInt:footballer.teamID];
     NSMutableArray* teamArray = [footballerDictionary objectForKey:numberTeam];
     if (teamArray == nil) {
     teamArray = [NSMutableArray arrayWithCapacity:100];
     [footballerDictionary setObject:teamArray forKey:numberTeam];
     }
     
     */
    //
    // Check if existing footballer exists, perhaps from a different parsing route
    NSString* footballerKey = [footballer generateKey];
    Footballer* existing = [footballerDictionary objectForKey:footballerKey];
    
    if (existing != nil) {
        //
        // Check for a duplicate parse ID - would happen if 2 players have same surname and date of birth (twins and unlikely coincidences)
        if ([existing checkDifferentParseID:footballer]) {
            //
            // Duplicate keys
            if (footballer.parseIndexTM != 0) {
                if (footballer.parseIndexTM == 39338) footballerKey = @"RobertsG_19840318";
                else if (footballer.parseIndexTM == 48306) footballerKey = @"ClarkeM_19801218";
                else if (footballer.parseIndexTM == 961926) return existing; // Actual duplicate
                else if (footballer.parseIndexTM == 682561) return existing; // Actual duplicate
                else if (footballer.parseIndexTM == 841547) return existing; // Actual duplicate
                else if (footballer.parseIndexTM == 222265) return existing; // Actual duplicate
                else if (footballer.parseIndexTM == 971719) return existing; // Actual duplicate
                else if (footballer.parseIndexTM == 298389) return existing; // Actual duplicate
                else if (footballer.parseIndexTM == 558154) return existing; // Actual duplicate
                else if (footballer.parseIndexTM == 3676) footballerKey = @"ChambersJ_19801120";
                else if (footballer.parseIndexTM == 3675) footballerKey = @"ChambersA_19801120";
                else if (footballer.parseIndexTM == 76572) footballerKey = @"JohnsonD_19850503";
                else if (footballer.parseIndexTM == 34851) footballerKey = @"JohnsonJ_19850503";
                else if (footballer.parseIndexTM == 199527) footballerKey = @"MurphyJa_19950224";
                else if (footballer.parseIndexTM == 199528) footballerKey = @"MurphyJo_19950224";
                else if (footballer.parseIndexTM == 406558) footballerKey = @"SessegnonS_20000518";
                else if (footballer.parseIndexTM == 392775) footballerKey = @"SessegnonR_20000518";
                else if (footballer.parseIndexTM == 293258) footballerKey = @"SarkicO_19970723";
                else if (footballer.parseIndexTM == 293257) footballerKey = @"SarkicM_19970723";
                else if (footballer.parseIndexTM == 118535) footballerKey = @"KeaneW_19930111";
                else if (footballer.parseIndexTM == 118534) footballerKey = @"KeaneM_19930111";
                else if (footballer.parseIndexTM == 679470) footballerKey = @"EkpitetaMarvel_19950826";
                else if (footballer.parseIndexTM == 570768) footballerKey = @"EkpitetaMarvin_19950826";
                else if (footballer.parseIndexTM == 390687) footballerKey = @"NewbyA_19951121";
                else if (footballer.parseIndexTM == 336855) footballerKey = @"NewbyE_19951121";
                else if (footballer.parseIndexTM == 756183) footballerKey = @"PriceL_19940065";
                else if (footballer.parseIndexTM == 723348) footballerKey = @"PriceB_19940065";
                else if (footballer.parseIndexTM == 105731) footballerKey = @"TurnbullP_19870107";
                else if (footballer.parseIndexTM == 66950) footballerKey = @"TurnbullS_19870107";
                else if (footballer.parseIndexTM == 291177) footballerKey = @"CaddenN_19960919";
                else if (footballer.parseIndexTM == 277782) footballerKey = @"CaddenC_19960919";
                else if (footballer.parseIndexTM == 307132) footballerKey = @"HoldsworthDa_19681108";
                else if (footballer.parseIndexTM == 13792) footballerKey = @"HoldsworthDe_19681108";
                else if (footballer.parseIndexTM == 17956) footballerKey = @"GunnlaugssonB_19730306";
                else if (footballer.parseIndexTM == 17955) footballerKey = @"GunnlaugssonA_19730306";
                else if (footballer.parseIndexTM == 10377) footballerKey = @"DunningD_19810108";
                else if (footballer.parseIndexTM == 98201) footballerKey = @"DunningR_19810108";
                else if (footballer.parseIndexTM == 313524) footballerKey = @"ReevesD_19671119";
                else if (footballer.parseIndexTM == 222303) footballerKey = @"ReevesA_19671119";
                else if (footballer.parseIndexTM == 313300) footballerKey = @"ClarkeC_19801218";
                else if (footballer.parseIndexTM == 48306) footballerKey = @"ClarkeM_19801218";
                else if (footballer.parseIndexTM == 44792) footballerKey = @"GibbsK_19890926";
                else if (footballer.parseIndexTM == 289444) footballerKey = @"GibbsJ_19890926";
                else if (footballer.parseIndexTM == 38073) footballerKey = @"OlssonMartin_19880517";
                else if (footballer.parseIndexTM == 72264) footballerKey = @"OlssonMarcus_19880517";
                else if (footballer.parseIndexTM == 44946) footballerKey = @"D'LaryeaN_19850903";
                else if (footballer.parseIndexTM == 28814) footballerKey = @"D'LaryeaJ_19850903";
                else if (footballer.parseIndexTM == 374659) footballerKey = @"Kelly-EvansDevon_19960921";
                else if (footballer.parseIndexTM == 347238) footballerKey = @"Kelly-EvansDion_19960921";
                else if (footballer.parseIndexTM == 348762) footballerKey = @"ColbeckJames_19930805";
                else if (footballer.parseIndexTM == 347238) footballerKey = @"ColbeckJay_19930805";
                else if (footballer.parseIndexTM == 558089) footballerKey = @"JonesK_19920101";
                else if (footballer.parseIndexTM == 156969) footballerKey = @"JonesA_19920101";
                else if (footballer.parseIndexTM == 279) footballerKey = @"SandE_19720719";
                else if (footballer.parseIndexTM == 15384) footballerKey = @"SandP_19720719";
                else if (footballer.parseIndexTM == 3519) footballerKey = @"BoerR_19700515";
                else if (footballer.parseIndexTM == 3518) footballerKey = @"BoerF_19700515";
                else if (footballer.parseIndexTM == 15362) footballerKey = @"BerezutskiV_19820620";
                else if (footballer.parseIndexTM == 15363) footballerKey = @"BerezutskiA_19820620";
                else if (footballer.parseIndexTM == 13388) footballerKey = @"ZewlakowMarcin_19760422";
                else if (footballer.parseIndexTM == 9613) footballerKey = @"ZewlakowMichal_19760422";
                else if (footballer.parseIndexTM == 29993) footballerKey = @"BenderS_19890427";
                else if (footballer.parseIndexTM == 30059) footballerKey = @"BenderL_19890427";
                else if (footballer.parseIndexTM == 481173) footballerKey = @"CamaraM_19970228";
                else if (footballer.parseIndexTM == 512916) footballerKey = @"CamaraP_19970228";
                else if (footballer.parseIndexTM == 450738) footballerKey = @"MiddletonB_19950412";
                else if (footballer.parseIndexTM == 240278) footballerKey = @"MiddletonH_19950412";
                else if (footballer.parseIndexTM == 963344) footballerKey = @"TaylorB_19960101";
                else if (footballer.parseIndexTM == 961130) footballerKey = @"TaylorA_19960101";
                else if (footballer.parseIndexTM == 87108) footballerKey = @"RobertsD_19840318";
                else if (footballer.parseIndexTM == 39338) footballerKey = @"RobertsG_19840318";
                else {
                    NSLog(@"Duplicate footballer %@, Key: %@, ID:%d (other ID:%d)", footballer.name, footballerKey, footballer.parseIndexTM, existing.parseIndexTM);
                    int ian = 10;
                }
            }
            if (footballer.parseIndexLFC != 0) {
                //
                // LFC lists players separately when they play for Liverpool and other clubs
                if (footballer.parseIndexLFC == 6893) footballerKey = @"LucasM_19330929";
                if ([footballer.name compare:existing.name] == NSOrderedSame) return existing;
            }
        }
        else {
            //
            // Update footballer if needed
            if ([existing compareAndUpdate:footballer]) modifiedFootballers = YES;
            if ([existing checkSeason:season teamID:footballer.teamID]) modifiedFootballers = YES;
            return existing;
            
        }
    }
    
    //
    // Create new footballer
    footballer.index = (int)footballerDictionary.count + 1;
    [footballer checkSeason:season teamID:footballer.teamID];
    modifiedFootballers = YES;
    [footballerDictionary setObject:footballer forKey:footballerKey];
    
    return footballer;
}

- (void)setFootballersModified {
    modifiedFootballers = YES;
}

- (NSUInteger)getFootballerCount {
    return footballerDictionary.count;
}

- (void)addFootballerComplete:(Footballer*)footballer  {
    NSString* footballerKey = [footballer generateKey];
    Footballer* existing = [footballerDictionary objectForKey:footballerKey];
    if (existing != nil) {
        NSLog(@"Duplicate footballer: %@", footballer.name);
        return;
    }
    
    //
    // Create new footballer
    footballer.index = (int)footballerDictionary.count + 1;
    // modifiedFootballers = YES;
    [footballerDictionary setObject:footballer forKey:footballerKey];
    
    if ([footballerKey compare:@"Dalglish_19510304"] == NSOrderedSame) {
        int value = [self getFootballerID:footballerKey];
        int ian = 10;
    }
    
    //
    // Add to export
    for (id key in teamFootballerDictionary) {
        if ([footballer hasPlayedForTeamID:key]) {
            NSMutableArray* players = teamFootballerDictionary[key];
            [players addObject:footballer];
        }
    }
    
    
}

- (int)getFootballerID:(NSString*)footballerKey  {
    Footballer* existing = [footballerDictionary objectForKey:footballerKey];
    if (existing == nil) {
        NSLog(@"Failed to find expected footballer: %@", footballerKey);
        return 0;
    }
    return existing.index;
}

- (Footballer*)getFootballer:(NSString*)footballerKey season:(int)season teamID:(int)teamID  {
    Footballer* existing = [footballerDictionary objectForKey:footballerKey];
    if (existing == nil) {
        NSLog(@"Failed to find expected footballer: %@", footballerKey);
        return 0;
    }
    
    //
    // Check footballer season
    [existing checkSeason:season teamID:teamID];
    return existing;
}

- (NSArray*)getFootballersForTeam:(int)teamID {
    NSMutableArray* footballers = [NSMutableArray arrayWithCapacity:500];
    for (id key in footballerDictionary) {
        Footballer* footballer = footballerDictionary[key];
        if ([footballer hasPlayedForTeamID:@(teamID)]) {
            [footballers addObject:footballer];
        }
    }
    return footballers;
}

- (Footballer*)getFootballer:(NSString*)footballerKey  {
    Footballer* existing = [footballerDictionary objectForKey:footballerKey];
    return existing;
}

#pragma mark - Club Footballers
- (bool)setClubFootballerIndex:(ClubFootballer*)clubFootballer teamID:(int)teamID {
    Footballer* existing = [footballerDictionary objectForKey:clubFootballer.dictionaryKey];
    if (existing == nil) return NO;
    /*
     if (existing.nameFull != nil) {
     //
     // Hack to restore overwritten data
     existing.role = [existing.birthPlace intValue];
     existing.birthPlace = existing.countryID == 0 ? nil : [@(existing.countryID) stringValue];
     existing.countryID = existing.deathDateNumber;
     existing.deathDateNumber = existing.birthDateNumber;
     existing.birthDateNumber = [existing.nameFull intValue];
     existing.nameFull = nil;
     }
     */
    clubFootballer.footballer = existing;
    clubFootballer.index = existing.index;
    [existing addPlayedForTeamID:@(teamID)];
    return YES;
}

- (void)addClubFootballer:(ClubFootballer*)clubFootballer teamID:(int)teamID {
    NSMutableArray* clubFootballerArray = [clubFootballerDictionary objectForKey:@(teamID)];
    if (clubFootballerArray == nil) {
        clubFootballerArray = [NSMutableArray arrayWithCapacity:1000];
        [clubFootballerDictionary setObject:clubFootballerArray forKey:@(teamID)];
    }
    [clubFootballerArray addObject:clubFootballer];
}

- (NSArray*) getClubPlayers:(int)teamID {
    NSMutableArray* clubFootballerArray = [clubFootballerDictionary objectForKey:@(teamID)];
    return clubFootballerArray;
}

#pragma mark - Matches
- (void)loadMatches {
    MatchParser* parser = [[MatchParser alloc] init];
    [parser parseAll];
    
}
- (void)loadMatchesTest {
    MatchParser* parser = [[MatchParser alloc] init];
    [parser parseTest];
    
}

- (void)loadMatchesSeason:(int)season {
    MatchParser* parser = [[MatchParser alloc] init];
    [parser parseSeason:season];
    
}

- (bool)checkMatch:(NSString*)matchKey {
    Match* match = [matchDictionary objectForKey:matchKey];
    if (match == nil) return false;
    
    return true;
    
}

- (bool)checkMatch:(NSString*)matchKey matchParseState:(MatchParseState)matchParseState {
    Match* match = [matchDictionary objectForKey:matchKey];
    if (match == nil) {
        NSLog(@"Match not found: %@", matchKey);
        return false;
    }
    return match.matchParseState >= matchParseState;
}

- (bool)checkMatch:(ParseType)parseType dataKey:(int)dataKey {
    NSString* parseTypeString = nil;
    if (parseType == ParseUEFA) {
        parseTypeString = @"parseUEFA";
    }
    if (parseTypeString == nil) return false;
    
    NSArray* resultArray = [[matchDictionary allValues] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(%@ == %d)", parseTypeString, dataKey]];
    
    if (resultArray.count == 0) return false;
    
    return true;
}


- (Match*)getMatch:(int)dateNumber teamID1:(int)teamID1 teamID2:(int)teamID2 {
    NSString* matchKey = [Match makeMatchKeyFromDateNumber:dateNumber team1:teamID1 team2:teamID2];
    Match* match = [matchDictionary objectForKey:matchKey];
    if (match != nil) return match;
    
    //
    // Try day before and after
    matchKey = [Match makeMatchKeyFromDateNumber:dateNumber - 1 team1:teamID1 team2:teamID2];
    match = [matchDictionary objectForKey:matchKey];
    if (match != nil) return match;
    matchKey = [Match makeMatchKeyFromDateNumber:dateNumber + 1 team1:teamID1 team2:teamID2];
    match = [matchDictionary objectForKey:matchKey];
    if (match != nil) return match;
    return nil;
}
- (Match*)getMatch:(NSString*)matchKey {
    Match* match = [matchDictionary objectForKey:matchKey];
    return match;
}

- (bool)addMatch:(Match*)match {
    if ([match.matchKey containsString:@""]) {
        int ina = 10;
    }
    
    Match* existingMatch = [matchDictionary objectForKey:match.matchKey];
    if (existingMatch != nil) {
        //
        // Don't add match if one already exists with events
        if (existingMatch.matchParseState >= MatchParseStateEventsPartial) {
            return false;
        }
        match.index = existingMatch.index;
    }
    else {
        match.index = (int)matchDictionary.count + 1;
    }
    [matchDictionary setObject:match forKey:match.matchKey];
    modifiedMatches = YES;
    
    return true;
}

- (void)createMatchEvents:(Match*)match team1Events:(NSArray*)team1Events team2Events:(NSArray*)team2Events {
    
}

- (NSUInteger)getMatchCount {
    return matchDictionary.count;
}

- (NSArray*)getModifiedMatchesInSeason:(int)season competition:(CompetitionType)competition {
    NSArray* resultArray = [[matchDictionary allValues] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(seasonNumber == %d) && (competition == %d) && (modified == YES)", season, competition]];
    
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateNumber"
                                                 ascending:YES];
    NSArray *sortedArray = [resultArray sortedArrayUsingDescriptors:@[sortDescriptor]];
    
    return sortedArray;
}

- (NSArray*)getAllMatchesInSeason:(int)season competition:(CompetitionType)competition {
    NSArray* resultArray = [[matchDictionary allValues] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(seasonNumber == %d) && (competition == %d)", season, competition]];
    
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateNumber"
                                                 ascending:YES];
    NSArray *sortedArray = [resultArray sortedArrayUsingDescriptors:@[sortDescriptor]];
    
    return sortedArray;
}

- (void)matchIsModified:(Match*)match {
    match.modified = YES;
    if (modifiedSeasonSet == nil) {
        modifiedSeasonSet = [NSMutableSet setWithCapacity:50];
    }
    [modifiedSeasonSet addObject:match.seasonNumber];
}

- (bool)areMatchesModified {
    return modifiedSeasonSet.count != 0;
}

#pragma mark - Match Stats

- (bool)checkMatchStats:(int)matchID {
    MatchStats* matchStats = [matchStatsDictionary objectForKey:@(matchID)];
    if (matchStats == nil) {
        NSLog(@"MatchStats not found: %d", matchID);
        return false;
    }
    return true;

}

- (bool)addMatchStats:(MatchStats*)matchStats {
    [matchStatsDictionary setObject:matchStats forKey:@(matchStats.matchID)];
    modifiedMatchStats = YES;
    return true;
}

- (NSArray*)getAllMatchStatsInSeason:(NSInteger)season {
    NSArray* resultArray = [[matchStatsDictionary allValues] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(seasonNumber == %ld)", season]];
    
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"matchID"
                                                 ascending:YES];
    NSArray *sortedArray = [resultArray sortedArrayUsingDescriptors:@[sortDescriptor]];
    
    return sortedArray;
}


#pragma mark - Events
- (void)addMatchEvents:(Match*)match {
    //
    // Add match events
    int team1ID = [match getTeam1ID];
    int team2ID = [match getTeam2ID];
    [self addTeamEvents:team1ID eventArray:match.team1Events match:match otherTeam:team2ID];
    [self addTeamEvents:team2ID eventArray:match.team2Events match:match otherTeam:team1ID];
}

- (void)addTeamEvents:(int)teamID eventArray:(NSArray*)eventArray match:(Match*)match  otherTeam:(int)otherTeam {
    //
    // Get team event dictionary
    NSNumber* numberTeam = [NSNumber numberWithInt:teamID];
    NSMutableArray* teamEventArray = [eventDictionary objectForKey:numberTeam];
    if (teamEventArray == nil) {
        teamEventArray = [NSMutableArray arrayWithCapacity:100];
        [eventDictionary setObject:teamEventArray forKey:numberTeam];
    }
    for (NSNumber* eventDetail in eventArray) {
        Event* event = [[Event alloc] init];
        event.matchID = match.index;
        event.teamID = otherTeam;
        event.locationID = match.locationID;
        event.matchDateNumber = match.dateNumber;
        event.competition = match.competition;
        event.season8 = match.season8;
        event.eventDetail = [eventDetail unsignedLongLongValue];
        [teamEventArray addObject:event];
    }
    
}



#pragma mark - Blobs

- (void)toBlobsTest {
    for (id key in matchDictionary) {
        Match* match = matchDictionary[key];
    }
}


- (void)toBlobs {
    //
    // Set data path
    NSString* dataPath  = [[NSUserDefaults standardUserDefaults] objectForKey:@"blobPath"];

    modifiedTeams = true;
    modifiedLocations = true;
    modifiedFootballers = true;
    modifiedTeams = true;
    //
    // Simple blobs
    NSMutableArray* modifiedArray = [NSMutableArray arrayWithCapacity:5];
    if (modifiedTeams) [modifiedArray addObject:teamDictionary];
    if (modifiedLocations) [modifiedArray addObject:locationDictionary];
    for (NSDictionary* dictionary in modifiedArray) {
        NSMutableData* buffer = [NSMutableData dataWithCapacity:5000];
        NSString* blobName = nil;
        for (id key in dictionary) {
            id<BlobCoding> blobCoder = dictionary[key];
            [blobCoder encodeToBlob:buffer];
            if (blobName == nil) {
                blobName = [[((NSObject*)blobCoder) class] getBlobName];
           }
        }

        //
        // Save to file
        [fileManager savePersistedData:dataPath dataName:BlobsFilePath dataType:nil dataKey:blobName blobData:buffer];
        // [fileManager savePersistedData:buffer dataName:BlobsFilePath dataKey:blobName];
    }

    //
    // Matches
    // Add matches to season array
    {
        NSMutableDictionary* seasonDictionary = [NSMutableDictionary dictionaryWithCapacity:200];
        for (id key in matchDictionary) {
            Match* match = matchDictionary[key];
            NSMutableArray* seasonArray = [seasonDictionary objectForKey:match.seasonNumber];
            if (seasonArray == nil) {
                seasonArray = [NSMutableArray arrayWithCapacity:200];
                [seasonDictionary setObject:seasonArray forKey:match.seasonNumber];
            }
            [seasonArray addObject:match];
        }
        for (id key in seasonDictionary) {
            NSMutableData* buffer = [NSMutableData dataWithCapacity:5000];
            NSMutableArray* seasonArray = seasonDictionary[key];
            for (Match* match in seasonArray) {
                [match encodeToBlob:buffer];
            }
            
            //
            // Save to file
            NSString* fileName = [NSString stringWithFormat:@"%@_%@", [Match getBlobName], key];
            [fileManager savePersistedData:dataPath dataName:BlobsFilePath dataType:[Match getBlobName] dataKey:fileName blobData:buffer];
        }
    }
    
    //
    // MatchStats
    // Add matchStats to season array
    {
        NSMutableDictionary* matchStatsSeasonDictionary = [NSMutableDictionary dictionaryWithCapacity:200];
        for (id key in matchStatsDictionary) {
            MatchStats* matchStats = matchStatsDictionary[key];
            NSMutableArray* seasonArray = [matchStatsSeasonDictionary objectForKey:matchStats.seasonNumber];
            if (seasonArray == nil) {
                seasonArray = [NSMutableArray arrayWithCapacity:200];
                [matchStatsSeasonDictionary setObject:seasonArray forKey:matchStats.seasonNumber];
            }
            [seasonArray addObject:matchStats];
        }
        for (id key in matchStatsSeasonDictionary) {
            NSMutableData* buffer = [NSMutableData dataWithCapacity:5000];
            NSMutableArray* seasonArray = matchStatsSeasonDictionary[key];
            for (MatchStats* matchStats in seasonArray) {
                [matchStats encodeToBlob:buffer];
            }
            
            //
            // Save to file
            NSString* fileName = [NSString stringWithFormat:@"%@_%@", [MatchStats getBlobName], key];
            [fileManager savePersistedData:dataPath dataName:BlobsFilePath dataType:[MatchStats getBlobName] dataKey:fileName blobData:buffer];
        }
    }
    
    //
    // Footballers
    if (modifiedFootballers) {
        //
        // Order footballers by index
        NSArray* orderedFootballers = [footballerDictionary keysSortedByValueUsingComparator:^NSComparisonResult(Footballer* footballer1, Footballer* footballer2){
            int value1 = footballer1.index;
            int value2 = footballer2.index;
            if ( value1 > value2 ) {
                return (NSComparisonResult)NSOrderedDescending;
            } else if ( value1 < value2 ) {
                return (NSComparisonResult)NSOrderedAscending;
            } else {
                return (NSComparisonResult)NSOrderedSame;
            }
        }];
        NSMutableData* buffer = [NSMutableData dataWithCapacity:5000];
        for (id key in orderedFootballers) {
            Footballer* footballer = footballerDictionary[key];
            [footballer encodeToBlob:buffer];
            
        }

        //
        // Save to file
        NSString* fileName = [Footballer getBlobName];
        [fileManager savePersistedData:dataPath dataName:BlobsFilePath dataType:nil dataKey:fileName blobData:buffer];
    }
    
    
    for (id key in teamFootballerDictionary) {
        NSArray* players = teamFootballerDictionary[key];
        if (players.count == 0) continue;
        NSMutableData* buffer = [NSMutableData dataWithCapacity:5000];
        for (Footballer* footballer in players) {
            [footballer encodeToBlob:buffer];
        }
        
        //
        // Save to file
        NSString* fileName = [NSString stringWithFormat:@"Team_%@", key];
        [fileManager savePersistedData:dataPath dataName:BlobsFilePath dataType:[Footballer getBlobName]  dataKey:fileName blobData:buffer];

    }


    //}

    //
    // Events
    for (NSNumber* teamId in eventDictionary) {
        NSString* fileName = [NSString stringWithFormat:@"%@_%@", [Event getBlobName], teamId];
        NSArray* eventArray = eventDictionary[teamId];
        NSMutableData* buffer = [NSMutableData dataWithCapacity:5000];
        for (Event* event in eventArray) {
            [event encodeToBlob:buffer];
        }
        [fileManager savePersistedData:dataPath dataName:BlobsFilePath dataType:[Event getBlobName]  dataKey:fileName blobData:buffer];
    }
    
    NSLog(@"Save blobs complete: %@", dataPath);

}

- (void)encodeTeamsToString:(NSMutableString*)csvString englishTeams:(bool)englishTeams {
    NSMutableArray* teamArray = [NSMutableArray arrayWithCapacity:1000];
    for (id key in teamDictionary) {
        Team* team = teamDictionary[key];
        if (englishTeams && team.countryID == CountryEngland) {
            [teamArray addObject:team];
        }
        else if (!englishTeams && team.countryID != CountryEngland && team.countryID != CountryUnknown) {
            [teamArray addObject:team];
        }
   }

    //
    // Order by ID
    NSArray* orderedArray = [teamArray sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        int first = ((Team*)a).index;
        int second = ((Team*)b).index;
        return first - second;
    }];

    for (Team* team in orderedArray) {
       // [APIFootballParser setTeamColour:team];
        [csvString appendString:[team encodeToString]];
   }

    
}

- (void)fromBlobs {
    NSString* dataPath  = [[NSUserDefaults standardUserDefaults] objectForKey:@"blobPath"];
    
    //
    // Teams
    {
        NSData* buffer = [fileManager loadPersistedData:dataPath dataName:BlobsFilePath dataType:nil dataKey:[Team getBlobName]];
        int index = 0;
        const char* bufferPointer = buffer.bytes;
        while (index < buffer.length) {
            Team* team = [[Team alloc] init];
            index += [team initFromBlob:(bufferPointer + index)];
            [teamDictionary setObject:team forKey:team.name];
            
            /*
            //
            // Footballers, per team
            NSNumber* numberTeam = [NSNumber numberWithInt:team.index];
            NSMutableArray* teamArray = [footballerDictionary objectForKey:numberTeam];
            if (teamArray == nil) {
                teamArray = [NSMutableArray arrayWithCapacity:100];
                [footballerDictionary setObject:teamArray forKey:numberTeam];
            }
            */
/*
            {
                NSString* fileName = [NSString stringWithFormat:@"%@_%d", [Footballer getBlobName], team.index];
                NSData* buffer2 = [fileManager loadPersistedData:dataPath dataName:BlobsFilePath dataType:nil dataKey:fileName];
                int index2 = 0;
                const char* bufferPointer2 = buffer2.bytes;
                while (index2 < buffer2.length) {
                    Footballer* footballer = [[Footballer alloc] init];
                    index2 += [footballer initFromBlob:(bufferPointer2 + index2)];
                    footballer.teamID = team.index;
                    NSString* footballerId = [NSString stringWithFormat:@"%@_%d", footballer.name, footballer.parseIndexLFC];
                    [teamArray addObject:footballer];
                }
                modifiedFootballers = false;
            }
 */
        }
        modifiedTeams = false;
    }
    
    //
    // Matches
    {
        int firstSeason = 1955;
        int lastSeason = 2020;
        NSString* matchBlobName = [Match getBlobName];
        for (int i = firstSeason; i < lastSeason; i++) {
            NSString* fileName = [NSString stringWithFormat:@"%@_%d", matchBlobName, i];
            NSData* buffer = [fileManager loadPersistedData:dataPath dataName:BlobsFilePath dataType:matchBlobName dataKey:fileName];
            if (buffer == nil) continue;
            int index = 0;
            const char* bufferPointer = buffer.bytes;
            while (index < buffer.length) {
                Match* match = [[Match alloc] init];
                index += [match initFromBlob:(bufferPointer + index)];
                [matchDictionary setObject:match forKey:match.matchKey];
            }
            modifiedMatches = false;
        }

    }
    
    //
    // Locations
    {
        NSData* buffer = [fileManager loadPersistedData:dataPath dataName:BlobsFilePath dataType:nil dataKey:[Location getBlobName]];
        int index = 0;
        const char* bufferPointer = buffer.bytes;
        while (index < buffer.length) {
            Location* location = [[Location alloc] init];
            index += [location initFromBlob:(bufferPointer + index)];
            [locationDictionary setObject:location forKey:location.name];
        }
        modifiedLocations = false;
    }
    
    //
    // Footballers
    {
        NSString* fileName = [Footballer getBlobName];
        NSData* buffer = [fileManager loadPersistedData:dataPath dataName:BlobsFilePath dataType:nil dataKey:fileName];
        int index = 0;
        const char* bufferPointer = buffer.bytes;
        while (index < buffer.length) {
            Footballer* footballer = [[Footballer alloc] init];
            index += [footballer initFromBlob:(bufferPointer + index)];
            // footballer.teamID = team.index;
            [footballer generateKey];
            [footballerDictionary setObject:footballer forKey:footballer.dictionaryKey];
        }
        modifiedFootballers = false;
    }
}

#pragma mark - CSV & JSON Export
- (void)exportCSVMatches {
    NSString* dataPath  = [[NSUserDefaults standardUserDefaults] objectForKey:@"dataPath"];
    
    int competitonArray[] = {CompetitionFACup, CompetitionLeagueCup,
        CompetitionPremierLeague, CompetitionChampionship, CompetitionLeagueOne, CompetitionLeagueTwo,
        CompetitionFirstDivision, CompetitionSecondDivision, CompetitionThirdDivision, CompetitionFourthDivision,
        CompetitionThirdDivisionSouth, CompetitionThirdDivisionNorth,
        CompetitionNationalLeague,
        CompetitionEuropeanCup,
        CompetitionEuropeanCupWinnersCup,
        CompetitionUEFACup,
        CompetitionEuropaLeague,
        CompetitionEuropaConferenceLeague,
        CompetitionCharityShield,
        CompetitionEuropeanSuperCup,
        CompetitionInterTotoCup,
        CompetitionClubWorldCup,
        CompetitionIntercontinentalCup,

    };
    
   // modifiedSeasonSet = [[NSMutableSet alloc] initWithArray: @[ @(1992), @(1993)]];
    
    // int competitonArray[] = {CompetitionLeagueCup };
    // int decade = 1870;
    bool forceCopy = true;
    int decade = 1870;
    int year = 0;
    int season = decade + year;
    for (NSNumber* seasonNumber in modifiedSeasonSet) {
        season = [seasonNumber intValue];
        decade = season / 10 * 10;
        year = season % 10;
        NSLog(@"*** Season %d ***", season);
        int matchProcessedCount = 0;
 
        int nextSeason = (decade + year + 1) % 100;
        NSString* seasonString = [NSString stringWithFormat:@"%ds/%d-%02d", decade, decade + year, nextSeason];
        for (int i = 0; i < sizeof(competitonArray) / sizeof(competitonArray[0]); i++) {
            int competiton = competitonArray[i];
            NSString* competitionName = [Match getCompetitionShortName:competiton];
            if (competitionName != nil) {
                //
                // Check if file exists or matches are modified
                NSArray* matchesModified = [self getModifiedMatchesInSeason:season competition:competiton];
                NSString* fileName = [NSString stringWithFormat:@"%@.csv", competitionName];
                if (matchesModified.count != 0 || ![fileManager doesFileExist:dataPath dataName:ExportMatchDirectory dataType:seasonString dataKey:fileName]) {
                    
/*
                NSMutableString* csvStringMissingMatches = [NSMutableString stringWithCapacity:10000];
                [csvStringMissingMatches appendString:@"DateString,Competition,MatchString\n"];
                int matchSkippedCount = 0;
*/
                    NSString* directoryName = [NSString stringWithFormat:@"%@/%@", seasonString, competitionName];
                    NSArray* matchArray = [self getAllMatchesInSeason:season competition:competiton];
                    if (matchArray.count != 0) {
                        NSLog(@"* Creating File: %@", fileName);
                        NSMutableString* csvString = [NSMutableString stringWithCapacity:10000];
                        [csvString appendString:[Match getStringExportHeader]];
                        for (Match* match in matchArray) {
                            NSString* stringEncode = [match encodeToString];
                            if (stringEncode != nil) {
                                [csvString appendString:stringEncode];
                            }
                            NSString* matchName = [match getFileName];
                            
                            //
                            // Encode to JSON if modified
                            if (match.modified && match.matchState != MatchStateFutureMatch) {
                            
                            //
                            // ToDo - checking the wrong file!
                                if (![fileManager doesFileExist:dataPath dataName:ExportMatchDirectory dataType:directoryName dataKey:matchName]) {
                                    NSString* matchJSON = [match encodeToJSONString:YES basicMatch:NO];
                                    if (matchJSON != nil) {
                                        NSString* saveDirectoryName = [NSString stringWithFormat:@"%@/Complete", directoryName];
                                        [fileManager savePersistedString:dataPath dataName:ExportMatchDirectory dataType:saveDirectoryName dataKey:matchName dataString:matchJSON];
                                        matchProcessedCount++;
                                    }
                                    else {
                                        matchJSON = [match encodeToJSONString:NO basicMatch:NO];
                                        if (matchJSON != nil) {
                                            NSString* saveDirectoryName = [NSString stringWithFormat:@"%@/Partial", directoryName];
                                            [fileManager savePersistedString:dataPath dataName:ExportMatchDirectory dataType:saveDirectoryName dataKey:matchName dataString:matchJSON];
                                            matchProcessedCount++;
                                        }
                                        else {
                                            /*
                                             matchJSON = [match encodeToJSONString:NO basicMatch:YES];
                                             if (matchJSON != nil) {
                                             NSString* saveDirectoryName = [NSString stringWithFormat:@"%@/Basic", directoryName];
                                             [fileManager savePersistedString:dataPath dataName:ExportMatchDirectory dataType:saveDirectoryName dataKey:matchName dataString:matchJSON];
                                             matchProcessedCount++;
                                             }
                                             else {
                                             [csvStringMissingMatches appendString:[NSString stringWithFormat:@"%d,%@,%@\n", match.dateNumber, competitionName, matchName]];
                                             matchSkippedCount++;
                                             }
                                             */
                                        }
                                    }
                                }
                            }
                        }
                        
                        //
                        // Save file
                        [fileManager savePersistedString:dataPath dataName:ExportMatchDirectory dataType:seasonString dataKey:fileName dataString:csvString];
                        //if (season == 1977) {
                       // [fileManager savePersistedString:dataPath dataName:ExportMatchDirectory dataType:seasonString dataKey:fileName dataString:csvString];
                        //}
                    }
                    /*
                    if (matchSkippedCount != 0) {
                        NSLog(@"  Skipped %d matches", matchSkippedCount);
                        NSString* fileName = [NSString stringWithFormat:@"%@.MissingMatches.csv", competitionName];
                       [fileManager savePersistedString:dataPath dataName:ExportMatchDirectory dataType:seasonString dataKey:fileName dataString:csvStringMissingMatches];
                    }
                    */

                }
            }
            else {
                NSLog(@"** Unsupported Competition: %d", competiton);
            }
        }
        if (matchProcessedCount != 0) {
            NSLog(@"  Processed %d matches", matchProcessedCount);
        }

        year++;
        if (year == 10) {
            decade += 10;
            year = 0;
        }
    }
    
    [modifiedSeasonSet removeAllObjects];
}

- (void)exportCSVMatchStats {
    if (!modifiedMatchStats) return;
    NSString* dataPath  = [[NSUserDefaults standardUserDefaults] objectForKey:@"dataPath"];
    
    NSInteger seasonStart = 2023; // CURRENT_SEASON;
    NSInteger seasonEnd = [self getCurrentSeason];
    for (NSInteger season = seasonStart; season <= seasonEnd; season++) {
        NSArray* matchStatsArray = [self getAllMatchStatsInSeason:season];
        if (matchStatsArray.count == 0) continue;
        
        NSInteger decade = season / 10 * 10;
        NSInteger year = season % 10;
        NSLog(@"*** Season %ld ***", (long)season);
        int matchProcessedCount = 0;
        NSInteger nextSeason = (decade + year + 1) % 100;
        NSString* seasonString = [NSString stringWithFormat:@"%lds/%ld-%02ld", decade, decade + year, nextSeason];

        NSString* fileName = [NSString stringWithFormat:@"%@%ld.csv", [MatchStats getBlobName], season];
        NSLog(@"* Creating File: %@", fileName);
        NSMutableString* csvString = [NSMutableString stringWithCapacity:10000];
        [csvString appendString:[MatchStats getStringExportHeader]];
        for (MatchStats* matchstats in matchStatsArray) {
            NSString* stringEncode = [matchstats encodeToString];
            if (stringEncode != nil) {
                [csvString appendString:stringEncode];
            }
        }
        
        [fileManager savePersistedString:dataPath dataName:ExportMatchDirectory dataType:seasonString dataKey:fileName dataString:csvString];
    }
}

- (void)exportCSVPlayers:(bool)test {
    
    //
    // Export players per team into csv, with parse ids for different parsers
    // if (!modifiedFootballers) return;
    bool forceCopy = true;
    NSString* dataPath  = [[NSUserDefaults standardUserDefaults] objectForKey:@"dataPath"];
    char letterChar = 'A';
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateNumber"
                                                 ascending:YES];
    while (letterChar < 'Z' + 1) {
        NSString* letter = [NSString stringWithFormat:@"%c", letterChar];
        NSPredicate* filter = [NSPredicate predicateWithFormat:@"SELF BEGINSWITH[c] %@",
                               letter]; // [c] makes this case insensitive
        NSArray* filteredFootballerKeys = [[footballerDictionary allKeys] filteredArrayUsingPredicate:filter];
        filteredFootballerKeys = [filteredFootballerKeys sortedArrayUsingSelector:@selector(compare:)];
        NSString* fileName = [NSString stringWithFormat:@"%@.csv", letter];
        if (forceCopy || ![fileManager doesFileExist:dataPath dataName:ExportPlayerDirectory dataType:nil dataKey:fileName]) {
            if (filteredFootballerKeys.count != 0) {
                NSLog(@"* Creating File: %@", fileName);
                NSMutableString* csvString = [NSMutableString stringWithCapacity:10000];
                [csvString appendString:[Footballer getStringExportHeader]];
                for (NSString* key in filteredFootballerKeys) {
                    Footballer* footballer = [footballerDictionary objectForKey:key];
                    if (test) {
                        if (!(footballer.parseIndexLFC != 0 && footballer.parseIndex11v11 == 0)) continue;
                    }
                    [csvString appendString:[footballer encodeToString]];
                }
                [fileManager savePersistedString:dataPath dataName:ExportPlayerDirectory dataType:test ? @"test" : @"" dataKey:fileName dataString:csvString];
                
            }
        }
        letterChar++;
       //  break;
    }

 }

- (void)exportCSVManagers:(bool)test {
    
    //
    // Export managers into csv, with parse ids for different parsers
    // if (!modifiedManagers) return;
    NSString* dataPath  = [[NSUserDefaults standardUserDefaults] objectForKey:@"dataPath"];
    NSArray* managerKeys = [managerDictionary allKeys];
    if (managerKeys.count != 0) {
        NSString* fileName = @"Managers.csv";
        NSLog(@"* Creating File: %@", fileName);
        NSMutableString* csvString = [NSMutableString stringWithCapacity:10000];
        [csvString appendString:[Footballer getStringExportHeader]];
        for (NSString* key in managerKeys) {
            Manager* manager = [managerDictionary objectForKey:key];
            [csvString appendString:[manager encodeToString]];
        }
        [fileManager savePersistedString:dataPath dataName:ExportManagerDirectory dataType:test ? @"test" : @"" dataKey:fileName dataString:csvString];
    }
}


- (void)exportJSONGames {
    NSString* dataPath  = [[NSUserDefaults standardUserDefaults] objectForKey:@"dataPath"];
    
    int competitonArray[] = {CompetitionFACup, CompetitionLeagueCup,
        CompetitionPremierLeague, CompetitionChampionship, CompetitionLeagueOne, CompetitionLeagueTwo,
        CompetitionFirstDivision, CompetitionSecondDivision, CompetitionThirdDivision, CompetitionFourthDivision,
        CompetitionThirdDivisionSouth, CompetitionThirdDivisionNorth,
        CompetitionNationalLeague,
        CompetitionEuropeanCup
    };
    
    // int competitonArray[] = {CompetitionLeagueCup };
    // int decade = 1870;
    int decade = 1870;
    int year = 0;
    int season = decade + year;
    while (season < 2020) {
        season = decade + year;
        NSLog(@"*** Season %d ***", season);
        int nextSeason = (decade + year + 1) % 100;
        NSString* seasonString = [NSString stringWithFormat:@"%ds/%d-%02d", decade, decade + year, nextSeason];
        for (int i = 0; i < sizeof(competitonArray) / sizeof(competitonArray[0]); i++) {
            int competiton = competitonArray[i];
            NSString* competitionName = [Match getCompetitionShortName:competiton];
            if (competitionName != nil) {
                //
                // Check if file exists
                NSString* fileName = [NSString stringWithFormat:@"%@.csv", competitionName];
                if (![fileManager doesFileExist:dataPath dataName:ExportMatchDirectory dataType:seasonString dataKey:fileName]) {
                    NSArray* matchArray = [self getModifiedMatchesInSeason:season competition:competiton];
                    if (matchArray.count != 0) {
                        NSLog(@"* Creating File: %@", fileName);
                        NSMutableString* csvString = [NSMutableString stringWithCapacity:10000];
                        [csvString appendString:@"DateString,Team1,Score,Team2,C Flag,Venue,Penalties,M Flag\n"];
                        for (Match* match in matchArray) {
                            [csvString appendString:[match encodeToString]];
                        }
                        //if (season == 1977) {
                        [fileManager savePersistedString:dataPath dataName:ExportMatchDirectory dataType:seasonString dataKey:fileName dataString:csvString];
                        //}
                    }
                }
            }
            else {
                NSLog(@"** Unsupported Competition: %d", competiton);
            }
        }
        
        year++;
        if (year == 10) {
            decade += 10;
            year = 0;
        }
    }
}

#pragma mark - Test
- (NSArray*)getFootballersTest {
    return [footballerDictionary allValues];
}



@end
