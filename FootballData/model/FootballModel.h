//
//  FootballModel.h
//  FootballData
//
//  Created by Ian Copeman on 11/11/2020.
//

#import <Foundation/Foundation.h>
#import "Footballer.h"
#import "Match.h"
#import "Competition.h"
#import "Event.h"
#import "Manager.h"
#import "ClubFootballer.h"
#import "MatchStats.h"

//
// Known parse types
typedef enum {
    ParseLFC,
    ParseUEFA,
    Parse11v11,
} ParseType;


//
// DateNumber
#define DATE_NUMBER_TO_YEAR(n) (n/10000)
#define DATE_NUMBER_TO_MONTH(n) ((n % 10000) / 100)
#define DATE_NUMBER_TO_DAY(n) (n % 100)
#define DATE_NUMBER_CREATE(y,m,d) (y * 10000 + m * 100 + d)

#define TIME_NUMBER_CREATE(h,m) (h * 100 + m)
#define TIME_NUMBER_TO_HOUR(n) (ABS(n / 100))
#define TIME_NUMBER_TO_MINUTE(n) (ABS(n % 100))
#define TIME_NUMBER_IS_PREVIOUS_DAY(n) (n < 0)


NS_ASSUME_NONNULL_BEGIN


@interface FootballModel : NSObject

+ (FootballModel*)getModel;
- (NSInteger) getCurrentSeason;
- (NSInteger) getCurrentDateNumber;
- (int)addLocation:(NSString*)locationName;
- (NSUInteger)getLocationCount;

- (CompetitionType)addCompetition:(NSString*)competitionName;

#pragma mark - Teams
- (void)loadTeams;
- (int)addTeam:(NSString*)teamName teamID:(int)teamID shortName:(NSString*)shortName vShortName:(NSString*)vShortName parseUEFA:(int)parseUEFA indexAPI:(int)indexAPI  parseTM:(int)parseTM latitudeString:(NSString*)latitudeString longitudeString:(NSString*)longitudeString countryID:(unsigned int)countryID colour:(int)colour;
- (int)addTeamUEFA:(NSString*)teamName parseUEFA:(int)parseUEFA season:(int)season countryCode:(NSString*)countryCode;
- (int)addTeam:(NSString*)teamName;
- (int)addTeam:(NSString*)teamName countryString:(NSString*)countryString;
- (NSUInteger)getTeamCount;
- (void)reportNewTeams;
- (NSString*)getTeamName:(int)teamID;
- (int)addTeam:(NSString*)teamName season:(int)season;
- (int)addTeam:(NSString*)teamName identInt:(int)identInt identKey:(NSString*)identKey;
- (int)getTeamID:(NSString*)teamName;
- (Team*)getTeam:(NSString*)teamName identInt:(int)identInt identKey:(NSString*)identKey;
- (Team*)getTeam:(int)teamID;
- (Team*)getTeamFromName:(NSString*)teamName;
- (Team*)getTeam:(int)identInt identKey:(NSString*)identKey;

#pragma mark - Matches
- (void)loadMatches;
- (void)loadMatchesTest;
- (void)loadMatchesSeason:(int)season;
- (bool)checkMatch:(NSString*)matchKey;
- (bool)checkMatch:(NSString*)matchKey matchParseState:(MatchParseState)matchParseState;
- (bool)checkMatch:(ParseType)parseType dataKey:(int)dataKey;
- (Match*)getMatch:(NSString*)matchKey;
- (Match*)getMatch:(int)dateNumber teamID1:(int)teamID1 teamID2:(int)teamID2;
- (bool)addMatch:(Match*)match;
- (void)addMatchEvents:(Match*)match;
- (NSUInteger)getMatchCount;
- (NSArray*)getModifiedMatchesInSeason:(int)season competition:(CompetitionType)competition;
- (NSArray*)getAllMatchesInSeason:(int)season competition:(CompetitionType)competition;
- (void)matchIsModified:(Match*)match;
- (bool)areMatchesModified;
// +(NSString*)checkEncoding:(NSString*)name;

#pragma mark - Match Stats
- (bool)checkMatchStats:(int)matchID;
- (bool)addMatchStats:(MatchStats*)matchStats;
- (void)loadMatchStats;


#pragma mark - Blobs
- (void)toBlobs;
- (void)fromBlobs;
- (void)clean;
- (void)encodeTeamsToString:(NSMutableString*)csvString englishTeams:(bool)englishTeams;


#pragma mark - CSV & JSON Export
- (void)exportCSVMatches;
- (void)exportCSVPlayers:(bool)test;
- (void)exportJSONGames;
- (void)exportCSVManagers:(bool)test;
- (void)exportCSVMatchStats;


#pragma mark - Footballers
- (void)loadFootballers;
- (Footballer*)checkFootballer:(int)identInt identKey:(NSString*)identKey teamID:(int)teamID season:(int)season;
- (Footballer*)checkFootballerName:(NSString*)name teamID:(int)teamID;
- (Footballer*)addFootballer:(Footballer*)footballer season:(int)season;
- (void)addFootballerComplete:(Footballer*)footballer;
- (void)setFootballersModified;
- (NSUInteger)getFootballerCount;
- (int)getFootballerID:(NSString*)footballerKey;
- (Footballer*)getFootballer:(NSString*)footballerKey season:(int)season teamID:(int)teamID;
- (Footballer*)checkFootballerWithKey:(NSString*)footballerKey season:(int)season teamID:(int)teamID parseID:(int)parseID;
- (NSArray*)getFootballersForTeam:(int)teamID;
- (Footballer*)getFootballer:(NSString*)footballerKey;
#pragma mark - Club Footballers
- (bool)setClubFootballerIndex:(ClubFootballer*)clubFootballer teamID:(int)teamID;
- (void)addClubFootballer:(ClubFootballer*)clubFootballer teamID:(int)teamID ;
- (NSArray*)getClubPlayers:(int)teamID;

#pragma mark - Managers
- (Manager*)checkManager:(int)identInt identKey:(NSString*)identKey teamID:(int)teamID season:(int)season;
- (Manager*)addManager:(Manager*)manager season:(int)season;
- (Manager*)getManager:(NSString*)managerKey;
- (NSUInteger)getManagerCount;
- (void)addManagerComplete:(Manager*)manager;
- (Manager*)checkManagerWithKey:(NSString*)footballerKey season:(int)season teamID:(int)teamID parseID:(int)parseID;

#pragma mark - Test
- (NSArray*)getFootballersTest;
@end

NS_ASSUME_NONNULL_END
