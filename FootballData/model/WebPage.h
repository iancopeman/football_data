//
//  WebPage.h
//  FootballData
//
//  Created by Ian Copeman on 16/11/2020.
//

#import <Foundation/Foundation.h>
#import "FileManager.h"

NS_ASSUME_NONNULL_BEGIN




@interface WebPage : NSObject

@property (strong, nonatomic) NSString* dataKey;
@property FileType webPageType;

-(void)setWebData:(NSString*)webData isDownloaded:(bool)isDownloaded;
- (NSString*)getDownloadedString;
- (NSString*)getWebPageString;


@end

NS_ASSUME_NONNULL_END
