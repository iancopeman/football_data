//
//  FileManager.h
//  FootballData
//
//  Created by Ian Copeman on 17/11/2020.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

#define SeasonFilePath (@"/Seasons/")
#define FootballerFilePath (@"/Footballers/")
#define ManagersFilePath (@"/Managers/")
#define CopyrightFilePath (@"/Copyright/")
#define MatchFilePath (@"/Matches/")

typedef enum {
    SeasonArchive,
    MatchPage,
    FootballerPage,
    Blob,
    WikiPage,
} FileType;

@interface FileManager : NSObject
- (NSString*)loadPersistedString:(NSString*)dataPath dataName:(NSString*)dataName dataType:(nullable NSString*)dataType dataKey:(NSString*)dataKey;
- (NSData*)loadPersistedData:(NSString*)dataPath dataName:(NSString*)dataName dataType:(nullable NSString*)dataType dataKey:(NSString*)dataKey;
- (NSString*)loadPersistedString:(NSURL*)filePath;

- (bool)savePersistedString:(NSURL*)fileURL dataString:(NSString*)dataString;
- (bool)savePersistedString:(NSString*)dataPath dataName:(NSString*)dataName dataType:(nullable NSString*)dataType dataKey:(NSString*)dataKey dataString:(NSString*)dataString;
- (bool)savePersistedData:(NSString*)dataPath dataName:(NSString*)dataName dataType:(nullable NSString*)dataType dataKey:(NSString*)dataKey blobData:(NSData*)blobData;

- (bool)doesFileExist:(NSString*)dataPath dataName:(NSString*)dataName dataType:(nullable NSString*)dataType dataKey:(NSString*)dataKey;
- (bool)doesFileExist:(NSURL*)fileURL;
- (void)deleteFile:(NSString*)dataPath dataName:(NSString*)dataName dataType:(nullable NSString*)dataType dataKey:(NSString*)dataKey;

@end

NS_ASSUME_NONNULL_END
