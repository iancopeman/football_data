//
//  WebPage.m
//  FootballData
//
//  Created by Ian Copeman on 16/11/2020.
//

#import "WebPage.h"



@implementation WebPage {
    NSString* webData;
    bool isDownloaded;
}

- (void)setWebData:(NSString*)webData isDownloaded:(bool)isDownloaded {
    self->webData = webData;
    self->isDownloaded = isDownloaded;
}

- (NSString*)getDownloadedString {
    if (!isDownloaded) return nil;
    return webData;
}
- (NSString*)getWebPageString {
    return webData;
}

@end
