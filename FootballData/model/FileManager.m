//
//  FileManager.m
//  FootballData
//
//  Created by Ian Copeman on 17/11/2020.
//

#import "FileManager.h"


@implementation FileManager {
    NSString* dataPath;

}

- (bool)doesFileExist:(NSString*)dataPath dataName:(NSString*)dataName dataType:(nullable NSString*)dataType dataKey:(NSString*)dataKey {
    NSURL* fileURL = [self getFileURL:dataPath dataName:dataName dataType:dataType dataKey:dataKey];
    return [self doesFileExist:fileURL];
}

- (bool)doesFileExist:(NSURL*)fileURL {
    NSError *error;
    if ([fileURL checkResourceIsReachableAndReturnError:&error] == NO) return NO;
    if (error != nil) {
        NSLog(@"%@", error.localizedDescription);
        return NO;
    }
    return YES;
    
}

- (NSString*)loadPersistedString:(NSURL*)fileURL {
    NSError *error;
    NSString* data = [NSString stringWithContentsOfURL:fileURL encoding:NSUTF8StringEncoding error:&error];
    if (error != nil) {
        NSLog(@"%@", error.localizedDescription);
        return nil;
    }
    return data;
    
}


- (NSString*)loadPersistedString:(NSString*)dataPath dataName:(NSString*)dataName dataType:(nullable NSString*)dataType dataKey:(NSString*)dataKey {
    NSURL* fileURL = [self getFileURL:dataPath dataName:dataName dataType:dataType dataKey:dataKey];
    NSError *error;
    NSString* data = [NSString stringWithContentsOfURL:fileURL encoding:NSUTF8StringEncoding error:&error];
    if (error != nil) {
       // NSLog(@"%@", error.localizedDescription);
        return nil;
    }
    return data;
    
}

- (NSData*)loadPersistedData:(NSString*)dataPath dataName:(NSString*)dataName dataType:(nullable NSString*)dataType dataKey:(NSString*)dataKey {
    NSURL* fileURL = [self getFileURL:dataPath dataName:dataName dataType:dataType dataKey:dataKey];
    NSData* blobData = [NSData dataWithContentsOfURL:fileURL];
    return blobData;
    
}

- (bool)savePersistedString:(NSURL*)fileURL dataString:(NSString*)dataString  {
    NSError *error;
    BOOL ok = [dataString writeToURL:fileURL atomically:YES
                            encoding:NSUTF8StringEncoding error:&error];
    return ok;
}


- (bool)savePersistedString:(NSString*)dataPath dataName:(NSString*)dataName dataType:(nullable NSString*)dataType dataKey:(NSString*)dataKey dataString:(NSString*)dataString {
    NSURL* fileURL = [self getFileURL:dataPath dataName:dataName dataType:dataType dataKey:dataKey];
    NSError *error;
    BOOL ok = [dataString writeToURL:fileURL atomically:YES
                            encoding:NSUTF8StringEncoding error:&error];
    return ok;
}

- (bool)savePersistedData:(NSString*)dataPath dataName:(NSString*)dataName dataType:(nullable NSString*)dataType dataKey:(NSString*)dataKey blobData:(NSData*)blobData {
    NSURL* fileURL = [self getFileURL:dataPath dataName:dataName dataType:dataType dataKey:dataKey];
    BOOL ok = [blobData writeToURL:fileURL atomically:YES];
    return ok;
    
}

- (NSURL*)getFileURL:(NSString*)dataPath dataName:(NSString*)dataName dataType:(nullable NSString*)dataType dataKey:(NSString*)dataKey {
    NSString* directoryPath = [dataPath stringByAppendingPathComponent:dataName];
    dataType = [dataType stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLPathAllowedCharacterSet]];
    directoryPath = [directoryPath stringByAppendingPathComponent:dataType];
    //directoryPath = [directoryPath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    NSURL* directoryURL = [NSURL URLWithString:directoryPath];
    [[NSFileManager defaultManager] createDirectoryAtURL:directoryURL withIntermediateDirectories:YES attributes:nil error:nil];
    
    //
    // Need to escape space characters
    dataKey = [dataKey stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLPathAllowedCharacterSet]];
    NSString* filePath =  [directoryPath stringByAppendingPathComponent:dataKey];
    return [NSURL URLWithString:filePath];

}

- (void)deleteFile:(NSString*)dataPath dataName:(NSString*)dataName dataType:(nullable NSString*)dataType dataKey:(NSString*)dataKey {
    NSURL* fileURL = [self getFileURL:dataPath dataName:dataName dataType:dataType dataKey:dataKey];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtURL:fileURL  error:NULL];
}


@end
