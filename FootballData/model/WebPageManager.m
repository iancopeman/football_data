//
//  WebPageManager.m
//  FootballData
//
//  Created by Ian Copeman on 05/11/2020.
//

#import "WebPageManager.h"

//
// Old files:
// /Users/Ian/Library/Containers/com.devlex.football.FootballData/Data/Library/Application\ Support/WebPages/Seasons

@implementation WebPageManager {
    FileManager* fileManager;
}

//
// Static singleton
static WebPageManager* sharedSingleton;

+ (void)initialize {
    static BOOL initialized = NO;
    if(!initialized)
    {
        initialized = YES;
        sharedSingleton = [[WebPageManager alloc] init];
        sharedSingleton->fileManager = [[FileManager alloc] init];
        
        
#if TARGET_IPHONE_SIMULATOR
#endif
    }
}

+ (WebPageManager*)getWebPageManager {
    return sharedSingleton;
}

- (NSString*)getWebPage:(NSString*)urlString {
    //urlString = [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSURL* url = [NSURL URLWithString:urlString];
    NSError* error;
    NSStringEncoding* encoding = NULL;
    NSString* webData = [NSString stringWithContentsOfURL:url usedEncoding:encoding error:&error];
    if (webData == nil) {
        urlString = [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        url = [NSURL URLWithString:urlString];
        webData = [NSString stringWithContentsOfURL:url usedEncoding:encoding error:&error];
    }
    return webData;
}











@end
