//
//  AppDelegate.m
//  FootballData
//
//  Created by Ian Copeman on 16/10/2020.
//

#import "AppDelegate.h"
#import "FootballModel.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
    

}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}


@end
