//
//  Document.h
//  FootballData
//
//  Created by Ian Copeman on 16/10/2020.
//

#import <Cocoa/Cocoa.h>

@interface Document : NSPersistentDocument
@end
