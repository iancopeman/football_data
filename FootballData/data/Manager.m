//
//  Manager.m
//  FootballData
//
//  Created by Ian Copeman on 31/03/2022.
//

#import "Manager.h"


@implementation Manager

+ (NSString*)getBlobName {
    return @"Managers";
}
+ (Manager*)managerFromCSVArray:(NSArray*)csvArray {
    Manager* manager = [[Manager alloc] init];
    [manager setFromCSVArray:csvArray];
    return manager;
}




@end
