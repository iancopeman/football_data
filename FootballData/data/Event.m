//
//  Event.m
//  FootballData
//
//  Created by Ian Copeman on 16/11/2020.
//

#import "Event.h"

@implementation Event

typedef struct  __attribute__((__packed__)) {
    UInt64 eventDetail;
    UInt32 matchID;
    UInt16 teamID;
    UInt16 locationID;
    UInt32 matchDateNumber;
    UInt8 competition;
    UInt8 season8;
    
} EventStructHeader;

typedef struct  __attribute__((__packed__)) {
    UInt16 playerID;
    UInt16 player2ID;
    UInt8 shirtNumber;
    UInt8 position;
    UInt8 eventMatchTime;
    UInt8 eventType;

} EventDetailHeader;


//
// Need to store:
// Event type - 1-8     (8)
// Time       - 1-120   (8)
// Position   - 1-11    (8)
// Shirt Num  - 1-99    (8)
// player2 ID - 1-10000 (16)
// player ID  - 1-10000 (16)


+ (NSNumber*)createEventBasic:(EventType)eventType playerID:(UInt16)playerID position:(char)position shirt:(char)shirt cleanSheet:(bool)cleanSheet resultType:(EventResultType)resultType {
    char eventTypeChar = eventType | (cleanSheet ? (1 << 4) : 0) | resultType << 5;
    unsigned long long1 = eventTypeChar << 24 | position << 8 | shirt;
    unsigned long long2 = (UInt16)playerID;
    unsigned long long event = long1 << 32 | long2;
    return [NSNumber numberWithUnsignedLongLong:event];
}


+ (NSNumber*)createEventAppearanceNumber:(UInt16)playerID position:(char)position shirt:(char)shirt cleanSheet:(bool)cleanSheet resultType:(EventResultType)resultType {
    return [self createEventBasic:EventApperance playerID:playerID position:position shirt:shirt cleanSheet:cleanSheet resultType:resultType];
}

+ (NSNumber*)createEventBenchNumber:(UInt16)playerID shirt:(char)shirt {
    unsigned long long1 = EventBench << 24;
    unsigned long long2 = (unsigned long long)playerID;
    unsigned long long event = long1 << 32 | long2;
    return [NSNumber numberWithUnsignedLongLong:event];
}

+ (NSNumber*)createEventGoalkeeperApperance:(UInt16)playerID position:(char)position shirt:(char)shirt cleanSheet:(bool)cleanSheet resultType:(EventResultType)resultType {
    return [self createEventBasic:EventGoalkeeperApperance playerID:playerID position:0 shirt:shirt cleanSheet:cleanSheet resultType:resultType];
}

+ (NSNumber*)createEventGoalNumber:(UInt16)playerID playerAssistID:(UInt16)playerAssistID time:(int)time {
    unsigned long long1 = EventGoal << 24 | time << 16;
    unsigned long long2 = (unsigned long long)playerAssistID << 16 | (UInt16)playerID;
    unsigned long long event = long1 << 32 | long2;
    return [NSNumber numberWithUnsignedLongLong:event];
}

+ (NSNumber*)createEventYellowCardNumber:(UInt16)playerID  time:(int)time {
    unsigned long long1 = EventYellowCard << 24 | time << 16;
    unsigned long long2 = (unsigned long long)playerID;
    unsigned long long event = long1 << 32 | long2;
    return [NSNumber numberWithUnsignedLongLong:event];
}

+ (NSNumber*)createEventRedCardNumber:(UInt16)playerID  time:(int)time {
    unsigned long long1 = EventRedCard << 24 | time << 16;
    unsigned long long2 = (unsigned long long)playerID;
    unsigned long long event = long1 << 32 | long2;
    return [NSNumber numberWithUnsignedLongLong:event];
}

+ (NSNumber*)createEventPenaltyNumber:(UInt16)playerID  time:(int)time {
    unsigned long long1 = EventPenalty << 24 | time << 16;
    unsigned long long2 = (unsigned long long)playerID;
    unsigned long long event = long1 << 32 | long2;
    return [NSNumber numberWithUnsignedLongLong:event];
}
+ (NSNumber*)createEventOwnGoalNumber:(UInt16)playerID  time:(int)time {
    unsigned long long1 = EventOwnGoal << 24 | time << 16;
    unsigned long long2 = (unsigned long long)playerID;
    unsigned long long event = long1 << 32 | long2;
    return [NSNumber numberWithUnsignedLongLong:event];
}
+ (NSNumber*)createEventSubNumber:(UInt16)playerIDIn playerIDOut:(UInt16)playerIDOut  time:(int)time cleanSheet:(bool)cleanSheet resultType:(EventResultType)resultType {
    char eventTypeChar = EventSub | (cleanSheet ? (1 << 4) : 0) | resultType << 5;
    unsigned long long1 = eventTypeChar << 24 | time << 16;;
    unsigned long long2 = (unsigned long long)playerIDOut << 16 | playerIDIn;
    unsigned long long event = long1 << 32 | long2;
    return [NSNumber numberWithUnsignedLongLong:event];
}
+ (bool)isGoalieApperance:(NSNumber*)eventNumber {
    unsigned long long event = [eventNumber unsignedLongLongValue];
    EventDetailHeader* eventDetail = (EventDetailHeader*)&event;
    EventType eventType = eventDetail->eventType & 0xF;
    return eventType == EventGoalkeeperApperance;
}
+ (bool)isStartingApperance:(NSNumber*)eventNumber {
    unsigned long long event = [eventNumber longLongValue];
    EventDetailHeader* eventDetail = (EventDetailHeader*)&event;
    EventType eventType = eventDetail->eventType & 0xF;
    return eventType == EventApperance;
}
+ (int)isGoalGetTime:(NSNumber*)eventNumber {
    unsigned long long event = [eventNumber longLongValue];
    EventDetailHeader* eventDetail = (EventDetailHeader*)&event;
    EventType eventType = eventDetail->eventType & 0xF;
    if (eventType == EventGoal ||
        eventType == EventPenalty ||
        eventType == EventOwnGoal) {
        return eventDetail->eventMatchTime;
    }
    return 0;
}
+ (bool)isOwnGoal:(NSNumber*)eventNumber {
    unsigned long long event = [eventNumber longLongValue];
    EventDetailHeader* eventDetail = (EventDetailHeader*)&event;
    EventType eventType = eventDetail->eventType & 0xF;
    return eventType == EventOwnGoal;
}

#pragma mark - BlobCoding
+ (NSString*)getBlobName {
    return @"Events";
}

- (void)encodeToBlob:(NSMutableData*)blobData {
    EventStructHeader header;
    header.eventDetail = self.eventDetail;
    header.matchID = self.matchID;
    header.teamID = self.teamID;
    header.locationID = self.locationID;
    header.matchDateNumber = self.matchDateNumber;
    header.competition = self.competition;
    header.season8 = self.season8;
    [blobData appendBytes:&header length:sizeof(EventStructHeader)];
}

- (int)initFromBlob:(const void*)blobData {
    EventStructHeader* header = (EventStructHeader*)blobData;
    self.eventDetail = header->eventDetail;
    self.matchID = header->matchID;
    self.teamID = header->teamID;
    self.locationID = header->locationID;
    self.matchDateNumber = header->matchDateNumber;
    self.competition = header->competition;
    self.season8 = header->season8;
    return sizeof(EventStructHeader);
}

@end
