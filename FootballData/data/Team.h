//
//  Team.h
//  FootballData
//
//  Created by Ian Copeman on 11/11/2020.
//

#import <Foundation/Foundation.h>
#import "BlobCoding.h"

NS_ASSUME_NONNULL_BEGIN

//
// Known country IDs
typedef enum {
    CountryUnknown,
    
    CountryEngland,
    CountryPortugual,
    CountrySerbia,
    CountryHungary,
    CountryBelgium, // 5
    CountryGermany,
    CountryScotland,
    CountrySweden,
    CountryPoland,
    CountryDenmark, // 10
    
    CountryFrance,
    CountryAustria,
    CountryNetherlands,
    CountrySpain,
    CountrySwitzerland, // 15
    CountryItaly,
    CountryLuxembourg,
    CountryRomania,
    CountryTurkey,
    CountrySlovakia, // 20

    CountryBulgaria,
    CountryIreland,
    CountryCzech,
    CountryCroatia,
    CountryNorthernIreland, // 25
    CountryFinland,
    CountryGreece,
    CountryNorway,
    CountryMalta,
    CountryAlbania, // 30
    
    CountryCyprus,
    CountryIceland,
    CountryRussia,
    CountryUkraine,
    CountryBosnia, //35
    CountryLatvia,
    CountryMacedonia,
    CountryWales,
    CountryBelarus,
    CountryFaroes,  // 40
    
    CountryLithuania,
    CountryMoldova,
    CountryKazakhstan,
    CountryArmenia,
    CountryGeorga, // 45
    CountryIsrael,
    CountrySlovenia,
    CountryEstonia,
    CountryAzerbaijan,
    CountryMontenegro, // 50
    
    CountrySanMarino,
    CountryAndora,
    CountryGibraltar,
    CountryLiechtenstein,
    CountryKosovo, // 55

    CountryUSA,
    CountryAustralia,
    CountryCanada,
    CountryJamaica,
    
    CountryUSSR, // 60
    CountryTrinidadAndTobago,
    CountryZimbabwe,
    CountryZambia,
    CountryNigeria,
    CountrySouthAfrica,
    CountryGhana,
    CountryUruguay,
    CountryNewZealand,
    CountryYugoslavia,
    
    CountryBrazil, // 70
    CountryBolivia,
    CountryBarbados,
    CountryColombia,
    CountryCostaRica,
    CountryGuyana,
    CountryLiberia,
    CountrySaintKittsAndNevis,
    CountryAlgeria,
    CountryArgentina,
    
    CountrySuriname, // 80
    CountryPeru,
    CountryChile,
    CountryIvoryCoast,
    CountryMorocco,
    CountryCameroon,
    CountryGuinea,
    CountryCongoDR,
    CountryMali,
    CountryParaguay,
    
    CountryHonduras, // 90
    CountryIran,
    CountrySenegal,
    CountryBermuda,
    CountryChina,
    CountryGrenada,
    CountryEcuador,
    CountryJapan,
    CountryPakistan,
    CountryTunisia,
    
    CountryEgypt,  // 100
    CountrySouthKorea,
    CountryMexico,
    CountryTogo,
    CountrySierraLeone,
    CountryCongo,
    CountryOman,
    CountryMartinique,
    CountryCuracao,
    CountryGabon,
    
    CountryAngola, // 110
    CountrySomalia,
    CountryPhilippines,
    CountryBenin,
    CountryAntiguaAndBarbuda,
    CountryCapeVerdeIslands,
    CountryBurundi,
    CountryGuineaBissau,
    CountryMontserrat,
    CountryKenya,
    
    CountryVenezuela, // 120
    CountrySaintLucia,
    CountryGambia,
    CountryEquatorialGuinea,
    CountryBurkinaFaso,
    CountryAfghanistan,
    CountryMauritania,
    CountryCuba,
    CountryGuadeloupe,
    CountryTanzania,
    
    CountryThailand, // 130
    CountrySeychelles,
    CountryStVincentAndGrenadine,
    CountrySaudiArabia,
    CountryDominica,
    CountryMalawi,
    CountryCentralAfricanRepublic,
    CountryQatar,
    CountryMozambique,
    CountryComoros,
    
    CountryHaiti, // 140
    CountryIraq,
    CountryLibya,
    CountryNamibia,
    CountryHongKong,
    CountrySudan,
    CountryMadagascar,
    CountryRwanda,
    CountryKuwait,
    CountryMauritius,
    
    CountrySriLanka, // 150
    CountryUganda,
    CountryLebanon,
    CountryIndia,
    CountrySingapore,
    CountryJersey,
    CountryGuernsey,
    CountryFrenchGuiana,
    CountryIndonesia,
    CountryJordan,
    
    CountryBVI,  // 160
    CountryReunion,
    CountryGuatemala,
    CountryCaymanIslands,
    CountrySaintMartin,
    CountryUzbekistan,
    CountryTahiti,
    CountryNorthKorea,
    CountryAruba,
    CountryMalaysia,
    
    CountryPanama, // 170

} CountryID;





//
// Team


@interface Team : NSObject<BlobCoding>
@property int index;
@property int countryID;
@property int colour;
@property int parseIndexAPI;
@property int parseUEFA;
@property int parseIndexTM;
@property NSString* name;
@property NSString* nameShort;
@property NSString* nameVShort;
@property NSString* latitudeString;
@property NSString* longitudeString;

+ (CountryID)getCountryID:(NSString*)countryString;
- (NSString*)encodeToString;
@end

NS_ASSUME_NONNULL_END
