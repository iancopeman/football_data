//
//  MatchStats.h
//  FootballData
//
//  Created by Ian Copeman on 11/06/2024.
//

#import <Foundation/Foundation.h>
#import "BlobCoding.h"

NS_ASSUME_NONNULL_BEGIN

#define MATCH_STATS_FIRST_SEASON (2023)


@interface MatchStatsTeam : NSObject
@property UInt16 teamID;
@property UInt8 shotsOn;
@property UInt8 shotsTotal;
@property UInt8 saves;
@property UInt8 fouls;
@property UInt8 corners;
@property UInt8 shotsIn;
@property UInt8 shotsOut;
@property UInt16 passes;
@property UInt16 passesGood;
@property UInt16 possession;

@end





@interface MatchStats : NSObject<BlobCoding>
@property int matchID;
@property int dateNumber;
@property (strong, nonatomic) NSString* teamName1;
@property (strong, nonatomic) NSString* teamName2;
@property (strong, nonatomic) NSNumber* seasonNumber;
@property (strong, nonatomic) MatchStatsTeam* teamStats1;
@property (strong, nonatomic) MatchStatsTeam* teamStats2;

+ (MatchStats*)initWithMatchID:(int)matchID seasonNumber:(NSNumber*)seasonNumber dateNumber:(int)dateNumber  teamName1:(NSString*)teamName1  teamName2:(NSString*)teamName2;
+ (NSString*)getStringExportHeader;
- (NSString*)encodeToString;
+ (void)loadMatchStatsFromString:(NSString*)fileString season:(int)season;
@end

NS_ASSUME_NONNULL_END
