//
//  Team.m
//  FootballData
//
//  Created by Ian Copeman on 11/11/2020.
//

#import "Team.h"

typedef struct  __attribute__((__packed__)) {
    UInt16 index;
    UInt16 size;
    UInt16 countryID;
    UInt32 colour;
    
} TeamStructHeader;

@implementation Team


+ (CountryID)getCountryID:(NSString*)countryString {
    if ([countryString compare:@"ENG"] == NSOrderedSame || [countryString compare:@"England"] == NSOrderedSame || [countryString compare:@"English"] == NSOrderedSame) return CountryEngland;
    if ([countryString compare:@"POR"] == NSOrderedSame || [countryString containsString:@"Portug"]) return CountryPortugual;
    if ([countryString compare:@"SRB"] == NSOrderedSame || [countryString containsString:@"Serbia"]) return CountrySerbia;
    if ([countryString compare:@"HUN"] == NSOrderedSame || [countryString compare:@"Hungary"] == NSOrderedSame || [countryString compare:@"Hungarian"] == NSOrderedSame) return CountryHungary;
    if ([countryString compare:@"BEL"] == NSOrderedSame || [countryString containsString:@"Belgi"]) return CountryBelgium;
    if ([countryString compare:@"GER"] == NSOrderedSame || [countryString compare:@"Germany"] == NSOrderedSame || [countryString compare:@"German"] == NSOrderedSame) return CountryGermany;
    if ([countryString compare:@"SCO"] == NSOrderedSame || [countryString compare:@"Scotland"] == NSOrderedSame || [countryString compare:@"Scottish"] == NSOrderedSame) return CountryScotland;
    if ([countryString compare:@"SWE"] == NSOrderedSame || [countryString containsString:@"Swede"]) return CountrySweden;
    if ([countryString compare:@"POL"] == NSOrderedSame || [countryString compare:@"Poland"] == NSOrderedSame || [countryString compare:@"Polish"] == NSOrderedSame) return CountryPoland;
    if ([countryString compare:@"DEN"] == NSOrderedSame || [countryString compare:@"Denmark"] == NSOrderedSame || [countryString compare:@"Danish"] == NSOrderedSame) return CountryDenmark;
    if ([countryString compare:@"FRA"] == NSOrderedSame || [countryString compare:@"France"] == NSOrderedSame || [countryString compare:@"French"] == NSOrderedSame) return CountryFrance;

    if ([countryString compare:@"AUT"] == NSOrderedSame || [countryString containsString:@"Austria"]) return CountryAustria;
    if ([countryString compare:@"NED"] == NSOrderedSame || [countryString compare:@"Netherlands"] == NSOrderedSame || [countryString compare:@"Dutch"] == NSOrderedSame) return CountryNetherlands;
    if ([countryString compare:@"ESP"] == NSOrderedSame || [countryString compare:@"Spain"] == NSOrderedSame || [countryString compare:@"Spanish"] == NSOrderedSame) return CountrySpain;
    if ([countryString compare:@"SUI"] == NSOrderedSame || [countryString compare:@"Switzerland"] == NSOrderedSame || [countryString compare:@"Swiss"] == NSOrderedSame) return CountrySwitzerland;
    if ([countryString compare:@"ITA"] == NSOrderedSame || [countryString containsString:@"Ital"] ) return CountryItaly;
    if ([countryString compare:@"LUX"] == NSOrderedSame || [countryString compare:@"Luxembourg"] == NSOrderedSame) return CountryLuxembourg;
    if ([countryString compare:@"ROU"] == NSOrderedSame || [countryString containsString:@"Romania"]) return CountryRomania;
    if ([countryString compare:@"TUR"] == NSOrderedSame || [countryString compare:@"Turkey"] == NSOrderedSame || [countryString compare:@"Turkish"] == NSOrderedSame || [countryString compare:@"Türkiye"] == NSOrderedSame) return CountryTurkey;
    if ([countryString compare:@"SVK"] == NSOrderedSame || [countryString containsString:@"Slovak"]) return CountrySlovakia;
    if ([countryString compare:@"BUL"] == NSOrderedSame || [countryString compare:@"Bulgaria"] == NSOrderedSame || [countryString compare:@"Bulgarian"] == NSOrderedSame) return CountryBulgaria;

    if ([countryString compare:@"CZE"] == NSOrderedSame || [countryString compare:@"Czech_Republic"] == NSOrderedSame || [countryString compare:@"Czech"] == NSOrderedSame || [countryString containsString:@"Czech Republic"] || [countryString containsString:@"Czechia"]) return CountryCzech;
    if ([countryString compare:@"CRO"] == NSOrderedSame || [countryString containsString:@"Croatia"]) return CountryCroatia;
    if ([countryString compare:@"NIR"] == NSOrderedSame || [countryString compare:@"Northern_Ireland"] == NSOrderedSame || [countryString compare:@"Northern Irish"] == NSOrderedSame || [countryString compare:@"Northern Ireland"] == NSOrderedSame) return CountryNorthernIreland;
    if ([countryString compare:@"IRL"] == NSOrderedSame || [countryString compare:@"Republic_of_Ireland"] == NSOrderedSame || [countryString compare:@"Irish"] == NSOrderedSame || [countryString compare:@"Ireland"] == NSOrderedSame) return CountryIreland;
   if ([countryString compare:@"FIN"] == NSOrderedSame || [countryString compare:@"Finland"] == NSOrderedSame || [countryString compare:@"Finnish"] == NSOrderedSame) return CountryFinland;
    if ([countryString compare:@"GRE"] == NSOrderedSame || [countryString containsString:@"Gree"]) return CountryGreece;
    if ([countryString compare:@"NOR"] == NSOrderedSame || [countryString compare:@"Norway"] == NSOrderedSame || [countryString compare:@"Norwegian"] == NSOrderedSame) return CountryNorway;
    if ([countryString compare:@"MLT"] == NSOrderedSame || [countryString compare:@"Malta"] == NSOrderedSame || [countryString containsString:@"Maltese"]) return CountryMalta;
    if ([countryString compare:@"ALB"] == NSOrderedSame || [countryString containsString:@"Albania"]) return CountryAlbania;
    if ([countryString compare:@"CYP"] == NSOrderedSame || [countryString containsString:@"Cypr"]) return CountryCyprus;

    if ([countryString compare:@"ISL"] == NSOrderedSame || [countryString compare:@"Iceland"] == NSOrderedSame || [countryString compare:@"Icelandic"] == NSOrderedSame) return CountryIceland;
    if ([countryString compare:@"RUS"] == NSOrderedSame || [countryString containsString:@"Russia"]) return CountryRussia;
    if ([countryString compare:@"UKR"] == NSOrderedSame || [countryString containsString:@"Ukrain"]) return CountryUkraine;
    if ([countryString compare:@"BIH"] == NSOrderedSame || [countryString compare:@"Bosnia_and_Herzegovina"] == NSOrderedSame || [countryString containsString:@"Bosnian"] || [countryString containsString:@"Bosnia-Herzegovina"]) return CountryBosnia;
    if ([countryString compare:@"LVA"] == NSOrderedSame || [countryString containsString:@"Latvia"]) return CountryLatvia;
    if ([countryString compare:@"MKD"] == NSOrderedSame || [countryString compare:@"FYR_Macedonia"] == NSOrderedSame || [countryString containsString:@"Macedonia"]) return CountryMacedonia;
    if ([countryString compare:@"WAL"] == NSOrderedSame || [countryString compare:@"Wales"] == NSOrderedSame || [countryString compare:@"Welsh"] == NSOrderedSame) return CountryWales;
    if ([countryString compare:@"BLR"] == NSOrderedSame || [countryString containsString:@"Belarus"]) return CountryBelarus;
    if ([countryString compare:@"FRO"] == NSOrderedSame || [countryString compare:@"Faroe_Islands"] == NSOrderedSame || [countryString compare:@"Faroe Islands"] == NSOrderedSame) return CountryFaroes;
    if ([countryString compare:@"LTU"] == NSOrderedSame || [countryString containsString:@"Lithuania"]) return CountryLithuania;

    if ([countryString compare:@"MDA"] == NSOrderedSame || [countryString containsString:@"Moldova"]) return CountryMoldova;
    if ([countryString compare:@"KAZ"] == NSOrderedSame || [countryString containsString:@"Kazakhstan"]) return CountryKazakhstan;
    if ([countryString compare:@"ARM"] == NSOrderedSame || [countryString containsString:@"Armenia"]) return CountryArmenia;
    if ([countryString compare:@"GEO"] == NSOrderedSame || [countryString containsString:@"Georgia"]) return CountryGeorga;
    if ([countryString compare:@"ISR"] == NSOrderedSame || [countryString containsString:@"Israel"]) return CountryIsrael;
    if ([countryString compare:@"SVN"] == NSOrderedSame || [countryString containsString:@"Sloven"]) return CountrySlovenia;
    if ([countryString compare:@"EST"] == NSOrderedSame || [countryString containsString:@"Estonia"]) return CountryEstonia;
    if ([countryString compare:@"AZE"] == NSOrderedSame || [countryString containsString:@"Azerbaijan"]) return CountryAzerbaijan;
    if ([countryString compare:@"MNE"] == NSOrderedSame || [countryString containsString:@"Montenegr"]) return CountryMontenegro;
    if ([countryString compare:@"SMR"] == NSOrderedSame || [countryString compare:@"San_Marino"] == NSOrderedSame) return CountrySanMarino;

    if ([countryString compare:@"AND"] == NSOrderedSame || [countryString containsString:@"Andora"]) return CountryAndora;
    if ([countryString compare:@"GIB"] == NSOrderedSame || [countryString containsString:@"Gibralta"]) return CountryGibraltar;
    if ([countryString compare:@"LIE"] == NSOrderedSame || [countryString containsString:@"Liechtenstein"]) return CountryLiechtenstein;
    if ([countryString compare:@"KOS"] == NSOrderedSame || [countryString containsString:@"Kosov"]) return CountryKosovo;
    if ([countryString containsString:@"Jersey"]) return CountryJersey;
    if ([countryString containsString:@"Guernsey"]) return CountryJersey;
    
    //
    // Non europe
    if ([countryString compare:@"USA"] == NSOrderedSame || [countryString compare:@"American"] == NSOrderedSame || [countryString containsString:@"United States"]) return CountryUSA;
    if ([countryString containsString:@"Australia"]) return CountryAustralia;
    if ([countryString containsString:@"Canad"]) return CountryCanada;
    if ([countryString containsString:@"Jamaica"]) return CountryJamaica;
    if ([countryString compare:@"USSR"] == NSOrderedSame) return CountryUSSR;
    if ([countryString compare:@"Trinidad_And_Tobago"] == NSOrderedSame || [countryString containsString:@"Trinidadian"] || [countryString compare:@"Trinidad and Tobago"] == NSOrderedSame) return CountryTrinidadAndTobago;
    if ([countryString containsString:@"Zimbabwe"]) return CountryZimbabwe;
    if ([countryString containsString:@"Zambia"]) return CountryZambia;
    if ([countryString containsString:@"Nigeria"]) return CountryNigeria;
    
    if ([countryString compare:@"South_Africa"] == NSOrderedSame || [countryString containsString:@"South Africa"]) return CountrySouthAfrica;
    if ([countryString containsString:@"Ghan"]) return CountryGhana;
    if ([countryString containsString:@"Urugua"]) return CountryUruguay;
    if ([countryString compare:@"New_Zealand"] == NSOrderedSame || [countryString compare:@"New Zealand"] == NSOrderedSame) return CountryNewZealand;
    if ([countryString compare:@"Yugoslavia"] == NSOrderedSame || [countryString containsString:@"Jugoslawien"]) return CountryYugoslavia;
    if ([countryString containsString:@"Brazil"]) return CountryBrazil;
    if ([countryString containsString:@"Bolivia"]) return CountryBolivia;
    if ([countryString containsString:@"Barbad"]) return CountryBarbados;
    if ([countryString containsString:@"Colombia"]) return CountryColombia;

    if ([countryString compare:@"Costa_Rica"] == NSOrderedSame || [countryString compare:@"Costa Rica"] == NSOrderedSame) return CountryCostaRica;
    if ([countryString containsString:@"Guyan"]) return CountryGuyana;
    if ([countryString containsString:@"Liberia"]) return CountryLiberia;
    if ([countryString compare:@"Saint_Kitts_And_Nevis"] == NSOrderedSame || [countryString containsString:@"Kittian"] || [countryString containsString:@"St. Kitts & Nevis"]) return CountrySaintKittsAndNevis;
    if ([countryString containsString:@"Algeria"]) return CountryAlgeria;
    if ([countryString containsString:@"Argentin"]) return CountryArgentina;
    if ([countryString containsString:@"Suriname"] ) return CountrySuriname;
    if ([countryString containsString:@"Peru"]) return CountryPeru;
    if ([countryString containsString:@"Chile"]) return CountryChile;
    if ([countryString compare:@"Ivory_Coast"] == NSOrderedSame || [countryString containsString:@"Ivorian"] || [countryString containsString:@"Cote d'Ivoire"] || [countryString containsString:@"Côte d'Ivoire"]) return CountryIvoryCoast;
    if ([countryString containsString:@"Morocc"]) return CountryMorocco;
    if ([countryString containsString:@"Cameroon"]) return CountryCameroon;
    if ([countryString containsString:@"Guinea"]) return CountryGuinea;
    if ([countryString compare:@"Congo_DR"] == NSOrderedSame || [countryString compare:@"Congolese (COD)"] == NSOrderedSame) return CountryCongoDR;
    
    if ([countryString containsString:@"Mali"]) return CountryMali;
    if ([countryString containsString:@"Paraguay"]) return CountryParaguay;
    if ([countryString containsString:@"Hondura"]) return CountryHonduras;
    if ([countryString containsString:@"Iran"]) return CountryIran;
    if ([countryString containsString:@"Senegal"]) return CountrySenegal;
    if ([countryString containsString:@"Bermuda"]) return CountryBermuda;
    
    if ([countryString containsString:@"Chin"]) return CountryChina;
    if ([countryString containsString:@"Grenada"]) return CountryGrenada;
    if ([countryString containsString:@"Ecuador"]) return CountryEcuador;
    if ([countryString containsString:@"Japan"]) return CountryJapan;
    if ([countryString containsString:@"Pakistan"]) return CountryPakistan;
    if ([countryString containsString:@"Tunisia"]) return CountryTunisia;
    if ([countryString containsString:@"Egypt"]) return CountryEgypt;
    if ([countryString containsString:@"Korea_South"] || [countryString containsString:@"Korean (KOR)"] || [countryString containsString:@"Korea, South"]) return CountrySouthKorea;
    if ([countryString containsString:@"Mexic"]) return CountryMexico;
    if ([countryString containsString:@"Togo"]) return CountryTogo;
    if ([countryString containsString:@"Sierra_Leone"] || [countryString containsString:@"Sierra Leone"]) return CountrySierraLeone;
    if ([countryString containsString:@"Congo"]) return CountryCongo;
    if ([countryString containsString:@"Oman"]) return CountryOman;
    if ([countryString containsString:@"Martinique"]) return CountryMartinique;
    if ([countryString containsString:@"Curacao"]) return CountryCuracao;
    if ([countryString containsString:@"Gabon"]) return CountryGabon;
    if ([countryString containsString:@"Angola"] ) return CountryAngola;
    if ([countryString containsString:@"Somali"]) return CountrySomalia;
    if ([countryString containsString:@"Philippines"] || [countryString containsString:@"Filipino"]) return CountryPhilippines;
    if ([countryString containsString:@"Benin"]) return CountryBenin;
    
    if ([countryString containsString:@"Antigua_and_Barbuda"] || [countryString containsString:@"Antigua"]) return CountryAntiguaAndBarbuda;
    if ([countryString containsString:@"Cape_Verde_Islands"] || [countryString containsString:@"Cape Verde"]) return CountryCapeVerdeIslands;
    if ([countryString containsString:@"Burundi"]) return CountryBurundi;
    if ([countryString containsString:@"Guinea-Bissau"]) return CountryGuineaBissau;
    if ([countryString containsString:@"Montserrat"]) return CountryMontserrat;
    if ([countryString containsString:@"Kenya"]) return CountryKenya;
    if ([countryString containsString:@"Venezuela"]) return CountryVenezuela;
    if ([countryString containsString:@"Saint_Lucia"] || [countryString containsString:@"St. Lucia"]) return CountrySaintLucia;
    if ([countryString containsString:@"Gambia"]) return CountryGambia;
    if ([countryString containsString:@"Equatorial_Guinea"]) return CountryEquatorialGuinea;
    if ([countryString containsString:@"Burkina_Faso"] || [countryString containsString:@"Burkinabe"] || [countryString containsString:@"Burkina Faso"]) return CountryBurkinaFaso;
    if ([countryString containsString:@"Afghanistan"]) return CountryAfghanistan;
    if ([countryString containsString:@"Mauritania"]) return CountryMauritania;
    if ([countryString containsString:@"Cuba"]) return CountryCuba;
    if ([countryString containsString:@"Guadeloupe"]) return CountryGuadeloupe;
    if ([countryString containsString:@"Tanzania"]) return CountryTanzania;
    if ([countryString containsString:@"Thai"]) return CountryThailand;
    
    if ([countryString containsString:@"Seychell"]) return CountrySeychelles;
    if ([countryString containsString:@"Saint_Vincent_And_Grenadine"] || [countryString containsString:@"St. Vincent"]) return CountryStVincentAndGrenadine;
    if ([countryString containsString:@"Saudi_Arabia"] || [countryString containsString:@"Saudi"]) return CountrySaudiArabia;
    if ([countryString containsString:@"Dominica"]) return CountryDominica;
    if ([countryString containsString:@"Malawi"]) return CountryMalawi;
    if ([countryString containsString:@"Central_African_Republic"] || [countryString containsString:@"Central African"]) return CountryCentralAfricanRepublic;
    if ([countryString containsString:@"Qatar"]) return CountryQatar;
    if ([countryString containsString:@"Mozambi"]) return CountryMozambique;
    if ([countryString containsString:@"Comoros"]) return CountryComoros;
    if ([countryString containsString:@"Haiti"]) return CountryHaiti;
    if ([countryString containsString:@"Iraq"]) return CountryIraq;
    if ([countryString containsString:@"Libya"]) return CountryLibya;
    if ([countryString containsString:@"Namibia"] ) return CountryNamibia;
    if ([countryString containsString:@"Hong_Kong"] ) return CountryHongKong;
    if ([countryString containsString:@"Sudan"]) return CountrySudan;
    if ([countryString containsString:@"Madagascar"] || [countryString containsString:@"Malagasy"]) return CountryMadagascar;
    if ([countryString containsString:@"Rwanda"]) return CountryRwanda;
    if ([countryString containsString:@"Kuwait"]) return CountryKuwait;
    if ([countryString containsString:@"Mauriti"] ) return CountryMauritius;
    if ([countryString containsString:@"Sri_Lanka"] || [countryString containsString:@"Sri Lanka"]) return CountrySriLanka;
    if ([countryString containsString:@"Uganda"]) return CountryUganda;
    if ([countryString containsString:@"Leban"]) return CountryLebanon;
    if ([countryString containsString:@"India"]) return CountryIndia;
    if ([countryString containsString:@"Singapore"]) return CountrySingapore;
    if ([countryString containsString:@"French Guiana"]) return CountryFrenchGuiana;
    if ([countryString containsString:@"Indonesia"]) return CountryFrenchGuiana;
    if ([countryString containsString:@"Jordan"]) return CountryJordan;
    if ([countryString containsString:@"British Virgin"]) return CountryBVI;
    if ([countryString containsString:@"Réunion"]) return CountryReunion;
    if ([countryString containsString:@"Guatemala"]) return CountryGuatemala;
    if ([countryString containsString:@"Cayman Islands"]) return CountryCaymanIslands;
    if ([countryString containsString:@"Saint-Martin"]) return CountrySaintMartin;
    if ([countryString containsString:@"Uzbekistan"]) return CountryUzbekistan;
    if ([countryString containsString:@"Tahiti"]) return CountryTahiti;
    if ([countryString containsString:@"Korea, North"]) return CountryNorthKorea;
    if ([countryString containsString:@"Aruba"]) return CountryAruba;
    if ([countryString containsString:@"Malaysia"]) return CountryMalaysia;
    if ([countryString containsString:@"Panama"]) return CountryPanama;

    
    static NSMutableArray* missingTeamList;
    if (missingTeamList == nil) {
        missingTeamList = [NSMutableArray arrayWithCapacity:10];
        [missingTeamList addObject:countryString];
        NSLog(@"Unknown Country string: %@", countryString);
    }
    else {
        if (![missingTeamList containsObject:countryString]) {
            [missingTeamList addObject:countryString];
            NSLog(@"Unknown Country string: %@", countryString);
        }
    }
    
    return CountryUnknown;
}

#pragma mark - BlobCoding
+ (NSString*)getBlobName {
    return @"Clubs";
}

- (void)encodeToBlob:(NSMutableData*)blobData {
    static char buffer[200];
    TeamStructHeader* header = (TeamStructHeader*)buffer;
    header->index = self.index;
    header->countryID = self.countryID;
    header->colour = self.colour;
    header->size = sizeof(TeamStructHeader);
    
    //
    // String sizes
    const char* cstrArray[] = {[self.name UTF8String],  [self.nameShort UTF8String],
        [self.nameVShort UTF8String],
    };
    int sizeArray = sizeof(cstrArray) / sizeof(cstrArray[0]);
    for (int i = 0; i < sizeArray; i++) {
        const char* cstr = cstrArray[i];
        if (cstr == nil) {
            header->size += 1;
        }
        else {
            header->size += strlen(cstr) + 1;
        }
    }
    [blobData appendBytes:header length:sizeof(TeamStructHeader)];
    
    //
    // String values
    for (int i = 0; i < sizeArray; i++) {
        const char* cstr = cstrArray[i];
        static char zeroByte = 0;
        if (cstr == nil) {
            [blobData appendBytes:&zeroByte length:1];
        }
        else {
            [blobData appendBytes:cstr length:strlen(cstr) + 1];
        }
    }
}

- (int)initFromBlob:(const void*)blobData {
    TeamStructHeader* header = (TeamStructHeader*)blobData;
    self.index = header->index;
    self.countryID = header->countryID;
    self.colour = header->colour;
    char* bufferPointer = (char*)blobData + sizeof(TeamStructHeader);
    self.name = [NSString stringWithUTF8String:bufferPointer];
    bufferPointer = bufferPointer + strlen(bufferPointer) + 1;
    self.nameShort = [NSString stringWithUTF8String:bufferPointer];
    bufferPointer = bufferPointer + strlen(bufferPointer) + 1;
    self.nameVShort = [NSString stringWithUTF8String:bufferPointer];

    return header->size;
}

- (NSString*)encodeToString {
    NSString* stringEncode = [NSString stringWithFormat:
                              @"%@,%d,%@,%@,%d,%d,%d,%@,%@,%X,%d\n",
                              self.name,
                              self.index,
                              self.nameShort,
                              self.nameVShort,
                              self.parseUEFA,
                              self.parseIndexAPI,
                              self.parseIndexTM,
                              self.latitudeString,
                              self.longitudeString,
                              self.colour,
                              self.countryID];
    return stringEncode;
}


@end
