//
//  BlobCoding.h
//  FootballData
//
//  Created by Ian Copeman on 17/11/2020.
//

#ifndef BlobCoding_h
#define BlobCoding_h

@protocol BlobCoding

- (void)encodeToBlob:(NSMutableData*)blobData;
- (int)initFromBlob:(const void*)blobData;
+ (NSString*)getBlobName;

@end



#endif /* BlobCoding_h */
