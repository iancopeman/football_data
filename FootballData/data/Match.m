//
//  Match.m
//  FootballData
//
//  Created by Ian Copeman on 11/11/2020.
//

#import "Match.h"
#import "FootballModel.h"

#define SEASON_ADJUST_VALUE (1792) // It's the year cast to a byte. Add 1792 to the value



@implementation Match {
    NSString* manager;
    NSString* managerOpposition;
    NSString* referee;
    NSDate* matchDate;
    NSString* dateStringFormatted;
    int goalMask;
    int team1ID;
    int team2ID;
    int scoreTeam1;
    int scoreTeam2;
    int penaltiesTeam1;
    int penaltiesTeam2;
    Manager* manager1;
    Manager* manager2;
    NSArray* teamFootballers1;
    NSArray* teamFootballers2;

}

typedef struct  __attribute__((__packed__)) {
    UInt32 index;
    UInt16 locationID;
    UInt16 homeTeamID;
    UInt16 team1ID;
    UInt16 team2ID;
    UInt8 state;
    UInt8 competition;
    UInt8 competitionFlag;
    UInt8 scoreTeam1;
    UInt8 scoreTeam2;
    UInt8 penaltiesTeam1;
    UInt8 penaltiesTeam2;
    UInt8 season8; // 8 bit season number. 1992-93 would be 1992
    UInt16 managerID1;
    UInt16 managerID2;
    UInt16 goalMask;
    UInt32 attendance;
    UInt32 dateNumber;
    UInt32 timeNumber;
} MatchStructHeader;

+ (NSString*)makeMatchKey:(NSString*)matchDateString team1:(int)team1 team2:(int)team2 {
    //
    // Check for missing date
    if ([matchDateString compare:@"NA"] == NSOrderedSame) return nil;
    
    //
    // Convert sting date
    matchDateString = [matchDateString stringByReplacingOccurrencesOfString:@"/" withString:@"."];
    
    //
    // Generate a unique key
    static NSDateFormatter *dateFormatter = nil;
    if (dateFormatter == nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd.MM.yyyy"];
    }
    NSDate* matchDate = [dateFormatter dateFromString:matchDateString];
    if (matchDate == nil) {
        // ESD & UEFA Date
        static NSDateFormatter *dateFormatter2 = nil;
        dateFormatter2 = [[NSDateFormatter alloc] init];
        [dateFormatter2 setDateFormat:@"yyyy-MM-dd"];
        matchDate = [dateFormatter2 dateFromString:matchDateString];
    }
    if (matchDate == nil) {
        NSLog(@"Can't pass date string: %@", matchDateString);
        return nil;
    }
    
    //
    // Make date number
    return [self makeMatchKeyFromDate:matchDate team1:team1 team2:team2];
}

+ (NSString*)makeMatchKeyFromDate:(NSDate*)matchDate team1:(int)team1 team2:(int)team2 {
    //
    // Make date number
    //
    // Set match year
    NSCalendar* gregorianCalandar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalandar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:matchDate];
    int dateNumber = (int)([components year] * 10000 + [components month] * 100 + [components day]);
    
    return [self makeMatchKeyFromDateNumber:dateNumber team1:team1 team2:team2];
}

+ (NSString*)makeMatchKeyFromDateNumber:(int)dateNumber team1:(int)team1 team2:(int)team2 {
    //
    // Ensure teams in ascending order
    bool switchTeams = team1 > team2;
    NSString* matchKey = [NSString stringWithFormat:@"%d_%d_%d", dateNumber, switchTeams ? team2 : team1, switchTeams ? team1 : team2];
    
    return matchKey;

}

- (void)setMatchTeamsAndDateString:(NSString*)matchDateString team1:(int)team1 team2:(int)team2 {
    //
    // Convert sting date
    matchDateString = [matchDateString stringByReplacingOccurrencesOfString:@"/" withString:@"."];
    
    //
    // Generate a unique key
    static NSDateFormatter *dateFormatter = nil;
    if (dateFormatter == nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd.MM.yyyy"];
    }
    matchDate = [dateFormatter dateFromString:matchDateString];
    if (matchDate == nil) {
        // ESD Date
        static NSDateFormatter *dateFormatter2 = nil;
        dateFormatter2 = [[NSDateFormatter alloc] init];
        [dateFormatter2 setDateFormat:@"yyyy-MM-dd"];
        matchDate = [dateFormatter2 dateFromString:matchDateString];
    }
    if (matchDate == nil) {
        NSLog(@"Can't pass date string: %@", matchDateString);
        return;
    }
    
    [self setMatchTeamsAndDate:matchDate team1:team1 team2:team2];
}

- (void)setMatchTeamsAndDate:(NSDate*)matchDate team1:(int)team1 team2:(int)team2 {
    //
    // Set match year
    NSCalendar* gregorianCalandar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalandar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:matchDate];
    
    int dateNumber = (int)(self.matchYear * 10000 + [components month] * 100 + [components day]);
    [self setMatchTeamsAndDateNumber:dateNumber team1:team1 team2:team2];
    
    //
    // Ensure teams in ascending order
    bool switchTeams = team1 > team2;
    self.matchKey = [NSString stringWithFormat:@"%d_%d_%d", self.dateNumber, switchTeams ? team2 : team1, switchTeams ? team1 : team2];
    
    //
    // Set state
    team1ID = team1;
    team2ID = team2;
    self.matchParseState = MatchParseStateTeams;
    
}

- (void)setMatchTeamsAndDateNumber:(int)dateNumber team1:(int)team1 team2:(int)team2 {
    //
    // Ensure teams in ascending order
    self.dateNumber = dateNumber;
    bool switchTeams = team1 > team2;
    self.matchKey = [NSString stringWithFormat:@"%d_%d_%d", self.dateNumber, switchTeams ? team2 : team1, switchTeams ? team1 : team2];
    
    //
    // Set state
    team1ID = team1;
    team2ID = team2;
    self.matchParseState = MatchParseStateTeams;
    
}


- (void)addAttribute:(NSString*)name value:(NSString*)value {
    if (name == nil || value == nil) return;
    
    if ([name localizedCaseInsensitiveContainsString:@"Opposition manager"]) {
        managerOpposition = value;
        return ;
    }
    if ([name localizedCaseInsensitiveContainsString:@"Manager"]) {
        manager = value;
        return ;
    }
    if ([name localizedCaseInsensitiveContainsString:@"Attendance"]) {
        value = [value stringByReplacingOccurrencesOfString:@"," withString:@""];
        self.attendance = [value intValue];
        return ;
    }
    if ([name localizedCaseInsensitiveContainsString:@"Referee"]) {
        referee = value;
        return ;
    }
    
}

- (void)addMatchEvents:(NSArray*)teamEvents teamID:(int)teamID hasAssists:(bool)hasAssists {
    if (teamEvents.count == 0) return;
    if (teamID == team1ID) self.team1Events = teamEvents;
    if (teamID == team2ID) self.team2Events = teamEvents;
    if (self.team1Events.count != 0 && self.team2Events.count != 0 ) {
        //
        // Check for 11 players and a goalie
        bool gotGoalie1 = NO;
        int startCount1 = 0;
        for (NSNumber* eventNumber in self.team1Events) {
            if ([Event isGoalieApperance:eventNumber]) gotGoalie1 = YES;
            if ([Event isStartingApperance:eventNumber]) startCount1++;
        }
        bool gotGoalie2 = NO;
        int startCount2 = 0;
        for (NSNumber* eventNumber in self.team2Events) {
            if ([Event isGoalieApperance:eventNumber]) gotGoalie2 = YES;
            if ([Event isStartingApperance:eventNumber]) startCount2++;
        }
        
        if (gotGoalie1 && startCount1 >= 10 && gotGoalie2 && startCount2 >= 10) {
            self.matchParseState = hasAssists ? MatchParseStateEventsAssists : MatchParseStateEventsFull;
        }
        else if (self.matchParseState == MatchParseStateEventsFull || self.matchParseState == MatchParseStateEventsAssists){
            self.matchParseState = MatchParseStateEventsPartial;
        }
    }
    else {
        self.matchParseState = MatchParseStateEventsPartial;
    }
}


- (void)setSeason:(int)season {
    self.seasonNumber = [NSNumber numberWithInt:season];
    self.season8 = (char)season;
}

- (bool)hasEvents {
    return self.team1Events != nil && self.team2Events != nil;
}

- (int)getTeam1ID {
    return team1ID;
}
- (int)getTeam2ID {
    return team2ID;
}

- (void)setScore:(int)score teamID:(int)teamID {
    if (teamID == team1ID) scoreTeam1 = score;
    if (teamID == team2ID) scoreTeam2 = score;
}

- (void)setPenalties:(int)score teamID:(int)teamID {
    if (teamID == team1ID) penaltiesTeam1 = score;
    if (teamID == team2ID) penaltiesTeam2 = score;
}

- (void)setFootballers:(NSArray*)footballers teamID:(int)teamID {
    if (teamID == team1ID) teamFootballers1 = footballers;
    if (teamID == team2ID) teamFootballers2 = footballers;
    
    //
    // ToDo: Get team name
    // Add team name to footballer
}

- (NSArray*)getFootballers:(int)teamID {
    if (teamID == team1ID) return teamFootballers1;
    if (teamID == team2ID) return teamFootballers2;
    return nil;
}

- (void)setManager:(Manager*)manager teamID:(int)teamID {
    if (manager == nil) return;
    if (teamID == team1ID) manager1 = manager;
    if (teamID == team2ID) manager2 = manager;
}


- (void)setReferee:(NSString*)refereeString {
    if ([refereeString class] == [NSNull class]) return;
    if (refereeString.length == 0) return;
    if ([refereeString compare:self.refereeString] == NSOrderedSame) return;
    self.refereeString = refereeString;
    self.modified = YES;

}

- (void)setLocation:(NSString*)locationName {
    if (locationName.length == 0) return;
    if ([locationName compare:self.locationString] == NSOrderedSame) return;
    
    if ([locationName containsString:@"\""]) {
        locationName = [locationName stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    }

    //
    // Set location string for exporting
    self.locationString = locationName;
    self.modified = YES;
    
    static FootballModel* model = nil;
    if (model == nil) {
        model = [FootballModel getModel];
    }
    self.locationID = [model addLocation:locationName];

}

- (void)setGoalMask:(int)_goalMask {
    goalMask = _goalMask;
}

- (bool)goalMaskCorrect {
    //
    // GoalMask is a bit mask if the order of goals scored. Set for Team1, Zero for Team2
    if (scoreTeam1 == 0 && scoreTeam2 == 0) {
        return goalMask == 0;
    }
    else if (scoreTeam1 == 0 && scoreTeam2 != 0) {
        return goalMask == 0;
    }
    
    //
    // Check that goals scored matches mask
    NSMutableDictionary* scoreDictionary = [NSMutableDictionary dictionaryWithCapacity:10];
    for (NSNumber* eventNumber in self.team1Events) {
        int goalTime = [Event isGoalGetTime:eventNumber];
        if (goalTime != 0) {
            NSNumber* goalTimeNumber = @(goalTime);
            if ([scoreDictionary objectForKey:goalTimeNumber] != nil) {
                return true; // Multiple goals at same time
            }
            NSNumber* value = [Event isOwnGoal:eventNumber] ? @(false) : @(true);
            [scoreDictionary setObject:value forKey:goalTimeNumber];
        }
    }
    for (NSNumber* eventNumber in self.team2Events) {
        int goalTime = [Event isGoalGetTime:eventNumber];
        if (goalTime != 0) {
            NSNumber* goalTimeNumber = @(goalTime);
            if ([scoreDictionary objectForKey:goalTimeNumber] != nil) {
                return true; // Multiple goals at same time
            }
            NSNumber* value = [Event isOwnGoal:eventNumber] ? @(true) : @(false);
            [scoreDictionary setObject:value forKey:goalTimeNumber];
        }
    }
    
    //
    // Check for poor data
    if (scoreDictionary.count < scoreTeam1 + scoreTeam2) {
        return true;
    }
    
    //
    // Order dictionary
    int goalCount = 0;
    int calculatedGoalMask = 0;
    NSArray* orderedArray = [[scoreDictionary allKeys] sortedArrayUsingSelector:@selector(compare:)];
    for (NSNumber* timeNumber in orderedArray) {
        bool isTeam1 = [[scoreDictionary objectForKey:timeNumber] boolValue];
        if (isTeam1) {
            calculatedGoalMask |= 1 << goalCount;
        }
        goalCount++;
    }
    if (goalMask != calculatedGoalMask) {
        return false;
    }
    return goalMask == calculatedGoalMask;
}



#pragma mark - BlobCoding
+ (NSString*)getBlobName {
    return @"Matches";
}

- (void)encodeToBlob:(NSMutableData*)blobData {
    MatchStructHeader header;
    header.index = self.index;
    header.locationID = self.locationID;
    header.homeTeamID = self.homeTeamID;
    header.team1ID = team1ID;
    header.team2ID = team2ID;
    header.state = self.matchState;
    header.competition = self.competition;
    header.competitionFlag = self.competitionFlag;
    header.scoreTeam1 = scoreTeam1;
    header.scoreTeam2 = scoreTeam2;
    header.penaltiesTeam1 = penaltiesTeam1;
    header.penaltiesTeam2 = penaltiesTeam2;
    header.managerID1 = manager1.index;
    header.managerID2 = manager2.index;
    header.attendance = self.attendance;
    header.season8 = self.season8;
    header.goalMask = goalMask;
    header.dateNumber = self.dateNumber;
    header.timeNumber = self.timeNumber;
    [blobData appendBytes:&header length:sizeof(MatchStructHeader)];
    
    int season = self.season8 + SEASON_ADJUST_VALUE;
    if (team1ID == 213 && team2ID == 548 && self.season8 == (char)2005) {
        int ian = 10;
    }

    /*
    UInt8 test = (char)2022;
    int t1 = test + SEASON_ADJUST_VALUE;
    if (self.index == 181839) {
        int ina = 10;
    }
     */
}

- (int)initFromBlob:(const void*)blobData {
    MatchStructHeader* header = (MatchStructHeader*)blobData;
    self.index = header->index;
    self.locationID = header->locationID;
    self.homeTeamID = header->homeTeamID;
    team1ID = header->team1ID;
    team2ID = header->team2ID;
    self.matchState = header->state;
    self.competition = header->competition;
    self.competitionFlag = header->competitionFlag;
    scoreTeam1 = header->scoreTeam1;
    scoreTeam2 = header->scoreTeam2;
    penaltiesTeam1 = header->penaltiesTeam1;
    penaltiesTeam2 = header->penaltiesTeam2;
    int managerID1 = header->managerID1;
    int managerID2 = header->managerID2;
    self.attendance = header->attendance;
    self.season8 = header->season8;
    goalMask = header->goalMask;
    self.seasonNumber = [NSNumber numberWithInt:(self.season8 + SEASON_ADJUST_VALUE)];
    self.dateNumber = header->dateNumber;
    self.timeNumber = header->timeNumber;
    self.matchKey = [NSString stringWithFormat:@"%d_%d_%d", self.dateNumber, team1ID, team2ID];

    return sizeof(MatchStructHeader);
}

+ (NSString*)getStringExportHeader {
    return @"DateString,Team1,Score,Team2,C Flag,Venue,Penalties,M Flag\n";
}


- (NSString*)encodeToString {
    //
    // Don't encode adjustment matches
    if (self.matchState == MatchStateAdjustment) return nil;
    
    //
    // ToDo: currently lose venue string
    static FootballModel* model = nil;
    if (model == nil) {
        model = [FootballModel getModel];
    }
    bool switchTeams = self.homeTeamID == team2ID;
    NSString* team1String = switchTeams ? [model getTeamName:team2ID] : [model getTeamName:team1ID];
    NSString* team2String = switchTeams ? [model getTeamName:team1ID] : [model getTeamName:team2ID];
    int score1 = switchTeams ? scoreTeam2 : scoreTeam1;
    int score2 = switchTeams ? scoreTeam1 : scoreTeam2;
    int penalties1 = switchTeams ? penaltiesTeam2 : penaltiesTeam1;
    int penalties2 = switchTeams ? penaltiesTeam1 : penaltiesTeam2;
    NSString* scoreString = @"";
    if (self.matchState == MatchStateFutureMatch && self.timeNumber != 0) {
        scoreString = [NSString stringWithFormat:@"%d:%d", TIME_NUMBER_TO_HOUR(self.timeNumber), TIME_NUMBER_TO_MINUTE(self.timeNumber)];
    }
    else {
        scoreString = [NSString stringWithFormat:@"%d-%d", score1, score2];
    }
    bool encodeLocation = [self.locationString containsString:@","];
    if (self.locationString.length != 0) {
        int ian = 10;
    }
    NSString* location = self.locationString == nil ? @"" : encodeLocation ? [NSString stringWithFormat:@"\"%@\"", self.locationString] : self.locationString;
    NSString* penalties = penalties1 + penalties2 == 0 ? @"" : [NSString stringWithFormat:@"%d-%d", penalties1, penalties2];
    NSString* stringEncode = [NSString stringWithFormat:
                                  @"%d,%@,%@,%@,%ld,%@,%@,%ld\n",
                              self.dateNumber,
                              team1String,
                              scoreString,
                              team2String,
                              (long)self.competitionFlag,
                              location,
                              penalties,
                              (long)self.matchFlag];
    return stringEncode;
}

- (NSString*)getFileName {
    static FootballModel* model = nil;
    if (model == nil) {
        model = [FootballModel getModel];
    }
    NSString* team1String = [model getTeamName:team1ID];
    NSString* team2String = [model getTeamName:team2ID];
    bool switchTeams = [team1String compare:team2String] == NSOrderedDescending;
    if (team1ID != 0 && team2ID == 0) {
        NSString* matchFileName = [NSString stringWithFormat:@"%d_%@.json", self.dateNumber, team1String];
        return matchFileName;
    }
    else {
        NSString* matchFileName = [NSString stringWithFormat:@"%d_%@_%@.json", self.dateNumber, switchTeams ? team2String : team1String, switchTeams ? team1String : team2String];
        return matchFileName;
    }
}

- (NSString*)encodeToJSONString:(bool)fullMatchOnly basicMatch:(bool)basicMatch {
    @try {
        return [self _encodeToJSONString:fullMatchOnly basicMatch:basicMatch];
    }
    
    @catch ( NSException *e ) {
        [self _encodeToJSONString:fullMatchOnly basicMatch:basicMatch];
        return nil;
    }
}

- (NSString*)_encodeToJSONString:(bool)fullMatchOnly basicMatch:(bool)basicMatch {
    if (self.matchState != MatchStateAdjustment) {
        if (fullMatchOnly) {
            //
            // only encode if have events
            if (self.matchParseState < MatchParseStateEventsFull) return nil;
        }
        else {
            if (!basicMatch) {
                if (self.matchParseState < MatchParseStateEventsPartial) return nil;
            }
        }
    }
    
    static FootballModel* model = nil;
    if (model == nil) {
        model = [FootballModel getModel];
    }
    NSString* teamString1 = [model getTeamName:team1ID];
    NSString* teamString2 = [model getTeamName:team2ID];
    int score1 = scoreTeam1;
    int score2 = scoreTeam2;

    //
    // set home team as team1, team2 or unknown 0
    int homeTeamFlag = 0;
    if (self.homeTeamID == team1ID) homeTeamFlag = 1;
    if (self.homeTeamID == team2ID) homeTeamFlag = 2;

    
    //
    // Create array of player keys
    NSMutableArray* team1 = [NSMutableArray arrayWithCapacity:teamFootballers1.count];
    NSMutableArray* team2 = [NSMutableArray arrayWithCapacity:teamFootballers2.count];
    for (Footballer* footballer in teamFootballers1) {
        [team1 addObject:[footballer generateKey]];
    }
    for (Footballer* footballer in teamFootballers2) {
        [team2 addObject:[footballer generateKey]];
    }
        
    //
    // Create array of player ids
    NSMutableArray* teamID1 = [NSMutableArray arrayWithCapacity:teamFootballers1.count];
    NSMutableArray* teamID2 = [NSMutableArray arrayWithCapacity:teamFootballers2.count];
    for (Footballer* footballer in teamFootballers1) {
        [teamID1 addObject:[NSNumber numberWithUnsignedShort:footballer.index]];
    }
    for (Footballer* footballer in teamFootballers2) {
        [teamID2 addObject:[NSNumber numberWithUnsignedShort:footballer.index]];
    }
    NSArray* events1 = self.team1Events;
    NSArray* events2 = self.team2Events;
    
    //
    // Get manager keys
    NSString* managerKey1 = [manager1 generateKey];
    NSString* managerKey2 = [manager2 generateKey];
    
    //
    // ToDo:
    // Add events, exploded with footballers keys as an array of dictionaries??
    NSMutableArray* eventArray1 = [NSMutableArray arrayWithCapacity:events1.count];
    {
        for (NSNumber* eventNumber in events1) {
            unsigned long long event = [eventNumber unsignedLongLongValue];
            EventHeader* eventHeader = (EventHeader*)&event;
            NSString* player1Key = nil;
            NSString* player2Key = nil;
            if (eventHeader->playerID != 0) {
                for (int i = 0; i < teamID1.count; i++) {
                    if ([teamID1[i] intValue] == eventHeader->playerID) {
                        player1Key = team1[i];
                        break;
                    }
                }
            }
            if (eventHeader->player2ID != 0) {
                for (int i = 0; i < teamID1.count; i++) {
                    if ([teamID1[i] intValue] == eventHeader->player2ID) {
                        player2Key = team1[i];
                        break;
                    }
                }
            }
            NSDictionary* eventDictionary = @{
                @"eventType" : @(eventHeader->eventType),
                @"player1ID" : player1Key == nil ? [NSNull null] : player1Key,
                @"player2ID" : player2Key == nil ? [NSNull null] : player2Key,
                @"shirtNumber" : @(eventHeader->shirtNumber),
                @"position" : @(eventHeader->position),
                @"eventMatchTime" : @(eventHeader->eventMatchTime),
            };
            [eventArray1 addObject:eventDictionary];
        }
    }
    NSMutableArray* eventArray2 = [NSMutableArray arrayWithCapacity:events2.count];
    {
        for (NSNumber* eventNumber in events2) {
            unsigned long long event = [eventNumber unsignedLongLongValue];
            EventHeader* eventHeader = (EventHeader*)&event;
            NSString* player1Key = nil;
            NSString* player2Key = nil;
            if (eventHeader->playerID != 0) {
                for (int i = 0; i < teamID2.count; i++) {
                    if ([teamID2[i] intValue] == eventHeader->playerID) {
                        player1Key = team2[i];
                        break;
                    }
                }
            }
            if (eventHeader->player2ID != 0) {
                for (int i = 0; i < teamID2.count; i++) {
                    if ([teamID2[i] intValue] == eventHeader->player2ID) {
                        player2Key = team2[i];
                        break;
                    }
                }
            }
            NSDictionary* eventDictionary = @{
                @"eventType" : @(eventHeader->eventType),
                @"player1ID" : player1Key == nil ? [NSNull null] : player1Key,
                @"player2ID" : player2Key == nil ? [NSNull null] : player2Key,
                @"shirtNumber" : @(eventHeader->shirtNumber),
                @"position" : @(eventHeader->position),
                @"eventMatchTime" : @(eventHeader->eventMatchTime),
            };
            [eventArray2 addObject:eventDictionary];
        }
    }


    NSDictionary* matchDictionary = @{
        @"dateNumber"  : @(self.dateNumber),
        @"timeNumber"  : @(self.timeNumber),
        @"matchState"  : @(self.matchState),
        @"homeTeamFlag"  : @(homeTeamFlag),
        @"teamString1"  : teamString1,
        @"teamString2"  : teamString2 == nil ? [NSNull null] : teamString2,
        @"score1"  : @(score1),
        @"score2"  : @(score2),
        @"goalMask"  : @(goalMask),
        @"competition"  : @(self.competition),
        @"competitionFlag"  : @(self.competitionFlag),
        @"attendance"  : @(self.attendance),
        @"referee"  : self.refereeString == nil ? [NSNull null] : self.refereeString,
        @"season"  : self.seasonNumber == nil ? [NSNull null] : self.seasonNumber,
        @"location"  : self.locationString == nil ? [NSNull null] : self.locationString,
        @"penaltiesTeam1"  : @(penaltiesTeam1),
        @"penaltiesTeam2"  : @(penaltiesTeam2),
        @"managerKey1"  : managerKey1 == nil ? [NSNull null] : managerKey1,
        @"managerKey2"  : managerKey2 == nil ? [NSNull null] : managerKey2,
        @"team1" : team1,
        @"team2" : team2,
        @"teamEvents1" : eventArray1 == nil || eventArray1.count == 0 ? [NSNull null] : eventArray1,
        @"teamEvents2" : eventArray1 == nil || eventArray2.count == 0 ? [NSNull null] : eventArray2,


    };
    NSError *error=nil;
    NSData *jsonData=[NSJSONSerialization dataWithJSONObject:matchDictionary options:NSJSONWritingPrettyPrinted error:&error];
    NSString* stringEncode =  [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
     return stringEncode;

}

- (void)initFromJSONString:(const NSString*)jsonString {
    NSError *error;
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary* matchDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
    if (matchDictionary == nil) {
        NSLog(@"Failed to load JSON string: %@", error.description);
        return;
    }
    static FootballModel* model = nil;
    if (model == nil) {
        model = [FootballModel getModel];
    }
    
    // Basic properties
    self.attendance = [[matchDictionary objectForKey:@"attendance"] intValue];
    id jsonValue = [matchDictionary objectForKey:@"referee"];
    if (jsonValue != nil && [jsonValue isKindOfClass:[NSString class]]) {
        self.refereeString = jsonValue;
    }
    jsonValue = [matchDictionary objectForKey:@"location"];
    if (jsonValue != nil && [jsonValue isKindOfClass:[NSString class]]) {
        self.location = jsonValue;
    }
    int season = [[matchDictionary objectForKey:@"season"] intValue];
    self.season = season;
    self.dateNumber = [[matchDictionary objectForKey:@"dateNumber"] intValue];
    self.timeNumber = [[matchDictionary objectForKey:@"timeNumber"] intValue];
    self.matchYear = DATE_NUMBER_TO_YEAR(self.dateNumber);
    self.competition = [[matchDictionary objectForKey:@"competition"] intValue];
    self.competitionFlag = [[matchDictionary objectForKey:@"competitionFlag"] intValue];
    scoreTeam1 = [[matchDictionary objectForKey:@"score1"] intValue];
    scoreTeam2 = [[matchDictionary objectForKey:@"score2"] intValue];
    goalMask = [[matchDictionary objectForKey:@"goalMask"] intValue];
    if (scoreTeam1 != 0 && goalMask == 0) {
        int ian = 10;
    }
    penaltiesTeam1 = [[matchDictionary objectForKey:@"penaltiesTeam1"] intValue];
    penaltiesTeam2 = [[matchDictionary objectForKey:@"penaltiesTeam2"] intValue];
    self.matchState = [[matchDictionary objectForKey:@"matchState"] intValue];
    jsonValue = [matchDictionary objectForKey:@"managerKey1"];
    if (jsonValue != nil && [jsonValue isKindOfClass:[NSString class]]) {
        NSString* managerKey1 = jsonValue;
        manager1 = [model getManager:managerKey1];
    }
    jsonValue = [matchDictionary objectForKey:@"managerKey2"];
    if (jsonValue != nil && [jsonValue isKindOfClass:[NSString class]]) {
        NSString* managerKey2 = jsonValue;
        manager2 = [model getManager:managerKey2];
    }

    if (penaltiesTeam1 == 6 && penaltiesTeam2 == 2) {
        int ian = 10;
    }
    
    if (self.dateNumber == 19961228) {
        int ian = 10;
    }

    
    // Teams
    NSString* teamString1 = [matchDictionary objectForKey:@"teamString1"];
    team1ID = [model addTeam:teamString1];
    if (self.matchState != MatchStateAdjustment) {
        NSString* teamString2 = [matchDictionary objectForKey:@"teamString2"];
        team2ID = [model addTeam:teamString2];
    }
    int homeTeamFlag = [[matchDictionary objectForKey:@"homeTeamFlag"] intValue];
    if (homeTeamFlag == 1) self.homeTeamID = team1ID;
    else if (homeTeamFlag == 2) self.homeTeamID = team2ID;
    if (team1ID == 669 && _dateNumber == 20210826) {
        int ian = 10;
    }

    
    // Players
    NSArray* teamPlayers1 = [matchDictionary objectForKey:@"team1"];
    NSMutableArray* teamFootballers1 = [NSMutableArray arrayWithCapacity:teamPlayers1.count];
    for (int i = 0; i < teamPlayers1.count; i++) {
        Footballer* footballer = [model getFootballer:teamPlayers1[i] season:season teamID:team1ID];
        if (footballer == nil)  {
            Footballer* footballer = [model getFootballer:teamPlayers1[i] season:season teamID:team1ID];
            raise(SIGSTOP);
            return;
        }
        teamFootballers1[i] = footballer;
    }
    [self setFootballers:teamFootballers1 teamID:team1ID];
    
    NSArray* teamPlayers2 = [matchDictionary objectForKey:@"team2"];
    NSMutableArray* teamFootballers2 = [NSMutableArray arrayWithCapacity:teamPlayers2.count];
    for (int i = 0; i < teamPlayers2.count; i++) {
        Footballer* footballer = [model getFootballer:teamPlayers2[i] season:season teamID:team2ID];
        if (footballer == nil)  {
            raise(SIGSTOP);
        }
        teamFootballers2[i] = footballer;
    }
    [self setFootballers:teamFootballers2 teamID:team2ID];

    // Events
    jsonValue = [matchDictionary objectForKey:@"teamEvents1"];
    if (jsonValue != nil && [jsonValue isKindOfClass:[NSArray class]]) {
        NSArray* teamEvents1 = jsonValue;
        NSMutableArray* eventArray1 = [NSMutableArray arrayWithCapacity:teamEvents1.count];
        for (NSDictionary* eventDictionary in teamEvents1) {
            unsigned long long event = 0;
            EventHeader* eventHeader = (EventHeader*)&event;
            eventHeader->eventType = [[eventDictionary objectForKey:@"eventType"] charValue];
            id player1ID = [eventDictionary objectForKey:@"player1ID"];
            if (player1ID != nil && [player1ID  isKindOfClass:[NSString class]]) {
                for (int i = 0; i < teamPlayers1.count; i++) {
                    if ([teamPlayers1[i] compare:player1ID] == NSOrderedSame) {
                        eventHeader->playerID = ((Footballer*)teamFootballers1[i]).index;
                        break;
                    }
                }
            }
            id player2ID = [eventDictionary objectForKey:@"player2ID"];
            if (player2ID != nil && [player2ID  isKindOfClass:[NSString class]]) {
                for (int i = 0; i < teamPlayers1.count; i++) {
                    if ([teamPlayers1[i] compare:player2ID] == NSOrderedSame) {
                        eventHeader->player2ID = ((Footballer*)teamFootballers1[i]).index;
                        break;
                    }
                }
            }
            eventHeader->shirtNumber = [[eventDictionary objectForKey:@"shirtNumber"] charValue];
            eventHeader->position = [[eventDictionary objectForKey:@"position"] charValue];
            eventHeader->eventMatchTime = [[eventDictionary objectForKey:@"eventMatchTime"] charValue];
            
            [eventArray1 addObject:[NSNumber numberWithUnsignedLongLong:event]];
        }
        [self addMatchEvents:eventArray1 teamID:team1ID hasAssists:self.matchParseState == MatchParseStateEventsAssists];
    }
    
    jsonValue = [matchDictionary objectForKey:@"teamEvents2"];
    if (jsonValue != nil && [jsonValue isKindOfClass:[NSArray class]]) {
        NSArray* teamEvents2 = jsonValue;
        NSMutableArray* eventArray2 = [NSMutableArray arrayWithCapacity:teamEvents2.count];
        for (NSDictionary* eventDictionary in teamEvents2) {
            unsigned long long event = 0;
            EventHeader* eventHeader = (EventHeader*)&event;
            eventHeader->eventType = [[eventDictionary objectForKey:@"eventType"] charValue];
            id player1ID = [eventDictionary objectForKey:@"player1ID"];
            if (player1ID != nil && [player1ID  isKindOfClass:[NSString class]]) {
                for (int i = 0; i < teamPlayers2.count; i++) {
                    if ([teamPlayers2[i] compare:player1ID] == NSOrderedSame) {
                        eventHeader->playerID = ((Footballer*)teamFootballers2[i]).index;
                        break;
                    }
                }
            }
            id player2ID = [eventDictionary objectForKey:@"player2ID"];
            if (player2ID != nil && [player2ID  isKindOfClass:[NSString class]]) {
                for (int i = 0; i < teamPlayers2.count; i++) {
                    if ([teamPlayers2[i] compare:player2ID] == NSOrderedSame) {
                        eventHeader->player2ID = ((Footballer*)teamFootballers2[i]).index;
                        break;
                    }
                }
            }
            eventHeader->shirtNumber = [[eventDictionary objectForKey:@"shirtNumber"] charValue];
            eventHeader->position = [[eventDictionary objectForKey:@"position"] charValue];
            eventHeader->eventMatchTime = [[eventDictionary objectForKey:@"eventMatchTime"] charValue];
            
            [eventArray2 addObject:[NSNumber numberWithUnsignedLongLong:event]];
        }
        [self addMatchEvents:eventArray2 teamID:team2ID hasAssists:self.matchParseState == MatchParseStateEventsAssists];
    }

    bool switchTeams = team1ID > team2ID;
    self.matchKey = [NSString stringWithFormat:@"%d_%d_%d", self.dateNumber, switchTeams ? team2ID : team1ID, switchTeams ? team1ID : team2ID];
    
    //
    // Set match state
    if (self.matchState == 0) {
        self.matchState = MatchStateResult;
    }
  
    if (team2ID == 669 && _dateNumber == 19921007) {
        int ian =  self.matchParseState;
        int ina = 10;
    }

    //
    // Only add match and its events if not already added with at least partial events
    if ([model addMatch:self]) {
        [model addMatchEvents:self];
    }
    
    //
    // Mark as not modified
    self.modified = false;


    
}

+ (NSString*)getCompetitionShortName:(CompetitionType)competition {
    if (competition == CompetitionFACup) return @"FAC";
    if (competition == CompetitionLeagueCup) return @"LC";
    if (competition == CompetitionCharityShield) return @"CS";

    if (competition == CompetitionPremierLeague) return @"PL";
    if (competition == CompetitionChampionship) return @"Ch";
    if (competition == CompetitionLeagueOne) return @"L1";
    if (competition == CompetitionLeagueTwo) return @"L2";
    
    if (competition == CompetitionFirstDivision) return @"1D";
    if (competition == CompetitionSecondDivision) return @"2D";
    if (competition == CompetitionThirdDivision) return @"3D";
    if (competition == CompetitionFourthDivision) return @"4D";
    
    if (competition == CompetitionThirdDivisionSouth) return @"3DS";
    if (competition == CompetitionThirdDivisionNorth) return @"3DN";
    
    if (competition == CompetitionNationalLeague) return @"NL";

    if (competition == CompetitionEuropeanCup) return @"EC";
    if (competition == CompetitionEuropaLeague) return @"EL";
    if (competition == CompetitionEuropaConferenceLeague) return @"ECL";
    if (competition == CompetitionUEFACup) return @"UEFA";
    if (competition == CompetitionEuropeanCupWinnersCup) return @"ECWC";
    if (competition == CompetitionInterTotoCup) return @"IT";
    if (competition == CompetitionEuropeanSuperCup) return @"ESC";
    if (competition == CompetitionClubWorldCup) return @"CWC";
    if (competition == CompetitionIntercontinentalCup) return @"IC";
    if (competition == CompetitionCLQ) return @"CLQ";
    if (competition == CompetitionUefaQ) return @"UCQ";
    if (competition == CompetitionEuropaLQ) return @"ELQ";

    return nil;
}
+ (CompetitionType)getCompetitionFromShortName:(NSString*)name {
    if ([name containsString:@"FAC"]) return CompetitionFACup;
    if ([name containsString:@"LC"]) return CompetitionLeagueCup;
    if ([name containsString:@"CS"]) return CompetitionCharityShield;

    if ([name containsString:@"PL"]) return CompetitionPremierLeague;
    if ([name containsString:@"Ch"]) return CompetitionChampionship;
    if ([name containsString:@"L1"]) return CompetitionLeagueOne;
    if ([name containsString:@"L2"]) return CompetitionLeagueTwo;
    
    if ([name containsString:@"1D"]) return CompetitionFirstDivision;
    if ([name containsString:@"2D"]) return CompetitionSecondDivision;
    if ([name containsString:@"3D"]) return CompetitionThirdDivision;
    if ([name containsString:@"4D"]) return CompetitionFourthDivision;
    
    if ([name containsString:@"3DS"]) return CompetitionThirdDivisionSouth;
    if ([name containsString:@"3DN"]) return CompetitionThirdDivisionNorth;
    
    if ([name containsString:@"ECL"]) return CompetitionEuropaConferenceLeague;
    if ([name containsString:@"ECWC"]) return CompetitionEuropeanCupWinnersCup;
    if ([name containsString:@"EC"]) return CompetitionEuropeanCup;
    if ([name containsString:@"EL"]) return CompetitionEuropaLeague;
    if ([name containsString:@"UEFA"]) return CompetitionUEFACup;
    if ([name containsString:@"ESC"]) return CompetitionEuropeanSuperCup;
    if ([name containsString:@"IT"]) return CompetitionInterTotoCup;
    if ([name containsString:@"ITC"]) return CompetitionInterTotoCup;

 
    if ([name containsString:@"CWC"]) return CompetitionClubWorldCup;
    if ([name containsString:@"IC"]) return CompetitionIntercontinentalCup;
    
    return CompetitionUnknown;
}

+ (CompetitionType)isNeutralVenue:(CompetitionType)competition competitionFlag:(NSInteger)competitionFlag {
    if (competitionFlag == COMPETITION_FINAL_FLAG) return true;
    if (competition == CompetitionFACup && competitionFlag == COMPETITION_FINAL_FLAG) return true;
    return false;
}


@end
