//
//  Location.h
//  FootballData
//
//  Created by Ian Copeman on 17/11/2020.
//

#import "Team.h"

NS_ASSUME_NONNULL_BEGIN

@interface Location : Team

@end

NS_ASSUME_NONNULL_END
