//
//  ClubFootballer.m
//  FootballData
//
//  Created by Ian Copeman on 18/11/2021.
//

#import "ClubFootballer.h"

//
// ClubFootballer
typedef struct  __attribute__((__packed__)) {
    UInt16 index;
    UInt16 size;
    UInt16 apperancesLeague;
    UInt16 apperancesCup1;
    UInt16 apperancesCup2;
    UInt16 apperancesOther;
    UInt16 goalsLeague;
    UInt16 goalsCup1;
    UInt16 goalsCup2;
    UInt16 goalsOther;
    UInt16 startYear;
    UInt16 endYear;
    UInt8 squadPlayer;
    UInt8 hallOfFame;

    
} ClubFootballerStructHeader;


@implementation ClubFootballer



+ (ClubFootballer*)clubFootballerFromCSVArray:(NSArray*)csvArray {
    ClubFootballer* footballer = [[ClubFootballer alloc] init];
    [footballer setFromCSVArray:csvArray];
    return footballer;
}

- (void)setFromCSVArray:(NSArray*)csvArray {
    int index = 0;
    self.dictionaryKey = csvArray[index++];
    
    self.apperancesLeague = [csvArray[index++] intValue];
    self.apperancesCup1 = [csvArray[index++] intValue];
    self.apperancesCup2 = [csvArray[index++] intValue];
    self.apperancesOther = [csvArray[index++] intValue];
    
    self.goalsLeague = [csvArray[index++] intValue];
    self.goalsCup1 = [csvArray[index++] intValue];
    self.goalsCup2 = [csvArray[index++] intValue];
    self.goalsOther = [csvArray[index++] intValue];
    
    if (csvArray.count > index) {
        self.squadPlayer = [csvArray[index++] intValue];
        self.startYear = [csvArray[index++] intValue];
        self.endYear = [csvArray[index++] intValue];
        self.hallOfFame = [csvArray[index++] intValue];
    }
    if (csvArray.count > index) {
        self.biography = csvArray[index++];
    }

}

+ (NSString*)getStringExportHeader {
    return @"Key,AppL,AppC1,AppC2,AppO,GoalL,GoalC1,GoalC2,GoalO,Squad,Start,End,HF,Bio\n";
}
- (NSString*)encodeClubFootballerToString {
    
    NSString* stringEncode = [NSString stringWithFormat:
                                  @"%@,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%@\n",
                              
                              self.dictionaryKey,

                              self.apperancesLeague,
                              self.apperancesCup1,
                              self.apperancesCup2, // 10
                              self.apperancesOther,

                              self.goalsLeague,
                              self.goalsCup1,
                              self.goalsCup2,
                              self.goalsOther, // 15
                              
                              self.squadPlayer,
                              self.startYear,
                              self.endYear,
                              self.hallOfFame,
                              
                              self.biography.length == 0 ? @"" : self.biography
    ];
    return stringEncode;
}

- (void)encodeClubFootballerToBlob:(NSMutableData*)blobData {
    static ClubFootballerStructHeader header;
    header.index = self.index;
    header.apperancesLeague = self.apperancesLeague;
    header.apperancesCup1 = self.apperancesCup1;
    header.apperancesCup2 = self.apperancesCup2;
    header.apperancesOther = self.apperancesOther;
    header.goalsLeague = self.goalsLeague;
    header.goalsCup1 = self.goalsCup1;
    header.goalsCup2 = self.goalsCup2;
    header.goalsOther = self.goalsOther;
    header.startYear = self.startYear;
    header.endYear = self.endYear;
    header.squadPlayer = self.squadPlayer;
    header.hallOfFame = self.hallOfFame;
    header.size = sizeof(ClubFootballerStructHeader);
    
    //
    // String sizes
    const char* cstrArray[] = { [self.biography UTF8String]
    };
    int sizeArray = sizeof(cstrArray) / sizeof(cstrArray[0]);
    for (int i = 0; i < sizeArray; i++) {
        const char* cstr = cstrArray[i];
        if (cstr == nil) {
            header.size += 1;
        }
        else {
            header.size += strlen(cstr) + 1;
        }
    }
    [blobData appendBytes:&header length:sizeof(ClubFootballerStructHeader)];
    
    //
    // String values
    for (int i = 0; i < sizeArray; i++) {
        const char* cstr = cstrArray[i];
        static char zeroByte = 0;
        if (cstr == nil) {
            [blobData appendBytes:&zeroByte length:1];
        }
        else {
            [blobData appendBytes:cstr length:strlen(cstr) + 1];
        }
    }
}


@end
