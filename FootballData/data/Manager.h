//
//  Manager.h
//  FootballData
//
//  Created by Ian Copeman on 31/03/2022.
//

#import "Footballer.h"

NS_ASSUME_NONNULL_BEGIN

@interface Manager : Footballer
+ (Manager*)managerFromCSVArray:(NSArray*)csvArray;
@end

NS_ASSUME_NONNULL_END
