//
//  Match.h
//  FootballData
//
//  Created by Ian Copeman on 11/11/2020.
//

#import <Foundation/Foundation.h>
#import "Competition.h"
#import "BlobCoding.h"
#import "Manager.h"

//
// Flag denoting a round of a cup like competition
typedef enum {
    COMPETITION_FINAL_FLAG = 20, // Start at 20, so we can support numerical rounds too
    COMPETITION_SEMI_FINAL_FLAG,
    COMPETITION_QUARTER_FINAL_FLAG,
    COMPETITION_R_16_FLAG,
    COMPETITION_GroupA_FLAG,
    COMPETITION_GroupB_FLAG,
    COMPETITION_GroupC_FLAG,
    COMPETITION_GroupD_FLAG,
    COMPETITION_GroupE_FLAG,
    COMPETITION_GroupF_FLAG,
    COMPETITION_GroupG_FLAG, // 30
    COMPETITION_GroupH_FLAG,
    COMPETITION_PRELIM_FLAG,
    COMPETITION_Q_1_FLAG,
    COMPETITION_Q_2_FLAG,
    COMPETITION_Q_3_FLAG,
    COMPETITION_Q_PO_FLAG,
    COMPETITION_1_GroupA_FLAG, // 1st Group stage
    COMPETITION_1_GroupB_FLAG,
    COMPETITION_1_GroupC_FLAG,
    COMPETITION_1_GroupD_FLAG, //40
    COMPETITION_1_GroupE_FLAG,
    COMPETITION_1_GroupF_FLAG,
    
    COMPETITION_2_GroupA_FLAG, // 2nd Group stage
    COMPETITION_2_GroupB_FLAG,
    COMPETITION_2_GroupC_FLAG,
    COMPETITION_2_GroupD_FLAG,
    
    COMPETITION_GroupI_FLAG,
    COMPETITION_GroupJ_FLAG,
    COMPETITION_GroupK_FLAG,
    COMPETITION_GroupL_FLAG, // 50

    COMPETITION_League_FLAG,
    COMPETITION_PlayOff_FLAG,

    
} CompetitionRounds;

typedef enum {
    MatchFlagUnknown,
    MatchFlagDecidedCoinToss,
    MatchFlagDecidedAwayGoals,
    MatchFlagDecidedWalkover,
} MatchFlags;



NS_ASSUME_NONNULL_BEGIN

typedef enum {
    MatchParseStateUnknown,
    MatchParseStateTeams,
    MatchParseStateEventsPartial,
    MatchParseStateEventsFull,
    MatchParseStateEventsAssists,
} MatchParseState;

typedef enum {
    MatchStateUnknown,
    MatchStateResult,
    MatchStateFutureMatch,
    MatchStatePostponed,
    MatchStateAdjustment,
} MatchStateType;

typedef struct  __attribute__((__packed__)) {
    UInt16 playerID;
    UInt16 player2ID;
    UInt8 shirtNumber;
    UInt8 position;
    UInt8 eventMatchTime;
    UInt8 eventType;
    
} EventHeader;





@interface Match : NSObject<BlobCoding>
@property int index;
@property int attendance;
@property int dateNumber;
@property int timeNumber;
//@property NSString* dateString;
@property int locationID;
@property int homeTeamID;
@property (strong, nonatomic) NSNumber* seasonNumber;
@property char season8;
@property NSInteger matchYear;
@property CompetitionType competition;
@property NSInteger competitionFlag;
@property NSInteger matchFlag;
@property int parseUEFA;
@property MatchParseState matchParseState;
@property MatchStateType matchState;
@property bool modified;

@property (strong, nonatomic) NSURL* fileURL;
@property (strong, nonatomic) NSString* locationString;
@property (strong, nonatomic) NSString* refereeString;
@property (strong, nonatomic) NSString* extraString;
@property (strong, nonatomic) NSString* matchKey;
@property (strong, nonatomic) NSArray* team1Events;
@property (strong, nonatomic) NSArray* team2Events;

+ (NSString*)makeMatchKey:(NSString*)matchDateString team1:(int)team1 team2:(int)team2;
+ (NSString*)makeMatchKeyFromDate:(NSDate*)matchDate team1:(int)team1 team2:(int)team2;
+ (NSString*)makeMatchKeyFromDateNumber:(int)dateNumber team1:(int)team1 team2:(int)team2;

- (void)setMatchTeamsAndDateString:(NSString*)matchDateString team1:(int)team1ID team2:(int)team2ID;
- (void)setMatchTeamsAndDate:(NSDate*)matchDate team1:(int)team1 team2:(int)team2;
- (void)setMatchTeamsAndDateNumber:(int)dateNumber team1:(int)team1 team2:(int)team2;
- (void)addAttribute:(NSString*)name value:(NSString*)value;
- (void)addMatchEvents:(NSArray*)teamEvents teamID:(int)teamID hasAssists:(bool)hasAssists;
- (bool)hasEvents;
- (void)setSeason:(int)season;
- (int)getTeam1ID;
- (int)getTeam2ID;
- (void)setScore:(int)score teamID:(int)teamID;
- (void)setFootballers:(NSArray*)footballers teamID:(int)teamID;
- (NSArray*)getFootballers:(int)teamID;
- (void)setPenalties:(int)score teamID:(int)teamID;
- (void)setManager:(Manager*)manager teamID:(int)teamID;
- (void)setLocation:(NSString*)locationString;
- (void)setGoalMask:(int)goalMask;
- (void)setReferee:(NSString*)refereeString;
+ (NSString*)getStringExportHeader;
- (NSString*)encodeToString;
- (NSString*)encodeToJSONString:(bool)fullMatchOnly basicMatch:(bool)basicMatch;
- (void)initFromJSONString:(const NSString*)jsonString;
- (NSString*)getFileName;
+ (NSString*)getCompetitionShortName:(CompetitionType)competition;
+ (CompetitionType)getCompetitionFromShortName:(NSString*)name;
+ (CompetitionType)isNeutralVenue:(CompetitionType)competition competitionFlag:(NSInteger)competitionFlag;

- (bool)goalMaskCorrect;
@end

NS_ASSUME_NONNULL_END
