//
//  Footballer.h
//  FootballData
//
//  Created by Ian Copeman on 09/11/2020.
//

#import <Foundation/Foundation.h>
#import "BlobCoding.h"
#import "Team.h"

NS_ASSUME_NONNULL_BEGIN



@interface Footballer : NSObject<BlobCoding>

@property UInt16 index;
@property (strong, nonatomic) NSString* name;
@property (strong, nonatomic) NSString* nameNice;
@property (strong, nonatomic) NSString* nameFull;
@property (strong, nonatomic) NSString* imageURL;
@property (strong, nonatomic) NSString* imageAttributation;
@property (strong, nonatomic) NSString* dictionaryKey;
@property (strong, nonatomic) NSString* birthPlace;

@property int teamID;
@property int role;
@property int licenseType;
@property int parseIndexLFC;
@property int parseIndex11v11;
@property int parseIndexFWP;
@property int parseIndexTM;
@property int parseIndexAPIFootballer;

@property CountryID countryID;
@property int birthDateNumber;
@property int deathDateNumber;

- (NSString*)generateKey;
+ (NSString*)generateKey:(int)birthDateNumber lastName:(NSString*)lastName;
- (bool)compareAndUpdate:(Footballer*)newFootballer;
- (void)addAttribute:(NSString*)name value:(NSString*)value;
+ (NSString*)urlToIdent:(NSString*)url;
+ (int)getFootballerRoleFromString:(NSString*)position;
- (bool)checkSeason:(int)season teamID:(NSInteger)teamID;
- (NSString*)encodeToString;
+ (Footballer*)footballerFromCSVArray:(NSArray*)csvArray;
- (bool)hasPlayedForTeamID:(NSNumber*)teamNumber;
+ (NSString*)getStringExportHeader;
- (bool)checkDifferentParseID:(Footballer*)newFootballer;
- (void)setFromCSVArray:(NSArray*)csvArray;
- (int)getStartYear;
- (int)getEndYear;
- (bool)addPlayedForTeamID:(NSNumber*)teamNumber;
@end

NS_ASSUME_NONNULL_END
