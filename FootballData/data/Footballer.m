//
//  Footballer.m
//  FootballData
//
//  Created by Ian Copeman on 09/11/2020.
//

#import "Footballer.h"
#import "StringSearch.h"

typedef enum {
    RoleUnknown,
    RoleGoalKeeper,
    RoleLeftBack,
    RoleCentralDefender,
    RoleRightBack,
    RoleMidfielder,
    RoleForward,
} RoleType;

//
// ToDo: add nationality, teams
@implementation Footballer {
    NSString* nationalityString;
    int startYear;
    int endYear;
    NSMutableArray* teamArray;
    NSString* loadedTeamArrayString;
}

//
// Footballer
typedef struct  __attribute__((__packed__)) {
    UInt16 index;
    UInt16 size;
    UInt16 startYearShort;
    UInt16 endYearShort;
    UInt8 role;
    UInt8 licenseType;
    UInt32 birthDateNumber;
    UInt32 deathDateNumber;
    UInt32 parseIndex;
    UInt32 parseIndexLive;

} FootballerStructHeader;


+ (NSString*)urlToIdent:(NSString*)url {
    NSRange range = [url rangeOfString:@"/" options:NSBackwardsSearch];
    if (range.length == 0) return nil;
    return [url substringFromIndex:range.location + 1];
}
- (void)addAttribute:(NSString*)name value:(NSString*)value {
    static NSDateFormatter* dateFormatIn = nil;
    if (dateFormatIn == nil) {
        dateFormatIn = [[NSDateFormatter alloc] init];
        [dateFormatIn setDateFormat:@"dd MMMM yyyy"];
    }
    static NSDateFormatter* dateFormatIn2 = nil;
    if (dateFormatIn2 == nil) {
        dateFormatIn2 = [[NSDateFormatter alloc] init];
        [dateFormatIn2 setDateFormat:@"yyyy"];
    }
    static NSDateFormatter* dateFormatIn3 = nil;
    if (dateFormatIn3 == nil) {
        dateFormatIn3 = [[NSDateFormatter alloc] init];
        [dateFormatIn3 setDateFormat:@"MMMM yyyy"];
    }

    static NSCalendar* gregorianCalandar = nil;
    if (gregorianCalandar == nil) {
        gregorianCalandar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    }

    if (name == nil || value == nil || value.length == 0) return;
    value = [value stringByTrimmingCharactersInSet:
              [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ([name localizedCaseInsensitiveContainsString:@"Birthdate"]
        || ([name localizedCaseInsensitiveContainsString:@"Birthdate"] && ![value localizedCaseInsensitiveContainsString:@"/"]) ) {
        // " 24 July 1870"
        NSDate* date = [dateFormatIn dateFromString:value];
        if (date == nil) {
            date = [dateFormatIn2 dateFromString:value];
            if (date == nil) {
                date = [dateFormatIn3 dateFromString:value];
            }
        }
        if (date == nil) {
            if ([value containsString:@"Unknown"]) return;
            int ian = 10;
        }
        NSDateComponents *components = [gregorianCalandar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:date];
        self.birthDateNumber = (int)([components year] * 10000 + [components month] * 100 + [components day]);
        return ;
    }
    if ([name localizedCaseInsensitiveContainsString:@"Date of birth"]) {
        // "02/10/1992"
        static NSDateFormatter* dateFormatInFA = nil;
        if (dateFormatInFA == nil) {
            dateFormatInFA = [[NSDateFormatter alloc] init];
            [dateFormatInFA setDateFormat:@"dd/MM/yy"];
        }
        NSDate* date = [dateFormatInFA dateFromString:value];
        if (date == nil) {
            date = [dateFormatIn2 dateFromString:value];
            if (date == nil) {
                date = [dateFormatIn3 dateFromString:value];
            }
        }
        if (date == nil) {
            int ian = 10;
        }
        NSDateComponents *components = [gregorianCalandar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:date];
        self.birthDateNumber = (int)([components year] * 10000 + [components month] * 100 + [components day]);

        return ;
    }
    if ([name localizedCaseInsensitiveContainsString:@"Date of death"]) {
        // " 24 July 1870"
        NSDate* date = [dateFormatIn dateFromString:value];
        if (date == nil) {
            date = [dateFormatIn2 dateFromString:value];
            if (date == nil) {
                date = [dateFormatIn3 dateFromString:value];
            }
        }
        if (date == nil) {
            int ian = 10;
        }
        NSDateComponents *components = [gregorianCalandar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:date];
        self.deathDateNumber = (int)([components year] * 10000 + [components month] * 100 + [components day]);
        return ;
    }
    if ([name localizedCaseInsensitiveContainsString:@"Birthplace"] ||
        [name localizedCaseInsensitiveContainsString:@"Place of birth"]) {
        if (self.birthPlace != nil && [self.birthPlace compare:value] != NSOrderedSame) {
            NSLog(@"2 Different birthplaces for footballer: %@, %@:%@", self.name, self.birthPlace, value);
        }
        self.birthPlace = value;
        return ;
    }
    if ([name localizedCaseInsensitiveContainsString:@"Full Name"]) {
        self.nameFull = value;
        return ;
    }
    if ([name localizedCaseInsensitiveContainsString:@"Nationality"]) {
        value = [StringSearch stringByStrippingHTML:value];
        nationalityString = value;
        self.countryID = [Team getCountryID:value];
        return ;
    }
    if ([name localizedCaseInsensitiveContainsString:@"Position"]) {
        self.role = [Footballer getFootballerRoleFromString:value];
        return ;
    }
}

+ (NSString*)generateKey:(int)birthDateNumber lastName:(NSString*)lastName {
    //
    // Convert key to ANSI
    
    lastName = [lastName stringByReplacingOccurrencesOfString:@"ð" withString:@"d"];
    lastName = [lastName stringByReplacingOccurrencesOfString:@"ß" withString:@"ss"];
    lastName = [lastName stringByReplacingOccurrencesOfString:@"&#039;" withString:@"'"];
    lastName = [lastName stringByReplacingOccurrencesOfString:@"Đ" withString:@"D"];
    lastName = [lastName stringByReplacingOccurrencesOfString:@"đ" withString:@"d"];
    lastName = [lastName stringByReplacingOccurrencesOfString:@"ı" withString:@"i"];
    lastName = [lastName stringByReplacingOccurrencesOfString:@"ł" withString:@"l"];
    lastName = [lastName stringByReplacingOccurrencesOfString:@"Ł" withString:@"l"];
    lastName = [lastName stringByReplacingOccurrencesOfString:@"’" withString:@"'"];
    lastName = [lastName stringByReplacingOccurrencesOfString:@"ö" withString:@"o"];
    lastName = [lastName stringByReplacingOccurrencesOfString:@"ə" withString:@"e"];
    lastName = [lastName stringByReplacingOccurrencesOfString:@"ž" withString:@"z"];
    lastName = [lastName stringByReplacingOccurrencesOfString:@"ć" withString:@"c"];
    lastName = [lastName stringByReplacingOccurrencesOfString:@"é" withString:@"e"];

    NSData *decode = [lastName dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString* lastNameASCII = [[NSString alloc] initWithData:decode encoding:NSASCIIStringEncoding];
    NSString* key = [NSString stringWithFormat:@"%@_%d", lastNameASCII, birthDateNumber];
    
    if ([key containsString:@"?"]) {
        NSString *result = [[NSString alloc] initWithData:[[lastName precomposedStringWithCompatibilityMapping] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES] encoding:NSASCIIStringEncoding];

        NSString* test = [lastName stringByFoldingWithOptions:0 locale:nil];
        int ian = 10;
    }
    return key;
}


- (NSString*)generateKey {
    if (self.dictionaryKey != nil) return self.dictionaryKey;
    //
    // Generate a unique key for footballer
    int keyNumber = self.birthDateNumber;
    if (keyNumber == 0) {
        //
        // Fudge missing birth dates for players after 1992
        if ([self.name containsString:@"Shane Cook"]) return @"Cook_19700101";
        if ([self.name containsString:@"Mathias Svensson"]) return @"Svensson_19740924";
        if ([self.name containsString:@"Robert Williams"]) return @"Williams_19650101";
        if ([self.name containsString:@"K Scriven"]) return @"Scriven_19841127";
        if ([self.name containsString:@"James Collins"]) return @"Collins_19840928";
        NSLog(@"Attempting to generate footballer key without birthdate: %@", self.name);
        
        //
        // Use first known team as a differentiator. 
        keyNumber = self.teamID;
    }
    NSString* lastName = [[self.name componentsSeparatedByString:@" "] lastObject];
    
    self.dictionaryKey = [Footballer generateKey:keyNumber lastName:lastName];
    if ([self.dictionaryKey compare:@"Wallace_19691002"] == NSOrderedSame) {
        //
        // Twins
        if ([self.name containsString:@"Rodney"]) self.dictionaryKey = @"WallaceRodney_19691002";
        if ([self.name containsString:@"Ray"]) self.dictionaryKey = @"WallaceRay_19691002";
    }
    return self.dictionaryKey;
}

- (bool)compareAndUpdate:(Footballer*)newFootballer {
    //
    // Check for new parsing ID
    if (self.parseIndexLFC == newFootballer.parseIndexLFC &&
        self.parseIndex11v11 == newFootballer.parseIndex11v11 &&
        self.parseIndexAPIFootballer == newFootballer.parseIndexAPIFootballer &&
        self.parseIndexTM == newFootballer.parseIndexTM) return NO;
    
    //
    //
    if (self.parseIndexLFC == 0 && newFootballer.parseIndexLFC != 0) {
        self.parseIndexLFC = newFootballer.parseIndexLFC;
        //
        // Check values?
        return YES;
    }
    if (self.parseIndexTM == 0 && newFootballer.parseIndexTM != 0) {
        self.parseIndexTM = newFootballer.parseIndexTM;
        //
        // Check values?
        return YES;
    }
    if (self.parseIndexAPIFootballer == 0 && newFootballer.parseIndexAPIFootballer != 0) {
        self.parseIndexAPIFootballer = newFootballer.parseIndexAPIFootballer;
        //
        // Check values?
        return YES;
    }
    return NO;
}

- (bool)checkDifferentParseID:(Footballer*)newFootballer {
    
    if (newFootballer.parseIndexLFC != 0 && (self.parseIndexLFC != 0 && self.parseIndexLFC != newFootballer.parseIndexLFC)) {
        //
        // Different ID
        return YES;
    }
    if (newFootballer.parseIndexTM != 0 && (self.parseIndexTM != 0 && self.parseIndexTM != newFootballer.parseIndexTM)) {
        //
        // Different ID
        return YES;
    }
    return NO;
}

- (bool)checkSeason:(int)season teamID:(NSInteger)teamID {
    bool returnValue = NO;
    if (teamArray == nil) {
        teamArray = [NSMutableArray arrayWithCapacity:5];
    }
    NSNumber* teamNumber = [NSNumber numberWithInteger:teamID];
    if (![teamArray containsObject:teamNumber]) {
        [teamArray addObject:teamNumber];
        returnValue = YES;
    }
    if (startYear == 0 || startYear > season) {
        startYear = season;
        returnValue = YES;
    }
    if (endYear < season && season != 0) {
        endYear = season;
        returnValue = YES;
    }
    return returnValue;
}

- (bool)hasPlayedForTeamID:(NSNumber*)teamNumber {
    return [teamArray containsObject:teamNumber];
}
- (bool)addPlayedForTeamID:(NSNumber*)teamNumber {
    if (![teamArray containsObject:teamNumber]) {
        if (teamArray == nil) {
            teamArray = [NSMutableArray arrayWithCapacity:5];
        }
        [teamArray addObject:teamNumber];
    }

    return [teamArray containsObject:teamNumber];
}
- (int)getStartYear {
    return startYear;
}
- (int)getEndYear {
    return endYear;
}


#pragma mark - BlobCoding
+ (NSString*)getBlobName {
    return @"Footballers";
}
- (void)encodeToBlob:(NSMutableData*)blobData {
    static FootballerStructHeader header;
    header.index = self.index;
    header.startYearShort = startYear;
    header.endYearShort = endYear;
    header.role = self.role;
    header.licenseType = self.licenseType;
    header.birthDateNumber = self.birthDateNumber;
    header.deathDateNumber = self.deathDateNumber;
    header.parseIndex = self.parseIndexTM;
    header.parseIndexLive = self.parseIndexAPIFootballer;
    header.size = sizeof(FootballerStructHeader);

    //
    // Test
    if (self.index == 7347) {
        int ian = 10;
    }
    if ([self.name localizedCaseInsensitiveContainsString:@"Solanke"]) {
        int ian = 10;
    }
    
    //
    // Create team array
    NSString* teamArrayString = loadedTeamArrayString;
    if (teamArrayString == nil) {
        NSMutableString* teamArrayStringNew = [NSMutableString stringWithCapacity:100];
        for (NSNumber* teamID in teamArray) {
            [teamArrayStringNew appendFormat:@"%d,", [teamID intValue] ];
        }
        teamArrayString = teamArrayStringNew;
    }
    
    
    //
    // String sizes
    const char* cstrArray[] = {[self.name UTF8String], [[self generateKey] UTF8String], [self.nameNice UTF8String],
        [self.nameFull UTF8String], [teamArrayString UTF8String], [self.imageURL UTF8String], [self.birthPlace UTF8String],
        [self.imageAttributation UTF8String],
    };
    int sizeArray = sizeof(cstrArray) / sizeof(cstrArray[0]);
    for (int i = 0; i < sizeArray; i++) {
        const char* cstr = cstrArray[i];
        if (cstr == nil) {
            header.size += 1;
        }
        else {
            /*
            if (strstr(cstr, "&#91;1&#93;")) {
                int  ian = 10;
            }
*/
            header.size += strlen(cstr) + 1;
        }
    }
    [blobData appendBytes:&header length:sizeof(FootballerStructHeader)];

    //
    // String values
    for (int i = 0; i < sizeArray; i++) {
        const char* cstr = cstrArray[i];
        static char zeroByte = 0;
        if (cstr == nil) {
            [blobData appendBytes:&zeroByte length:1];
        }
        else {
            [blobData appendBytes:cstr length:strlen(cstr) + 1];
        }
    }
}

- (int)initFromBlob:(const void*)blobData {
    FootballerStructHeader* header = (FootballerStructHeader*)blobData;
    self.index = header->index;
    startYear = header->startYearShort;
    endYear = header->endYearShort;
    self.role = header->role;
    self.licenseType = header->licenseType;
    self.birthDateNumber = header->birthDateNumber;
    self.deathDateNumber = header->deathDateNumber;
    self.parseIndexTM = header->parseIndex;
    self.parseIndexAPIFootballer = header->parseIndexLive;
    if (header->index == 352) {
        int ian = 10;
    }

    char* bufferPointer = (char*)blobData + sizeof(FootballerStructHeader);
    self.name = [NSString stringWithUTF8String:bufferPointer];

    bufferPointer = bufferPointer + strlen(bufferPointer) + 1;
    self.dictionaryKey = [NSString stringWithUTF8String:bufferPointer];

    bufferPointer = bufferPointer + strlen(bufferPointer) + 1;
    self.nameNice = [NSString stringWithUTF8String:bufferPointer];
    bufferPointer = bufferPointer + strlen(bufferPointer) + 1;
    if (self.nameNice == nil || self.nameNice.length == 0) self.nameNice = self.name;
    self.nameFull = [NSString stringWithUTF8String:bufferPointer];
    bufferPointer = bufferPointer + strlen(bufferPointer) + 1;
    NSString* teamArrayString = [NSString stringWithUTF8String:bufferPointer];
    loadedTeamArrayString = teamArrayString; // Store so we can save back to blob
    bufferPointer = bufferPointer + strlen(bufferPointer) + 1;
    self.imageURL = [NSString stringWithUTF8String:bufferPointer];
    bufferPointer = bufferPointer + strlen(bufferPointer) + 1;
    self.birthPlace = [NSString stringWithUTF8String:bufferPointer];
    bufferPointer = bufferPointer + strlen(bufferPointer) + 1;
    self.imageAttributation = [NSString stringWithUTF8String:bufferPointer];

    return header->size;
}

+ (NSString*)getStringExportHeader {
    return @"Key,Name,V11,LFC,TM,API,Name Nice,Name Full,BirthDate,DeathDate,CountryID,Birth Place,Role,Image,ImageA,LicenceType\n";
}
- (NSString*)encodeToString {
  /*
    if ([self.name localizedCaseInsensitiveContainsString:@"Harry Doherty"]) {
        int ian = 10;
    }
    */
    
    //
    // Encode birthplace
    bool encodeString = [self.birthPlace containsString:@","];
    NSString* location = self.birthPlace == nil ? @"" : encodeString ? [NSString stringWithFormat:@"\"%@\"", self.birthPlace] : self.birthPlace;
    
    /*
    //
    // Create team array
    NSMutableString* teamArrayString = [NSMutableString stringWithCapacity:100];
    bool addSeparator = NO;
    for (NSNumber* teamID in teamArray) {
        [teamArrayString appendFormat:@"%@%d", addSeparator ? @";" : @"", [teamID intValue]];
        addSeparator = YES;
    }
*/
    NSString* stringEncode = [NSString stringWithFormat:
                              @"%@,%@,%d,%d,%d,%d,%@,%@,%d,%d,%d,%@,%d,%@,%@,%d\n",
                              self.dictionaryKey,
                              self.name,
                              self.parseIndex11v11,
                              self.parseIndexLFC,
                              self.parseIndexTM, // 5
                              self.parseIndexAPIFootballer,
                              //teamArrayString,
                              self.nameNice == nil ? @"" : self.nameNice,
                              self.nameFull == nil ? @"" : self.nameFull,
                              self.birthDateNumber, // 10
                              self.deathDateNumber,
                              self.countryID,
                              location,
                              self.role,
                              self.imageURL == nil ? @"" : self.imageURL,
                              self.imageAttributation == nil ? @"" : self.imageAttributation,
                              self.licenseType

    ];
    return stringEncode;
}

+ (Footballer*)footballerFromCSVArray:(NSArray*)csvArray {
    Footballer* footballer = [[Footballer alloc] init];
    @try {
        [footballer setFromCSVArray:csvArray];
    }
    @catch ( NSException *e ) {
        [footballer setFromCSVArray:csvArray];

    }
    return footballer;
}

- (void)setFromCSVArray:(NSArray*)csvArray {
    int index = 0;
    self.dictionaryKey = csvArray[index++];
    self.name = csvArray[index++];
    self.parseIndex11v11 = [csvArray[index++] intValue];
    self.parseIndexLFC = [csvArray[index++] intValue];
    self.parseIndexTM = [csvArray[index++] intValue];
    self.parseIndexAPIFootballer = [csvArray[index++] intValue];
   //  NSString* teamArrayString = csvArray[index++];
    self.nameNice = csvArray[index++];
    self.nameFull = csvArray[index++];
    self.birthDateNumber = [csvArray[index++] intValue];
    self.deathDateNumber = [csvArray[index++] intValue];
    self.countryID = [csvArray[index++] intValue];
    self.birthPlace = csvArray[index++];
    self.role = [csvArray[index++] intValue];

    NSString* object = csvArray[index++];
    if (![object containsString:@"(null)"]) {
        self.imageURL = object;
    }
    object = csvArray[index++];
    if (![object containsString:@"(null)"]) {
        self.imageAttributation = object;
    }
    if (csvArray.count > index) {
        self.licenseType = [csvArray[index++] intValue];
    }
}


+ (int)getFootballerRoleFromString:(NSString*)position {
    if (position == nil) return RoleUnknown;
    if ([position localizedCaseInsensitiveContainsString:@"goalkeeper"]) {
        return RoleGoalKeeper;
    }
    if ([position localizedCaseInsensitiveContainsString:@"left-back"] ||
        [position localizedCaseInsensitiveContainsString:@"left back"] ||
        [position localizedCaseInsensitiveContainsString:@"left wing-back"]) {
        return RoleLeftBack;
    }
    if ([position localizedCaseInsensitiveContainsString:@"right-back"] ||
        [position localizedCaseInsensitiveContainsString:@"right back"] ||
        [position localizedCaseInsensitiveContainsString:@"right wing-back"]) {
        return RoleRightBack;
    }
    if ([position localizedCaseInsensitiveContainsString:@"Wing back"] ||
        [position localizedCaseInsensitiveContainsString:@"Wing-back"]) {
        return RoleRightBack;
    }
    if ([position localizedCaseInsensitiveContainsString:@"defender"] ||
        [position localizedCaseInsensitiveContainsString:@"central defender"]||
        [position localizedCaseInsensitiveContainsString:@"centre back"]||
        [position localizedCaseInsensitiveContainsString:@"centre-back"]||
        [position localizedCaseInsensitiveContainsString:@"centre half"] ||
        [position localizedCaseInsensitiveContainsString:@"centre-half"] ||
        [position localizedCaseInsensitiveContainsString:@"center-back"]||
        [position localizedCaseInsensitiveContainsString:@"full back"]||
        [position localizedCaseInsensitiveContainsString:@"full-back"]||
        [position localizedCaseInsensitiveContainsString:@"Sweeper"]||
        [position localizedCaseInsensitiveContainsString:@"fullback"]) {
        return RoleCentralDefender;
    }
    
    if ([position localizedCaseInsensitiveContainsString:@"left half"] ||
        [position localizedCaseInsensitiveContainsString:@"left-half"] ||
        [position localizedCaseInsensitiveContainsString:@"right half"] ||
        [position localizedCaseInsensitiveContainsString:@"right-half"] ||
        [position localizedCaseInsensitiveContainsString:@"half back"]||
        [position localizedCaseInsensitiveContainsString:@"half-back"]||
        [position localizedCaseInsensitiveContainsString:@"midfield"] ||
        [position localizedCaseInsensitiveContainsString:@"midfielder"] ||
        [position localizedCaseInsensitiveContainsString:@"midfileder"] ||
        [position localizedCaseInsensitiveContainsString:@"centre half"] ||
        [position localizedCaseInsensitiveContainsString:@"wing-half"] ||
        [position localizedCaseInsensitiveContainsString:@"wing half"] ||
        [position localizedCaseInsensitiveContainsString:@"utility"]) {
        return RoleMidfielder;
    }
    if ([position localizedCaseInsensitiveContainsString:@"winger"] ||
        [position localizedCaseInsensitiveContainsString:@"outside left"] ||
        [position localizedCaseInsensitiveContainsString:@"outside-left"] ||
        [position localizedCaseInsensitiveContainsString:@"outside right"] ||
        [position localizedCaseInsensitiveContainsString:@"outside-right"] ||
        [position localizedCaseInsensitiveContainsString:@"inside right"] ||
        [position localizedCaseInsensitiveContainsString:@"inside-right"] ||
        [position localizedCaseInsensitiveContainsString:@"inside left"] ||
        [position localizedCaseInsensitiveContainsString:@"inside-left"] ||
        [position localizedCaseInsensitiveContainsString:@"attack"] ||
        [position localizedCaseInsensitiveContainsString:@"striker"] ||
        [position localizedCaseInsensitiveContainsString:@"firward"] ||
        [position localizedCaseInsensitiveContainsString:@"forward"]) {
        return RoleForward;
    }

    NSLog(@"Failed to resolve role: %@", position);
    return RoleUnknown;
}


@end
