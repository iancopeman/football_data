//
//  Event.h
//  FootballData
//
//  Created by Ian Copeman on 16/11/2020.
//

#import <Foundation/Foundation.h>
#import "BlobCoding.h"

NS_ASSUME_NONNULL_BEGIN


//
// 8 bit field
// 4 Bits from this enum
// 5th Bit clean sheet
// 6-7 Bit: 1 - Win, 2 - Draw, 3 - Lose
typedef enum {
    EventApperance = 1,
    EventGoalkeeperApperance,
    EventSub,
    EventGoal,
    EventPenalty,
    EventOwnGoal,
    EventYellowCard,
    EventRedCard,
    EventBench,

} EventType;
typedef enum {
    EventResultWin = 1,
    EventResultDraw,
    EventResultLose,

} EventResultType;

#define GetEventResultType(event) ((event & 0x60) >> 5)
#define GetEventType(event) (event & 0xF)
#define IsEventACleanSheet(event) ((GetEventType(event) >= EventApperance && GetEventType(event) <= EventSub) && (((event >> 4) & 0x1) == 1))
#define IsEventAnApperance(event) (GetEventType(event) >= EventApperance && GetEventType(event) <= EventSub)
#define IsEventAGoal(event) (event >= EventGoal && event <= EventOwnGoal)
#define IsEventAGoalGoal(event) (event >= EventGoal && event <= EventPenalty)
#define IsEventACard(event) (event >= EventYellowCard && event <= EventRedCard)




@interface Event : NSObject<BlobCoding>

@property UInt64 eventDetail;
@property int matchID;
@property int teamID;
@property int locationID;
@property int matchDateNumber;
@property int competition;
@property char season8;


+ (NSNumber*)createEventAppearanceNumber:(UInt16)playerID position:(char)position  shirt:(char)shirt cleanSheet:(bool)cleanSheet resultType:(EventResultType)resultType;
+ (NSNumber*)createEventGoalkeeperApperance:(UInt16)playerID position:(char)position shirt:(char)shirt cleanSheet:(bool)cleanSheet resultType:(EventResultType)resultType;
+ (NSNumber*)createEventBenchNumber:(UInt16)playerID shirt:(char)shirt;
+ (NSNumber*)createEventGoalNumber:(UInt16)playerID playerAssistID:(UInt16)playerAssistID time:(int)time;
+ (NSNumber*)createEventPenaltyNumber:(UInt16)playerID  time:(int)time;
+ (NSNumber*)createEventYellowCardNumber:(UInt16)playerID  time:(int)time;
+ (NSNumber*)createEventRedCardNumber:(UInt16)playerID  time:(int)time;
+ (NSNumber*)createEventOwnGoalNumber:(UInt16)playerID  time:(int)time;
+ (NSNumber*)createEventSubNumber:(UInt16)playerIDIn playerIDOut:(UInt16)playerIDOut  time:(int)time cleanSheet:(bool)cleanSheet resultType:(EventResultType)resultType;
+ (bool)isGoalieApperance:(NSNumber*)eventNumber;
+ (bool)isStartingApperance:(NSNumber*)eventNumber;
+ (int)isGoalGetTime:(NSNumber*)eventNumber;
+ (bool)isOwnGoal:(NSNumber*)eventNumber;
@end

NS_ASSUME_NONNULL_END
