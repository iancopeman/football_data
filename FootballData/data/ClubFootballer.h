//
//  ClubFootballer.h
//  FootballData
//
//  Created by Ian Copeman on 18/11/2021.
//

#import <Foundation/Foundation.h>
#import "Footballer.h"

NS_ASSUME_NONNULL_BEGIN

@interface ClubFootballer : NSObject
@property int index;
@property int apperancesLeague;
@property int apperancesCup1;
@property int apperancesCup2;
@property int apperancesOther;
@property int goalsLeague;
@property int goalsCup1;
@property int goalsCup2;
@property int goalsOther;

@property int squadPlayer;
@property int startYear;
@property int endYear;
@property int hallOfFame;

@property (strong, nonatomic) NSString* biography;


@property (strong, nonatomic) Footballer* footballer;
@property (strong, nonatomic) NSString* dictionaryKey;


+ (ClubFootballer*)clubFootballerFromCSVArray:(NSArray*)csvArray;
+ (NSString*)getStringExportHeader;
- (void)encodeClubFootballerToBlob:(NSMutableData*)blobData;
- (NSString*)encodeClubFootballerToString;
@end

NS_ASSUME_NONNULL_END
