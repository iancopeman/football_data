//
//  MatchStats.m
//  FootballData
//
//  Created by Ian Copeman on 11/06/2024.
//

#import "MatchStats.h"
#import "NSString+CSVParser.h"
#import "FootballModel.h"

@implementation MatchStatsTeam
@end

@implementation MatchStats

typedef struct  __attribute__((__packed__)) {
    UInt16 teamID;
    UInt8 shotsOn;
    UInt8 shotsTotal;
    UInt8 saves;
    UInt8 fouls;
    UInt8 corners;
    UInt8 shotsIn;
    UInt8 shotsOut;
    UInt16 passes;
    UInt16 passesGood;
    UInt16 possession;
} MatchStatsTeamStruct;


typedef struct  __attribute__((__packed__)) {
    UInt32 matchID;
    MatchStatsTeamStruct teamStats1;
    MatchStatsTeamStruct teamStats2;
} MatchStatsHeader;


//
// Called when first creating MatchStats
+ (MatchStats*)initWithMatchID:(int)matchID seasonNumber:(NSNumber*)seasonNumber dateNumber:(int)dateNumber  teamName1:(NSString*)teamName1  teamName2:(NSString*)teamName2 {
    MatchStats* matchStats = [[MatchStats alloc] init];
    matchStats.matchID = matchID;
    matchStats.seasonNumber = seasonNumber;
    matchStats.dateNumber = dateNumber;
    matchStats.teamName1 = teamName1;
    matchStats.teamName2 = teamName2;
    matchStats.teamStats1 = [[MatchStatsTeam alloc] init];
    matchStats.teamStats2 = [[MatchStatsTeam alloc] init];
    return matchStats;
}


#pragma mark - BlobCoding
+ (NSString*)getBlobName {
    return @"MatchStats";
}

- (void)encodeToBlob:(NSMutableData*)blobData {
    MatchStatsHeader header;
    header.matchID = self.matchID;
    header.teamStats1.teamID = self.teamStats1.teamID;
    header.teamStats1.shotsOn = self.teamStats1.shotsOn;
    header.teamStats1.shotsTotal = self.teamStats1.shotsTotal;
    header.teamStats1.saves = self.teamStats1.saves;
    header.teamStats1.fouls = self.teamStats1.fouls;
    header.teamStats1.corners = self.teamStats1.corners;
    header.teamStats1.shotsIn = self.teamStats1.shotsIn;
    header.teamStats1.shotsOut = self.teamStats1.shotsOut;
    header.teamStats1.passes = self.teamStats1.passes;
    header.teamStats1.passesGood = self.teamStats1.passesGood;
    header.teamStats1.possession = self.teamStats1.possession;

    header.teamStats2.teamID = self.teamStats2.teamID;
    header.teamStats2.shotsOn = self.teamStats2.shotsOn;
    header.teamStats2.shotsTotal = self.teamStats2.shotsTotal;
    header.teamStats2.saves = self.teamStats2.saves;
    header.teamStats2.fouls = self.teamStats2.fouls;
    header.teamStats2.corners = self.teamStats2.corners;
    header.teamStats2.shotsIn = self.teamStats2.shotsIn;
    header.teamStats2.shotsOut = self.teamStats2.shotsOut;
    header.teamStats2.passes = self.teamStats2.passes;
    header.teamStats2.passesGood = self.teamStats2.passesGood;
    header.teamStats2.possession = self.teamStats2.possession;

    [blobData appendBytes:&header length:sizeof(MatchStatsHeader)];
}

- (int)initFromBlob:(const void*)blobData {
    return 0;
}


+ (NSString*)getStringExportHeader {
    return @"DateNumber,\
    TeamName1,ShotsOn,ShotsTotal,Saves,Fouls,Corners,ShotsIn,ShotsOut,Passes,PassesGood,Possession,\
    TeamName2,ShotsOn,ShotsTotal,Saves,Fouls,Corners,ShotsIn,ShotsOut,Passes,PassesGood,Possession\n";
}

- (NSString*)encodeToString {
    NSString* stringTeamStats1 = [NSString stringWithFormat:
                              @"%@,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d",
                              self.teamName1,
                              self.teamStats1.shotsOn,
                              self.teamStats1.shotsTotal,
                              self.teamStats1.saves,
                              self.teamStats1.fouls,
                              self.teamStats1.corners,
                              self.teamStats1.shotsIn,
                              self.teamStats1.shotsOut,
                              self.teamStats1.passes,
                              self.teamStats1.passesGood,
                              self.teamStats1.possession];
    
    NSString* stringTeamStats2 = [NSString stringWithFormat:
                              @"%@,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d",
                              self.teamName2,
                              self.teamStats2.shotsOn,
                              self.teamStats2.shotsTotal,
                              self.teamStats2.saves,
                              self.teamStats2.fouls,
                              self.teamStats2.corners,
                              self.teamStats2.shotsIn,
                              self.teamStats2.shotsOut,
                              self.teamStats2.passes,
                              self.teamStats2.passesGood,
                              self.teamStats2.possession];
    NSString* stringEncode = [NSString stringWithFormat:
                              @"%d,%@,%@\n",
                              self.dateNumber,
                              stringTeamStats1,
                              stringTeamStats2];
    return stringEncode;
}

+ (void)loadMatchStatsFromString:(NSString*)fileString season:(int)season {
    NSArray* rows = [fileString csvRows];
    int count = 0;
    for (NSArray *row in rows){
        int rowCount = 0;
        NSString* dateNumberString = row[rowCount++];
        if ([dateNumberString containsString:@"DateNumber"]) continue;
        NSString* teamName1 = row[rowCount++];
        NSString* shotsOnString1 = row[rowCount++];
        NSString* shotsTotalString1 = row[rowCount++];
        NSString* savesString1 = row[rowCount++];
        NSString* foulsString1 = row[rowCount++];
        NSString* cornersString1 = row[rowCount++];
        NSString* shotsInString1 = row[rowCount++];
        NSString* shotsOutString1 = row[rowCount++];
        NSString* passesString1 = row[rowCount++];
        NSString* passesGoodString1 = row[rowCount++];
        NSString* possessionString1 = row[rowCount++];
        
        NSString* teamName2 = row[rowCount++];
        NSString* shotsOnString2 = row[rowCount++];
        NSString* shotsTotalString2 = row[rowCount++];
        NSString* savesString2 = row[rowCount++];
        NSString* foulsString2 = row[rowCount++];
        NSString* cornersString2 = row[rowCount++];
        NSString* shotsInString2 = row[rowCount++];
        NSString* shotsOutString2 = row[rowCount++];
        NSString* passesString2 = row[rowCount++];
        NSString* passesGoodString2 = row[rowCount++];
        NSString* possessionString2 = row[rowCount++];
        
        
        static FootballModel* model = nil;
        if (model == nil) {
            model = [FootballModel getModel];
        }
        int matchDateNumber = [dateNumberString intValue];
        int teamID1 = [model addTeam:teamName1 season:season];
        int teamID2 = [model addTeam:teamName2 season:season];
        NSString* matchKey = [Match makeMatchKeyFromDateNumber:matchDateNumber team1:teamID1 team2:teamID2];
        Match* match = [model getMatch:matchKey];
        if (!match) {
            NSLog(@"* Failed to find match in database: %@", matchKey);
            break;
        }

        //
        // Create MatchStats
        MatchStats* matchStats = [[MatchStats alloc] init];
        matchStats.matchID = match.index;
        matchStats.seasonNumber = @(season);
        matchStats.dateNumber = matchDateNumber;
        matchStats.teamName1 = teamName1;
        matchStats.teamName2 = teamName2;
        matchStats.teamStats1 = [[MatchStatsTeam alloc] init];
        matchStats.teamStats2 = [[MatchStatsTeam alloc] init];
        
        matchStats.teamStats1.shotsOn = [shotsOnString1 intValue];
        matchStats.teamStats1.shotsTotal = [shotsTotalString1 intValue];
        matchStats.teamStats1.saves = [savesString1 intValue];
        matchStats.teamStats1.fouls = [foulsString1 intValue];
        matchStats.teamStats1.corners = [cornersString1 intValue];
        matchStats.teamStats1.shotsIn = [shotsInString1 intValue];
        matchStats.teamStats1.shotsOut = [shotsOutString1 intValue];
        matchStats.teamStats1.passes = [passesString1 intValue];
        matchStats.teamStats1.passesGood = [passesGoodString1 intValue];
        matchStats.teamStats1.possession = [possessionString1 intValue];

        matchStats.teamStats2.shotsOn = [shotsOnString2 intValue];
        matchStats.teamStats2.shotsTotal = [shotsTotalString2 intValue];
        matchStats.teamStats2.saves = [savesString2 intValue];
        matchStats.teamStats2.fouls = [foulsString2 intValue];
        matchStats.teamStats2.corners = [cornersString2 intValue];
        matchStats.teamStats2.shotsIn = [shotsInString2 intValue];
        matchStats.teamStats2.shotsOut = [shotsOutString2 intValue];
        matchStats.teamStats2.passes = [passesString2 intValue];
        matchStats.teamStats2.passesGood = [passesGoodString2 intValue];
        matchStats.teamStats2.possession = [possessionString2 intValue];

        [model addMatchStats:matchStats];
    }
}

@end
