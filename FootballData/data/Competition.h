//
//  Competition.h
//  FootballData
//
//  Created by Ian Copeman on 16/11/2020.
//

#ifndef Competition_h
#define Competition_h

typedef enum {
    CompetitionUnknown,
    
    CompetitionPremierLeague,
    CompetitionFirstDivision,
    CompetitionSecondDivision,
    CompetitionChampionship,
    CompetitionThirdDivision, //5
    CompetitionThirdDivisionSouth,
    CompetitionThirdDivisionNorth,
    CompetitionLeagueOne,
    CompetitionFourthDivision,
    CompetitionLeagueTwo, //10
    CompetitionLeagueTestMatch,
    CompetitionNationalLeague,
    CompetitionNationalLeagueNorth,
    CompetitionNationalLeagueSouth,

    CompetitionFACup, //15
    CompetitionLeagueCup,
    CompetitionCharityShield,
    CompetitionFootballLeagueSuperCup,
    CompetitionFootballLeagueCentenaryTrophy,
    
    CompetitionEuropeanCup, // 20 // Includes Champpions League
    CompetitionEuropeanCupWinnersCup,
    CompetitionInterCitiesFairsCup,
    CompetitionEuropeanSuperCup,
    CompetitionEuropaLeague,
    CompetitionWorldClubChampionship, // 25
    CompetitionUEFACup,
    CompetitionEuropaConferenceLeague,
    CompetitionInterTotoCup, // 1999-2020

    CompetitionClubWorldCup, // 1999
    CompetitionIntercontinentalCup, // 30 // 1960-2004

    CompetitionFriendly,
    CompetitionMinor,
    
    CompetitionCLQ, // Don't include on device
    CompetitionUefaQ, // Don't include on device
    CompetitionEuropaLQ, // Don't include on device

} CompetitionType;

#define IS_LEAGUE_COMPETITION(c) (c >= CompetitionPremierLeague && c <= CompetitionNationalLeagueSouth)
#define IS_EUROPE_COMPETITION(c) (c >= CompetitionEuropeanCup && c <= CompetitionIntercontinentalCup)


#endif /* Competition_h */
