//
//  StringSearch.h
//  FootballData
//
//  Created by Ian Copeman on 10/11/2020.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface StringSearch : NSObject

+ (StringSearch*)initWithString:(NSString*)seachString;
+ (StringSearch*)initWithString:(NSString*)seachString key:(NSString*)key startKey:(NSString*)startKey endKey:(NSString*)endKey;
+(NSString*)stringByStrippingHTML:(NSString*)string;
+(NSString*)stringByStrippingRegEx:(NSString*)s regex:(NSString*)regex;
- (void)setString:(NSString*)seachString;
- (void)setStringPos:(int)seachPos;
- (int)getStringPos;
- (NSString*)getString;
- (NSString*)getText:(nullable NSString*)startKey  endKey:(NSString*)endKey;
- (NSString*)getTextWithKey:(nullable NSString*)key startKey:(nullable NSString*)startKey  endKey:(NSString*)endKey;
- (NSString*)getSectionWithKey:(nullable NSString*)key startKey:(NSString*)startKey  endKey:(NSString*)endKey;

@end

NS_ASSUME_NONNULL_END

