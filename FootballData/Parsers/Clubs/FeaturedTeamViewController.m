//
//  FeaturedTeamViewController.m
//  FootballData
//
//  Created by Ian Copeman on 01/03/2023.
//

#import "FeaturedTeamViewController.h"
#import "WebPageManager.h"
#import "FootballModel.h"
#import "StringSearch.h"
#import "NSString+CSVParser.h"
#import "ClubFootballer.h"

//#define TeamName (@"Reading")
//#define FilePath (@"/Reading")
//#define URLBase (@"http://barryhugmansfootballers.com/club/508/")
#define FilePathClubPlayers (@"/Devlex/ClubPlayers/")
#define BlobsFilePath (@"/Blobs/")


// https://en.wikipedia.org/wiki/List_of_Reading_F.C._players
// https://www.11v11.com/teams/reading/tab/players/season/2018/ // Squads
// http://barryhugmansfootballers.com/club/508/20
// http://barryhugmansfootballers.com/club/508/700
// http://barryhugmansfootballers.com/player_more/2193
// https://royalsrecord.co.uk/


@interface FeaturedTeamViewController ()
@property (weak) IBOutlet NSComboBox *comboTeam;

@end

@implementation FeaturedTeamViewController {
    WebPageManager* webPageManager;
    FootballModel* model;
    FileManager* fileManager;
    NSString* dataPath;
//    NSString* dataType;
    int teamID;
    NSDictionary* supportedTeamDictionary;
    NSInteger selectedIndexTeam;

}


- (void)viewDidLoad {
    [super viewDidLoad];

    model = [FootballModel getModel];
    webPageManager = [WebPageManager getWebPageManager];
    fileManager = [[FileManager alloc] init];
    dataPath  = [[NSUserDefaults standardUserDefaults] objectForKey:@"dataPath"];
    supportedTeamDictionary = @{
        @"Reading" : @"clubs/508",
        @"Arsenal" : @"",
        @"Tottenham Hotspur" : @"",
    };

    
    //   dataType = @"clubs/508";
    NSArray* teamArray = [supportedTeamDictionary allKeys];
    for (NSString* name in teamArray) {
        [self.comboTeam addItemWithObjectValue:name];
    }
    selectedIndexTeam = [[[NSUserDefaults standardUserDefaults] objectForKey:@"FeaturedTeamIndex"] intValue];
    [self setSelectionCombo:selectedIndexTeam];
}

- (void)setSelectionCombo:(NSInteger)selectedIndex {
    if (selectedIndexTeam != self.comboTeam.indexOfSelectedItem) {
        if (selectedIndexTeam < self.comboTeam.numberOfItems) {
            [self.comboTeam selectItemAtIndex:selectedIndex];
        }
    }
}

- (IBAction)comboTeamChanged:(id)sender {
    if (selectedIndexTeam != self.comboTeam.indexOfSelectedItem) {
        [self setSelectionCombo:self.comboTeam.indexOfSelectedItem];
        selectedIndexTeam = self.comboTeam.indexOfSelectedItem;
    }
}

- (IBAction)createBlobPushed:(id)sender {
    NSString* teamName = self.comboTeam.objectValueOfSelectedItem;
     teamID = [model getTeamID:teamName];
    if (teamID == 1) {
        NSLog(@"Featured Team ID == 1, DB not loaded?");
        return;
    }
    [[NSUserDefaults standardUserDefaults] setInteger:selectedIndexTeam forKey:@"FeaturedTeamIndex"];

    
    NSMutableData* buffer = [NSMutableData dataWithCapacity:5000];
    NSArray* clubPlayerArray = [model getClubPlayers:teamID];
    for (ClubFootballer* clubFootballer in clubPlayerArray) {
        if ([clubFootballer.dictionaryKey containsString:@"Solanke_19970914"]) {
            int ian = 10;
        }
        [clubFootballer encodeClubFootballerToBlob:buffer];
    }
    //
    // Save to file
    NSString* blobPath  = [[NSUserDefaults standardUserDefaults] objectForKey:@"blobPath"];
    [fileManager savePersistedData:blobPath dataName:BlobsFilePath dataType:nil  dataKey:teamName blobData:buffer];
    NSLog(@"Created blob: %@", teamName);

}


- (IBAction)loadClubPlayersPushed:(id)sender {
    NSString* teamName = self.comboTeam.objectValueOfSelectedItem;
    teamID = [model getTeamID:teamName];
    if (teamID == 1) {
        NSLog(@"Featured Team ID == 1, DB not loaded?");
        return;
    }
    [[NSUserDefaults standardUserDefaults] setInteger:selectedIndexTeam forKey:@"FeaturedTeamIndex"];

    NSString* fileName = [NSString stringWithFormat:@"%@.csv", teamName];
    if ([fileManager doesFileExist:dataPath dataName:FilePathClubPlayers dataType:nil dataKey:fileName]) {
        NSLog(@"* Parsing File: %@", fileName);
        NSString* csvString = [fileManager loadPersistedString:dataPath dataName:FilePathClubPlayers dataType:nil dataKey:fileName];
        [self loadClubPlayersFromCSVString:csvString];
    }

    NSArray* clubPlayerArray = [model getClubPlayers:teamID];
    for (ClubFootballer* clubFootballer in clubPlayerArray) {
        if ([clubFootballer.dictionaryKey containsString:@"Solanke_19970914"]) {
            int ian = 10;
        }
        if (clubFootballer.footballer.birthDateNumber == 19551005) {
            int ian = 10;
        }
        if (![model setClubFootballerIndex:clubFootballer teamID:teamID]) {
            NSLog(@"* Failed to find ClubFootballer: %@", clubFootballer.dictionaryKey);
        }
        if ([clubFootballer.footballer.name localizedCaseInsensitiveContainsString:@"Shane Long"]) {
            int ian = 10;
        }

    }
    NSLog(@"Loaded ClubFootballer: %lu", (unsigned long)clubPlayerArray.count);
}

- (IBAction)saveClubPlayersPushed:(id)sender {
    NSString* teamName = self.comboTeam.objectValueOfSelectedItem;
    teamID = [model getTeamID:teamName];
    if (teamID == 1) {
        NSLog(@"Featured Team ID == 1, DB not loaded?");
        return;
    }
    [[NSUserDefaults standardUserDefaults] setInteger:selectedIndexTeam forKey:@"FeaturedTeamIndex"];

    NSString* fileName = [NSString stringWithFormat:@"%@.csv", teamName];
    NSMutableString* csvString = [NSMutableString stringWithCapacity:10000];
    NSArray* clubPlayerArray = [model getClubPlayers:teamID];
    if (clubPlayerArray.count == 0) return;
    
    [csvString appendString:[ClubFootballer getStringExportHeader]];
    for (ClubFootballer* clubFootballer in clubPlayerArray) {
        [csvString appendString:[clubFootballer encodeClubFootballerToString]];
    }
    [fileManager savePersistedString:dataPath dataName:FilePathClubPlayers dataType:nil dataKey:fileName dataString:csvString];
}

- (void)loadClubPlayersFromCSVString:(NSString*)csvString {
    NSArray* rows = [csvString csvRows];
    int count = 0;
    for (NSArray *row in rows){
        ++count;
        NSString* nameString = row[0];
        if (nameString.length == 3 && [nameString containsString:@"Key"]) continue;
        ClubFootballer* clubFootballer = [ClubFootballer clubFootballerFromCSVArray:row];
        if ([clubFootballer.dictionaryKey containsString:@"Solanke_19970914"]) {
            int ian = 10;
        }
        if (clubFootballer.apperancesLeague == 335) {
            int ian = 10;
        }
        [model addClubFootballer:clubFootballer teamID:teamID];
    }
}


- (IBAction)parsePlayersPushed:(id)sender {
    /* Reading Only
    // barryhugmansfootballers.com
    // Players from 1946-2014
    // Pages increment in 20 up to 660
    for (int pageIndex = 0; pageIndex <= 660; pageIndex+= 20) {
        NSString* parseURL = [NSString stringWithFormat:@"%@%d", URLBase, pageIndex];
        [self parseURL:parseURL pageIndex:pageIndex teamName:@""];
        // if (pageIndex == 500) break;
    }
*/
}

- (IBAction)checkCSVListsPushed:(id)sender {
    /* Reading Only
    NSString* fileName = @"Appearances1990.csv";
    if ([fileManager doesFileExist:dataPath dataName:FilePath dataType:nil dataKey:fileName]) {
        NSLog(@"* Parsing File: %@", fileName);
        NSString* csvString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:nil dataKey:fileName];
        [self loadPlayerStatsFromCSVString:csvString isApperanceFile:YES];
    }
    fileName = @"Goals1990.csv";
    if ([fileManager doesFileExist:dataPath dataName:FilePath dataType:nil dataKey:fileName]) {
        NSLog(@"* Parsing File: %@", fileName);
        NSString* csvString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:nil dataKey:fileName];
        [self loadPlayerStatsFromCSVString:csvString isApperanceFile:NO];
    }
    fileName = @"AppearancesRecords.csv";
    if ([fileManager doesFileExist:dataPath dataName:FilePath dataType:nil dataKey:fileName]) {
        NSLog(@"* Parsing File: %@", fileName);
        NSString* csvString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:nil dataKey:fileName];
        [self loadPlayerStatsFromCSVString:csvString isApperanceFile:YES];
    }
    fileName = @"GoalsRecords.csv";
    if ([fileManager doesFileExist:dataPath dataName:FilePath dataType:nil dataKey:fileName]) {
        NSLog(@"* Parsing File: %@", fileName);
        NSString* csvString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:nil dataKey:fileName];
        [self loadPlayerStatsFromCSVString:csvString isApperanceFile:NO];
    }
     */
}

- (void)loadPlayerStatsFromCSVString:(NSString*)csvString isApperanceFile:(bool)isApperanceFile {
    NSArray* clubPlayerArray = [model getClubPlayers:teamID];
    NSLog(@"Club Players Count: %lu", (unsigned long)clubPlayerArray.count);
    NSArray* rows = [csvString csvRows];
    int count = 0;
    int unFoundCount = 0;
    for (NSArray *row in rows){
        NSString* nameString = row[1];
        if (nameString.length == 4 && [nameString containsString:@"Name"]) continue;
        ++count;
        int index = 0;
        index++;
        NSString* name = row[index++];
        name = [name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSData *decode = [name dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        name = [[NSString alloc] initWithData:decode encoding:NSASCIIStringEncoding];
        bool found = NO;

        for (ClubFootballer* clubFootballer in clubPlayerArray) {
            if ([clubFootballer.footballer.name compare:name] == NSOrderedSame) {
                found = YES;
                //
                // Merge
                if (isApperanceFile) {
                    
                    clubFootballer.apperancesLeague = [row[index++] intValue];
                    clubFootballer.apperancesCup1 = [row[index++] intValue];
                    clubFootballer.apperancesCup2 = [row[index++] intValue];
                    clubFootballer.apperancesOther = [row[index++] intValue];
                }
                else {
                    
                    clubFootballer.goalsLeague = [row[index++] intValue];
                    clubFootballer.goalsCup1 = [row[index++] intValue];
                    clubFootballer.goalsCup2 = [row[index++] intValue];
                    clubFootballer.goalsOther = [row[index++] intValue];
                }
                break;
            }
        }
        if (!found) {
            NSArray* names = [name componentsSeparatedByString:@" "];
            NSString* lastName = [names lastObject];
            if ([lastName containsString:@"Leig'wood"]) lastName = @"Leigertwood";
            if ([lastName containsString:@"K'nishvili"]) lastName = @"Khizanishvili";
            if ([lastName containsString:@"d.l.Cruz"]) lastName = @"De La Cruz";
            if ([lastName containsString:@"Blatherw'k"]) lastName = @"Blatherwick";
            if ([lastName containsString:@"v.d.Kwaak"]) lastName = @"Van Der Kwaak";
            for (ClubFootballer* clubFootballer in clubPlayerArray) {
                if ([clubFootballer.footballer.name containsString:lastName]) {
                    found = YES;
                    break;
                }
            }
        }
        if (!found) {
            //
            //
            NSLog(@"Failed to find footballer: %@", name);
            unFoundCount++;
        }
//        Footballer* footballer = [Footballer footballerFromCSVArray:row];
        
//        [model addFootballerComplete:footballer];
    }
    NSLog(@"Failed to find footballers: %d", unFoundCount);
    int ian = 10;
}



- (IBAction)checkExistingPlayers:(id)sender {
    NSString* teamName = self.comboTeam.objectValueOfSelectedItem;
    teamID = [model getTeamID:teamName];
    if (teamID == 1) {
        NSLog(@"Featured Team ID == 1, DB not loaded?");
        return;
    }
    NSArray* clubPlayerArray = [model getClubPlayers:teamID];
    NSLog(@"Club Players Count: %lu", (unsigned long)clubPlayerArray.count);
    NSArray* footballerArray = [model getFootballersForTeam:teamID];
    NSLog(@"DB Players Count: %lu", (unsigned long)footballerArray.count);

    //
    // Check every footballer in DB is also in club footballer list
    int countNewFootballers = 0;
    for (Footballer* footballer in footballerArray) {
        // if ([footballer getStartYear] > 2014) continue;
        NSString* key = [footballer generateKey];
        bool found = NO;
        for (ClubFootballer* clubFootballer in clubPlayerArray) {
            if (footballer.birthDateNumber == clubFootballer.footballer.birthDateNumber) {
                NSString* key2 = [clubFootballer.footballer generateKey];
                if ([key2 compare:key] == NSOrderedSame) {
                    found = YES;
                    
                    //
                    // Merge
                    [self mergePlayers:clubFootballer footballer:footballer];
                    break;
                }
                else {
                    int ina = 10;
                }
            }

        }
        if (!found) {
            //
            // Lewis Grabban, Andy Yiadom, Chris Martin
            //
            // Add club footballer
            ClubFootballer* clubFootballer = [[ClubFootballer alloc] init];
            clubFootballer.footballer.name = footballer.name;
            clubFootballer.dictionaryKey = footballer.dictionaryKey;
            [self mergePlayers:clubFootballer footballer:footballer];
            [model addClubFootballer:clubFootballer teamID:teamID];
            //NSLog(@"Failed to find footballer: %@", footballer.name);
            //int ian = 10;
            countNewFootballers++;
        }
        int ian = 10;
    }
    NSLog(@"Added new club footballers: %d", countNewFootballers);

    
}

- (void)mergePlayers:(ClubFootballer*)clubFootballer footballer:(Footballer*)footballer {
/*    footballer.nameFull = @"";
    footballer.birthDateNumber = clubFootballer.birthDateNumber;
    footballer.deathDateNumber = 0;
    footballer.countryID = clubFootballer.countryID;
    footballer.birthPlace = clubFootballer.birthPlace;
    footballer.role = clubFootballer.role;
*/
/*
    if (clubFootballer.parseIndex11v11 == 0) clubFootballer.parseIndex11v11 = footballer.parseIndex11v11;
    if (clubFootballer.parseIndexLFC == 0) clubFootballer.parseIndexLFC = footballer.parseIndexLFC;
    if (clubFootballer.parseIndexTM == 0) clubFootballer.parseIndexTM = footballer.parseIndexTM;
    if (clubFootballer.parseIndexAPIFootballer == 0) clubFootballer.parseIndexAPIFootballer = footballer.parseIndexAPIFootballer;
    if (clubFootballer.nameNice == nil) clubFootballer.nameNice = footballer.nameNice;
    if (clubFootballer.nameFull == nil) clubFootballer.nameFull = footballer.nameFull;
    if (clubFootballer.birthDateNumber == 0) clubFootballer.birthDateNumber = footballer.birthDateNumber;
    if (clubFootballer.deathDateNumber == 0) clubFootballer.deathDateNumber = footballer.deathDateNumber;
    if (clubFootballer.countryID == 0) clubFootballer.countryID = footballer.countryID;
    if (clubFootballer.birthPlace == nil) clubFootballer.birthPlace = footballer.birthPlace;
    if (clubFootballer.role == 0) clubFootballer.role = footballer.role;
 */
    
}


- (void)parseURL:(NSString*)url pageIndex:(int)pageIndex teamName:(NSString*)teamName {
    /* Reading Specific
    NSString* dataKey =  [@(pageIndex) stringValue];
    NSString* webPageString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:dataType dataKey:dataKey];
    if (webPageString == nil) {
        webPageString = [webPageManager getWebPage:url];
        [fileManager savePersistedString:dataPath dataName:FilePath dataType:dataType dataKey:dataKey dataString:webPageString];
    }
    if (webPageString == nil) {
        return;
    }

    

    StringSearch* stringSearch = [StringSearch initWithString:webPageString];
    NSString* table = [stringSearch getSectionWithKey:@"<table border=0 cellspacing=0 cellpadding=3 border=0 class='tableListTable" startKey:@"<table" endKey: @"</table>"];
    StringSearch* stringSearchTable = [StringSearch initWithString:table];
    NSString* section = [stringSearchTable getText:@"<tbody" endKey: @"</tbody>"];
    while (section != nil) {
        StringSearch* playerSearch = [StringSearch initWithString:section];
        NSString* row = [playerSearch getText:@"<tr" endKey: @"</tr"];
        int appNumber = 0;
        int subNumber = 0;
        int goalNumber = 0;
        int role = 0;
        NSString* name = nil;
        int countryID = 0;
        NSString* birthPlace = nil;
        int birthDateNumber = 0;
        int deathDateNumber = 0;

        {
            StringSearch* rowSearch = [StringSearch initWithString:row];
            NSString* rowDetail = [rowSearch getText:@"<td" endKey: @"</td>"];
            StringSearch* rowDetailSearch = [StringSearch initWithString:rowDetail];
            NSString* surname = [rowDetailSearch getText:@"<b>" endKey:@","];
            NSString* first = [rowDetailSearch getText:@">" endKey:@"<"];
            first = [first stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            name = [NSString stringWithFormat:@"%@ %@", first, surname];
            
            rowDetail = [rowSearch getText:@"<td" endKey: @"</td>"];
            rowDetail = [rowSearch getText:@"<td" endKey: @"</td>"];
            rowDetailSearch = [StringSearch initWithString:rowDetail];
            NSString* roleString = [rowDetailSearch getText:@">" endKey:@"<"];
            role = [Footballer getFootballerRoleFromString:roleString];
            
            rowDetail = [rowSearch getText:@"<td" endKey: @"</td>"];
            rowDetailSearch = [StringSearch initWithString:rowDetail];
            NSString* apps = [rowDetailSearch getText:@"<b>" endKey:@"<"];
            appNumber = [apps intValue];
            rowDetail = [rowSearch getText:@"<td" endKey: @"</td>"];
            rowDetailSearch = [StringSearch initWithString:rowDetail];
            NSString* subs = [rowDetailSearch getText:@"<b>" endKey:@"<"];
            subNumber = [subs intValue];
            rowDetail = [rowSearch getText:@"<td" endKey: @"</td>"];
            rowDetailSearch = [StringSearch initWithString:rowDetail];
            NSString* goals = [rowDetailSearch getText:@"<b>" endKey:@"<"];
            goalNumber = [goals intValue];

        }
        row = [playerSearch getText:@"<tr" endKey: @"</tr"];
        {
            StringSearch* rowSearch = [StringSearch initWithString:row];
            NSString* rowDetail = [rowSearch getText:@"<td" endKey: @"/td>"];
            StringSearch* rowDetailSearch = [StringSearch initWithString:rowDetail];
            NSString* country = [rowDetailSearch getText:@"title=\"" endKey:@"\""];
            countryID = [Team getCountryID:country];
            
            NSString* birthPlaceString = [rowDetailSearch getText:@"/div>" endKey:@"<"];
            birthPlace = [birthPlaceString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            rowDetail = [rowSearch getText:@"<td" endKey: @"</td>"];
            rowDetailSearch = [StringSearch initWithString:rowDetail];
            NSString* birthDate = [rowDetailSearch getTextWithKey:@"<div" startKey:@">" endKey:@"<"];
            birthDateNumber  = [self parseDateNumber:birthDate];
            int ina = 10;
            
        }

        if ([name localizedCaseInsensitiveContainsString:@"Shane Long"]) {
            int ian = 10;
        }

        if (birthDateNumber < 18500101 || birthDateNumber > 20220101) {
            int ian = 10;
        }

        
        NSLog(@"Footballer: %@", name);
        NSLog(@"\t%d-%d-%d", appNumber, subNumber, goalNumber);
        NSLog(@"\t%d-%d-%@(%d)", role, birthDateNumber, birthPlace, countryID);
        ClubFootballer* clubFootballer = [[ClubFootballer alloc] init];
        NSString* lastName = [[name componentsSeparatedByString:@" "] lastObject];
        NSString* dictionaryKey = [Footballer generateKey:birthDateNumber lastName:lastName];
        clubFootballer.dictionaryKey = dictionaryKey;
        clubFootballer.apperancesLeague = appNumber + subNumber;
        clubFootballer.goalsLeague = goalNumber;
        clubFootballer.footballer = [model getFootballer:clubFootballer.dictionaryKey];
        if (clubFootballer.footballer == nil) {
            if (birthDateNumber == 0) {
                int ian = 10;
            }
            clubFootballer.footballer = [[Footballer alloc] init];
            clubFootballer.footballer.name = name;
            clubFootballer.footballer.birthPlace = birthPlace;
            clubFootballer.footballer.role = role;
            //            clubFootballer.footballer.countryID = countryID;
            clubFootballer.footballer.birthDateNumber = birthDateNumber;
            [model addFootballerComplete:clubFootballer.footballer];
        }
        [model addClubFootballer:clubFootballer teamID:teamID];

        section = [stringSearchTable getText:@"<tbody" endKey: @"</tbody>"];
    }

    */
    int ian = 10;
}

- (int)parseDateNumber:(NSString*)dateString {
    //
    // Dates come in as 12/02/1993
    NSString* dateNumberString = [dateString stringByReplacingOccurrencesOfString:@"/" withString:@""];
    int dateNumberIn = [dateNumberString intValue];
    int year = (dateNumberIn % 10000);
    int month = ((dateNumberIn / 10000) % 100);
    int day = dateNumberIn / 1000000;
    int dateNumberOut = (int)(year * 10000 + month * 100 + day);
    
    if (year < 1870 || year > 2030) {
        NSLog(@"Failed to parse date string: %@", dateString);
    }
    
    return dateNumberOut;
}

- (IBAction)addNewPlayersPushed:(id)sender {
    //
    // Check that all players are in the DB
    // Older players wont
    NSString* teamName = self.comboTeam.objectValueOfSelectedItem;
    teamID = [model getTeamID:teamName];
    if (teamID == 1) {
        NSLog(@"Featured Team ID == 1, DB not loaded?");
        return;
    }
    NSArray* clubPlayerArray = [model getClubPlayers:teamID];
    NSLog(@"Club Players Count: %lu", (unsigned long)clubPlayerArray.count);
    NSArray* footballerArray = [model getFootballersForTeam:teamID];
    NSLog(@"DB Players Count: %lu", (unsigned long)footballerArray.count);
    
    //
    // Check every footballer in club list is also in db
    int countNewFootballers = 0;
    for (ClubFootballer* clubFootballer in clubPlayerArray) {
        // if ([footballer getStartYear] > 2014) continue;
        NSString* key = [clubFootballer.footballer generateKey];
        bool found = NO;
        for (Footballer* footballer in footballerArray) {
            if (footballer.birthDateNumber == clubFootballer.footballer.birthDateNumber) {
                NSString* key2 = [footballer generateKey];
                if ([key2 compare:key] == NSOrderedSame) {
                    found = YES;
                    clubFootballer.index = footballer.index;
                    break;
                }
                else {
                    int ina = 10;
                }
            }
            
        }
        if (!found) {
            //
            // Add club footballer
            clubFootballer.footballer.teamID = teamID;
            Footballer* existing = [model addFootballer:clubFootballer.footballer season:0];
            if (existing != nil) {
                clubFootballer.index = existing.index;
            }
            //NSLog(@"Failed to find footballer: %@", footballer.name);
            //int ian = 10;
            countNewFootballers++;
        }
        int ian = 10;
    }
    NSLog(@"Added new db footballers: %d", countNewFootballers);
    NSArray* clubPlayerArray2 = [model getClubPlayers:teamID];
    NSLog(@"db footballers: %d", clubPlayerArray2.count);

}


@end
