//
//  WebParsersViewController.h
//  FootballData
//
//  Created by Ian Copeman on 15/11/2021.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface WebParsersViewController : NSViewController
- (IBAction)parsePush:(id)sender;
@end

NS_ASSUME_NONNULL_END
