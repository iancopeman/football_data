//
//  WikipediaPlayers.h
//  FootballData
//
//  Created by Ian Copeman on 24/11/2020.
//

#import "Team.h"

NS_ASSUME_NONNULL_BEGIN

@interface WikipediaPlayers : Team
- (void)parseAll;
- (void)parseCategory:(NSString*)teamName;
- (void)parseImages:(NSString*)teamName;
- (NSArray*)getTeamArray;
- (void)saveClubPlayers:(NSString*)teamName;

@end

NS_ASSUME_NONNULL_END
