//
//  WikipediaViewController.m
//  FootballData
//
//  Created by Ian Copeman on 16/09/2021.
//

#import "WikipediaViewController.h"
#import "WikipediaPlayers.h"

@interface WikipediaViewController ()
@property (weak) IBOutlet NSComboBox *comboTeam;

@end

@implementation WikipediaViewController {
    NSInteger selectedIndexTeam;
    WikipediaPlayers* wikiParser;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    wikiParser =  [[WikipediaPlayers alloc] init];
    NSArray* teamArray = [wikiParser getTeamArray];
    for (NSString* name in teamArray) {
        [self.comboTeam addItemWithObjectValue:name];
    }
    selectedIndexTeam = [[[NSUserDefaults standardUserDefaults] objectForKey:@"WikipediaParserTeamIndex"] intValue];

    [self setSelectionCombo:selectedIndexTeam];

}
- (IBAction)comboTeamChanged:(id)sender {
    if (selectedIndexTeam != self.comboTeam.indexOfSelectedItem) {
        [self setSelectionCombo:self.comboTeam.indexOfSelectedItem];
        selectedIndexTeam = self.comboTeam.indexOfSelectedItem;
    }
}

- (void)setSelectionCombo:(NSInteger)selectedIndex {
    if (selectedIndexTeam != self.comboTeam.indexOfSelectedItem) {
        if (selectedIndexTeam < self.comboTeam.numberOfItems) {
            [self.comboTeam selectItemAtIndex:selectedIndex];
        }
    }
}
- (IBAction)savePushed:(id)sender {
    [[NSUserDefaults standardUserDefaults] setInteger:selectedIndexTeam forKey:@"WikipediaParserTeamIndex"];
    NSString* teamName = self.comboTeam.objectValueOfSelectedItem;
    [wikiParser saveClubPlayers:teamName];
}


- (IBAction)parsePush:(id)sender {
   // WikipediaPlayers* wikiParser = [[WikipediaPlayers alloc] init];
   // [wikiParser parseAll];
}

- (IBAction)parseCategory:(id)sender {
    [[NSUserDefaults standardUserDefaults] setInteger:selectedIndexTeam forKey:@"WikipediaParserTeamIndex"];
    NSString* teamName = self.comboTeam.objectValueOfSelectedItem;
    [wikiParser parseCategory:teamName];
}
- (IBAction)parseImages:(id)sender {
    [[NSUserDefaults standardUserDefaults] setInteger:selectedIndexTeam forKey:@"WikipediaParserTeamIndex"];
    NSString* teamName = self.comboTeam.objectValueOfSelectedItem;
    [wikiParser parseImages:teamName];
}

@end
