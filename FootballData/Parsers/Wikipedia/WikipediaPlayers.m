//
//  WikipediaPlayers.m
//  FootballData
//
//  Created by Ian Copeman on 24/11/2020.
//

#import "WikipediaPlayers.h"
#import "WebPageManager.h"
#import "FootballModel.h"
#import "StringSearch.h"

#define FilePath (@"/Wiki/")
#define FilePathClubPlayers (@"/Devlex/ClubPlayers/")

//
// https://en.wikipedia.org/wiki/Category:Reading_F.C._players

@implementation WikipediaPlayers {
    WebPageManager* webPageManager;
    FootballModel* model;
    FileManager* fileManager;
    NSString* dataPath;
    NSCharacterSet* nonDigitCharacterSet;
    NSDictionary* supportedTeamDictionary;
}

- (instancetype)init {
    if (self = [super init]) {
        model = [FootballModel getModel];
        webPageManager = [WebPageManager getWebPageManager];
        fileManager = [[FileManager alloc] init];
        dataPath  = [[NSUserDefaults standardUserDefaults] objectForKey:@"dataPath"];
        supportedTeamDictionary = @{
           //  @"Reading" : @"https://en.wikipedia.org/wiki/Category:Reading_F.C._players",
            @"Arsenal" : @"https://en.wikipedia.org/wiki/Category:Arsenal_F.C._players",
            @"Aston Villa" : @"https://en.wikipedia.org/wiki/Category:Aston_Villa_F.C._players",
            @"AFC Bournemouth" : @"https://en.wikipedia.org/wiki/Category:AFC_Bournemouth_players",
            @"Brentford" : @"https://en.wikipedia.org/wiki/Category:Brentford_F.C._players",
            @"Brighton & Hove Albion" : @"https://en.wikipedia.org/wiki/Category:Brighton_%26_Hove_Albion_F.C._players",
            @"Burnley" : @"https://en.wikipedia.org/wiki/Category:Burnley_F.C._players",
            @"Chelsea" : @"https://en.wikipedia.org/wiki/Category:Chelsea_F.C._players",
            @"Crystal Palace" : @"https://en.wikipedia.org/wiki/Category:Crystal_Palace_F.C._players",
            @"Everton" : @"https://en.wikipedia.org/wiki/Category:Everton_F.C._players",
            @"Fulham" : @"https://en.wikipedia.org/wiki/Category:Fulham_F.C._players",
            @"Ipswich Town" : @"https://en.wikipedia.org/wiki/Category:Ipswich_Town_F.C._players",
            @"Leicester City" : @"https://en.wikipedia.org/wiki/Category:Leicester_City_F.C._players",
            @"Liverpool" : @"https://en.wikipedia.org/wiki/Category:Liverpool_F.C._players",
            @"Luton Town" : @"https://en.wikipedia.org/wiki/Category:Luton_Town_F.C._players",
            @"Manchester City" : @"https://en.wikipedia.org/wiki/Category:Manchester_City_F.C._players",
            @"Manchester United" : @"https://en.wikipedia.org/wiki/Category:Manchester_United_F.C._players",
            @"Newcastle United" : @"https://en.wikipedia.org/wiki/Category:Newcastle_United_F.C._players",
            @"Nottingham Forest" : @"https://en.wikipedia.org/wiki/Category:Nottingham_Forest_F.C._players",
            @"Southampton" : @"https://en.wikipedia.org/wiki/Category:Southampton_F.C._players",
           // @"Sheffield United" : @"https://en.wikipedia.org/wiki/Category:Sheffield_United_F.C._players",
            @"Tottenham Hotspur" : @"https://en.wikipedia.org/wiki/Category:Tottenham_Hotspur_F.C._players",
            @"West Ham United" : @"https://en.wikipedia.org/wiki/Category:West_Ham_United_F.C._players",
            @"Wolverhampton Wanderers" : @"https://en.wikipedia.org/wiki/Category:Wolverhampton_Wanderers_F.C._players",

        };
    }
    return self;
}

- (NSArray*)getTeamArray {
    return [supportedTeamDictionary allKeys];
}

- (void)saveClubPlayers:(NSString*)teamName  {
    int teamID = [model getTeamID:teamName];
    if (teamID == 1) {
        NSLog(@"TeamID Team ID == 1, DB not loaded?");
        return;
    }

    
    
    NSString* fileName = [NSString stringWithFormat:@"%@.csv", teamName];
    NSMutableString* csvString = [NSMutableString stringWithCapacity:10000];
    NSArray* clubPlayerArray = [model getClubPlayers:teamID];
    if (clubPlayerArray.count == 0) return;
    
    [csvString appendString:[ClubFootballer getStringExportHeader]];
    for (ClubFootballer* clubFootballer in clubPlayerArray) {
        [csvString appendString:[clubFootballer encodeClubFootballerToString]];
    }
    [fileManager savePersistedString:dataPath dataName:FilePathClubPlayers dataType:nil dataKey:fileName dataString:csvString];
    NSLog(@"Saved Club Players: %@", teamName);

}



- (void)parseCategory:(NSString*)teamName {

    NSString* parseURL = [supportedTeamDictionary objectForKey:teamName];
    if (parseURL == nil) return;

    // NSString* teamName = @"Reading";
    int teamID = [model getTeamID:teamName];
    if (teamID == 1) {
        NSLog(@"TeamID Team ID == 1, DB not loaded?");
        return;
    }
    if (teamID == 0) {
        NSLog(@"TeamID Team ID == 0, Unknown Team");
        return;
    }

    int count = 0;
    int changedCount = 0;
    while (parseURL != nil) {
        parseURL = [self parseURL2:parseURL teamID:teamID teamName:teamName dataKey:[NSString stringWithFormat:@"%@-%d", teamName, ++count] imagesOnly:NO changedCountPtr:&changedCount];
    }

    
    /*
    
    NSArray* pageArray = @[
        @"https://en.wikipedia.org/wiki/Category:Reading_F.C._players",
        @"https://en.wikipedia.org/w/index.php?title=Category:Reading_F.C._players&pagefrom=Clinch%2C+Tom%0ATom+Clinch#mw-pages",
        @"https://en.wikipedia.org/w/index.php?title=Category:Reading_F.C._players&pagefrom=Hadland%2C+Phil%0APhil+Hadland#mw-pages",
        @"https://en.wikipedia.org/w/index.php?title=Category:Reading_F.C._players&pagefrom=Makin%2C+Chris%0AChris+Makin#mw-pages",
        @"https://en.wikipedia.org/w/index.php?title=Category:Reading_F.C._players&pagefrom=Ritchie%2C+George%0AGeorge+Ritchie+%28footballer%2C+born+1889%29#mw-pages",
        @"https://en.wikipedia.org/w/index.php?title=Category:Reading_F.C._players&pagefrom=Williams%2C+Martin%0AMartin+Williams+%28footballer%29#mw-pages",
    ];
    
    int count = 0;
    for (NSString* parseURL in pageArray) {
        [self parseURL2:parseURL teamID:teamID teamName:teamName dataKey:[NSString stringWithFormat:@"%@-%d", teamName, ++count]];
    }
     */
    [model setFootballersModified];
    NSLog(@"Loaded Club Players: %@", teamName);

}

- (void)parseImages:(NSString*)teamName {
    
    NSString* parseURL = [supportedTeamDictionary objectForKey:teamName];
    if (parseURL == nil) return;
    
    int teamID = [model getTeamID:teamName];
    if (teamID == 1) {
        NSLog(@"TeamID Team ID == 1, DB not loaded?");
        return;
    }
    if (teamID == 0) {
        NSLog(@"TeamID Team ID == 0, Unknown Team");
        return;
    }

    NSUInteger footballerNumber = [model getFootballerCount];
    if (footballerNumber <= 10000) {
        NSLog(@"Load Footballer Blob");
        return;
    }
    NSLog(@"Parsing Images for %@", teamName);

    int count = 0;
    int changedCount = 0;
    while (parseURL != nil) {
        parseURL = [self parseURL2:parseURL teamID:teamID teamName:teamName dataKey:[NSString stringWithFormat:@"%@-%d", teamName, ++count] imagesOnly:YES changedCountPtr:&changedCount];
    }
    NSLog(@"Updated %d Footballers", changedCount);

    [model setFootballersModified];
}



- (void)parseAll {
    model = [FootballModel getModel];
    webPageManager = [WebPageManager getWebPageManager];
    fileManager = [[FileManager alloc] init];
    dataPath  = [[NSUserDefaults standardUserDefaults] objectForKey:@"dataPath"];
    NSString* teamName = @"Reading";
    int teamID = [model getTeamID:teamName];
    if (teamID == 1) {
        NSLog(@"TeamID Team ID == 1, DB not loaded?");
//        return;
    }

    NSString* parseURL = [NSString stringWithFormat:@"https://en.wikipedia.org/wiki/List_of_%@_F.C._players", teamName];
    [self parseURL:parseURL teamID:teamID teamName:teamName];
    parseURL = [NSString stringWithFormat:@"https://en.wikipedia.org/wiki/List_of_%@_F.C._players_(25-99_appearances)", teamName];
    [self parseURL:parseURL teamID:teamID teamName:teamName];
    parseURL = [NSString stringWithFormat:@"https://en.wikipedia.org/wiki/List_of_%@_F.C._players_(1-24_appearances)", teamName];
    [self parseURL:parseURL teamID:teamID teamName:teamName];

    //    [self parseURL:@"https://en.wikipedia.org/wiki/List_of_Liverpool_F.C._players_(25%E2%80%9399_appearances)"];
//    [self parseURL:@"https://en.wikipedia.org/wiki/List_of_Liverpool_F.C._players_(1%E2%80%9324_appearances)"];
    [model setFootballersModified];
}

- (void)parseURL:(NSString*)url teamID:(int)teamID teamName:(NSString*)teamName {

    
    NSString* dataKey =  [self generateDataKey:url];
    NSString* dataType = [NSString stringWithFormat:@"/Teams/%@", teamName];
    NSString* webPageString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:dataType dataKey:dataKey];
    if (webPageString == nil) {
        webPageString = [webPageManager getWebPage:url];
        [fileManager savePersistedString:dataPath dataName:FilePath dataType:dataType dataKey:dataKey dataString:webPageString];
    }
    if (webPageString == nil) {
        return;
    }

    //
    // Find season
    StringSearch* stringSearchTable = [StringSearch initWithString:webPageString];
    NSString* stringTable = [stringSearchTable getTextWithKey:@"layers with" startKey:@"<tbody>" endKey:@"<tbody>"];
    if (stringTable == nil) {
        NSLog(@"Can't find table in webpage");
        return;
    }
    
    if ([stringTable containsString:@"This list is"]) {
        stringTable = [stringSearchTable getText:@"<tbody>" endKey:@"<tbody>"];
        if (stringTable == nil) {
            NSLog(@"Can't find table in webpage");
            return;
        }
    }
    StringSearch* stringSearch = [StringSearch initWithString:[stringSearchTable getString]];
    NSString* section = [stringSearch getText:@"<tr" endKey: @"</tr"];
    section = [stringSearch getText:@"<tr" endKey: @"</tr"];
    StringSearch* rowSearch;
    int test = 0;
    while (section != nil) {
        section = [stringSearch getText:@"<tr" endKey: @"</tr"];
        if (section == nil) break;
        if ([section containsString:@"navbox-title"]) break;
        if (rowSearch == nil) {
            rowSearch = [StringSearch initWithString:section];
        }
        else {
            [rowSearch setString:section];
        }

        NSString* playerURLString = [rowSearch getText:@"href=\"" endKey: @"\""];
        NSString* nameString = [rowSearch getText:@">" endKey: @"<"];
        nameString = [nameString stringByTrimmingCharactersInSet:
                                   [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        nameString = [nameString stringByReplacingOccurrencesOfString:@"&#039;" withString:@"'"];
        if ([nameString containsString:@"rvaldsson"]) {
            int ina = 10;
        }
        
        [self parsePlayer:nameString teamID:teamID teamName:teamName url:playerURLString imagesOnly:NO];
        if (++test == 2) {
            //  break;
        }
    }
}

- (NSString*)parseURL2:(NSString*)url teamID:(int)teamID teamName:(NSString*)teamName dataKey:(NSString*)dataKey  imagesOnly:(bool)imagesOnly changedCountPtr:(int*)changedCountPtr  {
    NSString* dataType = [NSString stringWithFormat:@"/Teams/%@", teamName];
    NSString* webPageString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:dataType dataKey:dataKey];
    if (webPageString == nil) {
        webPageString = [webPageManager getWebPage:url];
        [fileManager savePersistedString:dataPath dataName:FilePath dataType:dataType dataKey:dataKey dataString:webPageString];
    }
    if (webPageString == nil) {
        return nil;
    }
    /*
    <div class="mw-category-group"><h3>&nbsp;</h3>
    <ul><li><a href="/wiki/List_of_Reading_F.C._players" title="List of Reading F.C. players">List of Reading F.C. players</a></li>
    <li><a href="/wiki/List_of_Reading_F.C._players_(1%E2%80%9324_appearances)" title="List of Reading F.C. players (1–24 appearances)">List of Reading F.C. players (1–24 appearances)</a></li>
    <li><a href="/wiki/List_of_Reading_F.C._players_(25%E2%80%9399_appearances)" title="List of Reading F.C. players (25–99 appearances)">List of Reading F.C. players (25–99 appearances)</a></li></ul></div>
     */
    //
    // Find section
    StringSearch* stringSearchTable = [StringSearch initWithString:webPageString];
    
    NSString* stringTable = [stringSearchTable getSectionWithKey:@"<div class=\"mw-category-group" startKey:@"<div" endKey:@"</div"];
    while (stringTable != nil) {
        bool skip = NO;
        if ([stringTable containsString:@"wartime guest"])skip = YES;
        if ([stringTable containsString:@"F.C. players"])skip = YES;
        if (!skip) {
            StringSearch* stringSearchSection = [StringSearch initWithString:stringTable];
            NSString* playerURLString = [stringSearchSection getText:@"href=\"" endKey: @"\""];
            NSString* nameString = [stringSearchSection getText:@">" endKey: @"<"];

            while (playerURLString != nil) {
                nameString = [nameString stringByReplacingOccurrencesOfString:@"\\(.*\\)"
                                                                   withString:@""
                                                                      options:NSRegularExpressionSearch range:NSMakeRange(0, nameString.length)];

                nameString = [nameString stringByTrimmingCharactersInSet:
                              [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                nameString = [nameString stringByReplacingOccurrencesOfString:@"&#039;" withString:@"'"];
                if ([nameString containsString:@"rvaldsson"]) {
                    nameString = @"Gunnar Heidar Thorvaldsson";
                }

                Footballer* footballer = [self parsePlayer:nameString teamID:teamID teamName:teamName url:playerURLString imagesOnly:imagesOnly];
                if (footballer != nil) {
                    (*changedCountPtr)++;
                }
                playerURLString = [stringSearchSection getText:@"href=\"" endKey: @"\""];
                nameString = [stringSearchSection getText:@">" endKey: @"<"];
            }
        }
        stringTable = [stringSearchTable getSectionWithKey:@"<div class=\"mw-category-group" startKey:@"<div" endKey:@"</div"];
    }
    
    NSString* stringNextURL = [stringSearchTable getSectionWithKey:@"previous page</a>" startKey:@"<a" endKey:@"/a>"];
    if (stringNextURL == nil) {
        stringNextURL = [stringSearchTable getSectionWithKey:@"previous page" startKey:@"<a" endKey:@"/a>"];
    }
    if ([stringNextURL containsString:@">next page<"]) {
        StringSearch* stringSearchSection = [StringSearch initWithString:stringNextURL];
        NSString* nextURLString = [stringSearchSection getText:@"href=\"" endKey: @"\""];
        nextURLString = [nextURLString stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
        NSString* newURL = [NSString stringWithFormat:@"https://en.wikipedia.org%@", nextURLString];
        // NSLog(@"URL: %@", newURL);
        return newURL;
    }

    return nil;
    
}

- (NSString *)extractNumberFromText:(NSString*)text {
    if (nonDigitCharacterSet == nil) {
        nonDigitCharacterSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    }
    NSArray* stringArray = [text componentsSeparatedByCharactersInSet:nonDigitCharacterSet];
    for (NSString* value in stringArray) {
        if (value.length != 0) return value;
    }
    return nil;
}

- (bool)skipPlayer:(NSString*)playerName teamName:(NSString*)teamName  {
    NSArray* minorPlayers = nil;
    if ([teamName compare:@"Liverpool"] == NSOrderedSame) {
        minorPlayers = @[ @"Joseph Pearson", @"James Kelso", @"Wally Richardson",
                                   @"James Henderson",  // Doesn't parse
        ];
    }
    for (int i = 0; i < minorPlayers.count; i++) {
        if ([playerName localizedCaseInsensitiveContainsString:minorPlayers[i]]) {
            return true;
        }
    }
    return false;
}

- (Footballer*)parsePlayer:(NSString*)playerName teamID:(int)teamID teamName:(NSString*)teamName url:(NSString*)playerURL imagesOnly:(bool)imagesOnly  {
    if ([self skipPlayer:playerName teamName:teamName]) return nil;
    
    //
    // Skip players who only played in minor games
 
    
    // NSLog(@"Processng Profile: %@", playerName);
    NSString* dataKey = playerName;
    if ([playerName localizedCaseInsensitiveContainsString:@"Fred Jones"]) {
        if ([playerURL containsString:@"1938"]) {
            dataKey = @"Fred Jones_1938";
        }
    }

    NSString* fullURL = [NSString stringWithFormat:@"https://en.wikipedia.org%@", playerURL];
    NSString* webPageString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:FootballerFilePath dataKey:dataKey];
    if (webPageString == nil) {
        webPageString = [webPageManager getWebPage:fullURL];
        [fileManager savePersistedString:dataPath dataName:FilePath dataType:FootballerFilePath dataKey:dataKey dataString:webPageString];
    }
    if (webPageString == nil) {
        return nil;
    }

    //
    // Look for InfoBox
    StringSearch* stringProfile = [StringSearch initWithString:webPageString key:@"infobox vcard" startKey:@"<tbody>" endKey:@"</tbody>"];
    if (stringProfile == nil) {
        // NSLog(@"Missing info box: %@", playerName);
        return nil;
    }


    //
    // Test
    if ([playerName localizedCaseInsensitiveContainsString:@"Sol Campbell"]) {
        int ian = 10;
    }
    if ([playerName containsString:@"Valentine"]) {
        int ian = 10;
    }

    /*
    //
    // Get parse Id
    NSString* stringIdent;
    StringSearch* stringSearchIdent = [StringSearch initWithString:webPageString];
    NSString* identString = [stringSearchIdent getText:@"lfchistory.net/Players/Player/Profile" endKey:@"\">"];
    if (identString == nil) {
        identString = [stringSearchIdent getText:@"lfchistory.net/players/player/profile" endKey:@"\">"];
    }
    if (identString == nil) {
        identString = [stringSearchIdent getText:@"lfchistory.net/player_profile" endKey:@"\">"];
    }
    if (identString != nil) {
        stringIdent = [self extractNumberFromText:identString];
        //
        // Check for second ident
        NSString* identString2 = [stringSearchIdent getText:@"lfchistory.net/Players/Player/Profile" endKey:@"\">"];
        if (identString2 == nil) {
            identString2 = [stringSearchIdent getText:@"lfchistory.net/players/player/profile" endKey:@"\">"];
        }
        if (identString2 == nil) {
            identString2 = [stringSearchIdent getText:@"lfchistory.net/player_profile" endKey:@"\">"];
        }
       if (identString2 != nil) {
           NSString* stringIdent2 = [self extractNumberFromText:identString2];
           if ([stringIdent compare:stringIdent2] != NSOrderedSame) {
               int ian = 10;
           }
       }
    }
    else {
       // NSLog(@"Missing iDent: %@", playerName);

    }
     */
    /*
    if ([[webPage getWebPageString] containsString:@"lfchistory.net"]) {
        StringSearch* stringIdent = [StringSearch initWithString:[webPage getWebPageString] key:@"id=\"External_links\"" startKey:@"lfchistory.net/player_profile.asp?player_id=" endKey:@"\">"];
        if (stringIdent == nil) {
            stringIdent = [StringSearch initWithString:[webPage getWebPageString] key:@"id=\"External_links\"" startKey:@"lfchistory.net/Players/Player/Profile/" endKey:@"\">"];
            if (stringIdent == nil) {
                stringIdent = [StringSearch initWithString:[webPage getWebPageString] key:@"id=\"External_links\"" startKey:@"lfchistory.net/players/player/profile/" endKey:@"\">"];
                if (stringIdent == nil) {
                    return nil;
                }
            }
        }
    }
     */
    //<a rel="nofollow" class="external text" href="http://lfchistory.net/player_profile.asp?player_id=635">LFCHistory.net profile</a>
    //www.lfchistory.net/Players/Player/Profile/
    //<a rel="nofollow" class="external text" href="http://lfchistory.net/player_profile.asp?player_id=635">LFCHistory.net profile</a>
    
    //
    // Look for image, must be before "Personal information"
    NSString* section = [stringProfile getText:@"<tr>" endKey: @"</tr"];
    StringSearch* rowSearch;
    int test = 0;
    NSString* playerImageURL = nil;
    NSString* imagePath = nil;
    NSString* playerPosition = nil;
    NSString* playerNameFull = nil;
    NSString* birthPlace = nil;
    NSString* positionString = nil;
    int birthDate = 0;
    int deathDate = 0;
    int startYear = 0;
    int endYear = 0;
    int role = 0;
    NSString* teamSearchString = teamName; // @"Reading F.C.";
    while (section != nil) {
        if (rowSearch == nil) {
            rowSearch = [StringSearch initWithString:section];
        }
        else {
            [rowSearch setString:section];
        }
        
        if (imagePath == nil) { // Just process first image
            if ([section containsString:@"<img alt="]) {
                imagePath = [rowSearch getText:@"href=\"" endKey: @"\""];
                playerImageURL = [rowSearch getText:@"src=\"" endKey: @"\""];
            }
            else if ([section containsString:@"infobox-image"]) {
                imagePath = [rowSearch getText:@"href=\"" endKey: @"\""];
                playerImageURL = [rowSearch getText:@"src=\"" endKey: @"\""];
            }
        }
        if ([section containsString:@">Full name<"]) {
            NSString* name = [rowSearch getTextWithKey:@"<td" startKey:@">" endKey: @"<"];
            playerNameFull =  [name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            if ([playerNameFull containsString:@"Valentine"]) {
                int ian = 10;
            }
        }
        if ([section containsString:@">Born<"]) {
            NSString* value = [rowSearch getTextWithKey:@"bday" startKey:@">" endKey: @"<"]; // 1890-02-09
            birthDate = [self parseDateNumber:value];
            value = [rowSearch getTextWithKey:@"birthplace" startKey:@">" endKey: @"</td"]; // 1890-02-09
            birthPlace =  [StringSearch stringByStrippingHTML:value];
            birthPlace =  [birthPlace stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        }
        if ([section containsString:@">Date of birth<"]) {
            NSString* value = [rowSearch getTextWithKey:@"bday" startKey:@">" endKey: @"<"]; // 1890-02-09
            if (value != nil) {
                birthDate = [self parseDateNumber:value];
                if (birthDate == 0) {
                    birthDate = [value intValue];
                }
            }
            else {
                value = [rowSearch getTextWithKey:@"<td" startKey:@">" endKey: @"<"]; // 19 September 1949
                birthDate = [self parseDateNumber2:value];
                if (birthDate == 0) {
                    birthDate = [[value stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] intValue];
                }
                if (birthDate == 0) {
                    birthDate = [self parseDateNumber3:value];
                }
            }
            if (birthDate != 0 && playerImageURL.length != 0 && imagesOnly) {
                break;
            }

        }
        if ([section containsString:@">Died<"]) {
            NSString* value = [rowSearch getTextWithKey:@"Died" startKey:@">(" endKey: @")<"]; // 1890-02-09
            deathDate = [self parseDateNumber:value];
        }
        else if ([section containsString:@">Date of death<"] ||
            [section containsString:@">Died<"]) {
            NSString* value = [rowSearch getTextWithKey:@"<td" startKey:@"(" endKey: @")"]; // 1890-02-09
            if (value != nil) {
                deathDate = [self parseDateNumber:value];
            }
            else {
                value = [rowSearch getTextWithKey:@"<td" startKey:@">" endKey: @"<"]; // 19 September 1949
                deathDate = [self parseDateNumber2:value];
            }
            //playerNameFull =  [value stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        }
        if ([section containsString:@">Place of birth<"]) {
            NSString* value = [rowSearch getTextWithKey:@"<td" startKey:@">" endKey: @"</td"]; // 1890-02-09
            birthPlace =  [StringSearch stringByStrippingHTML:value];
            birthPlace =  [birthPlace stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        }
        if (role != 0) {
            if ([section containsString:@">Position(s)<"]) {
                NSString* value = [rowSearch getTextWithKey:@"<td" startKey:@">" endKey: @"</td"]; // 1890-02-09
                positionString =  [StringSearch stringByStrippingHTML:value];
                role = [Footballer getFootballerRoleFromString:positionString];
                int ian = 10;
            }
        }
        if ([section containsString:teamSearchString]) {
            if (![section containsString:@"anager"]) {
                NSString* value = [rowSearch getTextWithKey:@"<span" startKey:@">" endKey: @"</span"]; // 1911–1921
                NSArray* yearArray = [value componentsSeparatedByString:@"–"];
                if (yearArray.count == 2) {
                    startYear = [yearArray[0] intValue];
                    endYear = [yearArray[1] intValue];
                }
                else if (yearArray.count == 1) {
                    startYear = [yearArray[0] intValue];
                    endYear = startYear;
                }
            }
            int ian = 10;
        }

        //NSString* playerURLString = [rowSearch getSection:@"href=\"" endKey: @"\""];
        //NSString* nameString = [rowSearch getSection:@">" endKey: @"<"];
        //[self parsePlayer:nameString teamID:1 url:playerURLString];
        if (++test == 2) {
            //  break;
        }
        section = [stringProfile getText:@"<tr>" endKey: @"</tr"];
    }
    if (birthDate < 18500101 || birthDate > 20220101) {
        if (birthDate > 1850 && birthDate < 2000) {
            birthDate = birthDate * 1000 + 0101;
        }
        else {
            birthDate = [self fixBirthDate:playerName teamName:teamName];
        }
        if (birthDate == 0) {
            NSLog(@"No BirthDate %@", playerName);
            if ([playerName containsString:@"Valentine"]) {
                int ian = 10;
            }
        }
    }
    
    if (imagesOnly) {
        if (playerImageURL == nil || imagePath == nil) {
            return nil;
        }
        NSString* lastName = [[playerName componentsSeparatedByString:@" "] lastObject];
        NSString* dictionaryKey = [Footballer generateKey:birthDate lastName:lastName];
        Footballer* footballer = [model getFootballer:dictionaryKey];
        if (footballer == nil) {
            return nil;
        }
        if (footballer.imageURL.length != 0) {
            return nil;
        }
        
        //
        // Have image and a footballer without image
        if ([self processImageCopyright:imagePath footballer:footballer]) {
            //
            // Supported image copyright
            if ([self checkImageURL:playerImageURL]) {
                footballer.imageURL = playerImageURL;
                NSLog(@"Setting image for %@", footballer.name);
                return footballer;
            }
        }
        return nil;
    }
    
    
   /*
    NSLog(@"%@", playerName);
    NSLog(@"\t%@", playerNameFull);
    NSLog(@"\t%d-%@-%d", birthDate, birthPlace, deathDate);
    NSLog(@"\t%d-%d:%d", startYear, endYear, role);
*/
    static int countNewFootballers = 0;
    static NSArray* clubFootballers = nil;
    if (clubFootballers == nil) {
        clubFootballers = [model getClubPlayers:0];
    }
    
    if (birthDate != 0) {
        NSString* lastName = [[playerName componentsSeparatedByString:@" "] lastObject];
        NSString* dictionaryKey = [Footballer generateKey:birthDate lastName:lastName];
        bool found = NO;
        for (ClubFootballer* clubFootballer in clubFootballers) {
            if (birthDate == clubFootballer.footballer.birthDateNumber) {
                NSString* key2 = [clubFootballer.footballer generateKey];
                if ([key2 compare:dictionaryKey] == NSOrderedSame) {
                    //
                    // Merge footballers
                    clubFootballer.startYear = startYear;
                    clubFootballer.endYear = endYear;
                    if (deathDate != 0) {
                        clubFootballer.footballer.deathDateNumber = deathDate;
                    }
                    if (clubFootballer.footballer.birthDateNumber == 0) {
                        clubFootballer.footballer.birthDateNumber = birthDate;
                    }
                    if (clubFootballer.footballer.role == 0) {
                        clubFootballer.footballer.role = role;
                    }
                    if (clubFootballer.footballer.birthPlace.length == 0) {
                        clubFootballer.footballer.birthPlace = birthPlace;
                    }
                    if (clubFootballer.footballer.nameFull.length == 0) {
                        clubFootballer.footballer.nameFull = playerNameFull;
                    }

                    found = YES;
                    
                    if (clubFootballer.footballer.imageURL.length == 0 && imagePath != nil && playerImageURL != nil) {
                        if ([self processImageCopyright:imagePath footballer:clubFootballer.footballer]) {
                            //
                            // Supported image copyright
                            if ([self checkImageURL:playerImageURL]) {
                                clubFootballer.footballer.imageURL = playerImageURL;
                            }
                        }
                    }
                    break;
                }
                else {
                    int ina = 10;
                }
            }
        }
        if (!found) {
            //
            // Add club footballer
            ClubFootballer* clubFootballer = [[ClubFootballer alloc] init];
            NSString* lastName = [[playerName componentsSeparatedByString:@" "] lastObject];
            if ([lastName containsString:@"Jr."] || [lastName containsString:@"Sr."]) {
                NSArray* components = [playerName componentsSeparatedByString:@" "];
                if (components.count > 2) {
                    lastName = components[components.count - 2];
                }
                else {
                    raise(SIGSTOP);
                }
            }
            NSString* dictionaryKey = [Footballer generateKey:birthDate lastName:lastName];
            clubFootballer.dictionaryKey = dictionaryKey;
            clubFootballer.startYear = startYear;
            clubFootballer.endYear = endYear;

            clubFootballer.footballer = [model getFootballer:clubFootballer.dictionaryKey];
            if (clubFootballer.footballer == nil) {
                if (birthDate == 0) {
                    int ian = 10;
                }
                clubFootballer.footballer = [[Footballer alloc] init];
                clubFootballer.footballer.birthDateNumber = birthDate;
                clubFootballer.footballer.name = playerName;
                [model addFootballerComplete:clubFootballer.footballer];
            }

            if (clubFootballer.footballer.deathDateNumber == 0) {
                clubFootballer.footballer.deathDateNumber = deathDate;
            }
            if (clubFootballer.footballer.birthDateNumber == 0) {
                clubFootballer.footballer.birthDateNumber = birthDate;
            }
            if (clubFootballer.footballer.role == 0) {
                clubFootballer.footballer.role = role;
            }
            if (clubFootballer.footballer.birthPlace.length == 0) {
                clubFootballer.footballer.birthPlace = birthPlace;
            }
            if (clubFootballer.footballer.nameFull.length == 0) {
                clubFootballer.footballer.nameFull = playerNameFull;
            }
            if (imagePath != nil && playerImageURL != nil) {
                if ([self processImageCopyright:imagePath footballer:clubFootballer.footballer]) {
                    //
                    // Supported image copyright
                    if ([self checkImageURL:playerImageURL]) {
                        clubFootballer.footballer.imageURL = playerImageURL;
                    }
                }
            }
            [model addClubFootballer:clubFootballer teamID:teamID];
            countNewFootballers++;
        }

    }
   // NSLog(@"Found New footballers: %d", countNewFootballers);

    /*
    
    //
    // Manually fix names
    NSArray* nameChanges = @[ @"Andrew Robertson", @"Andy Robertson",
                              @"Boudewijn Zenden", @"Bolo Zenden",
                              @"Jack Bamber", @"John Bamber",
                              @"Charlie Hewitt", @"Charles Hewitt",
                              @"Dick Allman", @"Messina Allman",
                              @"Harry Welfare", @"Henry Welfare",
                              @"Merfyn Jones", @"Mervyn Jones",
                              @"Leyton Maxwell", @"Layton Maxwell",
                              @"Tom Ince", @"Thomas Ince",
//                              @"", @"",
    ];
    
    for (int i = 0; i < nameChanges.count / 2; i++) {
        if ([playerName localizedCaseInsensitiveContainsString:nameChanges[i * 2]]) {
            playerName = nameChanges[i * 2 + 1];
            break;
        }
    }

    //
    // Find player in database
    int identInt = 0 ; // stringIdent.intValue;
    Footballer* footballer = nil;
    if (identInt != 0) {
       //  footballer = [model checkFootballer:identInt identKey:@"parseIndexLFC"];
    }
    else {
        footballer = [model checkFootballerName:playerName teamID:1];
    }
    
    
    if (footballer != nil) {
        //
        // Merge details
        footballer.nameFull = playerNameFull;
        footballer.role = [Footballer getFootballerRoleFromString:playerPosition];
        
        //
        // Get image copyright text
        if (playerImageURL != nil && imagePath != nil) {
            if ([self processImageCopyright:imagePath footballer:footballer]) {
                //
                // Supported image copyright
                footballer.imageURL = playerImageURL;
            }
        }

        
    }
    else {
        NSLog(@"Can't find footballer: %@", playerName);
    }
     */
    return nil;
    
}
/*
- (bool)processImageCopyright:(NSString*)imagePath footballer:(Footballer*)footballer {
    bool suportedImageFile = false;
    bool havePage = false;
    
    
    @try {
        NSString* fullURL = [NSString stringWithFormat:@"https://commons.wikimedia.org%@", imagePath];
        NSString* webPageString = [webPageManager getWebPage:fullURL];
        suportedImageFile = [self processImagePage:webPageString footballer:footballer];
        havePage = true;
    }
    @catch ( NSException *e ) {
    }
    
    if (!havePage) {
        //
        // Try alternative address
        @try {
            NSString* fullURL = [NSString stringWithFormat:@"https://en.wikipedia.org%@", imagePath];
            NSString* webPageString = [webPageManager getWebPage:fullURL];
            suportedImageFile = [self processImagePage:webPageString footballer:footballer];
            havePage = true;
        }
        @catch ( NSException *e ) {
        }
    }
    
    return suportedImageFile;
}

- (bool)processImagePage:(NSString*)webPageString footballer:(Footballer*)footballer {
    NSArray* copyrightClauses = @[ @"Non-free media information", @"Non-free use", @"copyright infringement"];
    for (NSString* clause in copyrightClauses) {
        if ([webPageString containsString:clause]) {
            return false;
        }
    }
    
    //
    // List of known license agreements
    NSArray* licenseAgreements = @[ @"NOT_under_a_free_license",
        @"public_domain", // 1
        @"Attribution-Share Alike 4.0 International",
        @"Attribution 2.0 Generic",
        @"Attribution 2.5 Generic",
        @"GNU Free Documentation License", //5
        @"Attribution-Share Alike 2.0 Generic",
        @"Attribution-Share Alike 3.0 Unported",
        @"Attribution-Share Alike 3.0 Australia",
        @"Attribution-Share Alike 2.5 Generic",
        @"Attribution-ShareAlike 3.0", // 10
        @"Attribution 3.0 Unported",
        @"Attribution 4.0 International",
        @"Attribution 3.0",
        @"Attribution-ShareAlike 2.0",
        @"Government Open Data License - India (GODL)", // 15
    ];
    
    for (int i = 2; i < licenseAgreements.count; i++) {
        NSString* agreementText = licenseAgreements[i];
        if ([webPageString containsString:agreementText]) {
            //
            // Get Author
            StringSearch* stringSearch = [StringSearch initWithString:webPageString];
            NSString* author = [stringSearch getTextWithKey:@"by <a" startKey:@"\">" endKey:@"</a>"];
            if (author == nil) {
                author = [stringSearch getText:@"th>Author</th><td>" endKey:@"</td>"];
                if (author == nil) {
                    NSString* authorCell = [stringSearch getTextWithKey:@">Author" startKey:@"<td>" endKey:@"</td>"];
                    if (authorCell == nil) {
                        author = [stringSearch getText:@"Original photograph by " endKey:@" published to"];
                        if (author == nil) {
                            author = [stringSearch getText:@"published by user " endKey:@" ("];
                            if (author == nil) {
                                return false;
                            }
                        }
                    }
                    else {
                        StringSearch* stringSearchCell = [StringSearch initWithString:authorCell];
                        author = [stringSearchCell getText:@"\">" endKey:@"</a>"];
                        if (author == nil) {
                            author = authorCell;
                        }
                    }
                }
            }
            [footballer setLicenseType:i];
            author = [StringSearch stringByStrippingHTML:author];
            author = [author stringByReplacingOccurrencesOfString:@"&#160;" withString:@""];
            author = [author stringByReplacingOccurrencesOfString:@"\"" withString:@""];
            NSRange rangeSearch;
            rangeSearch = [author
                           rangeOfString:@"\n"];
            if (rangeSearch.length != 0) {
                author = [author substringToIndex:rangeSearch.location];
            }
            author = [StringSearch stringByStrippingRegEx:author regex:@"[^\\x00-\\x7F]"];
            if (author.length == 0) {
                return false;
            }
            footballer.imageAttributation = author;
            return true;
        }
    }
}
*/
-(NSString *) stringByStrippingHTML:(NSString*)s {
    NSRange r;
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}


- (int)parseDateNumber:(NSString*)dateString {
    //
    // 2019-08-02
    NSString* dateNumberString = [dateString stringByReplacingOccurrencesOfString:@"-" withString:@""];
    int dateNumber = [dateNumberString intValue];
    return dateNumber;
}
- (int)parseDateNumber2:(NSString*)dateString {
    //
    // 15 August 1992
    static NSDateFormatter* dateFormatIn = nil;
    if (dateFormatIn == nil) {
        dateFormatIn = [[NSDateFormatter alloc] init];
        [dateFormatIn setDateFormat:@"dd MMMM yyyy"];
    }
    static NSCalendar* gregorianCalandar = nil;
    if (gregorianCalandar == nil) {
        gregorianCalandar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    }
    @try {
        NSDate* date = [dateFormatIn dateFromString:dateString];
        NSDateComponents *components = [gregorianCalandar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:date];
        return (int)([components year] * 10000 + [components month] * 100 + [components day]);
    }
    
    @catch ( NSException *e ) {
        // NSLog(@"Failed to parse date: %@", dateString);
        return 0;
    }
}

- (int)parseDateNumber3:(NSString*)dateString {
    static NSDateFormatter* dateFormatIn = nil;
    if (dateFormatIn == nil) {
        dateFormatIn = [[NSDateFormatter alloc] init];
        [dateFormatIn setDateFormat:@"MMMM yyyy"];
    }
    static NSCalendar* gregorianCalandar = nil;
    if (gregorianCalandar == nil) {
        gregorianCalandar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    }
    @try {
        NSDate* date = [dateFormatIn dateFromString:dateString];
        NSDateComponents *components = [gregorianCalandar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:date];
        return (int)([components year] * 10000 + [components month] * 100 + [components day]);
    }
    
    @catch ( NSException *e ) {
        // NSLog(@"Failed to parse date: %@", dateString);
        return 0;
    }

}


- (bool)checkImageURL:(NSString*)imageURL {
    NSArray* fileTypes = @[@".jpg", @".png", @".jpeg"];
    for (NSString* type in fileTypes) {
        NSRange rangeSearch;
        rangeSearch = [imageURL
                       rangeOfString:type options:NSBackwardsSearch | NSCaseInsensitiveSearch];
        if (rangeSearch.length != 0) {
            if (rangeSearch.location == imageURL.length - type.length) return true;
            NSLog(@"Failed to verify url: %@", imageURL);
            return false;
        }
    }
    return false;
}

- (bool)processImageCopyright:(NSString*)imagePath footballer:(Footballer*)footballer {
    NSString* fullURL = [NSString stringWithFormat:@"https://commons.wikimedia.org%@", imagePath];
    if ([imagePath localizedCaseInsensitiveContainsString:@"Dickie_Morris.jpg"]) {
        fullURL = @"https://en.wikipedia.org/wiki/File:Dickie_Morris.jpg";
        // https://en.wikipedia.org/wiki/File:Dickie_Morris.jpg
    }

    NSString* dataKey =  [self generateDataKey:fullURL];
    
    NSString* webPageString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:CopyrightFilePath dataKey:dataKey];
    if (webPageString == nil) {
        webPageString = [webPageManager getWebPage:fullURL];
        [fileManager savePersistedString:dataPath dataName:FilePath dataType:CopyrightFilePath dataKey:dataKey dataString:webPageString];
    }
 
    if (webPageString == nil || [webPageString localizedCaseInsensitiveContainsString:@"No file by this name exists"]) {
        return false;
    }
    
    //
    // Check for images marked not in the public domain
    // String[] copyrightClauses = { "NOT under a free license", "Non-free media", "copyright infringement"};
    NSArray* copyrightClauses = @[@"Non-free media information", @"Non-free use", @"copyright infringement"];
    for (NSString* clause in copyrightClauses) {
        if ([webPageString localizedCaseInsensitiveContainsString:clause]) {
            return false;
        }
    }
    
    //
    // List of known license agreements
    NSArray* licenseAgreements = @[@"NOT_under_a_free_license",
                                   @"public_domain", // 1
                                   @"Attribution-Share Alike 4.0 International",
                                   @"Attribution 2.0 Generic",
                                   @"Attribution 2.5 Generic",
                                   @"GNU Free Documentation License", //5
                                   @"Attribution-Share Alike 2.0 Generic",
                                   @"Attribution-Share Alike 3.0 Unported",
                                   @"Attribution-Share Alike 3.0 Australia",
                                   @"Attribution-Share Alike 2.5 Generic",
                                   @"Attribution-ShareAlike 3.0", // 10
                                   @"Attribution 3.0 Unported",
                                   @"Attribution 4.0 International",
                                   @"Attribution 3.0",
                                   @"Attribution-ShareAlike 2.0",
                                   @"for any purpose, provided that", // 15
                                   @"Attribution-Share Alike 1.0 Generic",
                                   @"Attribution 1.0 Generic",
                                   @"Open Government Licence version 1.0",
                                   @"Free Art License",
                                   @"Attribution-Share Alike 2.5 Spain", //20
    ];
    for (int i = 2; i < licenseAgreements.count; i++) {
        if ([webPageString localizedCaseInsensitiveContainsString:@"Bingham"]) {
            int test = 10;
        }
        
        if ([webPageString localizedCaseInsensitiveContainsString:licenseAgreements[i]]) {
            //
            // Get Author
            StringSearch* stringAuthor = [StringSearch initWithString:webPageString];
            NSString* author = [stringAuthor getText:@"th>Author</th><td>" endKey:@"</td>"];
            if (author == nil) {
                NSString* authorCell = [stringAuthor getTextWithKey:@">Author" startKey:@"<td>" endKey:@"</td>"];
                if (authorCell == nil) {
                    author = [stringAuthor getText:@"Original photograph by " endKey:@" published to"];
                    if (author == nil) {
                        bool t1 = [webPageString localizedCaseInsensitiveContainsString:@"Author"];
                        return false;
                    }
                }
                else {
                    StringSearch* stringAuthorCell = [StringSearch initWithString:authorCell];
                    author = [stringAuthorCell getText:@"\">" endKey:@"</a>"];
                    if (author == nil) {
                        author = authorCell;
                    }
                    if ([author containsString:@"<"]) {
                        return false;
                    }
                }
            }
            author = [StringSearch stringByStrippingHTML:author];
            author = [author stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
            author = [author stringByTrimmingCharactersInSet:
                      [NSCharacterSet whitespaceAndNewlineCharacterSet]];

            // NSLog(@"imageAttributation: <%@>", author);
            footballer.imageAttributation = author;
            footballer.licenseType = i;
            return true;
        }
    }
    
    //
    // Check public domain
    if ([webPageString localizedCaseInsensitiveContainsString:licenseAgreements[1]]) { // No license
        footballer.licenseType = 1;
        return true;
    }
    else if ([webPageString localizedCaseInsensitiveContainsString:@"now in the public domain"]) { // No license
        footballer.licenseType = 1;
        return true;
    }
    else if ([webPageString localizedCaseInsensitiveContainsString:@"for any purpose"]) { // No license
        footballer.licenseType = 1;
        return true;
    }

    
    NSLog(@"Failed to find License Agreement:%@", fullURL);
    return false;
    
}

- (NSString*)generateDataKey:(NSString*)urlString {
    
    //
    // URL: https://en.wikipedia.org/wiki/List_of_Liverpool_F.C._players
    NSString* key = @"wiki/";
    NSRange range = [urlString rangeOfString:key];
    if (range.length == 0) {
        NSLog(@"Cant find key in URL: %@", urlString);
        return nil;
    }
    return [urlString substringFromIndex:range.location + key.length];
}

- (int)fixBirthDate:(NSString*)playerName teamName:(NSString*)teamName  {
    if ([teamName compare:@"Reading"] == NSOrderedSame) {
        if ([playerName containsString:@"Jock Allan"]) return 18910101;
        if ([playerName containsString:@"Phil Bach"]) return 18720901;
        if ([playerName containsString:@"Bob Barr"]) return 18650401;
        if ([playerName containsString:@"Ron Bayliss"]) return 19440709;
        if ([playerName containsString:@"John Boden"]) return 18820101;
        if ([playerName containsString:@"Richard Brookes"]) return 18880101;
        if ([playerName containsString:@"Harry Bruce"]) return 19050101;
        if ([playerName containsString:@"Ben Butler"]) return 18870101;
        if ([playerName containsString:@"John Comrie"]) return 18890101;
        if ([playerName containsString:@"Bob Gordon"]) return 18700101;
        if ([playerName containsString:@"Harry Grundy"]) return 18830315;
        if ([playerName containsString:@"Uwe Hartenberger"]) return 19680201;
        if ([playerName containsString:@"Edward Hatton"]) return 18750101;
        if ([playerName containsString:@"Albert Hayhurst"]) return 19050917;
        if ([playerName containsString:@"Jock Johnstone"]) return 18960101;
        if ([playerName containsString:@"Abe Jones Jr."]) return 18990401;
        if ([playerName containsString:@"Fred Jones"]) return 18670101;
        if ([playerName containsString:@"Tony McPhee"]) return 19140401;

        if ([playerName containsString:@"Leslie Murphy"]) return 18900101;
        if ([playerName containsString:@"Andy Reid"]) return 19060101;
        if ([playerName containsString:@"H. Rossiter"]) return 18760101;
        if ([playerName containsString:@"Jack Smart"]) return 18850101;
        if ([playerName containsString:@"Jimmy Wilde"]) return 18940101;
        if ([playerName containsString:@"David Willis"]) return 18810701;
    }
    return 0;
}



@end
