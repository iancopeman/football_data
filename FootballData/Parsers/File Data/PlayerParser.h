//
//  PlayerParser.h
//  FootballData
//
//  Created by Ian Copeman on 23/11/2021.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PlayerParser : NSObject
- (void)parseAll;

@end

NS_ASSUME_NONNULL_END
