//
//  TeamsViewController.m
//  FootballData
//
//  Created by Ian Copeman on 16/09/2021.
//

#import "TeamsViewController.h"
#import "TeamParser.h"
#import "MatchParser.h"
#import "FootballModel.h"
#import "APIFootballParser.h"
#import "FootballDataConstants.h"

@interface TeamsViewController ()
@property (weak) IBOutlet NSButton *buttonExportMatches;
@property (weak) IBOutlet NSButton *buttonExportPlayers;
@property (weak) IBOutlet NSButton *buttonExportPlayersTest;
@property (weak) IBOutlet NSComboBox *comboMatchYear;

@end

@implementation TeamsViewController {
    bool loadedTeams;
    bool loadedPlayers;
    bool loadedMatches;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
    
    int selectedSeason = [[[NSUserDefaults standardUserDefaults] objectForKey:@"MatchYearSelected"] intValue];
    int firstYear = 1975;
    int lastYear = CURRENT_SEASON;
    for (int i = firstYear; i <= lastYear; i++) {
        [self.comboMatchYear addItemWithObjectValue:[NSString stringWithFormat:@"%d", i]];
    }
    if (selectedSeason >= firstYear && selectedSeason <= lastYear) {
        [self.comboMatchYear selectItemAtIndex:selectedSeason - firstYear];
    }
    else {
        [self.comboMatchYear selectItemAtIndex:0];
    }

}

- (IBAction)loadTeamsPushed:(id)sender {
    FootballModel* model = [FootballModel getModel];
    [model loadTeams];
    loadedTeams = YES;
}

- (IBAction)reportTeamsPushed:(id)sender {
    FootballModel* model = [FootballModel getModel];
    [model reportNewTeams];
}

- (IBAction)buttonSaveTeamCSVPushed:(id)sender {
    NSColor* test = [NSColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1.0];

    TeamParser* teamParser = [[TeamParser alloc] init];
    [teamParser saveTeamFile];
}

- (IBAction)loadMatchesPushed:(id)sender {
    FootballModel* model = [FootballModel getModel];
    [model loadMatches];
    loadedMatches = YES;
    if (loadedTeams && loadedPlayers && loadedMatches) {
        self.buttonExportMatches.enabled = YES;
        self.buttonExportPlayers.enabled = YES;
        self.buttonExportPlayersTest.enabled = YES;
    }

}
- (IBAction)loadMatchesSeasonPushed:(id)sender {
    FootballModel* model = [FootballModel getModel];
    NSInteger seasonInt = [self.comboMatchYear.objectValueOfSelectedItem intValue];
    [[NSUserDefaults standardUserDefaults] setInteger:seasonInt forKey:@"MatchYearSelected"];
    
    [model loadMatchesSeason:(int)seasonInt];
    loadedMatches = YES;
    if (loadedTeams && loadedPlayers && loadedMatches) {
        self.buttonExportMatches.enabled = YES;
        self.buttonExportPlayers.enabled = YES;
        self.buttonExportPlayersTest.enabled = YES;
    }
}
- (IBAction)loadMatchesTestPushed:(id)sender {
    FootballModel* model = [FootballModel getModel];
    NSInteger seasonInt = [self.comboMatchYear.objectValueOfSelectedItem intValue];
    [[NSUserDefaults standardUserDefaults] setInteger:seasonInt forKey:@"MatchYearSelected"];
    
    [model loadMatchesTest];
    loadedMatches = YES;
    if (loadedTeams && loadedPlayers && loadedMatches) {
        self.buttonExportMatches.enabled = YES;
        self.buttonExportPlayers.enabled = YES;
        self.buttonExportPlayersTest.enabled = YES;
    }
}
- (IBAction)exportCSVPushed:(id)sender {
    FootballModel* model = [FootballModel getModel];
    [model exportCSVMatches];
    [model exportCSVMatchStats];
}
- (IBAction)exportPlayersPushed:(id)sender {
    FootballModel* model = [FootballModel getModel];
    int playerNumber = [model getFootballerCount];
    if (playerNumber < 10000) {
        NSLog(@"Export Player ABORT!!");
        NSLog(@"Have players been loaded?");
        return;
    }
    [model exportCSVPlayers:NO];
    [model exportCSVManagers:NO];
}
- (IBAction)exportPlayersTestPushed:(id)sender {
    FootballModel* model = [FootballModel getModel];
    [model exportCSVPlayers:YES];
}

- (IBAction)testPushed:(id)sender {


    
    /*
 NSString* footballerKey = @"Martyn_19660811";
    FootballModel* model = [FootballModel getModel];
    int value = [model getFootballerID:footballerKey];
*/
    

    
/*
    NSString* test = @"ÀÁÅ É Ð ÓÖØ Ü Þ ß àáâã èéêë íîï ð ñ óôöø úü ý";
    NSLog(@"%@",test);
    NSData *decode = [test dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString* test2 = [[NSString alloc] initWithData:decode encoding:NSASCIIStringEncoding];
    NSLog(@"%@",test2);
*/
}
- (IBAction)loadPlayersPushed:(id)sender {
    FootballModel* model = [FootballModel getModel];
    [model loadFootballers];
    loadedPlayers = YES;

}
- (IBAction)parsePlayeyIDsPushed:(id)sender {
    APIFootballParser* footballParser = [[APIFootballParser alloc] init];
    [footballParser parsePlayers:CompetitionPremierLeague];
}

@end
