//
//  PlayerParser.m
//  FootballData
//
//  Created by Ian Copeman on 23/11/2021.
//

#import "PlayerParser.h"
#import "FootballModel.h"
#import "WebPageManager.h"
#import "StringSearch.h"
#import "NSString+CSVParser.h"

#define FilePath (@"/Devlex/Players/")
#define FilePathManagers (@"/Devlex/Managers/")


@implementation PlayerParser {
    WebPageManager* webPageManager;
    FootballModel* model;
    FileManager* fileManager;
    NSString* dataPath;
}

- (void)parseAll {
    model = [FootballModel getModel];
    fileManager = [[FileManager alloc] init];
    dataPath  = [[NSUserDefaults standardUserDefaults] objectForKey:@"dataPath"];
    webPageManager = [WebPageManager getWebPageManager];
    char letterChar = 'A';

    while (letterChar < 'Z' + 1) {
        NSString* fileName = [NSString stringWithFormat:@"%c.csv", letterChar];
        if ([fileManager doesFileExist:dataPath dataName:FilePath dataType:nil dataKey:fileName]) {
            NSLog(@"* Parsing File: %@", fileName);
            NSString* csvString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:nil dataKey:fileName];
            [self loadPlayersFromCSVString:csvString];
        }
        
        letterChar++;
        // break;
    }

    //
    // Manager file
    NSString* fileName = @"Managers.csv";
    if ([fileManager doesFileExist:dataPath dataName:FilePathManagers dataType:nil dataKey:fileName]) {
        NSLog(@"* Parsing File: %@", fileName);
        NSString* csvString = [fileManager loadPersistedString:dataPath dataName:FilePathManagers dataType:nil dataKey:fileName];
        NSArray* rows = [csvString csvRows];
        int count = 0;
        for (NSArray *row in rows){
            ++count;
            NSString* nameString = row[0];
            if (nameString.length == 3 && [nameString containsString:@"Key"]) continue;
            Manager* manager = [Manager managerFromCSVArray:row];
            
            [model addManagerComplete:manager];
            
            
        }
       //  [self loadPlayersFromCSVString:csvString];
    }

    
}

- (void)loadPlayersFromCSVString:(NSString*)csvString {
    NSArray* rows = [csvString csvRows];
    int count = 0;
    for (NSArray *row in rows){
        ++count;
        NSString* nameString = row[0];
        if (nameString.length == 3 && [nameString containsString:@"Key"]) continue;
        Footballer* footballer = [Footballer footballerFromCSVArray:row];
        /*
        if ([footballer.dictionaryKey containsString:@"?"]) {
            int ian = 10;
            NSLog(@"Delete footballer: %@", footballer.dictionaryKey);

        }
         */
        NSString* stringCheck = [footballer.dictionaryKey substringFromIndex:footballer.dictionaryKey.length - 2];
        if ([stringCheck compare:@"_0"] != NSOrderedSame) {
            [model addFootballerComplete:footballer];
        }
        else {
            NSLog(@"Deleting footballer: %@", footballer.dictionaryKey);
        }

        
 
    }
}

@end
