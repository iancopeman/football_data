//
//  MatchParser.m
//  FootballData
//
//  Created by Ian Copeman on 12/11/2021.
//

#import "MatchParser.h"
#import "FootballModel.h"
#import "WebPageManager.h"
#import "StringSearch.h"
#import "NSString+CSVParser.h"
#import "FootballDataConstants.h"

#define FilePath (@"/Devlex/Matches/")

@implementation MatchPurgeItem
@end

@implementation MatchParser {
    WebPageManager* webPageManager;
    FootballModel* model;
    FileManager* fileManager;
    NSString* dataPath;
}

- (void)parseTest {
   // [self purgeFiles:(int)1870 endSeason:CURRENT_SEASON];
    [self parse:1992 endSeason:1992];
}

- (void)parseSeason:(int)season {
    [self parse:season endSeason:season];
}

- (void)parseAll {
    //
    // Start at year 1870
    [self parse:1870 endSeason:CURRENT_SEASON];
}

- (void)parse:(int)startSeason endSeason:(int)endSeason {
    model = [FootballModel getModel];
    fileManager = [[FileManager alloc] init];
    dataPath  = [[NSUserDefaults standardUserDefaults] objectForKey:@"dataPath"];
    webPageManager = [WebPageManager getWebPageManager];

    //
    // Directories
    NSArray* subDirectories = @[ @"Complete", @"Partial", @"Basic"];
    
    //
    // Get file, don't download
    //
    // Start at year 1870
    int decade = startSeason / 10 * 10;
    int year = startSeason % 10;
    int season = decade + year;
    
    NSString* directoryPath = [dataPath stringByAppendingPathComponent:FilePath];

    while (season <= endSeason && season <= CURRENT_SEASON) {
        NSLog(@"*** Season %d ***", season);
        int nextSeason = (decade + year + 1) % 100;
        NSString* seasonString = [NSString stringWithFormat:@"%ds/%d-%02d/", decade, decade + year, nextSeason];
        
        
          NSURL* directoryURL = [NSURL URLWithString:[directoryPath  stringByAppendingPathComponent:seasonString]];
            
            NSError* error;
            NSArray * dirs = [[NSFileManager defaultManager]  contentsOfDirectoryAtURL:directoryURL
                                                            includingPropertiesForKeys:@[]
                                                                               options:NSDirectoryEnumerationSkipsSubdirectoryDescendants | NSDirectoryEnumerationSkipsHiddenFiles
                                                                                 error:&error];
            [dirs enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                NSURL* filename = (NSURL *)obj;
                if (filename.hasDirectoryPath) {
                    NSString *lastPathComponent = [filename lastPathComponent];
                    CompetitionType competitionType = [Match getCompetitionFromShortName:lastPathComponent];
                    if (competitionType != CompetitionUnknown) {
                        //
                        // Directories are loaded in order of subDirectories array
                        for (NSString* subDirectoryString in subDirectories) {
                            NSURL* subDirectory = [filename URLByAppendingPathComponent:subDirectoryString];
                            [self loadMatchesFromURL:subDirectory competitionType:competitionType];
                        }
                    }
                    else {
                        NSLog(@"Unsupported competition string: %@", lastPathComponent);
                    }
                }
                else {
                    //
                    // Check for MatchStats
                    int ian = 10;
     
                    NSString *lastPathComponent = [filename lastPathComponent];
                    if (![lastPathComponent containsString:@"MatchStats"]) {
                        CompetitionType competitionType = [Match getCompetitionFromShortName:lastPathComponent];
                        if (competitionType != CompetitionUnknown) {
                            [self loadMatchesFromFile:filename competitionType:competitionType season:season];
                        }
                        else {
                            NSLog(@"Unsupported competition string: %@", lastPathComponent);
                        }
                    }

                }
            }];
        
        
         
        year++;
        if (year == 10) {
            decade += 10;
            year = 0;
        }
        season = decade + year;

    }

    //
    // Parse MatchStats
    startSeason = MAX(startSeason, MATCH_STATS_FIRST_SEASON); // First year season data available
    decade = startSeason / 10 * 10;
    year = startSeason % 10;
    season = decade + year;
    while (season <= endSeason && season <= CURRENT_SEASON) {
        NSLog(@"*** MatchStats Season %d ***", season);
        int nextSeason = (decade + year + 1) % 100;
        NSString* seasonString = [NSString stringWithFormat:@"%ds/%d-%02d/", decade, decade + year, nextSeason];
        NSURL* directoryURL = [NSURL URLWithString:[directoryPath  stringByAppendingPathComponent:seasonString]];
        NSURL* fileURL = [directoryURL URLByAppendingPathComponent:[NSString stringWithFormat:@"%@%d.csv", [MatchStats getBlobName], season]];
        NSString* fileString = [fileManager loadPersistedString:fileURL];
        if (fileString != nil) {
            [MatchStats loadMatchStatsFromString:fileString season:season];
        }
        
        year++;
        if (year == 10) {
            decade += 10;
            year = 0;
        }
        season = decade + year;
    }
        
    
}

- (void)loadMatchesFromURL:(NSURL*)directoryURL competitionType:(CompetitionType)competitionType {
    NSError* error;
    NSArray * dirs = [[NSFileManager defaultManager]  contentsOfDirectoryAtURL:directoryURL
                                                    includingPropertiesForKeys:@[]
                                                                       options:NSDirectoryEnumerationSkipsSubdirectoryDescendants | NSDirectoryEnumerationSkipsHiddenFiles
                                                                         error:&error];
    [dirs enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSURL* filename = (NSURL *)obj;
        if (!filename.hasDirectoryPath) {
            if ([self->fileManager doesFileExist:filename]) {
                @try {
                    NSString* matchJSONString = [self->fileManager loadPersistedString:filename];
                    if (matchJSONString != nil) {
                        Match* match = [[Match alloc] init];
                        [match initFromJSONString:matchJSONString];
                        match.fileURL = filename;
                    }
                }
                @catch ( NSException *e ) {
                    int ian = 10;
                }
            }
        }
    }];
    
}

- (void)loadMatchesFromFile:(NSURL*)fileURL competitionType:(CompetitionType)competitionType season:(int)season {
  //  if (competitionType != CompetitionPremierLeague) return;
    // if (competitionType != CompetitionChampionship) return;

    bool resaveFile = false;
    
    NSString* fileString = [fileManager loadPersistedString:fileURL];
    if (fileString == nil) {
        return;
    }
    NSArray* rows = [fileString csvRows];
    int count = 0;
    for (NSArray *row in rows){
        if (row.count != 8) {
            break;
        }
        NSString* dateString = row[0];
        if ([dateString containsString:@"Date"]) continue;
        NSString* team1String = row[1];
        NSString* scoreString = row[2];
        NSString* team2String = row[3];
        NSString* cFlagString = row[4];
        NSString* venueString = row[5];
        NSString* penaltiesString = row[6];
        NSString* mFlagString = row[7];
             
        //
        // Parse Date
        int matchDateNumber = [dateString intValue];
        
        if (DATE_NUMBER_TO_YEAR(matchDateNumber) != season &&
            DATE_NUMBER_TO_YEAR(matchDateNumber) != season + 1) {
            resaveFile = true;
            if (matchDateNumber > 750) {
                matchDateNumber += season * 10000;
            }
            else {
                matchDateNumber += (season + 1) * 10000;
            }
            int ian = 10;
        }
        
        //
        // Get match key
        int team1ID = [model addTeam:team1String season:season];
        int team2ID = [model addTeam:team2String season:season];
        NSString* matchKey = [Match makeMatchKeyFromDateNumber:matchDateNumber team1:team1ID team2:team2ID];
        if (matchDateNumber == 19961228) {
            int ian = 10;
        }

        //
        // Check match
        Match* match = [model getMatch:matchKey];
        if (!match) {
            match = [[Match alloc] init];
            
            match.competition = competitionType;
            [match setMatchTeamsAndDateNumber:matchDateNumber team1:team1ID team2:team2ID];
            
            //
            // Get competiton flag
            match.competitionFlag = [cFlagString intValue];
            
            //
            // Venue
            if (venueString.length != 0) {
                match.locationID = [model addLocation:venueString];
            }
            else {
                //
                // No specified location, put first team as home team
                match.homeTeamID = team1ID;
            }
            
            //
            // Score
            if (scoreString.length == 0) {
                //
                // Future match
                match.matchState = MatchStateFutureMatch;
            }
            else if ([scoreString containsString:@":"]) {
                //
                // Future match with time
                match.matchState = MatchStateFutureMatch;
                NSArray* timeArray = [scoreString componentsSeparatedByString:@":"];
                int hour = [timeArray[0] intValue];
                int minute = [timeArray[1] intValue];
                match.timeNumber = TIME_NUMBER_CREATE(hour, minute);
            }
            else {
                NSArray* scoreArray = [scoreString componentsSeparatedByString:@"-"];
                int score1 = [scoreArray[0] intValue];
                int score2 = [scoreArray[1] intValue];
                [match setScore:score1 teamID:team1ID];
                [match setScore:score2 teamID:team2ID];
                match.matchState = MatchStateResult;
            }
            
            [match setSeason:season];
            
            [model addMatch:match];

            //
            // Add match events
            if (match.matchState == MatchStateResult) {
                [self loadMatchEventsFromFile:fileURL match:match];
            }

            
        }

        //
        // Set home team
        if (IS_LEAGUE_COMPETITION(competitionType)) {
            if (match.homeTeamID == 0) {
                match.homeTeamID = team1ID;
                // [model matchIsModified:match];
            }
        }
        
        /*
         if (count == 1500) {
         NSLog(@"Last match parsed key: %@", match.matchKey);
         
         break;
         }
         */
    }

    if (resaveFile) {
        NSLog(@"* Resaving corrupt file: %@", fileURL);
        NSMutableString* csvString = [NSMutableString stringWithCapacity:10000];
        [csvString appendString:@"DateString,Team1,Score,Team2,C Flag,Venue,Penalties,M Flag\n"];
        
        NSArray* rows = [fileString csvRows];
        int count = 0;
        for (NSArray *row in rows){
            if (row.count != 8) {
                break;
            }
            NSString* dateString = row[0];
            if ([dateString containsString:@"Date"]) continue;
            NSString* team1String = row[1];
            NSString* scoreString = row[2];
            NSString* team2String = row[3];
            NSString* cFlagString = row[4];
            NSString* venueString = row[5];
            NSString* penaltiesString = row[6];
            NSString* mFlagString = row[7];
            
            //
            // Parse Date
            int matchDateNumber = [dateString intValue];
            
            if (DATE_NUMBER_TO_YEAR(matchDateNumber) != season &&
                DATE_NUMBER_TO_YEAR(matchDateNumber) != season + 1) {
                resaveFile = true;
                if (matchDateNumber > 750) {
                    matchDateNumber += season * 10000;
                }
                else {
                    matchDateNumber += (season + 1) * 10000;
                }
            }
            NSString* stringEncode = [NSString stringWithFormat:
                                      @"%d,%@,%@,%@,%@,%@,%@,%@\n",
                                      matchDateNumber,
                                      team1String,
                                      scoreString,
                                      team2String,
                                      cFlagString,
                                      venueString,
                                      penaltiesString,
                                      mFlagString];
            
            [csvString appendString:stringEncode];
        }
        [fileManager savePersistedString:fileURL dataString:csvString];
    }
}

- (void)loadMatchEventsFromFile:(NSURL*)fileURL match:(Match*)match {
    NSURL* matchFile = [fileURL URLByDeletingPathExtension];
    NSString* matchFileName = [match getFileName];
    matchFile = [matchFile URLByAppendingPathComponent:matchFileName];
    if ([fileManager doesFileExist:matchFile]) {
        NSString* matchJSONString = [fileManager loadPersistedString:matchFile];
        if (matchJSONString == nil) {
            return;
        }
        [match initFromJSONString:matchJSONString];
    }

}

- (void)purgeFiles:(int)startSeason endSeason:(int)endSeason {
    model = [FootballModel getModel];
    fileManager = [[FileManager alloc] init];
    dataPath  = [[NSUserDefaults standardUserDefaults] objectForKey:@"dataPath"];
    webPageManager = [WebPageManager getWebPageManager];
    NSUInteger matchCount = [model getMatchCount];
    
    //
    // Directories
    NSArray* subDirectories = @[ @"Complete", @"Partial", @"Basic"];
    
    //
    // Get file, don't download
    //
    // Start at year 1870
    int decade = startSeason / 10 * 10;
    int year = startSeason % 10;
    int season = decade + year;
    
    NSString* directoryPath = [dataPath stringByAppendingPathComponent:FilePath];
    NSFileManager *fileManager = [NSFileManager defaultManager];

    while (season <= endSeason && season <= CURRENT_SEASON) {
        NSLog(@"*** Season %d ***", season);
        int nextSeason = (decade + year + 1) % 100;
        NSString* seasonString = [NSString stringWithFormat:@"%ds/%d-%02d/", decade, decade + year, nextSeason];
        
        
        NSURL* directoryURL = [NSURL URLWithString:[directoryPath  stringByAppendingPathComponent:seasonString]];
        
        NSError* error;
        NSArray * dirs = [[NSFileManager defaultManager]  contentsOfDirectoryAtURL:directoryURL
                                                        includingPropertiesForKeys:@[]
                                                                           options:NSDirectoryEnumerationSkipsSubdirectoryDescendants | NSDirectoryEnumerationSkipsHiddenFiles
                                                                             error:&error];
        [dirs enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NSURL* filename = (NSURL *)obj;
            if (filename.hasDirectoryPath) {
                NSString *lastPathComponent = [filename lastPathComponent];
                
                  
                CompetitionType competitionType = [Match getCompetitionFromShortName:lastPathComponent];
                if (competitionType != CompetitionUnknown) {

                    NSURL* subDirectory = [filename URLByAppendingPathComponent:@"Complete"];
                    NSArray* completeArray = [self loadMatchInfoFromURL:subDirectory competitionType:competitionType];
                    
                    subDirectory = [filename URLByAppendingPathComponent:@"Partial"];
                    NSArray* partialArray = [self loadMatchInfoFromURL:subDirectory competitionType:competitionType];

                    //
                    // Compare
                    for (MatchPurgeItem* itemTest in partialArray) {
                        for (MatchPurgeItem* itemComplete in completeArray) {
                            if ([itemTest.keyName compare:itemComplete.keyName] == NSOrderedSame &&
                                itemTest.keyDate >= itemComplete.keyDate - 1 && itemTest.keyDate <= itemComplete.keyDate + 1) {
                                //
                                // Remove file
                                NSLog(@"Remove duplicate file: %@", itemTest.fileURL);
                                [fileManager removeItemAtURL:itemTest.fileURL  error:NULL];
                            }
                        }
                    }
                    
                }
                else {
                    NSLog(@"Unsupported competition string: %@", lastPathComponent);
                }
            }
        }];
        
        
        
        year++;
        if (year == 10) {
            decade += 10;
            year = 0;
        }
        season = decade + year;
        
    }
    
    int ian = 10;
}

- (NSArray*)loadMatchInfoFromURL:(NSURL*)directoryURL competitionType:(CompetitionType)competitionType {
    NSMutableArray* matchArray = [NSMutableArray arrayWithCapacity:100];
    NSError* error;
    NSArray * dirs = [[NSFileManager defaultManager]  contentsOfDirectoryAtURL:directoryURL
                                                    includingPropertiesForKeys:@[]
                                                                       options:NSDirectoryEnumerationSkipsSubdirectoryDescendants | NSDirectoryEnumerationSkipsHiddenFiles
                                                                         error:&error];
    [dirs enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSURL* filename = (NSURL *)obj;
        if (!filename.hasDirectoryPath) {
            if ([self->fileManager doesFileExist:filename]) {
                @try {
                    // 19940107_Coventry City_Newcastle United.json
                    NSString* lastPath = [filename lastPathComponent];
                    NSArray* items = [lastPath componentsSeparatedByString:@"_"];
                    MatchPurgeItem* purgeItem = [[MatchPurgeItem alloc] init];
                    purgeItem.keyDate = [items[0] intValue];
                    purgeItem.fileURL = filename;
                    purgeItem.keyName = [lastPath substringFromIndex:9];
                    [matchArray addObject:purgeItem];
                    int ian = 10;
                }
                @catch ( NSException *e ) {
                    int ian = 10;
                }
            }
        }
    }];
    
    return matchArray;
    
}



@end


