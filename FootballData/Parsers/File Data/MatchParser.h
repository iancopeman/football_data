//
//  MatchParser.h
//  FootballData
//
//  Created by Ian Copeman on 12/11/2021.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MatchParser : NSObject
- (void)parseAll;
- (void)parseTest;
- (void)parseSeason:(int)season;
@end

@interface MatchPurgeItem : NSObject
@property (strong, nonatomic) NSURL* fileURL;
@property (strong, nonatomic) NSString* keyName;
@property int keyDate;
@end



NS_ASSUME_NONNULL_END
