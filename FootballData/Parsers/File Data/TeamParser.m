//
//  TeamParser.m
//  FootballData
//
//  Created by Ian Copeman on 13/08/2021.
//

#import "TeamParser.h"
#import "FootballModel.h"
#import "WebPageManager.h"
#import "NSString+CSVParser.h"
#import "ESDParser.h"

#define FilePath (@"/Devlex/Teams/")
#define FileNameEnglandClubs (@"England/EnglandClubs.csv")
#define FileNameEuropeanClubs (@"EuropeanClubs.csv")


@implementation TeamParser {
    FootballModel* model;
    FileManager* fileManager;
    NSString* dataPath;
}

- (void)loadTeamFile {
    if (fileManager == nil) {
        model = [FootballModel getModel];
        fileManager = [[FileManager alloc] init];
        dataPath  = [[NSUserDefaults standardUserDefaults] objectForKey:@"dataPath"];
    }
    [self loadTeamFile:FileNameEnglandClubs englishTeams:YES];
    [self loadTeamFile:FileNameEuropeanClubs englishTeams:NO];
    NSLog(@"Database Teams: %d", [model getTeamCount]);
}

- (void)loadTeamFile:(NSString*)fileName englishTeams:(bool)englishTeams {
    NSString* webPageString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:nil dataKey:fileName];
    NSArray* rows = [webPageString csvRows];
    int count = 0;
    bool resaveFile = NO;
    for (NSArray *row in rows){
        ++count;
        int index = 0;
        NSString* nameString = row[index++];
        if ([nameString containsString:@"Team Name"]) continue;
        nameString = [ESDParser modifyTeamName:nameString];
        NSString* teamIDString = row[index++];
        NSString* shortNameString = row[index++];
        if (shortNameString.length == 0) shortNameString = @"";
        NSString* vShortNameString = row[index++];
        if (vShortNameString.length == 0) vShortNameString = @"";
        NSString* uefaString = row[index++];
        NSString* indexAPIString = row[index++];
        NSString* tmString = row[index++];
        NSString* latitudeString = row[index++];
        NSString* longitudeString = row[index++];
        NSString* colourString = row[index++];
        NSString* countryIDString = row[index++];
        int teamID = [teamIDString intValue];
        if (teamID == 0) {
            resaveFile = YES;
        }
        int parseUEFA = [uefaString intValue];
        int indexAPI = [indexAPIString intValue];
        int parseTM = [tmString intValue];
        int countryID = [countryIDString intValue];
        unsigned int colour = 0;
        if (colourString.length > 1) {
            [[NSScanner scannerWithString:colourString] scanHexInt:&colour];
        }
        [model addTeam:nameString teamID:teamID shortName:shortNameString vShortName:vShortNameString parseUEFA:parseUEFA indexAPI:indexAPI parseTM:parseTM latitudeString:latitudeString longitudeString:longitudeString countryID:countryID colour:colour];
        
    }
    
    if (resaveFile) {
        //
        // Be careful doing this!
        raise(SIGSTOP);

        NSMutableString* csvString = [NSMutableString stringWithCapacity:10000];
        [csvString appendString:@"Team Name,ID,Short Name,Initials,UEFA,API,TM,Latitude,Longitude,Colour,CountryID\n"];
        [model encodeTeamsToString:csvString englishTeams:englishTeams];
        [fileManager savePersistedString:dataPath dataName:FilePath dataType:nil dataKey:fileName dataString:csvString];

    }
}

- (void)saveTeamFile {
    //
    // Out of date structure!
    
    if (fileManager == nil) {
        model = [FootballModel getModel];
        fileManager = [[FileManager alloc] init];
        dataPath  = [[NSUserDefaults standardUserDefaults] objectForKey:@"dataPath"];
    }

    {
        NSMutableString* csvString = [NSMutableString stringWithCapacity:10000];
        [csvString appendString:@"Team Name,ID,Short Name,Initials,UEFA,API,TM,Latitude,Longitude,Colour,CountryID\n"];
        [model encodeTeamsToString:csvString englishTeams:YES];
        [fileManager savePersistedString:dataPath dataName:FilePath dataType:nil dataKey:FileNameEnglandClubs dataString:csvString];
    }
    
    {
        NSMutableString* csvString = [NSMutableString stringWithCapacity:10000];
        [csvString appendString:@"Team Name,ID,Short Name,Initials,UEFA,API,TM,Latitude,Longitude,Colour,CountryID\n"];
        [model encodeTeamsToString:csvString englishTeams:NO];
        [fileManager savePersistedString:dataPath dataName:FilePath dataType:nil dataKey:FileNameEuropeanClubs dataString:csvString];
    }
    
  }

@end
