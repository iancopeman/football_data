//
//  TeamParser.h
//  FootballData
//
//  Created by Ian Copeman on 13/08/2021.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TeamParser : NSObject
- (void)loadTeamFile;
- (void)saveTeamFile;

@end

NS_ASSUME_NONNULL_END
