//
//  v11ViewController.m
//  FootballData
//
//  Created by Ian Copeman on 13/09/2021.
//

#import "V11ViewController.h"
#import "v11Parser.h"

@interface V11ViewController ()

@end

@implementation V11ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
}

- (IBAction)parsePush:(id)sender {
  v11Parser* parser = [[v11Parser alloc] init];
  [parser parseAll];
}


@end
