//
//  FWPViewController.m
//  FootballData
//
//  Created by Ian Copeman on 15/11/2021.
//

#import "WebParsersViewController.h"
#import "FWPParser.h"
#import "TMParser.h"
#import "Match.h"
#import "LFCSeasonParser.h"
#import "v11Parser.h"
#import "WebPageManager.h"
#import "FootballModel.h"
#import "NSString+CSVParser.h"
#import "APIFootballParser.h"
#import "FootballDataConstants.h"

@interface WebParsersViewController ()

@property (weak) IBOutlet NSComboBox *comboCompetition;
@property (weak) IBOutlet NSComboBox *comboSeason;
@property (weak) IBOutlet NSComboBox *comboSeasonLFC;
@property (weak) IBOutlet NSComboBoxCell *comboCompetitionV11;
@property (weak) IBOutlet NSComboBox *comboSeasonV11;

@end

@implementation WebParsersViewController {
    TMParser* parserTM;
    NSArray* parseArrayTM;
    NSInteger selectedIndexTM;
    NSInteger selectedSeasonTM;
    
    LFCSeasonParser* seasonParserLFC;
    NSInteger selectedSeasonLFC;

    v11Parser* parserV11;
    NSArray* parseArrayV11;
    NSInteger selectedIndexV11;
    NSInteger selectedSeasonV11;

    int lastYearToParse;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    lastYearToParse = CURRENT_SEASON;
    
    
    //
    // TMParser
    parserTM = [[TMParser alloc] init];
    parseArrayTM = [parserTM getCompetionArray];
    for (int i = 0; i < parseArrayTM.count / 2; i++) {
        CompetitionType competition = [parseArrayTM[i * 2] intValue];
        NSString* name = [Match getCompetitionShortName:competition];
        [self.comboCompetition addItemWithObjectValue:name];
    }
    selectedIndexTM = [[[NSUserDefaults standardUserDefaults] objectForKey:@"TMParserIndex"] intValue];
    selectedSeasonTM = [[[NSUserDefaults standardUserDefaults] objectForKey:@"TMParserSeason"] intValue];
    [self setSelectionCombo:selectedIndexTM];
    
    
    //
    // LFCSeasonParser
    seasonParserLFC = [[LFCSeasonParser alloc] init];
    selectedSeasonLFC = [[[NSUserDefaults standardUserDefaults] objectForKey:@"LFCParserSeason"] intValue];
    for (int i = LIVERPOOL_PARSER_FIRST_SEASON; i <= lastYearToParse; i++) {
        [self.comboSeasonLFC addItemWithObjectValue:[NSString stringWithFormat:@"%d", i]];
    }
    if (selectedSeasonLFC >= LIVERPOOL_PARSER_FIRST_SEASON && selectedSeasonLFC <= lastYearToParse) {
        [self.comboSeasonLFC selectItemAtIndex:selectedSeasonLFC - LIVERPOOL_PARSER_FIRST_SEASON];
    }
    else {
        [self.comboSeasonLFC selectItemAtIndex:0];
        selectedSeasonLFC = LIVERPOOL_PARSER_FIRST_SEASON;
    }

    //
    // 11V11Parser
    parserV11 = [[v11Parser alloc] init];
    parseArrayV11 = [parserV11 getCompetionArray];
    for (int i = 0; i < parseArrayV11.count / 2; i++) {
        CompetitionType competition = [parseArrayV11[i * 2] intValue];
        NSString* name = [Match getCompetitionShortName:competition];
        [self.comboCompetitionV11 addItemWithObjectValue:name];
    }
    selectedIndexV11 = [[[NSUserDefaults standardUserDefaults] objectForKey:@"V11ParserIndex"] intValue];
    selectedSeasonV11 = [[[NSUserDefaults standardUserDefaults] objectForKey:@"V11ParserSeason"] intValue];
    [self setSelectionComboV11:selectedIndexV11];

}

#pragma mark - TMParser
- (IBAction)comboSelectionChanged:(id)sender {
    if (selectedIndexTM != self.comboCompetition.indexOfSelectedItem) {
        [self setSelectionCombo:self.comboCompetition.indexOfSelectedItem];
        selectedIndexTM = self.comboCompetition.indexOfSelectedItem;
    }
}
- (IBAction)comboSeasonChanged:(id)sender {
    selectedSeasonTM = [self.comboSeason.objectValueOfSelectedItem intValue];
}

- (void)setSelectionCombo:(NSInteger)selectedIndex {
    if (selectedIndexTM != self.comboCompetition.indexOfSelectedItem) {
        [self.comboCompetition selectItemAtIndex:selectedIndex];
        
        //
        // Load year selection
        // int currentSelection = [self.comboSeason.objectValueOfSelectedItem intValue];
        int startYear = [parseArrayTM[selectedIndex * 2 + 1] intValue];
        [self.comboSeason removeAllItems];
        for (int i = startYear; i <= lastYearToParse; i++) {
            [self.comboSeason addItemWithObjectValue:[NSString stringWithFormat:@"%d", i]];
        }
        if (selectedSeasonTM >= startYear && selectedSeasonTM <= lastYearToParse) {
            [self.comboSeason selectItemAtIndex:selectedSeasonTM - startYear];
        }
        else {
            [self.comboSeason selectItemAtIndex:0];
            selectedSeasonTM = startYear;
        }
    }
}
- (IBAction)parsePush:(id)sender {
    FWPParser* parser = [[FWPParser alloc] init];
    [parser parseAll];
}
- (IBAction)parseTMSeasonPush:(id)sender {
    [[NSUserDefaults standardUserDefaults] setInteger:selectedIndexTM forKey:@"TMParserIndex"];
    NSInteger seasonInt = [self.comboSeason.objectValueOfSelectedItem intValue];
    [[NSUserDefaults standardUserDefaults] setInteger:seasonInt forKey:@"TMParserSeason"];
    
    CompetitionType competition = [parseArrayTM[selectedIndexTM * 2] intValue];
    [parserTM parseCompetition:competition season:seasonInt];
}

- (IBAction)parseTMAllPush:(id)sender {
    [[NSUserDefaults standardUserDefaults] setInteger:selectedIndexTM forKey:@"TMParserIndex"];
    CompetitionType competition = [parseArrayTM[selectedIndexTM * 2] intValue];
    [parserTM parseAll];
}

#pragma mark - API Football Parser

- (IBAction)parseAPITest:(id)sender {
    APIFootballParser* footballParser = [[APIFootballParser alloc] init];
    [footballParser  parseTest];
}

- (IBAction)parseAPIFAllPush:(id)sender {
    [[NSUserDefaults standardUserDefaults] setInteger:selectedIndexTM forKey:@"TMParserIndex"];
    NSInteger seasonInt = [self.comboSeason.objectValueOfSelectedItem intValue];
    [[NSUserDefaults standardUserDefaults] setInteger:seasonInt forKey:@"TMParserSeason"];
    
    CompetitionType competition = [parseArrayTM[selectedIndexTM * 2] intValue];
    APIFootballParser* footballParser = [[APIFootballParser alloc] init];
    [footballParser  parseCompetition:competition season:seasonInt];
}

#pragma mark - LFCSeasonParser



- (IBAction)parseLFCAll:(id)sender {
    [seasonParserLFC parseAll];

}
- (IBAction)parseLFCSeason:(id)sender {
    NSInteger seasonInt = [self.comboSeasonLFC.objectValueOfSelectedItem intValue];
    [[NSUserDefaults standardUserDefaults] setInteger:seasonInt forKey:@"LFCParserSeason"];

    [seasonParserLFC parseSeason:seasonInt];
}

#pragma mark - v11Parser

- (IBAction)comboSelectionChangedV11:(id)sender {
    if (selectedIndexV11 != self.comboCompetitionV11.indexOfSelectedItem) {
        [self setSelectionComboV11:self.comboCompetitionV11.indexOfSelectedItem];
        selectedIndexV11 = self.comboCompetitionV11.indexOfSelectedItem;
    }
}
- (IBAction)comboSeasonChangedV11:(id)sender {
    selectedSeasonV11 = [self.comboSeasonV11.objectValueOfSelectedItem intValue];
}

- (void)setSelectionComboV11:(NSInteger)selectedIndex {
    if (selectedIndexV11 != self.comboCompetitionV11.indexOfSelectedItem) {
        [self.comboCompetitionV11 selectItemAtIndex:selectedIndex];
        
        //
        // Load year selection
        int startYear = [parseArrayV11[selectedIndex * 2 + 1] intValue];
        [self.comboSeasonV11 removeAllItems];
        for (int i = startYear; i <= lastYearToParse; i++) {
            [self.comboSeasonV11 addItemWithObjectValue:[NSString stringWithFormat:@"%d", i]];
        }
        if (selectedSeasonV11 >= startYear && selectedSeasonV11 <= lastYearToParse) {
            [self.comboSeasonV11 selectItemAtIndex:selectedSeasonV11 - startYear];
        }
        else {
            [self.comboSeasonV11 selectItemAtIndex:0];
            selectedSeasonV11 = startYear;
        }
    }
}
- (IBAction)parseV11Push:(id)sender {
    [[NSUserDefaults standardUserDefaults] setInteger:selectedIndexV11 forKey:@"V11ParserIndex"];
    NSInteger seasonInt = [self.comboSeasonV11.objectValueOfSelectedItem intValue];
    [[NSUserDefaults standardUserDefaults] setInteger:seasonInt forKey:@"V11ParserSeason"];
    
    CompetitionType competition = [parseArrayV11[selectedIndexV11 * 2] intValue];
    [parserV11 parseCompetition:competition season:seasonInt];
}

- (IBAction)buttontestV11Push:(id)sender {
    [parserV11 test];
}

#pragma mark - Deductions
#define FilePath (@"/Devlex/Matches/")


- (IBAction)parseDeductionsPushed:(id)sender {
    FileManager* fileManager = [[FileManager alloc] init];
    FootballModel* model = [FootballModel getModel];
    NSString* dataPath  = [[NSUserDefaults standardUserDefaults] objectForKey:@"dataPath"];
    
    NSString* fileString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:nil dataKey:@"Deductions.csv"];
    if (fileString == nil) {
        return;
    }
    NSArray* rows = [fileString csvRows];
    int count = 0;
    for (NSArray *row in rows){
        if (row.count != 8) {
            break;
        }
        
        NSString* dateString = row[0];
        if ([dateString containsString:@"Date"]) continue;
        NSString* teamString = row[1];
        NSString* pointsString = row[2];
        NSString* seasonString = row[3];
        NSString* gameweekString = row[4];
        NSString* competitionString = row[5];
        NSString* reasonString = row[6];
        
        //
        // Parse numbers
        int matchDateNumber = [dateString intValue];
        int season = [seasonString intValue];
        int gameweek = [gameweekString intValue];
        int points = [pointsString intValue];
        int reason = [reasonString intValue];
        int competitionType = [Match getCompetitionFromShortName:competitionString];
        if (competitionType == CompetitionUnknown) {
            raise(SIGSTOP);
            return;
        }
        
        //
        // Get match key
        int teamID = [model addTeam:teamString season:season];
        NSString* matchKey = [Match makeMatchKeyFromDateNumber:matchDateNumber team1:teamID team2:0];
        
        //
        // Check match
        Match* match = [model getMatch:matchKey];
        if (!match) {
            match = [[Match alloc] init];
            
            match.competition = competitionType;
            [match setMatchTeamsAndDateNumber:matchDateNumber team1:teamID team2:0];
            [match setPenalties:points teamID:teamID];
            [match setPenalties:reason teamID:0];
            [match setSeason:season];
            match.competitionFlag = gameweek;

            //
            // Set state as adjustment
            match.matchState = MatchStateAdjustment;
            
            [model addMatch:match];
            [model matchIsModified:match];
            count++;
        }
    }
    
    if (count > 0) {
        NSLog(@"Added %d deductions. Export Matches!", count);
    }
    else {
        NSLog(@"No new deductions found");

    }
}

@end
