//
//  UEFAParser.h
//  FootballData
//
//  Created by Ian Copeman on 12/08/2021.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UEFAParser : NSObject
- (void)parseAll;
- (void)test;
@end

NS_ASSUME_NONNULL_END
