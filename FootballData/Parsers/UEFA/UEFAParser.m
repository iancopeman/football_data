//
//  UEFAParser.m
//  FootballData
//
//  Created by Ian Copeman on 12/08/2021.
//

#import "UEFAParser.h"
#import "FootballModel.h"
#import "WebPageManager.h"
#import "StringSearch.h"

//
// Has matches and scores but only lists players by surname
// https://www.uefa.com/uefachampionsleague/history/seasons/1959/matches

//
// Has events but not team sheet
// https://www.uefa.com/uefaeuropaleague/match/2012441--dinamo-tbilisi-vs-tottenham/

#define FilePath (@"/UEFA/")

@implementation UEFAParser {
    WebPageManager* webPageManager;
    FootballModel* model;
    FileManager* fileManager;
    NSString* baseURL;
    NSString* dataPath;
}

- (id) init {
    self = [super init];
    if (self != nil) {
        self->model = [FootballModel getModel];
        self->fileManager = [[FileManager alloc] init];
        self->dataPath  = [[NSUserDefaults standardUserDefaults] objectForKey:@"dataPath"];
        self->webPageManager = [WebPageManager getWebPageManager];
    }
    return self;
}




- (void)test {
 // https://www.uefa.com/uefaeuropaleague/match/2012441--dinamo-tbilisi-vs-tottenham/
    int season = 2013;
    NSString* matchURL = @"https://www.uefa.com/uefaeuropaleague/match/2012441--dinamo-tbilisi-vs-tottenham/";
    NSString* dataKey = [self generateDataKey:MatchPage urlString:matchURL];
    [self parseMatch:matchURL dataKey:dataKey seasonID:season];

}

- (void)parseAll {
    NSUInteger matchCount = [model getMatchCount];
    baseURL = @"https://www.uefa.com";


 
    //
    // First UEFA Cup season: 1971
    // https://www.uefa.com/uefaeuropaleague/history/seasons/1971/matches/
    // https://www.uefa.com/uefachampionsleague/history/seasons/1960/matches/
    NSString* baseURL = @"https://www.uefa.com/uefaeuropaleague/history/seasons/";
    int seasonIndex = 2010;
    //int seasonIndex = 2000;
    bool ok = true;
    int count = 0;
    do {
        NSString* fullURL = [NSString stringWithFormat:@"%@%d/matches/", baseURL, seasonIndex];
        NSString* dataKey = [[NSNumber numberWithInt:seasonIndex] stringValue];
        NSString* webPageString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:SeasonFilePath dataKey:dataKey];
        if (webPageString == nil) {
            webPageString = [webPageManager getWebPage:fullURL];
            [fileManager savePersistedString:dataPath dataName:FilePath dataType:SeasonFilePath dataKey:dataKey dataString:webPageString];
        }
        
        if (webPageString != nil) {
            if ([webPageString containsString:@"Server Error"]) {
                break;
            }
            [self parseSeason:webPageString seasonID:seasonIndex];
        }
        else {
            ok = false;
        }
        seasonIndex++;
        
        if (++count == 10) break;

    } while (ok);

    // https://www.uefa.com/uefaeuropaleague/history/seasons/1971/?iv=true
    
}

- (void)parseSeason:(NSString*)webData seasonID:(int)seasonID {
     NSLog(@"UEFA parsing season: %d", seasonID);

    StringSearch* stringSearch = [StringSearch initWithString:webData];
    NSString* matchSection = [stringSearch getSectionWithKey:@"season-matches__wrap" startKey:@"<div" endKey:@"</div"];
    stringSearch = [StringSearch initWithString:matchSection];
    NSString* match = [stringSearch getSectionWithKey:@"<div class=\"match-list-history" startKey:@"<div" endKey:@"</div"];
    while (match != nil) {
        //
        // Get match
        StringSearch* stringSearchMatch = [StringSearch initWithString:match];
        NSString* matchURL = [stringSearchMatch getText:@"<a href='" endKey:@"'"];
        if (matchURL == nil) {
            matchURL = [stringSearchMatch getText:@"<a href=\"" endKey:@"\""];
        }
        NSString* dataKey = [self generateDataKey:MatchPage urlString:matchURL];
        int dataKeyNumber = [dataKey intValue];
        
        //
        // Check match
        if ([model checkMatch:ParseUEFA dataKey:dataKeyNumber]) {
            //
            // Already parsed
        }
        else {
            [self parseMatch:matchURL dataKey:dataKey seasonID:seasonID];
        }
        
        
        match = [stringSearch getSectionWithKey:@"<div class=\"match-list-history" startKey:@"<div" endKey:@"</div"];
        if (match == nil) {
            NSLog(@"Last match url: %@", matchURL);
        }

    }
    NSLog(@"UEFA parse complete");
   // int length = webData.length;// NSString* test = [webData substringFromIndex:[stringSearch getStringPos]];

    int ian = 10;

}

- (bool)parseMatch:(NSString*)matchURL dataKey:(NSString*)dataKey seasonID:(int)seasonID {
    //
    // Get Webpage
    // NSLog(@"Parsing match:%@", matchURL);
    NSString* fullURL = baseURL != nil ? [NSString stringWithFormat:@"%@%@", baseURL, matchURL] : matchURL;
    
    NSString* dataType = [NSString stringWithFormat:@"/Matches/%d/", seasonID];

    NSString* webPageString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:dataType dataKey:dataKey];
    if (webPageString == nil) {
        webPageString = [webPageManager getWebPage:fullURL];
        [fileManager savePersistedString:dataPath dataName:FilePath dataType:dataType dataKey:dataKey dataString:webPageString];
    }
    if (webPageString == nil) {
        NSLog(@"Cant get webpage: %@", fullURL);
        return false;
    }

    // <span class="team-name__country-code"> (NED)</span>
    
    StringSearch* stringSearch = [StringSearch initWithString:webPageString];

    NSString* matchSection = [stringSearch getSectionWithKey:@"<script type=\"application/ld+json\">" startKey:@"<script" endKey:@"</script>"];
    if (![matchSection containsString:@"SportsEvent"]) {
        matchSection  = [stringSearch getSectionWithKey:@"<script type=\"application/ld+json\">" startKey:@"<script" endKey:@"</script>"];
    }
    StringSearch* stringSearchMatch = [StringSearch initWithString:matchSection];
    NSString* teamsSection = [stringSearchMatch getTextWithKey:@"name\":" startKey:@"\"" endKey:@"\""];
    NSString* dateSection = [stringSearchMatch getTextWithKey:@"startDate\":" startKey:@"\"" endKey:@"\""];
    NSString* locationString = [stringSearchMatch getTextWithKey:@"StadiumOrArena\",\"name\":" startKey:@"\"" endKey:@"\""];
    NSString* team2IDString = [stringSearchMatch getTextWithKey:@"awayTeam" startKey:@"clubs/" endKey:@"--"];
    NSString* team1IDString = [stringSearchMatch getTextWithKey:@"homeTeam" startKey:@"clubs/" endKey:@"--"];

    //
    // Hack to fix St-Étienne 1-4 Mönchengladbach
    if ([teamsSection containsString:@"St-Étienne"]) {
        teamsSection = [teamsSection stringByReplacingOccurrencesOfString:@"St-Étienne" withString:@"St Étienne"];
    }
    if ([teamsSection containsString:@"Dinamo-93"]) {
        teamsSection = [teamsSection stringByReplacingOccurrencesOfString:@"Dinamo-93" withString:@"Dinamo 93"];
    }
    if ([teamsSection containsString:@"Slavia-Mozyr"]) {
        teamsSection = [teamsSection stringByReplacingOccurrencesOfString:@"Slavia-Mozyr" withString:@"Slavia Mozyr"];
    }
    if ([teamsSection containsString:@"Beira-Mar"]) {
        teamsSection = [teamsSection stringByReplacingOccurrencesOfString:@"Beira-Mar" withString:@"Beira Mar"];
    }
    if ([teamsSection containsString:@"Alma-Ata"]) {
        teamsSection = [teamsSection stringByReplacingOccurrencesOfString:@"Alma-Ata" withString:@"Alma Ata"];
    }
    if ([teamsSection containsString:@"Olimpik-Şüvälan"]) {
        teamsSection = [teamsSection stringByReplacingOccurrencesOfString:@"Olimpik-Şüvälan" withString:@"Olimpik Şüvälan"];
    }
    if ([teamsSection containsString:@"Iskra-Stal"]) {
        teamsSection = [teamsSection stringByReplacingOccurrencesOfString:@"Iskra-Stal" withString:@"Iskra Stal"];
    }
    if ([teamsSection containsString:@"CSKA-Sofia"]) {
        teamsSection = [teamsSection stringByReplacingOccurrencesOfString:@"CSKA-Sofia" withString:@"CSKA Sofia"];
    }
    if ([teamsSection containsString:@"Ararat-Armenia"]) {
        teamsSection = [teamsSection stringByReplacingOccurrencesOfString:@"Ararat-Armenia" withString:@"Ararat Armenia"];
    }
    

    
    //
    // Teams Tottenham 1-1 Wolves
    NSRange range = [teamsSection rangeOfString:@"-"];
    NSString* teamScore1 = [teamsSection substringToIndex:range.location];
    NSRange range1 = [teamScore1 rangeOfString:@" " options:NSBackwardsSearch];
    NSString* team1 = [teamScore1 substringToIndex:range1.location];
    NSString* score1 = [teamScore1 substringFromIndex:range1.location + 1];
    NSString* teamScore2 = [teamsSection substringFromIndex:range.location + 1];
    NSRange range2 = [teamScore2 rangeOfString:@" "];
    NSString* score2 = [teamScore2 substringToIndex:range2.location];
    NSString* team2 = [teamScore2 substringFromIndex:range2.location + 1];
    int uefaTeamID1 = [team1IDString intValue];
    int uefaTeamID2 = [team2IDString intValue];

    if ([dataKey containsString:@"2021796"]) {
        int ian = 10;
    }
    NSString* teamCountryCode1 = [stringSearch getTextWithKey:@"team-name__country-code" startKey:@"(" endKey:@")"];
    NSString* teamCountryCode2 = [stringSearch getTextWithKey:@"team-name__country-code" startKey:@"(" endKey:@")"];
    

    
    int teamID1 = [model addTeamUEFA:team1 parseUEFA:uefaTeamID1 season:seasonID countryCode:teamCountryCode1];
    int teamID2 = [model addTeamUEFA:team2 parseUEFA:uefaTeamID2 season:seasonID countryCode:teamCountryCode2];
    
    //
    // Date 1972-05-17T19:45:00+00:00
    range = [dateSection rangeOfString:@"T"];
    NSString* dateString = [dateSection substringToIndex:range.location];
    
    //
    // Get match key
    NSString* matchKey = [Match makeMatchKey:dateString team1:teamID1 team2:teamID2];


    return true;

}

- (NSString*)generateDataKey:(FileType)pageType urlString:(NSString*)urlString {
    if (pageType == MatchPage) {
        //
        // URL: https://www.uefa.com/uefaeuropaleague/match/64113--tottenham-vs-wolves/
        StringSearch* stringSearch = [StringSearch initWithString:urlString];
        NSString* key = [stringSearch getText:@"match/" endKey:@"--"];
        return key;
    }
    if (pageType == FootballerPage) {
    }
    return nil;
}



@end
