//
//  UEFAViewController.m
//  FootballData
//
//  Created by Ian Copeman on 12/08/2021.
//

#import "UEFAViewController.h"
#import "UEFAParser.h"

@interface UEFAViewController ()

@end

@implementation UEFAViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
}

- (IBAction)buttonParseUEFALeaguePush:(id)sender {
    UEFAParser* uefaParser = [[UEFAParser alloc] init];
    [uefaParser parseAll];

}
- (IBAction)buttonTestPush:(id)sender {
    UEFAParser* uefaParser = [[UEFAParser alloc] init];
    [uefaParser test];
}

@end
