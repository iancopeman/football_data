//
//  LFCSeasonParser .m
//  FootballData
//
//  Created by Ian Copeman on 10/11/2020.
//

#import "LFCSeasonParser.h"
#import "StringSearch.h"
#import "WebPageManager.h"
#import "FootballModel.h"

#define FilePath (@"/LFC/")


@implementation LFCSeasonParser {
    WebPageManager* webPageManager;
    FootballModel* model;
    NSCharacterSet* nonDigitCharacterSet;
    FileManager* fileManager;
    int liverpoolTeamID;
    NSString* dataPath;
    int matchesModified;

}


- (void)parseSeason:(NSInteger)season {
    if (model == nil) {
        model = [FootballModel getModel];
        webPageManager = [WebPageManager getWebPageManager];
        dataPath  = [[NSUserDefaults standardUserDefaults] objectForKey:@"dataPath"];
        fileManager = [[FileManager alloc] init];
        
        //
        // Get liverpool team ID
        liverpoolTeamID = [model addTeam:@"Liverpool"];
        if (liverpoolTeamID == 1) {
            NSLog(@"Liverpool Team ID == 1, DB not loaded?");
        }
   }

    NSUInteger matchCount = [model getMatchCount];
    NSUInteger footballerCount = [model getFootballerCount];
    matchesModified = 0;
    
    NSInteger seasonIndex = [self getSeasonIndex:season];
    if (seasonIndex == 0) {
        NSLog(@"Attempting to parse LFC with unsupported season: %ld", (long)season);
        return;
    }
    NSString* baseURL = @"https://www.lfchistory.net/SeasonArchive/Games/";
    NSString* fullURL = [NSString stringWithFormat:@"%@%ld", baseURL, (long)seasonIndex];
    NSString* dataKey = [self generateDataKey:SeasonArchive urlString:fullURL];
    
    NSString* webPageString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:SeasonFilePath dataKey:dataKey];
    if (webPageString == nil) {
        webPageString = [webPageManager getWebPage:fullURL];
        if (webPageString != nil) {
            [fileManager savePersistedString:dataPath dataName:FilePath dataType:SeasonFilePath dataKey:dataKey dataString:webPageString];
        }
    }
    
    if (webPageString != nil) {
        if ([webPageString containsString:@"Server Error"]) {
            NSLog(@"Attempting to parse LFC, webPageString contains error. Season: %ld", (long)season);
            return;
        }
        [self parse:webPageString];
    }
    
    NSUInteger matchCount2 = [model getMatchCount];
    if (matchCount == matchCount2) {
        NSLog(@"No matches found");
    }
    else {
        NSLog(@"Found %lu new matches", matchCount2 - matchCount);
    }
    NSLog(@"Modified %d matches", matchesModified);
    
    NSUInteger footballerCount2 = [model getFootballerCount];
    if (footballerCount == footballerCount2) {
        NSLog(@"No footballers found");
    }
    else {
        NSLog(@"Found %lu new footballers", footballerCount2 - footballerCount);
    }
}

- (void)parseAll {
    if (model == nil) {
        model = [FootballModel getModel];
        webPageManager = [WebPageManager getWebPageManager];
        dataPath  = [[NSUserDefaults standardUserDefaults] objectForKey:@"dataPath"];
        fileManager = [[FileManager alloc] init];

        //
        // Get liverpool team ID
        liverpoolTeamID = [model addTeam:@"Liverpool"];
        if (liverpoolTeamID == 1) {
            NSLog(@"Liverpool Team ID == 1, DB not loaded?");
        }
   }
    NSUInteger matchCount = [model getMatchCount];
    NSUInteger footballerCount = [model getFootballerCount];
    matchesModified = 0;
    
    //
    // Get liverpool team ID
    liverpoolTeamID = [model addTeam:@"Liverpool"];
    if (liverpoolTeamID == 1) {
        NSLog(@"Liverpool Team ID == 1, DB not loaded?");
        //return;
    }
    
    //
    // First season 1893-94: 97
    int seasonIndex = 1;
    NSString* baseURL = @"https://www.lfchistory.net/SeasonArchive/Games/";
    bool ok = true;
    int count = 0;
    do {
        if (seasonIndex == 38) { // Empty season
            seasonIndex++;
            continue;
        }
        if (seasonIndex == 105) { // 1939 - skip
            seasonIndex++;
            continue;
        }
        NSString* fullURL = [NSString stringWithFormat:@"%@%d", baseURL, seasonIndex];
        NSString* dataKey = [self generateDataKey:SeasonArchive urlString:fullURL];

        NSString* webPageString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:SeasonFilePath dataKey:dataKey];
        if (webPageString == nil) {
            webPageString = [webPageManager getWebPage:fullURL];
            if (webPageString != nil) {
                [fileManager savePersistedString:dataPath dataName:FilePath dataType:SeasonFilePath dataKey:dataKey dataString:webPageString];
            }
        }
       
        
        if (webPageString != nil) {
            if ([webPageString containsString:@"Server Error"]) {
                break;
            }
            // NSLog(@"Parsing match:%@", fullURL);
            [self parse:webPageString];
            /*
            if (savePage) {
                [webPageManager saveWebPage:webPage ];
            }
             */
        }
        else {
            ok = false;
        }
        seasonIndex++;
        // if (++count == 1) break;
        
        if (seasonIndex == 131) { // 126 == 2016
            break;
        }

        
    } while (ok);

 //   [model toBlobs];

    NSUInteger matchCount2 = [model getMatchCount];
    if (matchCount == matchCount2) {
        NSLog(@"No matches found");
    }
    else {
        NSLog(@"Found %lu new matches", matchCount2 - matchCount);
    }
    NSLog(@"Modified %d matches", matchesModified);
    
    NSUInteger footballerCount2 = [model getFootballerCount];
    if (footballerCount == footballerCount2) {
        NSLog(@"No footballers found");
    }
    else {
        NSLog(@"Found %lu new footballers", footballerCount2 - footballerCount);
    }
}

- (NSString*)generateDataKey:(FileType)pageType urlString:(NSString*)urlString {
    if (pageType == SeasonArchive) {
        //
        // URL: https://www.lfchistory.net/SeasonArchive/Games/100
        NSRange range = [urlString rangeOfString:@"Games"];
        if (range.length == 0) {
            NSLog(@"Cant find key in URL: %@", urlString);
            return nil;
        }
        return [[urlString substringFromIndex:range.location] stringByReplacingOccurrencesOfString:@"/" withString:@"_"];
    }
    if (pageType == MatchPage) {
        //
        // URL: https://www.lfchistory.net/SeasonArchive/Game/4462
        NSRange range = [urlString rangeOfString:@"Game"];
        if (range.length == 0) {
            NSLog(@"Cant find key in URL: %@", urlString);
            return nil;
        }
        return [[urlString substringFromIndex:range.location] stringByReplacingOccurrencesOfString:@"/" withString:@"_"];
    }
    if (pageType == FootballerPage) {
        //
        // URL: https://www.lfchistory.net/Players/Player/Profile/860
        bool opposition = [urlString localizedCaseInsensitiveContainsString:@"opposition"];
        
        NSRange range = [urlString rangeOfString:@"/" options:NSBackwardsSearch];
        if (range.length == 0) return nil;
        NSString* idString = [urlString substringFromIndex:range.location + 1];
        if (opposition) {
            return [NSString stringWithFormat:@"O_%@", idString];
        }
        return idString;
    }
    return nil;
}


- (void)parse:(NSString*)webData {
    

    //
    // Find season
    StringSearch* stringSearchYear = [StringSearch initWithString:webData key:@"<h4>Games" startKey:@"for the " endKey:@"-"];
    if (stringSearchYear == nil) {
        NSLog(@"Can't find year in webpage");
        return;
    }
    int year = [[stringSearchYear getString] intValue];
    //if (year < 1992) return;
    
    NSLog(@"Parsing year: %d", year);

    //
    // Look for "<b>Official</b>"
    NSRange rangeStart = [webData rangeOfString:@"<b>Official</b>"];
    if (rangeStart.length != 0) {
        //
        // Create substring with table
        rangeStart.length = webData.length - rangeStart.location;
        NSRange rangeEnd = [webData
                            rangeOfString:@"</table>"
                            options:NSLiteralSearch
                            range:rangeStart];
        NSRange rangeTable;
        rangeTable.location = rangeStart.location;
        rangeTable.length = rangeEnd.location - rangeStart.location;

        NSString* table = [webData substringWithRange:rangeTable];
        StringSearch* stringSearch = [StringSearch initWithString:table];
        NSString* section = [stringSearch getText:@"<tr" endKey: @"</tr"];
        StringSearch* rowSearch;
        int test = 0;
        while (section != nil) {
            section = [stringSearch getText:@"<tr" endKey: @"</tr"];
            if (section == nil) break;
            if (rowSearch == nil) {
                rowSearch = [StringSearch initWithString:section];
            }
            else {
                [rowSearch setString:section];
            }
            [rowSearch getText:@"<td>" endKey: @"</td"];
            NSString* matchDateString = [rowSearch getText:@"<td>" endKey: @"</td"];
            if (matchDateString == nil) continue;
            
            //
            // Fudge to catch matches with wrong dates
            if ([matchDateString compare:@"30.01.1902"] == NSOrderedSame) matchDateString = @"29.01.1902";
            if ([matchDateString compare:@"15.01.1914"] == NSOrderedSame) matchDateString = @"14.01.1914";
            if ([matchDateString compare:@"19.11.1986"] == NSOrderedSame) matchDateString = @"18.11.1986";
            if ([matchDateString compare:@"03.01.1993"] == NSOrderedSame) matchDateString = @"02.01.1993";
            if ([matchDateString compare:@"22.09.1993"] == NSOrderedSame) matchDateString = @"21.09.1993";
            if ([matchDateString compare:@"21.08.2001"] == NSOrderedSame) matchDateString = @"22.08.2001";
            if ([matchDateString compare:@"30.01.1929"] == NSOrderedSame) matchDateString = @"31.01.1929";
            if ([matchDateString compare:@"30.12.1899"] == NSOrderedSame) matchDateString = @"29.12.1899";

            // match.dateString = matchDateString;
            
            [rowSearch getText:@"<td" endKey: @"</td"];
            [rowSearch getText:@"<td" endKey: @"</td"];
            NSString* matchURLString = [rowSearch getText:@"<td>" endKey: @"</td"];
            NSString* teamString = [rowSearch getText:@"<td>" endKey: @"</td"];
            NSString* locationString = [rowSearch getText:@"<td>" endKey: @"</td"];
            NSString* competitionString = [rowSearch getText:@"<td>" endKey: @"</td"];
            
            CompetitionType competition = [model addCompetition:competitionString];
            if (competition == CompetitionMinor) continue;
           // if (competition != CompetitionPremierLeague) continue;

            
            //
            // Parse Date
            int matchDateNumber = [self parseDateNumber:matchDateString];
            if (matchDateNumber == 0) continue;
            
            //
            // Add Team
            teamString = [LFCSeasonParser checkEncoding:teamString];
            int teamID = [self addTeam:teamString];

            
            //
            // Check match
            Match* match = [model getMatch:matchDateNumber teamID1:liverpoolTeamID teamID2:teamID];
            if (!match) {
                //
                // Add match
                Match* match = [[Match alloc] init];
                [match setMatchTeamsAndDateNumber:matchDateNumber team1:liverpoolTeamID team2:teamID];
                match.locationID = [model addLocation:locationString];
                match.competition = competition;
                [match setSeason:year];
                [model addMatch:match];

             }
            
            //
            //  Check of we can improve match parsing
            if (match.matchParseState >= MatchParseStateEventsFull) continue;

            if ([matchURLString containsString:@"href"]) {
                [rowSearch setString:matchURLString];
                NSString* matchURL = [rowSearch getText:@"\"" endKey: @"\">"];
                if ([self parseMatch:matchURL teamID:teamID match:match season:year]) {
                }
            }
            
            if (++test == 1) {
               // break;
            }
        }
        
    }
    
 

    int ian = 10;
    
}

- (bool)parseMatch:(NSString*)matchURL teamID:(int)oppostionTeamID match:(Match*)match season:(int)season {
    MatchParseState oldMatchState = match.matchParseState;
    NSString* fullURL = [NSString stringWithFormat:@"https://www.lfchistory.net%@", matchURL];
    NSString* dataKey = [self generateDataKey:MatchPage urlString:fullURL];
    NSString* webPageString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:MatchFilePath dataKey:dataKey];
    if (webPageString == nil) {
        webPageString = [webPageManager getWebPage:fullURL];
        [fileManager savePersistedString:dataPath dataName:FilePath dataType:MatchFilePath dataKey:dataKey dataString:webPageString];
    }
 //   NSLog(@"Parsing match:%@", fullURL);
    
    //
    // Get score
    StringSearch* stringSearchScore = [StringSearch initWithString:webPageString key:@"lf-match_result--detail" startKey:@"lf-match_score--table" endKey:@"</table>"];
    NSString* team1 = [stringSearchScore getText:@"lf-club_name\">" endKey:@"</td>"];
    NSString* score1 = [stringSearchScore getText:@"lf-club_score\">" endKey:@"</td>"];
    NSString* score2 = [stringSearchScore getText:@"lf-club_score\">" endKey:@"</td>"];
    // NSString* team2 = [stringSearchScore getText:@"lf-club_name\">" endKey:@"</td>"];
    bool liverpoolTeam1 = [team1 localizedCaseInsensitiveContainsString:@"liverpool"];
    [match setScore:[score1 intValue] teamID:liverpoolTeam1 ? liverpoolTeamID : oppostionTeamID];
    [match setScore:[score2 intValue] teamID:liverpoolTeam1 ? oppostionTeamID: liverpoolTeamID];
 
    //
    // Get LFC Team
    StringSearch* stringSearchTeam = [StringSearch initWithString:webPageString key:@"Starting Lineup" startKey:@"lf-table_primary" endKey:@"</table>"];
    NSString* section = [stringSearchTeam getText:@"<tr" endKey: @"</tr"];
    StringSearch* rowSearch;
    NSMutableArray* lfcTeam = [NSMutableArray arrayWithCapacity:20];
    NSMutableArray* lfcEvents = [NSMutableArray arrayWithCapacity:20];
    NSMutableArray* oppositionTeam = [NSMutableArray arrayWithCapacity:20];
    NSMutableArray* oppositionEvents = [NSMutableArray arrayWithCapacity:20];
    int position = 1;
    bool addedGoalie = NO;
    
    //
    // Calculate winner
    EventResultType resultType1 = EventResultDraw;
    EventResultType resultType2 = EventResultDraw;
    if (score1 > score2) {
        resultType1 = EventResultWin;
        resultType2 = EventResultLose;
    }
    if (score1 < score2) {
        resultType1 = EventResultLose;
        resultType2 = EventResultWin;
    }

    
    while (section != nil) {
        if (rowSearch == nil) {
            rowSearch = [StringSearch initWithString:section];
        }
        else {
            [rowSearch setString:section];
        }
        NSString* shirtNumberString = [rowSearch getTextWithKey:@"<td" startKey:@">" endKey: @"<"];
        int shirtNumber = shirtNumberString.intValue;
        NSString* nameURLString = [rowSearch getText:@"<a href=\"" endKey: @"\""];
        NSString* nameString = [rowSearch getText:@">" endKey:@"<"];
        nameString = [LFCSeasonParser checkEncoding:nameString];
        
        //
        // Add apperance etc
        Footballer* footballer = [self parsePlayer:nameString teamID:liverpoolTeamID url:nameURLString season:season];
        if (!addedGoalie) {
            [lfcEvents addObject:[Event createEventGoalkeeperApperance:footballer.index position:position++ shirt:shirtNumber cleanSheet:liverpoolTeam1 ? score2 == 0 : score1 == 0 resultType:liverpoolTeam1 ? resultType1 : resultType2]];
            addedGoalie = YES;
        }
        else {
            [lfcEvents addObject:[Event createEventAppearanceNumber:footballer.index position:position++ shirt:shirtNumber cleanSheet:liverpoolTeam1 ? score2 == 0 : score1 == 0 resultType:liverpoolTeam1 ? resultType1 : resultType2]];
        }
        [lfcTeam addObject:footballer];
       // [lfcIDs addObject:[NSNumber numberWithInt:footballer.index]];
        section = [stringSearchTeam getText:@"<tr" endKey: @"</tr"];

    }
    
    //
    // Get LFC Team
    StringSearch* stringSearchBench = [StringSearch initWithString:webPageString key:@"Substitutes" startKey:@"lf-table_primary" endKey:@"</table>"];
    section = [stringSearchBench getText:@"<tr" endKey: @"</tr"];
    while (section != nil) {
        [rowSearch setString:section];
        NSString* shirtNumberString = [rowSearch getTextWithKey:@"<td" startKey:@">" endKey: @"<"];
        int shirtNumber = shirtNumberString.intValue;
        NSString* nameURLString = [rowSearch getText:@"<a href=\"" endKey: @"\""];
        NSString* nameString = [rowSearch getText:@">" endKey:@"<"];
        nameString = [LFCSeasonParser checkEncoding:nameString];
        
        //
        // Add apperance etc
        Footballer* footballer = [self parsePlayer:nameString teamID:liverpoolTeamID url:nameURLString season:season];
        [lfcEvents addObject:[Event createEventBenchNumber:footballer.index shirt:shirtNumber]];
        [lfcTeam addObject:footballer];
        //[lfcIDs addObject:[NSNumber numberWithInt:footballer.index]];
        section = [stringSearchBench getText:@"<tr" endKey: @"</tr"];
        
    }

    
    //
    // Opposition
    StringSearch* stringSearchOpposition = [StringSearch initWithString:webPageString key:@"Opposition Lineup" startKey:@"<ul" endKey:@"</ul"];
    section = [stringSearchOpposition getText:@"<li>" endKey: @"</li>"];
    position = 1;
    addedGoalie = NO;
    while (section != nil) {
        if (rowSearch == nil) {
            rowSearch = [StringSearch initWithString:section];
        }
        else {
            [rowSearch setString:section];
        }
        NSString* nameURLString = [rowSearch getText:@"<a href=\"" endKey: @"\""];
        NSString* nameString = [rowSearch getText:@">" endKey:@"<"];
        nameString = [LFCSeasonParser checkEncoding:nameString];
        Footballer* footballer = [self parsePlayer:nameString teamID:oppostionTeamID url:nameURLString season:season];
        if (!addedGoalie) {
            [oppositionEvents addObject:[Event createEventGoalkeeperApperance:footballer.index position:position++ shirt:0 cleanSheet:liverpoolTeam1 ? score1 == 0 : score2 == 0 resultType:liverpoolTeam1 ? resultType2 : resultType1]];
            addedGoalie = YES;
        }
        else {
            [oppositionEvents addObject:[Event createEventAppearanceNumber:footballer.index position:position++ shirt:0 cleanSheet:liverpoolTeam1 ? score1 == 0 : score2 == 0 resultType:liverpoolTeam1 ? resultType2 : resultType1]];
        }
        [oppositionTeam addObject:footballer];
        section = [stringSearchOpposition getText:@"<li>" endKey: @"</li>"];
    }

    //
    // LFC Substitution
    StringSearch* stringSearchSubs = [StringSearch initWithString:webPageString key:@"> Substitutions <" startKey:@"<ul>" endKey:@"</ul>"];
    if (stringSearchSubs != 0) {
        NSString* section = [stringSearchSubs getText:@"<li" endKey: @"</li>"];
        while (section != nil) {
            if (rowSearch == nil) {
                rowSearch = [StringSearch initWithString:section];
            }
            else {
                [rowSearch setString:section];
            }
            NSString* nameURLStringOut = [rowSearch getText:@"<a href=\"" endKey: @"\""];
            NSString* nameStringOut = [rowSearch getText:@">" endKey:@"<"];
            nameStringOut = [LFCSeasonParser checkEncoding:nameStringOut];
            NSString* nameURLStringIn = [rowSearch getText:@"<a href=\"" endKey: @"\""];
            NSString* nameStringIn = [rowSearch getText:@">" endKey:@"<"];
            nameStringIn = [LFCSeasonParser checkEncoding:nameStringIn];
            NSString* timeStringSection = [rowSearch getText:@"<span>" endKey:@"</span>"];
            NSString* timeString = [self extractNumberFromText:timeStringSection];
            int time = [timeString intValue];
            Footballer* playerOut = [self parsePlayer:nameStringOut teamID:liverpoolTeamID url:nameURLStringOut season:season];
            Footballer* playerIn = [self parsePlayer:nameStringIn teamID:liverpoolTeamID url:nameURLStringIn season:season];
            [lfcEvents addObject:[Event createEventSubNumber:playerIn.index playerIDOut:(short)playerOut.index time:time cleanSheet:liverpoolTeam1 ? score2 == 0 : score1 == 0 resultType:liverpoolTeam1 ? resultType1 : resultType2]];
            [lfcTeam addObject:playerIn];
            //[lfcIDs addObject:[NSNumber numberWithInt:playerIn.index]];
            section = [stringSearchSubs getText:@"<li" endKey: @"</li>"];
            
        }
    }
    
    //
    // Opposition Substitution
    stringSearchSubs = [StringSearch initWithString:webPageString key:@"> Opposition substitutions <" startKey:@"<ul>" endKey:@"</ul>"];
    if (stringSearchSubs != 0) {
        NSString* section = [stringSearchSubs getText:@"<li" endKey: @"</li>"];
        while (section != nil) {
            if (rowSearch == nil) {
                rowSearch = [StringSearch initWithString:section];
            }
            else {
                [rowSearch setString:section];
            }
            NSString* nameURLStringOut = [rowSearch getText:@"<a href=\"" endKey: @"\""];
            NSString* nameStringOut = [rowSearch getText:@">" endKey:@"<"];
            nameStringOut = [LFCSeasonParser checkEncoding:nameStringOut];
            NSString* nameURLStringIn = [rowSearch getText:@"<a href=\"" endKey: @"\""];
            NSString* nameStringIn = [rowSearch getText:@">" endKey:@"<"];
            nameStringIn = [LFCSeasonParser checkEncoding:nameStringIn];
            NSString* timeStringSection = [rowSearch getText:@"<span>" endKey:@"</span>"];
            NSString* timeString = [self extractNumberFromText:timeStringSection];
            int time = [timeString intValue];
            Footballer* playerOut = [self parsePlayer:nameStringOut teamID:oppostionTeamID url:nameURLStringOut season:season];
            Footballer* playerIn = [self parsePlayer:nameStringIn teamID:oppostionTeamID url:nameURLStringIn season:season];
            [oppositionEvents addObject:[Event createEventSubNumber:playerIn.index playerIDOut:(short)playerOut.index time:time cleanSheet:liverpoolTeam1 ? score1 == 0 : score2 == 0 resultType:liverpoolTeam1 ? resultType2 : resultType1]];
            [oppositionTeam addObject:playerIn];
            section = [stringSearchSubs getText:@"<li" endKey: @"</li>"];
        }
    }

    //
    // LFC Goals
    StringSearch* stringSearchGoals = [StringSearch initWithString:webPageString key:@"> Goals " startKey:@"<ul>" endKey:@"</ul>"];
    if (stringSearchGoals != 0) {
        NSString* section = [stringSearchGoals getText:@"<li" endKey: @"</li>"];
        while (section != nil) {
            if (rowSearch == nil) {
                rowSearch = [StringSearch initWithString:section];
            }
            else {
                [rowSearch setString:section];
            }
            //
            // Check for Own Goal
            if ([section containsString:@"O/G"]) {
                //
                // >Jimmy Methven O/G<span> 25'</span>
                NSString* nameString = [rowSearch getText:@">" endKey:@" O/G"];
                NSString* timeString = [rowSearch getText:@"span>" endKey:@"'<"];
                int time = [timeString intValue];
                nameString = [self correctName:nameString];
                bool foundName = false;
                for (int i = 0; i < oppositionTeam.count; i++) {
                    Footballer* footballer = oppositionTeam[i];
                    if ([footballer.name compare:nameString] == NSOrderedSame) {
                        [oppositionEvents addObject:[Event createEventOwnGoalNumber:footballer.index time:time]];
                        foundName = true;
                        break;
                    }
                }
                if (!foundName && oppositionTeam.count != 0) {
                    int ian = 10;
                }
            }
            else {
                NSString* nameURLString = [rowSearch getText:@"<a href=\"" endKey: @"\""];
                NSString* nameString = [rowSearch getText:@">" endKey:@"<"];
                nameString = [LFCSeasonParser checkEncoding:nameString];
                NSString* timeString = [rowSearch getText:@"span>" endKey:@"'"];
                int time = [timeString intValue];
                NSString* assistString = [rowSearch getText:@"(" endKey:@")"];
                short assistPlayerID = 0;
                if (assistString.length != 0) {
                    bool foundName = false;
                    for (int i = 0; i < lfcTeam.count; i++) {
                        Footballer* footballer = lfcTeam[i];
                        if ([footballer.name compare:assistString] == NSOrderedSame) {
                            assistPlayerID = footballer.index;
                            foundName = true;
                            break;
                        }
                    }
                    if (!foundName) {
                        int ian = 10;
                    }

                }
                bool penalty = [section containsString:@" pen<"];
                Footballer* player = [self parsePlayer:nameString teamID:liverpoolTeamID url:nameURLString season:season];
                if (penalty) {
                    [lfcEvents addObject:[Event createEventPenaltyNumber:player.index time:time]];
                }
                else {
                    [lfcEvents addObject:[Event createEventGoalNumber:player.index playerAssistID:assistPlayerID time:time]];
                }
            }
            
            
            section = [stringSearchGoals getText:@"<li" endKey: @"</li>"];
            
        }
    }
 
    //
    // Opposition Goals
    stringSearchGoals = [StringSearch initWithString:webPageString key:@"> Opposition goals <" startKey:@"<ul>" endKey:@"</ul>"];
    if (stringSearchGoals != 0) {
        NSString* section = [stringSearchGoals getText:@"<li" endKey: @"</li>"];
        while (section != nil) {
            if (rowSearch == nil) {
                rowSearch = [StringSearch initWithString:section];
            }
            else {
                [rowSearch setString:section];
            }
            if ([section containsString:@"O/G"]) {
                //
                // >Jimmy Methven O/G<span> 25'</span>
                NSString* nameString = [rowSearch getText:@">" endKey:@" O/G"];
                NSString* timeString = [rowSearch getText:@"span>" endKey:@"'<"];
                int time = [timeString intValue];
                nameString = [self correctName:nameString];
                bool foundName = false;
                for (int i = 0; i < lfcTeam.count; i++) {
                    Footballer* footballer = lfcTeam[i];
                    if ([footballer.name compare:nameString] == NSOrderedSame) {
                        [lfcEvents addObject:[Event createEventOwnGoalNumber:footballer.index  time:time]];
                        foundName = true;
                        break;
                    }
                }
                if (!foundName) {
                    int ian = 10;
                }
            }
            else {
                NSString* nameURLString = [rowSearch getText:@"<a href=\"" endKey: @"\""];
                NSString* nameString = [rowSearch getText:@">" endKey:@"<"];
                nameString = [LFCSeasonParser checkEncoding:nameString];
                NSString* timeString = [rowSearch getText:@"span>" endKey:@"'"];
                int time = [timeString intValue];
                bool penalty = [section containsString:@" pen<"];
                Footballer* player = [self parsePlayer:nameString teamID:oppostionTeamID url:nameURLString season:season];
                if (penalty) {
                    [oppositionEvents addObject:[Event createEventPenaltyNumber:player.index time:time]];
                }
                else {
                    [oppositionEvents addObject:[Event createEventGoalNumber:player.index playerAssistID:0 time:time]];
                }
            }

            section = [stringSearchGoals getText:@"<li" endKey: @"</li>"];
        }
    }

    //
    // Red Cards
    StringSearch* stringSearchCards = [StringSearch initWithString:webPageString key:@"> Red Cards <" startKey:@"<ul>" endKey:@"</ul>"];
    if (stringSearchCards != 0) {
        NSString* section = [stringSearchCards getText:@"<li" endKey: @"</li>"];
        while (section != nil) {
            if (rowSearch == nil) {
                rowSearch = [StringSearch initWithString:section];
            }
            else {
                [rowSearch setString:section];
            }
            NSString* nameURLString = [rowSearch getText:@"<a href=\"" endKey: @"\""];
            NSString* nameString = [rowSearch getText:@">" endKey:@"<"];
            nameString = [LFCSeasonParser checkEncoding:nameString];
            NSString* timeString = [rowSearch getText:@"span>" endKey:@"'"];
            int time = [timeString intValue];
            Footballer* player = [self parsePlayer:nameString teamID:liverpoolTeamID url:nameURLString season:season];
            [lfcEvents addObject:[Event createEventRedCardNumber:player.index time:time]];
            
            section = [stringSearchCards getText:@"<li" endKey: @"</li>"];
        }
    }

    
    stringSearchCards = [StringSearch initWithString:webPageString key:@"> Opposition red Cards <" startKey:@"<ul>" endKey:@"</ul>"];
    if (stringSearchCards != 0) {
        NSString* section = [stringSearchCards getText:@"<li" endKey: @"</li>"];
        while (section != nil) {
            if (rowSearch == nil) {
                rowSearch = [StringSearch initWithString:section];
            }
            else {
                [rowSearch setString:section];
            }
            NSString* nameURLString = [rowSearch getText:@"<a href=\"" endKey: @"\""];
            NSString* nameString = [rowSearch getText:@">" endKey:@"<"];
            nameString = [LFCSeasonParser checkEncoding:nameString];
            NSString* timeString = [rowSearch getText:@"span>" endKey:@"'"];
            int time = [timeString intValue];
            Footballer* player = [self parsePlayer:nameString teamID:oppostionTeamID url:nameURLString season:season];
            [oppositionEvents addObject:[Event createEventRedCardNumber:player.index  time:time]];

            section = [stringSearchCards getText:@"<li" endKey: @"</li>"];
        }
    }

    
    // Other details
    StringSearch* stringSearchDetails = [StringSearch initWithString:webPageString key:@"> Other details <" startKey:@"<dl" endKey:@"</dl"];
    if (stringSearchDetails != 0) {
        //
        // Add attributes
        NSString* section = [stringSearchDetails getText:@"<dd" endKey: @"/dd"];
        StringSearch* rowSearch;
        while (section != nil) {
            if (rowSearch == nil) {
                rowSearch = [StringSearch initWithString:section];
            }
            else {
                [rowSearch setString:section];
            }
            NSString* profileAttribute = [rowSearch getText:@"<strong>" endKey: @"</strong"];
            [rowSearch getText:@"<a href" endKey:@"\""];
            NSString* profileValue = [rowSearch getText:@">" endKey:@"<"];
            [match addAttribute:profileAttribute value:profileValue];
            section = [stringSearchDetails getText:@"<dd" endKey: @"/dd"];
        }
    }
    
    //
    // Add events
    [match setFootballers:lfcTeam teamID:liverpoolTeamID];
    [match setFootballers:oppositionTeam teamID:oppostionTeamID];

    [match addMatchEvents:lfcEvents teamID:liverpoolTeamID hasAssists:NO];
    [match addMatchEvents:oppositionEvents teamID:oppostionTeamID hasAssists:NO];
    [model addMatchEvents:match];

    if (oldMatchState < match.matchParseState) {
        [model matchIsModified:match];
        matchesModified++;
    }
    return true;
    
}

- (NSString*)urlToIdent:(NSString*)url {
    
    //
    // Players with duplicate entries
    NSArray* urlDuplicated = @[ @"/Opposition/Players/Profile/1894", @"/Opposition/Players/Profile/9593", // Ricardo Carvalho
                                @"/Opposition/Players/Profile/14735", @"/Opposition/Players/Profile/14371", // Josh Sims
                                @"/Opposition/Players/Profile/12339", @"/Opposition/Players/Profile/12337", // Andrew Wilson
                                @"/Opposition/Players/Profile//12339", @"/Opposition/Players/Profile/12337", // Andrew Wilson
                                @"/Opposition/Players/Profile/5864", @"/Opposition/Players/Profile/5834", // Harry Johnson
                                @"/Opposition/Players/Profile//5864", @"/Opposition/Players/Profile/5834", // Harry Johnson
                                @"/Opposition/Players/Profile/13133", @"/Opposition/Players/Profile/13132", // Nico Kovac
                                @"/Opposition/Players/Profile/5520", @"/Opposition/Players/Profile/5519", // George Hunter
                                @"/Opposition/Players/Profile/8406", @"/Opposition/Players/Profile/8404" // Jack Nicholas
    ];
    
    for (int i = 0; i < urlDuplicated.count / 2; i++) {
        if ([url containsString:urlDuplicated[i * 2]]) {
            url = urlDuplicated[i * 2 + 1];
            break;
        }
    }

    if ([url containsString:@"12339"]) {
        int ian = 10;
    }
    if ([url containsString:@"5864"]) {
        int ian = 10;
    }

    
    NSString* ident = [Footballer urlToIdent:url];
    return ident;
}

- (Footballer*)parsePlayer:(NSString*)playerName teamID:(int)teamID url:(NSString*)playerURL season:(int)season  {
 
    //
    // Test
    if ([playerName localizedCaseInsensitiveContainsString:@"Charlie Ada"]) {
        int ian = 10;
    }
   
    //
    // Get footballer identifier
    NSString* identString = [self urlToIdent:playerURL];
    int identInt = identString.intValue;
    if (identInt == 0) {
        return nil;
    }
    
    //
    // Error: https://www.lfchistory.net//SeasonArchive/Game/1699
    if (identInt == 4475) {
        identInt = 13829;
    }

    //
    // Lookup player in model
    Footballer* footballer = [model checkFootballer:identInt identKey:@"parseIndexLFC" teamID:teamID season:season];
    if (footballer != nil) return footballer;
    if (playerURL == nil) {
        if ([playerName containsString:@"Alfred Townsend"]) {
            return nil;
        }
        int ian = 10;
    }
    
    
    //
    // Add footballer
    NSString* fullURL = [NSString stringWithFormat:@"https://www.lfchistory.net%@", playerURL];
    NSString* dataKey = [self generateDataKey:FootballerPage urlString:fullURL];
    NSString* webPageString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:FootballerFilePath dataKey:dataKey];
    if (webPageString == nil) {
        webPageString = [webPageManager getWebPage:fullURL];
        [fileManager savePersistedString:dataPath dataName:FilePath dataType:FootballerFilePath dataKey:dataKey dataString:webPageString];
    }
    
    StringSearch* stringSearchProfile = [StringSearch initWithString:webPageString key:@"lf-profile_history" startKey:@"<dl>" endKey:@"</dl>"];

    //
    // Create new footballer
    footballer = [[Footballer alloc] init];
    footballer.name = playerName;
    footballer.parseIndexLFC = identInt;
    footballer.teamID = teamID;
    
    //
    // Add attributes
    NSString* section = [stringSearchProfile getText:@"<dd" endKey: @"/dd"];
    StringSearch* rowSearch;
    while (section != nil) {
        if (rowSearch == nil) {
            rowSearch = [StringSearch initWithString:section];
        }
        else {
            [rowSearch setString:section];
        }
        NSString* profileAttribute = [rowSearch getText:@"<strong>" endKey: @"</strong"];
        NSString* profileValue = [rowSearch getText:@">" endKey:@"<"];
        [footballer addAttribute:profileAttribute value:profileValue];
        section = [stringSearchProfile getText:@"<dd" endKey: @"/dd"];
    }
    
    if (footballer.birthDateNumber == 0) {
        if ([footballer.name containsString:@"Stefan Roman"]) footballer.birthDateNumber = 19410101;
        if ([footballer.name containsString:@"Mario Morocutti"]) footballer.birthDateNumber = 19410501;
        if ([footballer.name containsString:@"Jean-Pierre Hnatov"]) footballer.birthDateNumber = 19480101;
        if ([footballer.name containsString:@"Petar Baralic"]) footballer.birthDateNumber = 19511003;
        if ([footballer.name containsString:@"Mihalj Keri"]) footballer.birthDateNumber = 19510115;
        if ([footballer.name containsString:@"Joe Pearson"]) footballer.birthDateNumber = 19510115;
        if ([footballer.name containsString:@"Robert Strain"]) footballer.birthDateNumber = 19510101;
        if ([footballer.name containsString:@"Geoff Gorman"]) footballer.birthDateNumber = 19510101;
        if ([footballer.name containsString:@"Walter McFarland"]) footballer.birthDateNumber = 19510101;
        if ([footballer.name containsString:@"John McPolin"]) footballer.birthDateNumber = 19510101;
        if ([footballer.name containsString:@"Ronnie McAteer"]) footballer.birthDateNumber = 19510101;
        if ([footballer.name containsString:@"Paul Kirk"]) footballer.birthDateNumber = 19510101;
        if ([footballer.name containsString:@"Drew Cooke"]) footballer.birthDateNumber = 19510101;
        if ([footballer.name containsString:@"George McCann"]) footballer.birthDateNumber = 19510101;
        if ([footballer.name containsString:@"Terry Collins"]) footballer.birthDateNumber = 19510101;
        if ([footballer.name containsString:@"Robert McQuillan"]) footballer.birthDateNumber = 19510101;
        if ([footballer.name containsString:@"Bob Gillespie"]) footballer.birthDateNumber = 19510101;
        if ([footballer.name containsString:@"George Lennox"]) footballer.birthDateNumber = 19510101;
        if ([footballer.name containsString:@"Bekir Barcin"]) footballer.birthDateNumber = 19490101;
        if ([footballer.name containsString:@"Necmi Perekli"]) footballer.birthDateNumber = 19480101;
        if ([footballer.name containsString:@"John Owens"]) footballer.birthDateNumber = 19550101;
        if ([footballer.name containsString:@"John King"]) footballer.birthDateNumber = 19550101;
        if ([footballer.name containsString:@"Barry Whitbread"]) footballer.birthDateNumber = 19490101;
        if ([footballer.name containsString:@"Leo Flanagan"]) footballer.birthDateNumber = 19570101;
        if ([footballer.name containsString:@"Sean Byrne"]) footballer.birthDateNumber = 19570101;
        if ([footballer.name containsString:@"Willie Crawley"]) footballer.birthDateNumber = 19570101;
        if ([footballer.name containsString:@"Oliver Ralph"]) footballer.birthDateNumber = 19570101;
            }

    //
    // Add to model
    footballer = [model addFootballer:footballer season:season];
    

    return footballer;
}

- (NSString *)extractNumberFromText:(NSString*)text {
    if (nonDigitCharacterSet == nil) {
        nonDigitCharacterSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    }
    return [[text componentsSeparatedByCharactersInSet:nonDigitCharacterSet] componentsJoinedByString:@""];
}

- (NSString *)correctName:(NSString*)name {
    //
    // Typos
    NSArray* namesWithTypos = @[ @"Mick Lyons", @"Mike Lyons",
                                 @"Joleon Lescott", @"Jolean Lescott",
                                 @"Pablo Zabelata", @"Pablo Zabaleta",
                                 @"Havard Nordtveit", @"Håvard Nordtveit",
                                 @"Langley", @"Ambrose Langley",
                                 @"William Higgins", @"Sandy Higgins",
                                 @"Jussi Jaaskelainen", @"Jussi Jääskeläinen",
                                 @"Remzi Giray Kacar", @"Remzi Kaçar",
                                 @"Miles", @"Freddie Miles",
                                 @"Rob Turnbull", @"Bobby Turnbull",
                                 @"Benjamin Howard Baker", @"Howard Baker",
                                 @"Thomas Wilson", @"Tommy Wilson",
                                 @"William Flint", @"Billy Flint",
                                 @"Laurie Crown", @"Laurie Crown",
                                 @"Alf Maitland", @"Alfred Maitland",
                                 @"Herbert Roberts", @"Herbie Roberts",
                                 @"Thomas Johnson", @"Tom Johnson",
                                 @"Reg Allen", @"Robert Allen",
                                 @"Les Compton", @"Denis Compton",
                                 @"Richard Robinson", @"Dicky Robinson",
                                 @"William Whitaker", @"Billy Whitaker",
                                 @"Glen Cockerill", @"Glenn Cockerill",
                                 @"Anthony Allen", @"Tony Allen",
                                 @"Sammy Chapman", @"Bob Chapman",
                                 @"Anthony Byrne", @"Tony Byrne",
                                 @"Dave Mackay", @"Dave MacKay",
                                 @"Léon Mond", @"Léon Jang Mond",
                                 @"Frank Carrodus", @"Francis Carrodus",
                                 @"Ken Thomson", @"Kenny Thomson",
                                 @"John Hills", @"Johnny Hills",
                                 @"Cecil Irwin", @"Cec Irwin",
                                 @"Delfi Geli", @"Delfí Geli",
                                 @"Graham Barlow", @"Andy Barlow",
                                 @"Robert Green", @"Robert Green",
                                 @"Giannelli Imbula Wanga", @"Giannelli Imbula",
                                 
                                 
    ];
    for (int i = 0; i < namesWithTypos.count / 2; i++) {
        if ([name containsString:namesWithTypos[i*2]]) {
            return namesWithTypos[i*2 + 1];
        }
    }
    return name;
}

+(NSString*)checkEncoding:(NSString*)name {
    //
    // Check name for url encoding
    // https://www.utf8-chartable.de/unicode-utf8-table.pl?utf8=dec&unicodeinhtml=dec
    if ([name containsString:@"&"]) {
        name = [name stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
        name = [name stringByReplacingOccurrencesOfString:@"&#39;" withString:@"'"];
        name = [name stringByReplacingOccurrencesOfString:@"&#192;" withString:@"À"];
        name = [name stringByReplacingOccurrencesOfString:@"&#193;" withString:@"Á"];
        name = [name stringByReplacingOccurrencesOfString:@"&#197;" withString:@"Å"];
        name = [name stringByReplacingOccurrencesOfString:@"&#201;" withString:@"É"];
        name = [name stringByReplacingOccurrencesOfString:@"&#208;" withString:@"Ð"];
        name = [name stringByReplacingOccurrencesOfString:@"&#211;" withString:@"Ó"];
        name = [name stringByReplacingOccurrencesOfString:@"&#214;" withString:@"Ö"];
        name = [name stringByReplacingOccurrencesOfString:@"&#216;" withString:@"Ø"];
        name = [name stringByReplacingOccurrencesOfString:@"&#220;" withString:@"Ü"];
        name = [name stringByReplacingOccurrencesOfString:@"&#222;" withString:@"Þ"];
        name = [name stringByReplacingOccurrencesOfString:@"&#223;" withString:@"ß"];
        name = [name stringByReplacingOccurrencesOfString:@"&#224;" withString:@"à"];
        name = [name stringByReplacingOccurrencesOfString:@"&#225;" withString:@"á"];
        name = [name stringByReplacingOccurrencesOfString:@"&#226;" withString:@"â"];
        name = [name stringByReplacingOccurrencesOfString:@"&#227;" withString:@"ã"];
        name = [name stringByReplacingOccurrencesOfString:@"&#228;" withString:@"ä"];
        name = [name stringByReplacingOccurrencesOfString:@"&#229;" withString:@"å"];
        name = [name stringByReplacingOccurrencesOfString:@"&#230;" withString:@"æ"];
        name = [name stringByReplacingOccurrencesOfString:@"&#231;" withString:@"ç"];
        name = [name stringByReplacingOccurrencesOfString:@"&#232;" withString:@"è"];
        name = [name stringByReplacingOccurrencesOfString:@"&#233;" withString:@"é"];
        name = [name stringByReplacingOccurrencesOfString:@"&#234;" withString:@"ê"];
        name = [name stringByReplacingOccurrencesOfString:@"&#235;" withString:@"ë"];
        name = [name stringByReplacingOccurrencesOfString:@"&#237;" withString:@"í"];
        name = [name stringByReplacingOccurrencesOfString:@"&#238;" withString:@"î"];
        name = [name stringByReplacingOccurrencesOfString:@"&#239;" withString:@"ï"];
        name = [name stringByReplacingOccurrencesOfString:@"&#240;" withString:@"ð"];
        name = [name stringByReplacingOccurrencesOfString:@"&#241;" withString:@"ñ"];
        name = [name stringByReplacingOccurrencesOfString:@"&#243;" withString:@"ó"];
        name = [name stringByReplacingOccurrencesOfString:@"&#244;" withString:@"ô"];
        name = [name stringByReplacingOccurrencesOfString:@"&#246;" withString:@"ö"];
        name = [name stringByReplacingOccurrencesOfString:@"&#248;" withString:@"ø"];
        name = [name stringByReplacingOccurrencesOfString:@"&#250;" withString:@"ú"];
        name = [name stringByReplacingOccurrencesOfString:@"&#252;" withString:@"ü"];
        name = [name stringByReplacingOccurrencesOfString:@"&#253;" withString:@"ý"];
    }
    if ([name containsString:@"\""]) {
        name = [name stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    }
    
    if ([name containsString:@"&"] && ![name containsString:@" & "]) {
        NSLog(@"Unhandled html escape: %@", name);
        int ian = 10;
    }
    return name;
}

- (int)addTeam:(NSString*)teamName  {
    teamName = [LFCSeasonParser modifyTeamName:teamName];
    return [model addTeam:teamName];
}

+ (NSString*)modifyTeamName:(NSString*)originalName {
    
    //
    // English
    if ([originalName compare:@"Ardwick"] == NSOrderedSame) return @"Manchester City";
    if ([originalName compare:@"Barnsley St Peters"] == NSOrderedSame) return @"Barnsley";
    if ([originalName compare:@"Bournemouth"] == NSOrderedSame) return @"AFC Bournemouth";
    if ([originalName compare:@"Bournemouth & Boscombe Athletic"] == NSOrderedSame) return @"AFC Bournemouth";
    if ([originalName compare:@"Birmingham"] == NSOrderedSame) return @"Birmingham City";
    if ([originalName compare:@"Burslem Port Vale"] == NSOrderedSame) return @"Port Vale";
    if ([originalName compare:@"Chester"] == NSOrderedSame) return @"Chester City";
    if ([originalName compare:@"Glossop FC"] == NSOrderedSame) return @"Glossop North End";
    if ([originalName compare:@"Leicester Fosse"] == NSOrderedSame) return @"Leicester City";
    if ([originalName compare:@"Millwall Athletic"] == NSOrderedSame) return @"Millwall";
    if ([originalName compare:@"Newton Heath"] == NSOrderedSame) return @"Manchester United";
    
    if ([originalName compare:@"Stoke"] == NSOrderedSame) return @"Stoke City";
    if ([originalName compare:@"Swansea Town"] == NSOrderedSame) return @"Swansea City";
    if ([originalName compare:@"The Wednesday"] == NSOrderedSame) return @"Sheffield Wednesday";
    if ([originalName compare:@"Woolwich Arsenal"] == NSOrderedSame) return @"Arsenal";
    if ([originalName compare:@"Wolves"] == NSOrderedSame) return @"Wolverhampton Wanderers";
    if ([originalName compare:@"Yeovil & Petters Utd"] == NSOrderedSame) return @"Yeovil";
    if ([originalName compare:@"Yeovil Town"] == NSOrderedSame) return @"Yeovil";
    if ([originalName compare:@"Nantwich"] == NSOrderedSame) return @"Nantwich Town";

    if ([originalName compare:@"Red Bull Salzburg"] == NSOrderedSame) return @"Salzburg";
    if ([originalName compare:@"MK Dons"] == NSOrderedSame) return @"Milton Keynes Dons";
 //   if ([originalName compare:@"Napoli"] == NSOrderedSame) return @"SSC Napoli";
    if ([originalName compare:@"Paris St Germain"] == NSOrderedSame) return @"Paris Saint-Germain";
    if ([originalName compare:@"FC Midtjylland"] == NSOrderedSame) return @"Midtjylland";
    if ([originalName compare:@"Atalanta B.C."] == NSOrderedSame) return @"Atalanta";
    if ([originalName compare:@"Red Bull Leipzig"] == NSOrderedSame) return @"VfB Leipzig";
    if ([originalName compare:@"Knattspyrnufélag Reykjavíkur"] == NSOrderedSame) return @"KR Reykjavik";

    if ([originalName compare:@"Porto"] == NSOrderedSame) return @"FC Porto";
    
    if ([originalName compare:@"Honved"] == NSOrderedSame) return @"Budapest Honved SE";
    if ([originalName compare:@"TSV Munich"] == NSOrderedSame) return @"TSV 1860 Munchen";
    if ([originalName compare:@"Vitoria Setubal"] == NSOrderedSame) return @"Vitória F.C.";
    if ([originalName compare:@"Servette"] == NSOrderedSame) return @"Servette Geneve";
    if ([originalName compare:@"Strømsgodset"] == NSOrderedSame) return @"Stromsgodset IF";
    if ([originalName compare:@"Hamburg SV"] == NSOrderedSame) return @"Hamburger SV";
    if ([originalName compare:@"Kuusysi Lahti"] == NSOrderedSame) return @"FC Kuusysi";
    if ([originalName compare:@"Auxerre"] == NSOrderedSame) return @"AJ Auxerre";
    if ([originalName compare:@"Swarowski Tirol"] == NSOrderedSame) return @"FC Tirol";
    if ([originalName compare:@"Spartak Vladikavkaz"] == NSOrderedSame) return @"Alania Vladikavkaz";
    if ([originalName compare:@"Brøndby"] == NSOrderedSame) return @"Brondby IF";
    if ([originalName compare:@"Mypa 47"] == NSOrderedSame) return @"MYPA";
    if ([originalName compare:@"Strasbourg"] == NSOrderedSame) return @"RC Strasbourg";
    if ([originalName compare:@"Rapid Bucharest"] == NSOrderedSame) return @"Rapid Bucuresti";
    if ([originalName compare:@"Vitesse Arnhem"] == NSOrderedSame) return @"Vitesse";
    if ([originalName compare:@"Steaua  Bucharest"] == NSOrderedSame) return @"Steaua Bucuresti";
    if ([originalName compare:@"Lille"] == NSOrderedSame) return @"Lille OSC";
    if ([originalName compare:@"Utrecht"] == NSOrderedSame) return @"DOS Utrecht";
    if ([originalName compare:@"Sparta Prague"] == NSOrderedSame) return @"AC Sparta Praha";
    if ([originalName compare:@"Braga"] == NSOrderedSame) return @"Sporting Braga";
    if ([originalName compare:@"Heart Of Midlothian"] == NSOrderedSame) return @"Heart of Midlothian";
    if ([originalName compare:@"Young Boys"] == NSOrderedSame) return @"BSC Young Boys";
    if ([originalName compare:@"Anzhi Makhachkala"] == NSOrderedSame) return @"FC Anji";
    if ([originalName compare:@"Zenit Saint Petersburg"] == NSOrderedSame) return @"Zenit St. Petersburg";
    if ([originalName compare:@"name"] == NSOrderedSame) return @"name";
    
    return originalName;
}

- (int)parseDateNumber:(NSString*)matchDateString {
    //
    // Dates come in as dd.MM.yyyy
    NSString* dateNumberString = [matchDateString stringByReplacingOccurrencesOfString:@"." withString:@""];
    int dateNumberIn = [dateNumberString intValue];
    int year = (dateNumberIn % 10000);
    int month = ((dateNumberIn / 10000) % 100);
    int day = dateNumberIn / 1000000;
    int dateNumberOut = (int)(year * 10000 + month * 100 + day);
    
    if (year < 1870 || year > 2030) {
        NSLog(@"Failed to parse date string: %@", matchDateString);
    }
    
    return dateNumberOut;
}

- (NSInteger)getSeasonIndex:(NSInteger)season {
    if (season <= 1899) {
        return 96 + season - 1892;
    }
    if (season <= 1909) {
        return 86 + season - 1900;
    }
    if (season <= 1914) {
        return 80 + season - 1910;
    }
    if (season == 1915) return 112;
    if (season == 1916) return 106;
    if (season == 1917) return 113;
    if (season == 1918) return 107;
    if (season == 1919) return 85;
    if (season <= 1929) {
        return 70 + season - 1920;
    }
    if (season <= 1938) {
        return 61 + season - 1930;
    }
    if (season == 1939) return 105;
    if (season == 1940) return 108;
    if (season == 1941) return 109;
    if (season == 1942) return 114;
    if (season == 1943) return 110;
    if (season == 1944) return 111;
    if (season == 1945) return 60;
    if (season <= 1958) {
        return 47 + season - 1946;
    }
    if (season <= 1980) {
        return 1 + season - 1959;
    }
    if (season == 1981) return 24;
    if (season == 1982) return 23;
    if (season <= 1990) {
        return 25 + season - 1983;
    }
    if (season == 1991) return 34;
    if (season == 1992) return 33;
    if (season <= 1995) {
        return 35 + season - 1993;
    }
    if (season <= 1998) {
        return 39 + season - 1996;
    }
    if (season == 1999) return 43;
    if (season == 2000) return 42;
    if (season <= 2003) {
        return 44 + season - 2001;
    }
    if (season == 2004) return 104;
    if (season <= 2021) {
        return 115 + season - 2005;
    }
    return 0;
}




@end
