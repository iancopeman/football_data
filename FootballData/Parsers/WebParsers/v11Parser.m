//
//  v11Parser.m
//  FootballData
//
//  Created by Ian Copeman on 13/09/2021.
//

#import "v11Parser.h"
#import "FootballModel.h"
#import "StringSearch.h"
#import "WebPageManager.h"

// https://www.11v11.com/competitions/premier-league/

#define FilePath (@"/11v11/")


@implementation v11Parser {
    WebPageManager* webPageManager;
    FootballModel* model;
    FileManager* fileManager;
    NSString* dataPath;
    NSString* baseURL;
    
}

- (NSArray*)getCompetionArray {
    NSArray* parseArray = @[
        @(CompetitionPremierLeague), @(1991),
        @(CompetitionChampionship), @(2015),
        @(CompetitionLeagueOne), @(2016),
        @(CompetitionLeagueTwo), @(2016),
        @(CompetitionFACup), @(1975),
        @(CompetitionLeagueCup), @(1975),
    ];
    
    return parseArray;
}

- (id) init {
    self = [super init];
    if (self != nil) {
        self->model = [FootballModel getModel];
        self->fileManager = [[FileManager alloc] init];
        self->dataPath  = [[NSUserDefaults standardUserDefaults] objectForKey:@"dataPath"];
        self->webPageManager = [WebPageManager getWebPageManager];
        self->baseURL = @"https://www.11v11.com";

    }
    return self;
}


- (void)test {
    
    NSUInteger matchCount = [model getMatchCount];
    NSUInteger playerCount = [model getFootballerCount]; // NL- 2010, FA - 1961

    // 2011
    // https://www.11v11.com/matches/heart-of-midlothian-v-tottenham-hotspur-18-august-2011-294986/
    // https://www.11v11.com/matches/tottenham-hotspur-v-heart-of-midlothian-25-august-2011-295061/

    // 2013
    // https://www.11v11.com/matches/dinamo-tbilisi-v-tottenham-hotspur-22-august-2013-304951/
    // https://www.11v11.com/matches/tottenham-hotspur-v-dinamo-tbilisi-29-august-2013-305037/
    int season = 2013;
    NSString* matchURL = @"https://www.11v11.com/matches/tottenham-hotspur-v-dinamo-tbilisi-29-august-2013-305037/";
    [self parseMatch:matchURL seasonID:season];

    NSUInteger newMatchCount = [model getMatchCount];
    NSLog(@"Added %lu matches", newMatchCount - matchCount);
    matchCount = newMatchCount;
    NSUInteger newPlayerCount = [model getFootballerCount];
    NSLog(@"Added %lu footballers", newPlayerCount - playerCount);

}

- (void)parseCompetition:(int)competition season:(NSInteger)season {

    
    NSArray* parseArray = @[
        @(CompetitionPremierLeague), @(1991), @"%d", @"",
        @(CompetitionChampionship), @(2015), @"%dCH", @"",
        @(CompetitionLeagueOne), @(2016), @"%dL1", @"",
        @(CompetitionLeagueTwo), @(2016), @"%dL2", @"",
        @(CompetitionFACup), @(1975), @"%dFA", @"",
        @(CompetitionLeagueCup), @(1975), @"%dLC", @"league-cup",
    ];

    
    NSUInteger matchCount = [model getMatchCount];
    NSUInteger playerCount = [model getFootballerCount]; // NL- 2010, FA - 1961

    for (int i = 0; i < parseArray.count / 4; i++) {
        if ([parseArray[i * 4] intValue] == competition) {
            if ([parseArray[i * 4 + 1] intValue] > season) {
                NSLog(@"Attempting to parse %@ with unsupported season: %ld", [Match getCompetitionShortName:competition], (long)season);
                return;
            }
            
            NSLog(@"11v11 parsing season: %@ - %ld", [Match getCompetitionShortName:competition], (long)season);
            NSString* dataKeyFormat = parseArray[i * 4 + 2];
            NSString* dataKey = [NSString stringWithFormat:dataKeyFormat, season];
            NSString* webPageString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:SeasonFilePath dataKey:dataKey];
            if (webPageString == nil) {
                continue;
                /*
                // https://www.11v11.com/competitions/league-cup/1975/matches/
                NSString* fullURL = [NSString stringWithFormat:@"%@/competitions/%@/%d/matches/", baseURL, parseArray[i * 4 + 3], season];
                webPageString = [webPageManager getWebPage:fullURL];
                [fileManager savePersistedString:dataPath dataName:FilePath dataType:SeasonFilePath dataKey:dataKey dataString:webPageString];
                 */
            }
            
            if (webPageString != nil) {
                if ([webPageString containsString:@"Server Error"]) {
                    break;
                }
                [self parseSeason:webPageString seasonID:(int)season];
            }
            break;
        }
    }
    
    NSUInteger newMatchCount = [model getMatchCount];
    NSLog(@"Added %lu matches", newMatchCount - matchCount);
    matchCount = newMatchCount;
    NSUInteger newPlayerCount = [model getFootballerCount];
    NSLog(@"Added %lu footballers", newPlayerCount - playerCount);
    
}

- (void)parseAll {
    
    //NSString* baseURL = @"https://www.11v11.com/competitions/premier-league";
    
    //
    // Note: Can't load these pages directly, as they lazy load.
    // Simply loading them will result in missing matches at the bottom of the page
    //
    
    //int seasonIndex = 1992;
    int seasonIndex = 2001;
    bool ok = true;
    int count = 0;
    //NSArray* matchFormatList = @[@"%d", @"%dFA", @"%dLC", @"%dCH", @"%dL1", @"%dL2" ];
    NSArray* matchFormatList = @[ @"%dFA"];
    NSUInteger matchCount = [model getMatchCount];
    NSUInteger playerCount = [model getFootballerCount]; // NL- 2010, FA - 1961

    do {
       // NSString* fullURL = [NSString stringWithFormat:@"%@/%d/matches", baseURL, seasonIndex + 1];
        NSLog(@"11v11 parsing season: %d", seasonIndex);
        for (NSString* dataKeyFormat in matchFormatList) {
            NSString* dataKey = [NSString stringWithFormat:dataKeyFormat, seasonIndex];
            NSString* webPageString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:SeasonFilePath dataKey:dataKey];
            if (webPageString == nil) {
                continue;
            }
            
            if (webPageString != nil) {
                if ([webPageString containsString:@"Server Error"]) {
                    break;
                }
                [self parseSeason:webPageString seasonID:seasonIndex];
            }
            else {
                ok = false;
            }
        }
        
        seasonIndex++;

        
        NSUInteger newMatchCount = [model getMatchCount];
        NSLog(@"Added %lu matches", newMatchCount - matchCount);
        matchCount = newMatchCount;
        NSUInteger newPlayerCount = [model getFootballerCount];
        NSLog(@"Added %lu footballers", newPlayerCount - playerCount);

        
        if (seasonIndex == 2021) break;
        
        [NSThread sleepForTimeInterval:1.0f];
        if (++count == 1) break;

    } while (ok);
     
 
}

- (void)parseSeason:(NSString*)webData seasonID:(int)seasonID {
    
    StringSearch* stringSearch = [StringSearch initWithString:webData];
    NSString* matchText = [stringSearch getText:@"<a href=\"" endKey:@"\""];
    int count = 0;
    while (matchText != nil) {
        //
        // Get match
        NSString* matchURL = [NSString stringWithFormat:@"%@%@", baseURL, matchText];
        NSLog(@"Parse match url: %@", matchURL);

        [self parseMatch:matchURL seasonID:seasonID];
        //if (++count == 1) break;
        
        matchText = [stringSearch getText:@"<a href=\"" endKey:@"\""];
        if (matchText == nil) {
            // NSLog(@"Last match url: %@", matchURL);
        }
        
        
        
    }

    // NSLog(@"11v11 parse complete, %d matches", count);
    // int length = webData.length;// NSString* test = [webData substringFromIndex:[stringSearch getStringPos]];
    
    int ian = 10;
    
}

- (void)parseMatch:(NSString*)matchURL seasonID:(int)seasonID {
    
    NSString* dataKey = [self generateDataKey:matchURL];
    NSString* dataType = [NSString stringWithFormat:@"%@%d/", MatchFilePath, seasonID];
    NSString* webPageString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:dataType dataKey:dataKey];
    if (webPageString == nil) {
        webPageString = [webPageManager getWebPage:matchURL];
        [fileManager savePersistedString:dataPath dataName:FilePath dataType:dataType dataKey:dataKey dataString:webPageString];
    }
    if (webPageString == nil) {
        return;
    }
    
    //
    // Get date
    StringSearch* stringSearch = [StringSearch initWithString:webPageString];
    NSString* dateString = [stringSearch getTextWithKey:@"<title>" startKey:@", " endKey:@" - 11v11"];

    //
    // Basic data
    NSString* basicData = [stringSearch getSectionWithKey:@"<div class=\"basicData" startKey:@"<div" endKey:@"</div"];
    StringSearch* stringSearchData = [StringSearch initWithString:basicData];
    NSString* matchRefereeString = [stringSearchData getTextWithKey:@"Referee" startKey:@"<td>" endKey:@"</td"];
    NSString* matchCompetitionString = [stringSearchData getTextWithKey:@"Competition" startKey:@"<td>" endKey:@"</td"];
    NSString* matchLocationString = [stringSearchData getTextWithKey:@"Venue" startKey:@"<td>" endKey:@"</td"];
    NSString* matchAttendanceString = [stringSearchData getTextWithKey:@"Attendance" startKey:@"<td>" endKey:@"</td"];
    matchAttendanceString = [matchAttendanceString stringByReplacingOccurrencesOfString:@"," withString:@""];
    int matchAttendance = [matchAttendanceString intValue];

    NSString* teamNews = [stringSearch getSectionWithKey:@"<div class=\"teams-new" startKey:@"<div" endKey:@"</div"];
    StringSearch* stringSearchTeams = [StringSearch initWithString:teamNews];

    //
    // Teams and scores
    NSString* matchTeam1 = [stringSearchTeams getText:@"<h2" endKey:@"</h2>"];
    StringSearch* stringSearchTeamScore = [StringSearch initWithString:matchTeam1];
    NSString* team1ScoreString = [stringSearchTeamScore getText:@"score\">" endKey:@"<"];
    int team1Score = [team1ScoreString intValue];
    NSString* team1String = [stringSearchTeamScore getTextWithKey:@"<a href" startKey:@">" endKey:@"<"];
    if (team1String == nil) {
        team1String = [stringSearchTeamScore getText:@"\n" endKey:@"\n"];
    }
    NSString* matchTeam2 = [stringSearchTeams getText:@"<h2" endKey:@"</h2>"];
    stringSearchTeamScore = [StringSearch initWithString:matchTeam2];
    NSString* team2ScoreString = [stringSearchTeamScore getText:@"score\">" endKey:@"<"];
    int team2Score = [team2ScoreString intValue];
    NSString* team2String = [stringSearchTeamScore getTextWithKey:@"<a href" startKey:@">" endKey:@"<"];
    if (team2String == nil) {
        team2String = [stringSearchTeamScore getText:@"\n" endKey:@"\n"];
    }
    
    //
    // Add Teams
    if ([team2String containsString:@"Liverpool"]) {
        int ian = 10;
    }
    int teamID1 = [self addTeam:team1String];
    int teamID2 = [self addTeam:team2String];
    if (teamID1 == 0) {
        int ian = 10;
    }
    if (teamID2 == 0) {
        int ian = 10;
    }
    
    //
    // Parse Date
    int matchDateNumber = [self parseDateNumber:dateString];
    if (matchDateNumber == 19911116 && [team1String containsString:@"Mansfield"]) {
        matchDateNumber = 19911127;
    }
    if (matchDateNumber == 19911116 && [team1String containsString:@"Windsor"]) {
        matchDateNumber = 19911126;
    }

    //
    // Check match
    Match* match = [model getMatch:matchDateNumber teamID1:teamID1 teamID2:teamID2];
    if (!match) {
        NSLog(@"** Team: %@  Date: %d ", team1String, matchDateNumber);
        match = [[Match alloc] init];
        [match setMatchTeamsAndDateNumber:matchDateNumber team1:teamID1 team2:teamID2];
        [match setSeason:seasonID];
        [match setHomeTeamID:teamID1]; // Home team is first team 
       // NSLog(@"11v11 creating match: %@: %@ v %@ (%@)", dateString, team1String, team2String, matchCompetitionString);
        [model matchIsModified:match];
        [model addMatch:match];
    }
    
    //
    //  Check of we can improve match parsing
    if (match.matchParseState >= MatchParseStateEventsFull) return;
    MatchParseState oldMatchState = match.matchParseState;

    //
    // Full parse
    //
    // Goals
    NSString* goalsText1 = nil;
    NSString* goalsText2 = nil;
    NSString* goalsText = [stringSearchTeams getSectionWithKey:@"<div class=\"goals" startKey:@"<div" endKey:@"</div"];
    if (goalsText != nil) {
        StringSearch* stringSearchGoals = [StringSearch initWithString:goalsText];
        goalsText1 = [stringSearchGoals getTextWithKey:@"<h4>Goals:</h4>" startKey:@"<tbody>" endKey:@"</tbody>"];
        goalsText2 = [stringSearchGoals getTextWithKey:@"<h4>Goals:</h4>" startKey:@"<tbody>" endKey:@"</tbody>"];
    }
    
    //
    // Lineups
    NSString* lineupsText1 = [stringSearchTeams getSectionWithKey:@"<div class=\"home" startKey:@"<div" endKey:@"</div"];
    NSString* lineupsText2 = [stringSearchTeams getSectionWithKey:@"<div class=\"away" startKey:@"<div" endKey:@"</div"];
    
    //
    // Subs
    NSString* subsText1 = nil;
    NSString* subsText2 = nil;
    NSString* subsText = [stringSearchTeams getSectionWithKey:@"<div class=\"substitutions" startKey:@"<div" endKey:@"</div"];
    if (subsText != nil) {
        StringSearch* stringSearchSubs = [StringSearch initWithString:subsText];
        subsText1 = [stringSearchSubs getSectionWithKey:@"<div class=\"home" startKey:@"<div" endKey:@"</div"];
        subsText2 = [stringSearchSubs getSectionWithKey:@"<div class=\"away" startKey:@"<div" endKey:@"</div"];
    }
    
    
    //
    // Cards
    NSString* cardsText1 = nil;
    NSString* cardsText2 = nil;
    NSString* cardsText = [stringSearchTeams getSectionWithKey:@"<div class=\"cards" startKey:@"<div" endKey:@"</div"];
    if (cardsText != nil) {
        StringSearch* stringSearchCards = [StringSearch initWithString:cardsText];
        cardsText1 = [stringSearchCards getTextWithKey:@"<h4>Cards:</h4>" startKey:@"<tbody>" endKey:@"</tbody>"];
        cardsText2 = [stringSearchCards getTextWithKey:@"<h4>Cards:</h4>" startKey:@"<tbody>" endKey:@"</tbody>"];
    }
    
    //
    // Sub lineup
    NSString* subsLineupText1 = [stringSearchTeams getSectionWithKey:@"<div class=\"home" startKey:@"<div" endKey:@"</div"];
    NSString* subsLineupText2 = [stringSearchTeams getSectionWithKey:@"<div class=\"away" startKey:@"<div" endKey:@"</div"];
    
    //
    // ToDo:
    // Parse footballers to match object
    //
    // Calculate winner
    EventResultType resultType1 = EventResultDraw;
    EventResultType resultType2 = EventResultDraw;
    if (team1Score > team2Score) {
        resultType1 = EventResultWin;
        resultType2 = EventResultLose;
    }
    if (team1Score < team2Score) {
        resultType1 = EventResultLose;
        resultType2 = EventResultWin;
    }

    //
    // Get players
    NSMutableArray* teamNames1 = [NSMutableArray arrayWithCapacity:20];
    NSMutableArray* teamFootballers1 = [NSMutableArray arrayWithCapacity:20];
    NSMutableArray* teamEvents1 = [NSMutableArray arrayWithCapacity:20];
    [self processSquad:lineupsText1 teamID:teamID1 teamNames:teamNames1 teamIDs:teamFootballers1 seasonID:seasonID];
    [self processApperances:teamFootballers1 teamEvents:teamEvents1 cleanSheet:team2Score == 0 resultType:resultType1];
    [self processSquad:subsLineupText1 teamID:teamID1 teamNames:teamNames1 teamIDs:teamFootballers1 seasonID:seasonID];
    
    NSMutableArray* teamNames2 = [NSMutableArray arrayWithCapacity:20];
    NSMutableArray* teamFootballers2 = [NSMutableArray arrayWithCapacity:20];
    NSMutableArray* teamEvents2 = [NSMutableArray arrayWithCapacity:20];
    [self processSquad:lineupsText2 teamID:teamID2 teamNames:teamNames2 teamIDs:teamFootballers2 seasonID:seasonID];
    [self processApperances:teamFootballers2 teamEvents:teamEvents2 cleanSheet:team1Score == 0 resultType:resultType2];
    [self processSquad:subsLineupText2 teamID:teamID2 teamNames:teamNames2 teamIDs:teamFootballers2 seasonID:seasonID];
    
    //
    // Get goals
    [self processGoals:goalsText1 teamNames1:teamNames1 teamIDs1:teamFootballers1 teamEvents1:teamEvents1  teamNames2:teamNames2 teamIDs2:teamFootballers2 teamEvents2:teamEvents2];
    [self processGoals:goalsText2 teamNames1:teamNames2 teamIDs1:teamFootballers2 teamEvents1:teamEvents2  teamNames2:teamNames1 teamIDs2:teamFootballers1 teamEvents2:teamEvents1];
    
    //
    // Get subs
    [self processSubs:subsText1 teamNames:teamNames1 teamIDs:teamFootballers1 teamEvents:teamEvents1 cleanSheet:team2Score == 0 resultType:resultType1];
    [self processSubs:subsText2 teamNames:teamNames2 teamIDs:teamFootballers2 teamEvents:teamEvents2 cleanSheet:team1Score == 0 resultType:resultType2];
    
    //
    // Get cards
    [self processCards:cardsText1 teamNames:teamNames1 teamIDs:teamFootballers1 teamEvents:teamEvents1];
    [self processCards:cardsText2 teamNames:teamNames2 teamIDs:teamFootballers2 teamEvents:teamEvents2];
    
    //
    // Add match
    match.competition = [model addCompetition:matchCompetitionString];
    if (match.competition == CompetitionFACup) {
        match.competitionFlag = [self getCompetitionFlag:matchCompetitionString];
    }
    if (matchLocationString.length != 0 && [match.locationString compare:matchLocationString] != NSOrderedSame) {
        match.locationString = matchLocationString;
        [model matchIsModified:match];
    }
    if (matchRefereeString.length != 0 && [match.refereeString compare:matchRefereeString] != NSOrderedSame) {
        match.refereeString = matchRefereeString;
        [model matchIsModified:match];
    }
    match.attendance = matchAttendance;
    [match setReferee:matchRefereeString];
    [match setLocation:matchLocationString];
    [match setScore:team1Score teamID:teamID1];
    [match setScore:team2Score teamID:teamID2];
    [match setFootballers:teamFootballers1 teamID:teamID1];
    [match setFootballers:teamFootballers2 teamID:teamID2];
    [match addMatchEvents:teamEvents1 teamID:teamID1 hasAssists:NO];
    [match addMatchEvents:teamEvents2 teamID:teamID2 hasAssists:NO];
    if (oldMatchState < match.matchParseState) {
        [model addMatchEvents:match];
        [model matchIsModified:match];
    }
    if (match.modified) {
        int ian = 10;
    }

        
    return;
}

- (int)getCompetitionFlag:(NSString*)competitionName {
    if ([competitionName localizedCaseInsensitiveContainsString:@"1st round replay"]) {
        return 1;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"1st round"]) {
        return 1;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"2nd round replay"]) {
        return 2;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"2nd round"]) {
        return 2;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"3rd round replay"]) {
        return 3;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"3rd round"]) {
        return 3;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"4th round replay"]) {
        return 4;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"4th round"]) {
        return 4;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"5th round replay"]) {
        return 5;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"5th round"]) {
        return 5;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"Quarter-final replay"]) {
        return COMPETITION_QUARTER_FINAL_FLAG;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"Quarter-final"]) {
        return COMPETITION_QUARTER_FINAL_FLAG;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"6th round replay"]) {
        return COMPETITION_QUARTER_FINAL_FLAG;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"6th round"]) {
        return COMPETITION_QUARTER_FINAL_FLAG;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"Semi-final replay"]) {
        return COMPETITION_SEMI_FINAL_FLAG;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"Semi-final"]) {
        return COMPETITION_SEMI_FINAL_FLAG;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"Final replay"]) {
        return COMPETITION_FINAL_FLAG;
    }
    if ([competitionName localizedCaseInsensitiveContainsString:@"Final"]) {
        return COMPETITION_FINAL_FLAG;
    }
    NSLog(@"Unknown Competition flag: %@", competitionName);
    return 0;
}

- (void)processApperances:(NSMutableArray*)teamFootballer teamEvents:(NSMutableArray*)teamEvents cleanSheet:(bool)cleanSheet resultType:(EventResultType)resultType  {
    for (int i = 0; i < teamFootballer.count; i++) {
        int index = ((Footballer*)teamFootballer[i]).index;
        [teamEvents addObject:[Event createEventAppearanceNumber:index position:i + 1 shirt:0 cleanSheet:cleanSheet resultType:resultType]];
    }
}

- (void)processCards:(NSString*)stringCards teamNames:(NSMutableArray*)teamNames teamIDs:(NSMutableArray*)teamFootballers teamEvents:(NSMutableArray*)teamEvents  {
    StringSearch* stringSearchCards = [StringSearch initWithString:stringCards];
    NSString* section = [stringSearchCards getText:@"<tr" endKey: @"</tr"];
    while (section != nil) {
        StringSearch* stringSearchEvent = [StringSearch initWithString:section];
        NSString* nameString = [stringSearchEvent getText:@"td>" endKey:@"</td>"];
        NSString* timeString = [stringSearchEvent getText:@"time\">" endKey:@"<"];
        int time = [timeString intValue];
        NSString* typeString = [stringSearchEvent getText:@"card\">" endKey:@"<"];
        for (int i = 0; i < teamNames.count; i++) {
            NSString* name = teamNames[i];
            if ([nameString containsString:name]) {
                int index = ((Footballer*)teamFootballers[i]).index;
                if ([typeString containsString:@"R"]) {
                    [teamEvents addObject:[Event createEventRedCardNumber:index time:time]];
                }
                else {
                   [teamEvents addObject:[Event createEventYellowCardNumber:index time:time]];
                }
                break;
            }
        }
        section = [stringSearchCards getText:@"<tr" endKey: @"</tr"];
    }
}


- (void)processSubs:(NSString*)stringSubs teamNames:(NSMutableArray*)teamNames teamIDs:(NSMutableArray*)teamFootballers teamEvents:(NSMutableArray*)teamEvents  cleanSheet:(bool)cleanSheet resultType:(EventResultType)resultType {
    StringSearch* stringSearchSubs = [StringSearch initWithString:stringSubs];
    NSString* section = [stringSearchSubs getText:@"<tr" endKey: @"</tr"];
    while (section != nil) {
        StringSearch* stringSearchEvent = [StringSearch initWithString:section];
        NSString* nameStringIn = [stringSearchEvent getText:@"substitute\">" endKey:@"<"];
        NSString* nameStringOut = [stringSearchEvent getText:@"substituted\">" endKey:@"<"];
        NSString* timeString = [stringSearchEvent getText:@"time\">" endKey:@"<"];
        int time = [timeString intValue];
        int playerIn = 0;
        int playerOut = 0;
        for (int i = 0; i < teamNames.count; i++) {
            NSString* name = teamNames[i];
            if ([nameStringIn containsString:name]) {
                playerIn = ((Footballer*)teamFootballers[i]).index;
            }
            if ([nameStringOut containsString:name]) {
                playerOut = ((Footballer*)teamFootballers[i]).index;
            }
            if (playerIn != 0 &&  playerOut != 0) break;
        }
        if (playerIn == 0 ||  playerOut == 0) {
            int ian = 10;
        } else {
            [teamEvents addObject:[Event createEventSubNumber:playerIn playerIDOut:playerOut time:time cleanSheet:cleanSheet resultType:resultType]];
        }
        section = [stringSearchSubs getText:@"<tr" endKey: @"</tr"];
    }
}

- (void)processGoals:(NSString*)stringGoals teamNames1:(NSMutableArray*)teamNames1 teamIDs1:(NSMutableArray*)teamFootballer1 teamEvents1:(NSMutableArray*)teamEvents1  teamNames2:(NSMutableArray*)teamNames2 teamIDs2:(NSMutableArray*)teamFootballer2 teamEvents2:(NSMutableArray*)teamEvents2 {
    StringSearch* stringSearchGoals = [StringSearch initWithString:stringGoals];
    NSString* section = [stringSearchGoals getText:@"<tr" endKey: @"</tr"];
    while (section != nil) {
        StringSearch* stringSearchEvent = [StringSearch initWithString:section];
        NSString* nameString = [stringSearchEvent getText:@"td>" endKey:@"</td>"];
        NSString* qualifierString = [stringSearchEvent getText:@"qualifiers\">" endKey:@"<"];
        NSString* timeString = [stringSearchEvent getText:@"time\">" endKey:@"<"];
        int time = [timeString intValue];
        if ([qualifierString containsString:@"own goal"]) {
            //
            // Own goal - find name in oppositer team
            for (int i = 0; i < teamNames2.count; i++) {
                NSString* name = teamNames2[i];
                if ([nameString containsString:name]) {
                    int index = ((Footballer*)teamFootballer2[i]).index;
                    [teamEvents2 addObject:[Event createEventOwnGoalNumber:index time:time]];
                    break;
                }
            }
        }
        else {
            for (int i = 0; i < teamNames1.count; i++) {
                NSString* name = teamNames1[i];
                if ([nameString containsString:name]) {
                    int index = ((Footballer*)teamFootballer1[i]).index;
                    if ([qualifierString containsString:@"penalty"]) {
                        [teamEvents1 addObject:[Event createEventPenaltyNumber:index time:time]];
                    }
                    else {
                        [teamEvents1 addObject:[Event createEventGoalNumber:index playerAssistID:0 time:time]];
                    }
                    break;
                }
            }
        }
        section = [stringSearchGoals getText:@"<tr" endKey: @"</tr"];
    }

}

- (void)processSquad:(NSString*)squad teamID:(int)teamID teamNames:(NSMutableArray*)teamNames teamIDs:(NSMutableArray*)teamFootballers seasonID:(int)seasonID {
    StringSearch* stringSearch = [StringSearch initWithString:squad];
    NSString* playerText = [stringSearch getSectionWithKey:@"<div class=\"player" startKey:@"<div" endKey:@"</div"];
    while (playerText != nil) {
        StringSearch* stringSearchPlayer = [StringSearch initWithString:playerText];
        NSString* urlString = [stringSearchPlayer getText:@"a href=\"" endKey:@"\""];
        NSString* nameString = [stringSearchPlayer getText:@">" endKey:@"<"];
        nameString = [nameString stringByTrimmingCharactersInSet:
                       [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString* idString = [self generateDataKey:urlString];
        
        //
        // find player
        Footballer* footballer = [self parsePlayer:nameString teamID:teamID idString:idString urlString:urlString seasonID:seasonID];
        if (footballer == nil) {
            footballer = [self parsePlayer:nameString teamID:teamID idString:idString urlString:urlString seasonID:seasonID];
            int ian = 10;
        }
        
        [teamNames addObject:nameString];
        [teamFootballers addObject:footballer];

        playerText = [stringSearch getSectionWithKey:@"<div class=\"player" startKey:@"<div" endKey:@"</div"];
        
    }
}

- (Footballer*)parsePlayer:(NSString*)playerName teamID:(int)teamID idString:(NSString*)idString urlString:(NSString*)urlString seasonID:(int)seasonID  {
    
    //
    // Get footballer identifier
    int identInt = idString.intValue;
    if (identInt == 0) {
        return nil;
    }
    if (identInt == 3661) {
        int ian = 10;;
    }
    
    //
    // Lookup player in model
    Footballer* footballer = [model checkFootballer:identInt identKey:@"parseIndex11v11" teamID:teamID season:seasonID];
    if (footballer != nil) return footballer;
    
    //
    // Add footballer
    NSString* fullURL = [NSString stringWithFormat:@"%@%@", baseURL, urlString];
    NSString* webPageString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:FootballerFilePath dataKey:idString];
    if (webPageString == nil) {
        webPageString = [webPageManager getWebPage:fullURL];
        [fileManager savePersistedString:dataPath dataName:FilePath dataType:FootballerFilePath dataKey:idString dataString:webPageString];
    }
    if (webPageString == nil) {
        return nil;
    }
    
    //
    // Create new footballer
    footballer = [[Footballer alloc] init];
    footballer.name = playerName;
    footballer.parseIndex11v11 = identInt;
    footballer.teamID = teamID;
    
    //
    // Country
    StringSearch* stringSearchProfile = [StringSearch initWithString:webPageString];
    NSString* countryString = [stringSearchProfile getTextWithKey:@"country\">" startKey:@"<img src=\"" endKey:@" class=\"flag"];
    if (countryString != nil) {
        NSRange range = [countryString rangeOfString:@"/" options:NSBackwardsSearch];
        NSRange range2 = [countryString rangeOfString:@"." options:NSBackwardsSearch];
        NSString* countryCodeString = [countryString substringWithRange:NSMakeRange(range.location + 1, range2.location - range.location - 1)];
        footballer.countryID = [Team getCountryID:countryCodeString];

    }
    
    
    //
    // Add attributes
    NSString* basicData = [stringSearchProfile getSectionWithKey:@"<div class=\"basicData" startKey:@"<div" endKey:@"</div"];
    StringSearch* rowSearch = [StringSearch initWithString:basicData];
    while (rowSearch != nil) {
        NSString* profileAttribute = [rowSearch getText:@"b>" endKey: @"</b"];
        NSString* profileValue = [rowSearch getText:@"<td>" endKey:@"</td"];
        if (profileAttribute == nil || profileValue == nil) break;
        [footballer addAttribute:profileAttribute value:profileValue];
    }
    
    if (footballer.birthDateNumber == 0) {
          //
          // Fudge date of birth
          footballer.birthDateNumber = (seasonID - 25) * 10000 + 0101;
    }

    
    //
    // Add to model
    footballer = [model addFootballer:footballer season:seasonID];
    
    return footballer;
    
}

- (NSString*)generateDataKey:(NSString*)urlString {
    //
    // URL: https://www.11v11.com/matches/arsenal-v-coventry-city-14-august-1993-21249/
    NSRange range = [urlString rangeOfString:@"-" options:NSBackwardsSearch];
    if (range.length == 0) return nil;
    NSString* idString = [urlString substringWithRange:NSMakeRange(range.location + 1, urlString.length - range.location - 2)];
    return idString;
}

- (int)parseDateNumber:(NSString*)dateString {
    //
    // 15 August 1992
    static NSDateFormatter* dateFormatIn = nil;
    if (dateFormatIn == nil) {
        dateFormatIn = [[NSDateFormatter alloc] init];
        [dateFormatIn setDateFormat:@"dd MMMM yyyy"];
    }
    static NSCalendar* gregorianCalandar = nil;
    if (gregorianCalandar == nil) {
        gregorianCalandar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    }
    @try {
        NSDate* date = [dateFormatIn dateFromString:dateString];
        NSDateComponents *components = [gregorianCalandar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:date];
        return (int)([components year] * 10000 + [components month] * 100 + [components day]);
    }
    
    @catch ( NSException *e ) {
        NSLog(@"Failed to parse date: %@", dateString);
        return 0;
    }
}


- (int)addTeam:(NSString*)teamName {
    if ([teamName compare:@"Newport (IOW)"] == NSOrderedSame) {
        teamName = @"Newport IOW";
    }
    if ([teamName compare:@"Guiseley"] == NSOrderedSame) {
        teamName = @"Guiseley AFC";
    }
    if ([teamName compare:@"Billericay Town"] == NSOrderedSame) {
        teamName = @"Billericay";
    }
    if ([teamName compare:@"Hednesford"] == NSOrderedSame) {
        teamName = @"Hednesford Town";
    }
    if ([teamName compare:@"Kings Lynn"] == NSOrderedSame) {
        teamName = @"King's Lynn";
    }
    if ([teamName compare:@"Kings Lynn Town"] == NSOrderedSame) {
        teamName = @"King's Lynn";
    }
    if ([teamName compare:@"Lancaster"] == NSOrderedSame) {
        teamName = @"Lancaster City";
    }
    if ([teamName compare:@"Radcliffe Borough"] == NSOrderedSame) {
        teamName = @"Radcliffe Boro";
    }
    if ([teamName compare:@"Harrogate Railway Athletic"] == NSOrderedSame) {
        teamName = @"Harrogate Railway";
    }
    if ([teamName compare:@"Grantham Town"] == NSOrderedSame) {
        teamName = @"Grantham";
    }
    if ([teamName compare:@"Bishops Stortford"] == NSOrderedSame) {
        teamName = @"Bishop's Stortford";
    }
    if ([teamName compare:@"FC United of Manchester"] == NSOrderedSame) {
        teamName = @"United of Manchester";
    }
    if ([teamName compare:@"Hayes & Yeading United"] == NSOrderedSame) {
        teamName = @"Hayes & Yeading";
    }
    if ([teamName compare:@"FC Halifax Town"] == NSOrderedSame) {
        teamName = @"Halifax Town";
    }
    if ([teamName compare:@"Maidstone United [1992]"] == NSOrderedSame) {
        teamName = @"Maidstone United";
    }
    if ([teamName compare:@"Hereford FC"] == NSOrderedSame) {
        teamName = @"Hereford United";
    }
    if ([teamName compare:@"South Shields [1974]"] == NSOrderedSame) {
        teamName = @"South Shields";
    }
    if ([teamName compare:@"Knowsley United"] == NSOrderedSame) {
        teamName = @"Knowsley United FC";
    }
    if ([teamName compare:@"Molesey"] == NSOrderedSame) {
        teamName = @"Molesey FC";
    }
    if ([teamName compare:@"Kettering"] == NSOrderedSame) {
        teamName = @"Kettering Town";
    }
    if ([teamName compare:@"Rugby"] == NSOrderedSame) {
        teamName = @"Rugby Town";
    }
    if ([teamName compare:@"Telford United"] == NSOrderedSame) {
        teamName = @"AFC Telford United";
    }
    if ([teamName compare:@"Telford"] == NSOrderedSame) {
        teamName = @"AFC Telford United";
    }
    if ([teamName compare:@"Weston-super-Mare"] == NSOrderedSame) {
        teamName = @"Weston-Super-Mare";
    }
    if ([teamName compare:@"Stevenage Borough"] == NSOrderedSame) {
        teamName = @"Stevenage";
    }
    if ([teamName compare:@"Hampton and Richmond Borough"] == NSOrderedSame) {
        teamName = @"Hampton & Richmond";
    }
    if ([teamName compare:@"Hayes and Yeading United"] == NSOrderedSame) {
        teamName = @"Hayes & Yeading";
    }
    if ([teamName compare:@"Tonbridge Angels"] == NSOrderedSame) {
        teamName = @"Tonbridge";
    }
    if ([teamName compare:@"Halesowen"] == NSOrderedSame) {
        teamName = @"Halesowen Town";
    }
    if ([teamName compare:@"Gloucester"] == NSOrderedSame) {
        teamName = @"Gloucester City";
    }
    if ([teamName compare:@"Fleetwood Rangers"] == NSOrderedSame) {
        teamName = @"Fleetwood Town";
    }
    if ([teamName compare:@"Littlehampton"] == NSOrderedSame) {
        teamName = @"Littlehampton Town";
    }
    if ([teamName compare:@"Windsor and Eton"] == NSOrderedSame) {
        teamName = @"Windsor & Eton";
    }
    return [model addTeam:teamName];
}


@end
