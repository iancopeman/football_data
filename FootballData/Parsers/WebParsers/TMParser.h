//
//  TMParser.h
//  FootballData
//
//  Created by Ian Copeman on 25/11/2021.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TMParser : NSObject
- (void)parseAll;
- (void)parseCompetition:(int)competition season:(NSInteger)season;
- (NSArray*)getCompetionArray;
@end

NS_ASSUME_NONNULL_END
