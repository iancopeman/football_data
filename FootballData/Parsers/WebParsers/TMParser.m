//
//  TMParser.m
//  FootballData
//
//  Created by Ian Copeman on 25/11/2021.
//

#import "TMParser.h"
#import "FootballModel.h"
#import "StringSearch.h"
#import "WebPageManager.h"
#import "NSString+CSVParser.h"

#define FilePath (@"/TM/")
#define TM_TIMEDIFFERENCE (5) // Game times are listed US eastern
//
// ToDo:
// Possible process bench warmers
// Process managers
// Kick off time

// https://www.transfermarkt.us/championship/gesamtspielplan/wettbewerb/GB2/saison_id/2019
// https://www.transfermarkt.us/premier-league/gesamtspielplan/wettbewerb/GB1?saison_id=2001&spieltagVon=1&spieltagBis=38

// https://www.transfermarkt.co.in/uefa-cup/gesamtspielplan/pokalwettbewerb/UEFA/saison_id/1971
// https://www.transfermarkt.us/community-shield/startseite/pokalwettbewerb/GBCS?saison_id=1990

// https://www.transfermarkt.co.uk/first-division-91-92-/gesamtspielplan/wettbewerb/EFD1/saison_id/1991
// https://www.transfermarkt.us/first-division/gesamtspielplan/wettbewerb/EFD1/saison_id/1991
// https://www.transfermarkt.us/uefa-intertoto-cup-2009-/startseite/pokalwettbewerb/UI?saison_id=1995
// https://www.transfermarkt.us/ui-cup/gesamtspielplan/wettbewerb/UI/saison_id/1991
// https://www.transfermarkt.us/ui-cup/gesamtspielplan/pokalwettbewerb/UI/saison_id/1995
@implementation TMParser {
    WebPageManager* webPageManager;
    FootballModel* model;
    FileManager* fileManager;
    NSString* dataPath;
    NSString* baseURL;
    
    int footballerCount;
    
}

- (NSArray*)getCompetionArray {
    NSArray* parseArray = @[
        @(CompetitionPremierLeague), @(1992),
        @(CompetitionChampionship), @(2004),
        @(CompetitionLeagueOne), @(2004),
        @(CompetitionLeagueTwo), @(2004),
        @(CompetitionNationalLeague), @(2010),
        @(CompetitionFACup), @(1975),
        @(CompetitionLeagueCup), @(1975),
        @(CompetitionEuropeanCup), @(1955),
        @(CompetitionEuropaLeague), @(2009),
        @(CompetitionEuropaConferenceLeague), @(2021),
        @(CompetitionUEFACup), @(1971),
        @(CompetitionEuropeanCupWinnersCup), @(1960), // 1998 last year
        @(CompetitionCharityShield), @(1990),
        @(CompetitionFirstDivision), @(1950), // 1991 last year
        @(CompetitionInterTotoCup), @(1994), // 2008 last year
        @(CompetitionEuropeanSuperCup), @(1972),
        @(CompetitionClubWorldCup), @(1999),
        @(CompetitionIntercontinentalCup), @(1959), // 2004 last year
        @(CompetitionCLQ), @(1997),
        @(CompetitionUefaQ), @(1989),
        @(CompetitionEuropaLQ), @(2009),
    ];
    return parseArray;
}

- (void)parseCompetition:(int)competition season:(NSInteger)season {
    if (model == nil) {
        model = [FootballModel getModel];
        webPageManager = [WebPageManager getWebPageManager];
        fileManager = [[FileManager alloc] init];
        dataPath  = [[NSUserDefaults standardUserDefaults] objectForKey:@"dataPath"];
        baseURL = @"https://www.transfermarkt.us";
    }
    
    NSArray* parseArray = @[
        @(CompetitionPremierLeague), @(1992), @"https://www.transfermarkt.us/premier-league/gesamtspielplan/wettbewerb/GB1/saison_id/",
        @(CompetitionChampionship), @(2004), @"https://www.transfermarkt.us/championship/gesamtspielplan/wettbewerb/GB2/saison_id/",
        @(CompetitionLeagueOne), @(2004), @"https://www.transfermarkt.us/league-one/gesamtspielplan/wettbewerb/GB3/saison_id/",
        @(CompetitionLeagueTwo), @(2004), @"https://www.transfermarkt.us/league-two/gesamtspielplan/wettbewerb/GB4/saison_id/",
        @(CompetitionNationalLeague), @(2010), @"https://www.transfermarkt.us/national-league/gesamtspielplan/wettbewerb/CNAT/saison_id/",
        @(CompetitionFACup), @(1975), @"https://www.transfermarkt.us/fa-cup/gesamtspielplan/pokalwettbewerb/FAC/saison_id/",
        @(CompetitionLeagueCup), @(1975), @"https://www.transfermarkt.us/efl-cup/gesamtspielplan/pokalwettbewerb/CGB/saison_id/",
        
        @(CompetitionEuropeanCup), @(1955), @"https://www.transfermarkt.us/europapokal-der-landesmeister/gesamtspielplan/pokalwettbewerb/EPL/saison_id/",
        @(CompetitionEuropaLeague), @(2009), @"https://www.transfermarkt.us/europa-league/gesamtspielplan/pokalwettbewerb/EL/saison_id/",
        @(CompetitionEuropaConferenceLeague), @(2021), @"https://www.transfermarkt.us/uefa-europa-conference-league/gesamtspielplan/pokalwettbewerb/UCOL/saison_id/",
        
        @(CompetitionUEFACup), @(1971), @"https://www.transfermarkt.us/uefa-cup/gesamtspielplan/pokalwettbewerb/UEFA/saison_id/",
        @(CompetitionEuropeanCupWinnersCup), @(1960), @"https://www.transfermarkt.com/europapokal-der-pokalsieger/gesamtspielplan/pokalwettbewerb/EPP/saison_id/",
        @(CompetitionCharityShield), @(1990), @"https://www.transfermarkt.us/community-shield/gesamtspielplan/pokalwettbewerb/GBCS/saison_id/",
        @(CompetitionFirstDivision), @(1950), @"https://www.transfermarkt.us/first-division/gesamtspielplan/wettbewerb/EFD1/saison_id/",
        @(CompetitionInterTotoCup), @(1994), @"https://www.transfermarkt.us/ui-cup/gesamtspielplan/pokalwettbewerb/UI/saison_id/",
        @(CompetitionEuropeanSuperCup), @(1972), @"https://www.transfermarkt.us/uefa-super-cup/gesamtspielplan/pokalwettbewerb/UI/saison_id/",
        @(CompetitionClubWorldCup), @(1999), @"https://www.transfermarkt.us/fifa-club-world-cup/gesamtspielplan/pokalwettbewerb/KLUB/saison_id/",
        @(CompetitionIntercontinentalCup), @(1959), @"https://www.transfermarkt.us/intercontinental-cup/gesamtspielplan/pokalwettbewerb/WEPO/saison_id/",
        
        
        @(CompetitionCLQ), @(1997), @"https://www.transfermarkt.us/uefa-champions-league-qualifying/gesamtspielplan/pokalwettbewerb/CLQ/saison_id/",
        @(CompetitionUefaQ), @(1989), @"https://www.transfermarkt.us/uefa-cup-qualifikation/gesamtspielplan/pokalwettbewerb/UEQ/saison_id/",
        @(CompetitionEuropaLQ), @(2009), @"https://www.transfermarkt.us/europa-league-qualifikation/gesamtspielplan/pokalwettbewerb/ELQ/saison_id/",

    ];
    NSString* championsLeagueFormat = @"https://www.transfermarkt.us/uefa-champions-league/gesamtspielplan/pokalwettbewerb/CL/saison_id/";
    
    //
    // Time limeited competitions
    if (competition == CompetitionEuropeanCupWinnersCup && season > 1998) {
        return;
    }
    if (competition == CompetitionIntercontinentalCup && season > 2004) {
        return;
    }

    NSUInteger matchCount = [model getMatchCount];
    NSUInteger playerCount = [model getFootballerCount];
    NSUInteger managerCount = [model getManagerCount];
    
    for (int i = 0; i < parseArray.count / 3; i++) {
        if ([parseArray[i * 3] intValue] == competition) {
            if ([parseArray[i * 3 + 1] intValue] > season) {
                NSLog(@"Attempting to parse %@ with unsupported season: %ld", [Match getCompetitionShortName:competition], (long)season);
                return;
            }
            
            NSString* dataKey = [NSString stringWithFormat:@"%ld_%@", (long)season, [Match getCompetitionShortName:competition]];
            NSString* webPageString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:SeasonFilePath dataKey:dataKey];
            if (webPageString == nil) {
                NSString* fullURL = [NSString stringWithFormat:@"%@%ld", parseArray[i * 3 + 2], (long)season];
                if (competition == CompetitionEuropeanCup && season >= 1992) {
                    fullURL = [NSString stringWithFormat:@"%@%ld", championsLeagueFormat, (long)season];
                }
                webPageString = [webPageManager getWebPage:fullURL];
                if ([model getCurrentSeason] != season) {
                    [fileManager savePersistedString:dataPath dataName:FilePath dataType:SeasonFilePath dataKey:dataKey dataString:webPageString];
                }
            }
            
            [self parseSeason:webPageString seasonID:(int)season competitionType:competition];
            
            break;
        }
        
    }
    
    NSUInteger newMatchCount = [model getMatchCount];
    NSLog(@"Added %lu matches", newMatchCount - matchCount);
    NSUInteger newPlayerCount = [model getFootballerCount];
    NSLog(@"Added %lu footballers", newPlayerCount - playerCount);
    NSUInteger newManagerCount = [model getManagerCount];
    NSLog(@"Added %lu managers", newManagerCount - managerCount);
}


- (void)parseAll {
    if (model == nil) {
        model = [FootballModel getModel];
        webPageManager = [WebPageManager getWebPageManager];
        fileManager = [[FileManager alloc] init];
        dataPath  = [[NSUserDefaults standardUserDefaults] objectForKey:@"dataPath"];
        baseURL = @"https://www.transfermarkt.us";
    }
    NSUInteger matchCount = [model getMatchCount];
    NSUInteger playerCount = [model getFootballerCount]; // NL- 2010, FA - 1961
    
    
    NSArray* parseArray = @[
        /*
         @(CompetitionPremierLeague), @(1992), @"https://www.transfermarkt.us/premier-league/gesamtspielplan/wettbewerb/GB1/saison_id/",
         
         @(CompetitionChampionship), @(2004), @"https://www.transfermarkt.us/championship/gesamtspielplan/wettbewerb/GB2/saison_id/",
         */
        
        /*
         @(CompetitionLeagueOne), @(2004), @"https://www.transfermarkt.us/league-one/gesamtspielplan/wettbewerb/GB3/saison_id/",
         @(CompetitionLeagueTwo), @(2004), @"https://www.transfermarkt.us/league-two/gesamtspielplan/wettbewerb/GB4/saison_id/",
         @(CompetitionNationalLeague), @(2010), @"https://www.transfermarkt.us/national-league/gesamtspielplan/wettbewerb/CNAT/saison_id/",
         */
       // @(CompetitionCharityShield), @(1930), //@"https://www.transfermarkt.us/community-shield/gesamtspielplan/pokalwettbewerb/GBCS/saison_id/",
     //   @(CompetitionEuropeanSuperCup), @(1972), @"https://www.transfermarkt.us/uefa-super-cup/gesamtspielplan/pokalwettbewerb/USC/saison_id/",

        @(CompetitionClubWorldCup), @(1999), @"https://www.transfermarkt.us/fifa-club-world-cup/gesamtspielplan/pokalwettbewerb/KLUB/saison_id/",
        
        
       // @(CompetitionCLQ), @(1997), @"https://www.transfermarkt.us/uefa-champions-league-qualifying/gesamtspielplan/pokalwettbewerb/CLQ/saison_id/",
        
         // https://www.transfermarkt.us/uefa-cup-qualifikation/spieltag/pokalwettbewerb/UEQ/saison_id/2008/gruppe/2RH
        
    //    @(CompetitionIntercontinentalCup), @(1959), @"https://www.transfermarkt.us/intercontinental-cup/gesamtspielplan/pokalwettbewerb/WEPO/saison_id/",
        
        
      //  @(CompetitionFACup), @(1992), @"https://www.transfermarkt.us/fa-cup/gesamtspielplan/pokalwettbewerb/FAC/saison_id/",
      //  @(CompetitionLeagueCup), @(1992), @"https://www.transfermarkt.us/efl-cup/startseite/pokalwettbewerb/CGB/saison_id/",
    ];
    
    
    //NSArray* matchFormatList = @[@"%d", @"%dFA", @"%dLC", @"%dL2", @"national-league" ];
    int seasonStart = 1999;
    //int seasonStart = 2010;
    int count = 0;
    for (int season = seasonStart; season < 2005; season++) {
        footballerCount = 0;
        for (int i = 0; i < parseArray.count / 3; i++) {
            if ([parseArray[i * 3 + 1] intValue] > season) continue;
            int firstSeason = [parseArray[i * 2 + 1] intValue];
            CompetitionType competition = [parseArray[i * 3] intValue];
            
            //
            // Skip missing years
            if (competition == CompetitionEuropeanSuperCup) {
                if (season == 1974) continue;
            }

            NSString* competitionName = [Match getCompetitionShortName:competition];
            NSString* dataKey = [NSString stringWithFormat:@"%d_%@", season, competitionName];
            NSString* webPageString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:SeasonFilePath dataKey:dataKey];
            if (webPageString == nil) {
                NSString* fullURL = [NSString stringWithFormat:@"%@%d", parseArray[i * 3 + 2], season];
                webPageString = [webPageManager getWebPage:fullURL];
                [fileManager savePersistedString:dataPath dataName:FilePath dataType:SeasonFilePath dataKey:dataKey dataString:webPageString];
            }
            
            
            [self parseSeason:webPageString seasonID:season competitionType:competition];
            
        }
        NSUInteger newMatchCount = [model getMatchCount];
        NSLog(@"Added %lu matches", newMatchCount - matchCount);
        matchCount = newMatchCount;
        NSUInteger newPlayerCount = [model getFootballerCount];
        NSLog(@"Added %lu footballers", newPlayerCount - playerCount);
        playerCount = newPlayerCount;
        // if (++count == 1) break;
    }
    
    
}

- (void)parseSeason:(NSString*)webPageString seasonID:(int)seasonID competitionType:(CompetitionType)competitionType {
    if (competitionType == CompetitionCLQ) {
        NSLog(@"Setting competition to Champions League");
        competitionType = CompetitionEuropeanCup;
    }
    if (competitionType == CompetitionUefaQ) {
        NSLog(@"Setting competition to UEFA Cup");
        competitionType = CompetitionUEFACup;
    }
    if (competitionType == CompetitionEuropaLQ) {
        NSLog(@"Setting competition to Europa League Cup");
        competitionType = CompetitionEuropaLeague;
    }
    NSLog(@"Parsing season: %d, %@", seasonID, [Match getCompetitionShortName:competitionType]);
    
    StringSearch* stringSearch = [StringSearch initWithString:webPageString];
    NSString* tableData = [stringSearch getTextWithKey:@"<div class=\"box" startKey:@"<table>" endKey:@"</table"];
    if (tableData == nil) {
        tableData = [stringSearch getTextWithKey:@"Knockout" startKey:@"<table" endKey:@"</table"];
    }
    int matchCount = 0;
    while (tableData != nil) {
        //
        // Find matches
        StringSearch* stringSearchMatches = [StringSearch initWithString:tableData];
        
        //
        // Get match section
        NSString* matchSection = [stringSearchMatches getSectionWithKey:@"<td class=\"zentriert hauptlink" startKey:@"<td" endKey:@"</td>"];
        
        while (matchSection != nil) {
            bool processMatch = [matchSection containsString:@"Match report"] ||
            [matchSection containsString:@"ergebnis-link"];
            if (!processMatch) {
                if ([matchSection containsString:@"Match preview"]) {
                    if (seasonID < [model getCurrentSeason]) {
                        processMatch = true;
                    }
                }
            }
            StringSearch* stringSearchMatchSection = [StringSearch initWithString:matchSection];
            if (processMatch) {
                NSString* matchPath = [stringSearchMatchSection getText:@"href=\"" endKey:@"\""];
                NSString* dataKey = [self generateDataKey:matchPath];
                NSString* dataType = [NSString stringWithFormat:@"%@%d/", MatchFilePath, seasonID];
                NSString* matchPageString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:dataType dataKey:dataKey];
                if ([matchPageString containsString:@"<title>500 Internal Server Error"]) {
                    matchPageString = nil;
                }
                if ([matchPageString containsString:@"Bad Gateway"]) {
                    matchPageString = nil;
                }
                bool downloaded = NO;
                if (matchPageString == nil) {
                    NSString* fullURL = [NSString stringWithFormat:@"%@%@", baseURL, matchPath];
                    matchPageString = [webPageManager getWebPage:fullURL];
                    downloaded = YES;
                }
                if (matchPageString == nil) {
                    return;
                }
                NSLog(@"parseMatch: %@", matchPath);
                
                if ([self parseMatch:matchPageString seasonID:seasonID competitionType:competitionType]) {
                    if (downloaded) {
                        [fileManager savePersistedString:dataPath dataName:FilePath dataType:dataType dataKey:dataKey dataString:matchPageString];
                    }
                }
            }
            else if ([matchSection containsString:@"Match preview"]) {
                
                NSString* matchPath = [stringSearchMatchSection getText:@"href=\"" endKey:@"\""];
                NSString* fullURL = [NSString stringWithFormat:@"%@%@", baseURL, matchPath];
                NSString* matchPageString = [webPageManager getWebPage:fullURL];
                [self parseMatchPreview:matchPageString seasonID:seasonID competitionType:competitionType];
                int ian = 10;
                
                
            }
            
            matchSection = [stringSearchMatches getSectionWithKey:@"<td class=\"zentriert hauptlink" startKey:@"<td" endKey:@"</td>"];
            matchCount++;
            
        }
        /*
         NSString* matchPath = [stringSearchMatches getTextWithKey:@"ergebnis-link" startKey:@"href=\"" endKey:@"\""];
         while (matchPath != nil) {
         NSString* dataKey = [self generateDataKey:matchPath];
         NSString* dataType = [NSString stringWithFormat:@"%@%d/", MatchFilePath, seasonID];
         NSString* matchPageString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:dataType dataKey:dataKey];
         if ([matchPageString containsString:@"<title>500 Internal Server Error"]) {
         matchPageString = nil;
         }
         if (matchPageString == nil) {
         NSString* fullURL = [NSString stringWithFormat:@"%@%@", baseURL, matchPath];
         matchPageString = [webPageManager getWebPage:fullURL];
         [fileManager savePersistedString:dataPath dataName:FilePath dataType:dataType dataKey:dataKey dataString:matchPageString];
         }
         if (matchPageString == nil) {
         return;
         }
         NSLog(@"parseMatch: %@", matchPath);
         
         [self parseMatch:matchPageString seasonID:seasonID competitionType:competitionType];
         
         matchPath = [stringSearchMatches getTextWithKey:@"ergebnis-link" startKey:@"href=\"" endKey:@"\""];
         matchCount++;
         
         }
         */
        // break;
        tableData =[stringSearch getTextWithKey:@"<div class=\"box" startKey:@"<table>" endKey:@"</table"];
        if (tableData == nil) {
            tableData = [stringSearch getTextWithKey:@"Knockout stage</div>" startKey:@"<table" endKey:@"</table"];
        }
        
    }
    
    
    
    // <a title="Match report" class="ergebnis-link" id="1019745" href="/spielbericht/index/spielbericht/1019745">1:0</a>
}

- (bool)parseMatch:(NSString*)matchPageString seasonID:(int)seasonID competitionType:(CompetitionType)competitionType {
    StringSearch* stringSearch = [StringSearch initWithString:matchPageString];
    NSString* tableData = [stringSearch getSectionWithKey:@"<div class=\"sb-team sb-heim" startKey:@"<div" endKey:@"</div"];
    StringSearch* stringSearchInfo = [StringSearch initWithString:tableData];
    NSString* teamString1 = [stringSearchInfo getText:@"title=\"" endKey:@"\""];
    NSString* teamIDString1 = [stringSearchInfo getText:@"verein/" endKey:@"/s"];
    
    tableData = [stringSearch getSectionWithKey:@"<div class=\"sb-spieldaten" startKey:@"<div" endKey:@"</div"];
    stringSearchInfo = [StringSearch initWithString:tableData];
    NSString* roundString = [stringSearchInfo getTextWithKey:@"sb-datum" startKey:@">" endKey:@"<"];
    NSString* matchDayString = [stringSearchInfo getTextWithKey:@"saison_id" startKey:@"spieltag/" endKey:@"\""];
    NSString* dateString = [stringSearchInfo getText:@"datum/" endKey:@"\""];
    if ([dateString containsString:@"1984-05-30"]) {
        int ian = 10;
    }
    NSString* timeString = [stringSearchInfo getText:@"|&nbsp;&nbsp;" endKey:@"</p>"];
    timeString = [timeString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    int hourNumber = [self parseTimeString:timeString];

    NSString* scoreString = [[stringSearchInfo getText:@"endstand\">" endKey:@"<"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSArray* scoreArray = [scoreString componentsSeparatedByString:@":"];
    int score1 = [scoreArray[0] intValue];
    int score2 = [scoreArray[1] intValue];
    int penalties1 = 0;
    int penalties2 = 0;
    bool penalties = [tableData containsString:@"on pens"];
    if (penalties) {
        penalties1 = score1;
        penalties2 = score2;
        score1 = 0;
        score2 = 0;
    }
    NSString* locationString = [stringSearchInfo getTextWithKey:@"stadion" startKey:@">" endKey:@"<"];
    NSString* attendanceString = [stringSearchInfo getText:@"Attendance: " endKey:@"<"];
    NSString* refereeString = [stringSearchInfo getTextWithKey:@"Referee:" startKey:@"title=\"" endKey:@"\""];
    
    tableData = [stringSearch getSectionWithKey:@"<div class=\"sb-team sb-gast" startKey:@"<div" endKey:@"</div"];
    stringSearchInfo = [StringSearch initWithString:tableData];
    NSString* teamString2 = [stringSearchInfo getText:@"title=\"" endKey:@"\""];
    NSString* teamIDString2 = [stringSearchInfo getText:@"verein/" endKey:@"/s"];
    if ([teamString2  containsString:@"Manchester U"]) {
        int ian = 10;
    }
    
    //
    // Parse Date
    int matchDateNumber = 0;
    @try {
        
        matchDateNumber = [self parseDateNumber:dateString season:seasonID teamString1:teamString1 teamString2:teamString2 hourNumber:hourNumber];
        if (matchDateNumber == 0 && seasonID == 2005 && [teamString1 containsString:@"Burnley FC"] && [teamString2 containsString:@"Watford"]) matchDateNumber = 20051217;
        if (matchDateNumber == 0 && seasonID == 2006 && [teamString1 containsString:@"Crystal Palace"] && [teamString2 containsString:@"Coventry City"]) matchDateNumber = 20060923;
        
        if (matchDateNumber == 19960116 && [teamString2  containsString:@"Manchester U"]) {
            int ian = 10;
        }
        if (matchDateNumber == 20220204) {
            int ian = 10;
        }
    }
    @catch ( NSException *e ) {
        matchDateNumber = [self parseDateNumber:dateString season:seasonID teamString1:teamString1 teamString2:teamString2 hourNumber:hourNumber];
        int ian = 10;
    }
    
    NSLog(@"* Match: %@ - %@, %d", teamString1, teamString2, matchDateNumber);
    if (matchDateNumber == 20061125) {
        int ian = 10;
    }
    
    //
    // Check date
    if (matchDateNumber / 10000 != seasonID && matchDateNumber / 10000 != seasonID + 1) {
        if ([teamString1 compare:@"Panionios Athens"] == NSOrderedSame && [teamString2 compare:@"Ferencvárosi TC"] == NSOrderedSame) {
            //
            // Match cancelled
            return true;;
        }
        int ian = 10;
    }
    // matchDateNumber++;
    //
    // Add Teams
    int teamID1 = 0;
    int teamID2 = 0;
    bool matchHasEnglishTeam = NO;
    if (IS_EUROPE_COMPETITION(competitionType)) {
        //
        // Find team Id
        int teamParseID = [teamIDString1 intValue];
        Team* team1 = [self addTeamEurope:teamString1 teamParseID:(int)teamParseID];
        teamID1 = team1.index;
        teamParseID = [teamIDString2 intValue];
        Team* team2 = [self addTeamEurope:teamString2 teamParseID:(int)teamParseID];
        teamID2 = team2.index;
        matchHasEnglishTeam = team1.countryID == CountryEngland || team2.countryID == CountryEngland;
    }
    else {
        teamID1 = [self addTeam:teamString1];
        teamID2 = [self addTeam:teamString2];
        matchHasEnglishTeam = YES;
    }
    if (teamID1 == 0 || teamID2 == 0) {
        if (matchHasEnglishTeam) {
             NSLog(@"*** Failed to find team IDs, %@ & %@", teamString1, teamString2);
        }
        return true;;
        
    }
    NSString* matchKey = [Match makeMatchKeyFromDateNumber:matchDateNumber team1:teamID1 team2:teamID2];
    
    //
    // Check match
    bool minorMatch = false;
    Match* match = [model getMatch:matchKey];
    if (!match) {
        // NSLog(@"Team: %@  Date: %d ", teamString1, matchDateNumber);
        match = [[Match alloc] init];
        [match setMatchTeamsAndDateNumber:matchDateNumber team1:teamID1 team2:teamID2];
        [match setLocation:locationString];
        match.competition = competitionType;
        match.competitionFlag = [self getCompetitionFlag:roundString];
        if (match.competitionFlag == 0) {
            match.competitionFlag = [self getCompetitionFlag:matchDayString];
        }
        if (![Match isNeutralVenue:match.competition competitionFlag:match.competitionFlag]) {
            [match setHomeTeamID:teamID1]; // Home team is first team unless final
        }
        [match setSeason:seasonID];
        [match setScore:score1 teamID:teamID1];
        [match setScore:score2 teamID:teamID2];
        int timeNumber = [self parseTimeNumber:timeString dateNumber:matchDateNumber];
        match.timeNumber = timeNumber;
        [model matchIsModified:match];
        [model addMatch:match];
    }
    
    //
    //  Check of we can improve match parsing
    if (match.matchParseState == MatchParseStateEventsAssists) return true;
    if (match.matchParseState == MatchParseStateEventsFull) return true;
    if (!matchHasEnglishTeam) return true;
    if (match.competition == CompetitionFACup && (match.competitionFlag == 1 || match.competitionFlag == 2)) {
        minorMatch = true;
    }
    if (minorMatch) return true;

    MatchParseState oldMatchState = match.matchParseState;
    // return;
    //
    // Full parse
    bool haveAssists = NO;
    
    //
    // First team
    [stringSearch getText:@"Timeline" endKey:@"<"];
    
    
    NSMutableArray* teamFootballers1 = [NSMutableArray arrayWithCapacity:20];
    NSMutableArray* teamEvents1 = [NSMutableArray arrayWithCapacity:20];
    NSMutableArray* teamFootballers2 = [NSMutableArray arrayWithCapacity:20];
    NSMutableArray* teamEvents2 = [NSMutableArray arrayWithCapacity:20];
    
    
    //
    // Sections
    NSString* section = [stringSearch getSectionWithKey:@"<div class=\"box" startKey:@"<div" endKey:@"</div"];
    NSString* sectionSquad = nil;
    NSString* sectionGoals = nil;
    NSString* sectionSubs = nil;
    NSString* sectionCards = nil;
    
    while (section != nil) {
        if ([section containsString:@"Line-Ups"]) {
            sectionSquad = section;
            
        }
        else if ([section containsString:@"Goals"]) {
            sectionGoals = section;
        }
        else if ([section containsString:@"missed penalties"]) {
        }
        else if ([section containsString:@"Substitutions"]) {
            sectionSubs = section;
        }
        else if ([section containsString:@"Cards"]) {
            sectionCards = section;
        }
        section = [stringSearch getSectionWithKey:@"<div class=\"box" startKey:@"<div" endKey:@"</div"];
    }
    
    //
    // Count Goals
    int countedScore1 = 0;
    int countedScore2 = 0;
    int goalMask = 0;
    int goalCount = 0;
    if (sectionGoals != nil) {
        StringSearch* stringSearchSection = [StringSearch initWithString:sectionGoals];
        NSString* infoString = [stringSearchSection getSectionWithKey:@"<li class=\"sb-aktion" startKey:@"<li" endKey:@"</li"];
        while (infoString != nil) {
            StringSearch* stringSearchInfo = [StringSearch initWithString:infoString];
            
            NSString* teamString = [stringSearchInfo getTextWithKey:@"aktion-wappen" startKey:@"title=\"" endKey:@"\""];
            bool team1 = [teamString compare:teamString1] == NSOrderedSame;
            
            //
            // Count goals
            if (team1) {
                countedScore1++;
                goalMask |= 1 << goalCount;
            }
            else {
                countedScore2++;
            }
            goalCount++;
            infoString = [stringSearchSection getSectionWithKey:@"<li class=\"sb-aktion" startKey:@"<li" endKey:@"</li"];
        }
    }
    
    //
    // Calculate winner
    EventResultType resultType1 = EventResultDraw;
    EventResultType resultType2 = EventResultDraw;
    if (countedScore1 > countedScore2) {
        resultType1 = EventResultWin;
        resultType2 = EventResultLose;
    }
    if (countedScore1 < countedScore2) {
        resultType1 = EventResultLose;
        resultType2 = EventResultWin;
    }
    
    
    //
    // Process Squad
    Manager* manager1 = nil;
    Manager* manager2 = nil;
    if (sectionSquad != nil) {
        StringSearch* stringSearchBoxes = [StringSearch initWithString:sectionSquad];
        
        //
        // First team
        NSString* teamBox = [stringSearchBoxes getSectionWithKey:@"<div class=\"large-6 columns" startKey:@"<div" endKey:@"</div"];
        StringSearch* stringSearchSection = [StringSearch initWithString:teamBox];
        tableData = [stringSearchSection getSectionWithKey:@"<div class=\"large-7 columns small-12 aufstellung-vereinsseite" startKey:@"<div" endKey:@"</div"];
        if (tableData != nil) {
            [self processSquad:tableData teamID:teamID1 teamFootballers:teamFootballers1 teamEvents:teamEvents1 seasonID:seasonID cleanSheet:countedScore2 == 0 resultType:resultType1];
            
            //
            // Get manager
            NSString* managerURL = [stringSearchSection getTextWithKey:@"<div>Manager:" startKey:@"href=\"" endKey:@"\""];
            if (managerURL != nil) {
                NSString* idString = [self generateDataKey:managerURL];
                manager1 = [self parseManager:idString teamID:teamID1 urlString:managerURL seasonID:seasonID];
            }
            
        }
        else {
            tableData = [stringSearchSection getSectionWithKey:@"<div class=\"large-6" startKey:@"<div" endKey:@"</div"];
            [self processSquad2:tableData teamID:teamID1 teamFootballers:teamFootballers1 teamEvents:teamEvents1 seasonID:seasonID cleanSheet:countedScore2 == 0  resultType:resultType1];
            //
            // Get manager
            StringSearch* stringSearchManager = [StringSearch initWithString:tableData];
            NSString* managerURL = [stringSearchManager getTextWithKey:@"Manager" startKey:@"href=\"" endKey:@"\""];
            if (managerURL.length == 0) {
                if (matchDateNumber == 20191110) managerURL = @"/mark-collier/profil/trainer/68815";
            }
            if (managerURL != nil) {
                NSString* idString = [self generateDataKey:managerURL];
                manager1 = [self parseManager:idString teamID:teamID1 urlString:managerURL seasonID:seasonID];
            }
        }
        
        //
        // Second team
        teamBox = [stringSearchBoxes getSectionWithKey:@"<div class=\"large-6 columns" startKey:@"<div" endKey:@"</div"];
        stringSearchSection = [StringSearch initWithString:teamBox];
        tableData = [stringSearchSection getSectionWithKey:@"<div class=\"large-7 columns small-12 aufstellung-vereinsseite" startKey:@"<div" endKey:@"</div"];
        if (tableData != nil) {
            [self processSquad:tableData teamID:teamID2 teamFootballers:teamFootballers2 teamEvents:teamEvents2 seasonID:seasonID cleanSheet:countedScore1 == 0  resultType:resultType2];
            //
            // Get manager
            NSString* managerURL = [stringSearchSection getTextWithKey:@"<div>Manager:" startKey:@"href=\"" endKey:@"\""];
            if (managerURL != nil) {
                NSString* idString = [self generateDataKey:managerURL];
                manager2 = [self parseManager:idString teamID:teamID2 urlString:managerURL seasonID:seasonID];
            }
        }
        else {
            tableData = [stringSearchSection getSectionWithKey:@"<div class=\"large-6" startKey:@"<div" endKey:@"</div"];
            [self processSquad2:tableData teamID:teamID2 teamFootballers:teamFootballers2 teamEvents:teamEvents2 seasonID:seasonID cleanSheet:countedScore1 == 0  resultType:resultType2];
            //
            // Get manager
            StringSearch* stringSearchManager = [StringSearch initWithString:tableData];
            NSString* managerURL = [stringSearchManager getTextWithKey:@"Manager" startKey:@"href=\"" endKey:@"\""];
            if (managerURL != nil) {
                NSString* idString = [self generateDataKey:managerURL];
                manager2 = [self parseManager:idString teamID:teamID2 urlString:managerURL seasonID:seasonID];
            }
        }
        
    }
    
    //
    // Process Subs
    if (sectionSubs != nil) {
        StringSearch* stringSearchSection = [StringSearch initWithString:sectionSubs];
        NSString* infoString = [stringSearchSection getSectionWithKey:@"<li class=\"sb-aktion" startKey:@"<li" endKey:@"</li"];
        while (infoString != nil) {
            StringSearch* stringSearchInfo = [StringSearch initWithString:infoString];
            NSString* time1String = [stringSearchInfo getText:@"background-position: " endKey:@"p"];
            NSString* time2String = [stringSearchInfo getText:@"x " endKey:@"px"];
            int time1 = [time1String intValue];
            int time2 = [time2String intValue];
            int time = time1 / -36 + time2 / -36 * 10 + 1;
            
            int pos = [stringSearchInfo getStringPos];
            NSString* idURLString1 = [stringSearchInfo getTextWithKey:@"spielerbild" startKey:@"href=\"" endKey:@"\""];
            [stringSearchInfo setStringPos:pos];
            NSString* idString1 = [stringSearchInfo getTextWithKey:@"spielerbild" startKey:@"spieler/" endKey:@"\""];
            int idOut = [idString1 intValue];
            
            pos = [stringSearchInfo getStringPos];
            NSString* idURLString2 = [stringSearchInfo getTextWithKey:@"spielerbild" startKey:@"href=\"" endKey:@"\""];
            [stringSearchInfo setStringPos:pos];
            NSString* idString2 = [stringSearchInfo getTextWithKey:@"spielerbild" startKey:@"spieler/" endKey:@"\""];
            int idIn = [idString2 intValue];
            NSString* teamString = [stringSearchInfo getTextWithKey:@"aktion-wappen" startKey:@"title=\"" endKey:@"\""];
            bool team1 = [teamString compare:teamString1] == NSOrderedSame;
            
            
            //
            // Need to ensure IDs are in team
            [self checkAddPlayer:idOut teamID:team1 ? teamID1 : teamID2 teamFootballers:team1 ? teamFootballers1 : teamFootballers2 teamEvents:team1 ? teamEvents1 : teamEvents2 seasonID:seasonID urlString:idURLString1];
            [self checkAddPlayer:idIn teamID:team1 ? teamID1 : teamID2 teamFootballers:team1 ? teamFootballers1 : teamFootballers2 teamEvents:team1 ? teamEvents1 : teamEvents2 seasonID:seasonID urlString:idURLString2];
            
            
            bool cleanSheet = team1 ? countedScore2 == 0 : countedScore1 == 0;
            EventResultType resultType = team1 ? resultType1 : resultType2;
            [self processEvent:EventSub id1:idIn id2:idOut time:time teamFootballers:team1 ? teamFootballers1 : teamFootballers2 teamEvents:team1 ? teamEvents1 : teamEvents2 cleanSheet:cleanSheet resultType:resultType];
            
            infoString = [stringSearchSection getSectionWithKey:@"<li class=\"sb-aktion" startKey:@"<li" endKey:@"</li"];
        }
    }
    
    //
    // Process Goals
    if (sectionGoals != nil) {
        StringSearch* stringSearchSection = [StringSearch initWithString:sectionGoals];
        NSString* infoString = [stringSearchSection getSectionWithKey:@"<li class=\"sb-aktion" startKey:@"<li" endKey:@"</li"];
        while (infoString != nil) {
            StringSearch* stringSearchInfo = [StringSearch initWithString:infoString];
            NSString* time1String = [stringSearchInfo getText:@"background-position: " endKey:@"p"];
            NSString* time2String = [stringSearchInfo getText:@"x " endKey:@"px"];
            int time1 = [time1String intValue];
            int time2 = [time2String intValue];
            int time = time1 / -36 + time2 / -36 * 10 + 1;
            
            
            NSString* idString = [stringSearchInfo getTextWithKey:@"spielerbild" startKey:@"spieler/" endKey:@"\""];
            int id1 = [idString intValue];
            
            NSString* idAssistString = [stringSearchInfo getTextWithKey:@"Assist:" startKey:@"spieler/" endKey:@"/"];
            
            
            NSString* teamString = [stringSearchInfo getTextWithKey:@"aktion-wappen" startKey:@"title=\"" endKey:@"\""];
            bool ownGoal = [infoString containsString:@"Own-goal"];
            bool team1 = [teamString compare:teamString1] == NSOrderedSame;
            bool penalty = [infoString containsString:@"Penalty"];
            
            if (ownGoal) {
                team1 = !team1; // Switch teams for own goal
            }
            bool cleanSheet = team1 ? countedScore2 == 0 : countedScore1 == 0;
            EventResultType resultType = team1 ? resultType1 : resultType2;
            if (ownGoal) {
                [self processEvent:EventOwnGoal id1:id1 id2:0 time:time teamFootballers:team1 ? teamFootballers1 : teamFootballers2 teamEvents:team1 ? teamEvents1 : teamEvents2 cleanSheet:cleanSheet resultType:resultType];
            }
            else if (penalty) {
                [self processEvent:EventPenalty id1:id1 id2:0 time:time teamFootballers:team1 ? teamFootballers1 : teamFootballers2 teamEvents:team1 ? teamEvents1 : teamEvents2 cleanSheet:cleanSheet resultType:resultType];
            }
            else {
                int id2 = [idAssistString intValue];
                if (id2 != 0) {
                    haveAssists = YES;
                }
                [self processEvent:EventGoal id1:id1 id2:id2 time:time teamFootballers:team1 ? teamFootballers1 : teamFootballers2 teamEvents:team1 ? teamEvents1 : teamEvents2 cleanSheet:cleanSheet resultType:resultType];
            }
            
            infoString = [stringSearchSection getSectionWithKey:@"<li class=\"sb-aktion" startKey:@"<li" endKey:@"</li"];
        }
        
    }
    
    //
    // Process Cards
    if (sectionCards != nil) {
        StringSearch* stringSearchSection = [StringSearch initWithString:sectionCards];
        NSString* infoString = [stringSearchSection getSectionWithKey:@"<li class=\"sb-aktion" startKey:@"<li" endKey:@"</li"];
        while (infoString != nil) {
            StringSearch* stringSearchInfo = [StringSearch initWithString:infoString];
            NSString* time1String = [stringSearchInfo getText:@"background-position: " endKey:@"p"];
            NSString* time2String = [stringSearchInfo getText:@"x " endKey:@"px"];
            int time1 = [time1String intValue];
            int time2 = [time2String intValue];
            int time = time1 / -36 + time2 / -36 * 10 + 1; // Time of 121 indicates missing time info
            
            bool yellowCard = [infoString containsString:@"sb-gelb"];
            
            NSString* idString = [stringSearchInfo getTextWithKey:@"spielerbild" startKey:@"spieler/" endKey:@"\""];
            int id1 = [idString intValue];
            NSString* teamString = [stringSearchInfo getTextWithKey:@"aktion-wappen" startKey:@"title=\"" endKey:@"\""];
            bool team1 = [teamString compare:teamString1] == NSOrderedSame;
            bool cleanSheet = team1 ? countedScore2 == 0 : countedScore1 == 0;
            EventResultType resultType = team1 ? resultType1 : resultType2;
            [self processEvent:yellowCard ? EventYellowCard : EventRedCard id1:id1 id2:0 time:time teamFootballers:team1 ? teamFootballers1 : teamFootballers2 teamEvents:team1 ? teamEvents1 : teamEvents2 cleanSheet:cleanSheet resultType:resultType];
            
            infoString = [stringSearchSection getSectionWithKey:@"<li class=\"sb-aktion" startKey:@"<li" endKey:@"</li"];
        }
    }
    /*
     //
     // Process Missed Penalties
     if (sectionMissedPenalties != nil) {
     StringSearch* stringSearchSection = [StringSearch initWithString:sectionMissedPenalties];
     NSString* infoString = [stringSearchSection getSectionWithKey:@"<li class=\"sb-aktion" startKey:@"<li" endKey:@"</li"];
     while (infoString != nil) {
     StringSearch* stringSearchInfo = [StringSearch initWithString:infoString];
     NSString* time1String = [stringSearchInfo getText:@"background-position: " endKey:@"p"];
     NSString* time2String = [stringSearchInfo getText:@"x " endKey:@"px"];
     int time1 = [time1String intValue];
     int time2 = [time2String intValue];
     int time = time1 / -36 + time2 / -36 * 10 + 1;
     
     NSString* teamString = [stringSearchInfo getTextWithKey:@"spielstand" startKey:@"title=\"" endKey:@"\""];
     NSString* idString = [stringSearchInfo getTextWithKey:@"spielerbild" startKey:@"spieler/" endKey:@"\""];
     int id1 = [idString intValue];
     bool team1 = [teamString compare:teamString1] == NSOrderedSame;
     //[self processGoal:idString idAssistString:nil time:time teamFootballers:team1 ? teamFootballers1 : teamFootballers2 teamEvents:team1 ? teamEvents1 : teamEvents2];
     
     infoString = [stringSearchSection getSectionWithKey:@"<li class=\"sb-aktion" startKey:@"<li" endKey:@"</li"];
     }
     }
     */
    
    //
    // Add match
    if (match.competition != competitionType) {
        match.competition = competitionType;
        [model matchIsModified:match];
    }
    [match setLocation:locationString];
    [match setGoalMask:goalMask];
    if (countedScore1 != 0 && goalMask == 0) {
        int ian = 10;
    }
    
    [match setReferee:refereeString];
    [match setManager:manager1 teamID:teamID1];
    [match setManager:manager2 teamID:teamID2];
    attendanceString = [attendanceString stringByReplacingOccurrencesOfString:@"." withString:@""];
    int matchAttendance = [attendanceString intValue];
    match.attendance = matchAttendance;
    
    //
    // Check score
    if (!penalties && (countedScore1 != score1 || countedScore2 != score2)) {
        if (teamEvents1.count == 0 || teamEvents2.count == 0) {
            countedScore1 = score1;
            countedScore2 = score2;
        }
        else if ((matchDateNumber == 19950920 && [teamString1  containsString:@"Swindon"]) ||
                 (matchDateNumber == 19971014 && [teamString1  containsString:@"Arsenal"]) ||
                 (matchDateNumber == 20101106 && [teamString1  containsString:@"Chelmsford"]) ||
                 (matchDateNumber == 20171105 && [teamString1  containsString:@"Exeter"]) ||
                 (matchDateNumber == 20171105 && [teamString1  containsString:@"Dartford"]) ||
                 (matchDateNumber == 20010113 && [teamString1  containsString:@"Manchester"]) ||
                 (matchDateNumber == 20010407 && [teamString1  containsString:@"Derby"]) ||
                 (matchDateNumber == 20021215 && [teamString1  containsString:@"Tottenham"]) ||
                 (matchDateNumber == 20220205 && [teamString1  containsString:@"Southampton"]) ||
                 (matchDateNumber == 20150811 && [teamString1  containsString:@"Fleetwood"])) {
            countedScore1 = score1;
            countedScore2 = score2;
        }
        else if ((matchDateNumber == 19940921 && [teamString1  containsString:@"Newcastle United"]) ||
                 (matchDateNumber == 19950920 && [teamString1  containsString:@"Wolverhampton"])) {
            // page error, do nothing
        }
        else {
            NSLog(@"Counted and actual scores don't match");
            int ian = 10;
        }
    }
    
    [match setScore:countedScore1 teamID:teamID1];
    [match setScore:countedScore2 teamID:teamID2];
    [match setPenalties:penalties1 teamID:teamID1];
    [match setPenalties:penalties2 teamID:teamID2];
    [match setFootballers:teamFootballers1 teamID:teamID1];
    [match setFootballers:teamFootballers2 teamID:teamID2];
    [match addMatchEvents:teamEvents1 teamID:teamID1 hasAssists:haveAssists];
    [match addMatchEvents:teamEvents2 teamID:teamID2 hasAssists:haveAssists];
    [model addMatchEvents:match];
    match.matchState = MatchStateResult;
    
    if (oldMatchState < match.matchParseState) {
        [model matchIsModified:match];
    }
    
    
    return true;
}

- (void)parseMatchPreview:(NSString*)matchPageString seasonID:(int)seasonID competitionType:(CompetitionType)competitionType {
    StringSearch* stringSearch = [StringSearch initWithString:matchPageString];
    NSString* tableData = [stringSearch getSectionWithKey:@"<div class=\"sb-team sb-heim" startKey:@"<div" endKey:@"</div"];
    StringSearch* stringSearchInfo = [StringSearch initWithString:tableData];
    NSString* teamString1 = [stringSearchInfo getText:@"title=\"" endKey:@"\""];
    NSString* teamIDString1 = [stringSearchInfo getText:@"verein/" endKey:@"/s"];
    
    tableData = [stringSearch getSectionWithKey:@"<div class=\"sb-spieldaten" startKey:@"<div" endKey:@"</div"];
    stringSearchInfo = [StringSearch initWithString:tableData];
    NSString* roundString = [stringSearchInfo getTextWithKey:@"sb-datum" startKey:@">" endKey:@"<"];
    NSString* matchDayString = [stringSearchInfo getTextWithKey:@"saison_id" startKey:@"spieltag/" endKey:@"\""];
    NSString* dateString = [stringSearchInfo getText:@"datum/" endKey:@"\""];
    if ([dateString containsString:@"1984-05-30"]) {
        int ian = 10;
    }
    NSString* timeString = [stringSearchInfo getText:@"|&nbsp;&nbsp;" endKey:@"</p>"];
    timeString = [timeString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    int hourNumber = [self parseTimeString:timeString];
    
    NSString* locationString = [stringSearchInfo getTextWithKey:@"stadion" startKey:@">" endKey:@"<"];
    NSString* attendanceString = [stringSearchInfo getText:@"Attendance: " endKey:@"<"];
    NSString* refereeString = [stringSearchInfo getTextWithKey:@"Referee:" startKey:@"title=\"" endKey:@"\""];
    
    tableData = [stringSearch getSectionWithKey:@"<div class=\"sb-team sb-gast" startKey:@"<div" endKey:@"</div"];
    stringSearchInfo = [StringSearch initWithString:tableData];
    NSString* teamString2 = [stringSearchInfo getText:@"title=\"" endKey:@"\""];
    NSString* teamIDString2 = [stringSearchInfo getText:@"verein/" endKey:@"/s"];
    
    //
    // Parse Date
    int matchDateNumber = [self parseDateNumber:dateString season:seasonID teamString1:teamString1 teamString2:teamString2 hourNumber:hourNumber];
    
    //
    // Add Teams
    int teamID1 = 0;
    int teamID2 = 0;
    bool matchHasEnglishTeam = NO;
    if (IS_EUROPE_COMPETITION(competitionType)) {
        //
        // Find team Id
        int teamParseID = [teamIDString1 intValue];
        Team* team1 = [self addTeamEurope:teamString1 teamParseID:(int)teamParseID];
        teamID1 = team1.index;
        teamParseID = [teamIDString2 intValue];
        Team* team2 = [self addTeamEurope:teamString2 teamParseID:(int)teamParseID];
        teamID2 = team2.index;
        matchHasEnglishTeam = team1.countryID == CountryEngland || team2.countryID == CountryEngland;
    }
    else {
        teamID1 = [self addTeam:teamString1];
        teamID2 = [self addTeam:teamString2];
        matchHasEnglishTeam = YES;
    }
    if (teamID1 == 0 || teamID2 == 0) {
        if (matchHasEnglishTeam) {
             NSLog(@"Failed to find team IDs, %@ & %@", teamString1, teamString2);
        }
        return;
        
    }
    
    NSString* matchKey = [Match makeMatchKeyFromDateNumber:matchDateNumber team1:teamID1 team2:teamID2];
    
    //
    // Check match
    Match* match = [model getMatch:matchKey];
    if (!match) {
        match = [[Match alloc] init];
        [match setMatchTeamsAndDateNumber:matchDateNumber team1:teamID1 team2:teamID2];
        match.competition = competitionType;
        match.matchState = MatchStateFutureMatch;
        match.competitionFlag = [self getCompetitionFlag:roundString];
        if (match.competitionFlag == 0) {
            match.competitionFlag = [self getCompetitionFlag:matchDayString];
        }
        if (![Match isNeutralVenue:match.competition competitionFlag:match.competitionFlag]) {
            [match setHomeTeamID:teamID1]; // Home team is first team unless final
        }
        [match setSeason:seasonID];
        int timeNumber = [self parseTimeNumber:timeString dateNumber:matchDateNumber];
        match.timeNumber = timeNumber;
        [model matchIsModified:match];
        [model addMatch:match];
    }
    
    
}

- (void)processEvent:(EventType)eventType id1:(int)id1 id2:(int)id2 time:(int)time teamFootballers:(NSMutableArray*)teamFootballers teamEvents:(NSMutableArray*)teamEvents cleanSheet:(bool)cleanSheet resultType:(EventResultType)resultType {
    int playerID1 = 0;
    int playerID2 = 0;
    for (Footballer* footballer in teamFootballers) {
        if (footballer.parseIndexTM == id1) {
            playerID1 = footballer.index;
        }
        if (footballer.parseIndexTM == id2) {
            playerID2 = footballer.index;
        }
        if (playerID1 != 0 && (playerID2 != 0 || id2 == 0)) break;
    }
    
    //
    // Sometimes subs are not listed with players
    if (playerID1 == 0 || (playerID2 != 0 && id2 == 0)) {
        // Sometimes owngoals not reported correctly
        // See:
        // https://www.transfermarkt.us/west-bromwich-albion_middlesbrough-fc/index/spielbericht/28387
        // https://www.11v11.com/matches/west-bromwich-albion-v-middlesbrough-14-november-2004-26643/
        int ian = 10;
    }
    
    if (eventType == EventGoal) {
        [teamEvents addObject:[Event createEventGoalNumber:playerID1 playerAssistID:playerID2 time:time]];
    }
    else if (eventType == EventPenalty) {
        [teamEvents addObject:[Event createEventPenaltyNumber:playerID1 time:time]];
    }
    else if (eventType == EventOwnGoal) {
        [teamEvents addObject:[Event createEventOwnGoalNumber:playerID1 time:time]];
    }
    else if (eventType == EventSub) {
        [teamEvents addObject:[Event createEventSubNumber:playerID1 playerIDOut:playerID2 time:time cleanSheet:cleanSheet resultType:resultType]];
    }
    else if (eventType == EventRedCard) {
        [teamEvents addObject:[Event createEventRedCardNumber:playerID1 time:time]];
    }
    else if (eventType == EventYellowCard) {
        [teamEvents addObject:[Event createEventYellowCardNumber:playerID1 time:time]];
    }
    
}

- (void)processSquad:(NSString*)squad teamID:(int)teamID teamFootballers:(NSMutableArray*)teamFootballers teamEvents:(NSMutableArray*)teamEvents seasonID:(int)seasonID cleanSheet:(bool)cleanSheet resultType:(EventResultType)resultType {
    StringSearch* stringSearchInfo = [StringSearch initWithString:squad];
    NSString* section = [stringSearchInfo getSectionWithKey:@"<div class=\"aufstellung-spieler-container" startKey:@"<div" endKey:@"</div"];
    //  NSString* urlString = [stringSearchInfo getTextWithKey:@"name" startKey:@"href=\"" endKey:@"\""];
    //
    // Goalkeepers are show at the bottom of the screen, with the 'top: 80%;' flag
    int position = 1;
    bool foundGoalie = NO;
    while (section != nil) {
        StringSearch* stringSearchSection = [StringSearch initWithString:section];
        bool isGoalie = [section containsString:@"top: 80%;"];
        NSString* shirtString = [stringSearchSection getText:@"rn\">" endKey:@"<"];
        int shirtNumber = [shirtString intValue];
        NSString* urlString = [stringSearchSection getTextWithKey:@"name" startKey:@"href=\"" endKey:@"\""];
        NSString* idString = [self generateDataKey:urlString];
        Footballer* footballer = [self parsePlayer:idString teamID:teamID urlString:urlString seasonID:seasonID];
        [teamFootballers addObject:footballer];
        if (isGoalie) {
            [teamEvents addObject:[Event createEventGoalkeeperApperance:footballer.index position:position++ shirt:shirtNumber cleanSheet:cleanSheet resultType:resultType]];
            foundGoalie = YES;
        }
        else {
            [teamEvents addObject:[Event createEventAppearanceNumber:footballer.index position:position++ shirt:shirtNumber cleanSheet:cleanSheet resultType:resultType]];
        }
        
        section = [stringSearchInfo getSectionWithKey:@"<div class=\"aufstellung-spieler-container" startKey:@"<div" endKey:@"</div"];
    }
    if (!foundGoalie) {
        NSLog(@"Failed to find goalie in team");
    }
}

- (void)checkAddPlayer:(int)playerID teamID:(int)teamID teamFootballers:(NSMutableArray*)teamFootballers teamEvents:(NSMutableArray*)teamEvents seasonID:(int)seasonID urlString:(NSString*)urlString {
    
    //
    // Find player in team
    for (Footballer* footballer in teamFootballers) {
        if (footballer.parseIndexTM == playerID) {
            return;
        }
    }
    
    //
    // Hack to catch playerss with faulty parse IDs
    // if (playerID == 48110) playerID = ;
    //
    // Add player
    NSString* identString = [NSString stringWithFormat:@"%d", playerID];
    Footballer* footballer = [self parsePlayer:identString teamID:teamID urlString:urlString seasonID:seasonID];
    [teamFootballers addObject:footballer];
    //  [teamEvents addObject:[Event createEventApperanceNumber:footballer.index position:teamEvents.count + 1 shirt:0]];
}

- (void)processSquad2:(NSString*)squad teamID:(int)teamID teamFootballers:(NSMutableArray*)teamFootballers teamEvents:(NSMutableArray*)teamEvents seasonID:(int)seasonID cleanSheet:(bool)cleanSheet resultType:(EventResultType)resultType {
    StringSearch* stringSearchInfo = [StringSearch initWithString:squad];
    NSString* section = [stringSearchInfo getText:@"<tr>" endKey:@"</tr>"];
    //  NSString* urlString = [stringSearchInfo getTextWithKey:@"name" startKey:@"href=\"" endKey:@"\""];
    bool foundGoalie = NO;
    bool foundSomeData = NO;
    int position = 1;
    while (section != nil) {
        if ([section containsString:@"Manager"]) break;
        bool isGoalie = [section containsString:@"Goalkeeper"];
        StringSearch* stringSearchSection = [StringSearch initWithString:section];
        NSString* urlString = [stringSearchSection getTextWithKey:@"title" startKey:@"href=\"" endKey:@"\""];
        while (urlString != nil) {
            foundSomeData = YES;
            NSString* idString = [self generateDataKey:urlString];
            Footballer* footballer = [self parsePlayer:idString teamID:teamID urlString:urlString seasonID:seasonID];
            [teamFootballers addObject:footballer];
            if (isGoalie) {
                [teamEvents addObject:[Event createEventGoalkeeperApperance:footballer.index position:position++ shirt:0 cleanSheet:cleanSheet resultType:resultType]];
                foundGoalie = YES;
            }
            else {
                [teamEvents addObject:[Event createEventAppearanceNumber:footballer.index position:position++ shirt:0 cleanSheet:cleanSheet resultType:resultType]];
            }
            urlString = [stringSearchSection getTextWithKey:@"title" startKey:@"href=\"" endKey:@"\""];
        }
        
        section = [stringSearchInfo getText:@"<tr>" endKey:@"</tr>"];
    }
    if (foundSomeData && !foundGoalie) {
        NSLog(@"Failed to find goalie in team");
    }
}


- (Footballer*)parsePlayer:(NSString*)idString teamID:(int)teamID urlString:(NSString*)urlString seasonID:(int)seasonID  {
    
    //
    // Get footballer identifier
    int identInt = [idString intValue];
    if (identInt == 173495) {
        int ian = 10;;
    }
    
    //
    // Lookup player in model
    Footballer* footballer = [model checkFootballer:identInt identKey:@"parseIndexTM" teamID:teamID season:seasonID];
    if (footballer != nil) return footballer;
    
    //     // Add footballer
    NSString* webPageString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:FootballerFilePath dataKey:idString];
    if (webPageString == nil) {
        NSString* fullURL = [NSString stringWithFormat:@"%@%@", baseURL, urlString];
        webPageString = [webPageManager getWebPage:fullURL];
        if ([webPageString containsString:@"<title>500 Internal Server Error"]) {
            webPageString = [webPageManager getWebPage:fullURL];
        }
        if (![webPageString containsString:@"<title>500 Internal Server Error"]) {
            [fileManager savePersistedString:dataPath dataName:FilePath dataType:FootballerFilePath dataKey:idString dataString:webPageString];
        }
        else {
            return nil;
        }
    }
    if (webPageString == nil) {
        return nil;
    }
    
    NSString* nationalityString = nil;
    NSString* positionString = nil;
    NSString* birthPlaceString = nil;
    NSString* birthDateString = nil;
    NSString* deathDateString = nil;
    StringSearch* stringSearch = [StringSearch initWithString:webPageString];
    NSString* nameString = [stringSearch getSectionWithKey:@"<h1 itemprop=\"name" startKey:@"<h1" endKey:@"</h1>"];
    if (nameString != nil) {
        nameString = [StringSearch stringByStrippingHTML:nameString];
        NSString* tableData = [stringSearch getSectionWithKey:@"<div class=\"dataBottom" startKey:@"<div" endKey:@"</div"];
        StringSearch* stringSearchInfo = [StringSearch initWithString:tableData];
        birthDateString = [[stringSearchInfo getTextWithKey:@"birthDate\"" startKey:@">" endKey:@"<"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        birthPlaceString = [stringSearchInfo getTextWithKey:@"birthPlace\"" startKey:@">" endKey:@"<"];
        nationalityString = [stringSearchInfo getTextWithKey:@"nationality\"" startKey:@">" endKey:@"<"];
        deathDateString = [[stringSearchInfo getTextWithKey:@"deathDate\"" startKey:@">" endKey:@"("] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        positionString = [[stringSearchInfo getTextWithKey:@"Position:" startKey:@"dataValue\">" endKey:@"<"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    }
    else {
        nameString = [stringSearch getSectionWithKey:@"<h1 " startKey:@"<h1" endKey:@"</h1>"];
        StringSearch* stringSearch2 = [StringSearch initWithString:nameString];
        if ([nameString containsString:@"</span>"]) {
            nameString = [stringSearch2 getText:@"</span>" endKey:@"</h1>"];
        }
        nameString = [StringSearch stringByStrippingHTML:nameString];
        nameString = [nameString stringByTrimmingCharactersInSet:
                      [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        NSString* tableData = [stringSearch getSectionWithKey:@"<div class=\"data-header__info-box" startKey:@"<div" endKey:@"</div"];
        StringSearch* stringSearchInfo = [StringSearch initWithString:tableData];
        birthDateString = [[stringSearchInfo getTextWithKey:@"birthDate\"" startKey:@">" endKey:@"<"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        birthPlaceString = [stringSearchInfo getTextWithKey:@"birthPlace\"" startKey:@">" endKey:@"<"];
        nationalityString = [stringSearchInfo getTextWithKey:@"nationality\"" startKey:@"title=\"" endKey:@"\""];
        deathDateString = [[stringSearchInfo getTextWithKey:@"deathDate\"" startKey:@">" endKey:@"("] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        positionString = [[stringSearchInfo getTextWithKey:@"Position:" startKey:@"data-header__content\">" endKey:@"<"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        int ian = 10;
    }
    
    //
    // Sanity check
    if (nameString.length == 0 ||
        nationalityString.length == 0 ||
        positionString.length ==0) {
        int ian = 10;
    }
    
    
    
    //
    // Create new footballer
    footballer = [[Footballer alloc] init];
    footballer.name = nameString;
    footballer.parseIndexTM = identInt;
    footballer.teamID = teamID;
    footballer.countryID = [Team getCountryID:nationalityString];
    if (birthDateString.length == 0) {
        int ian = 10;
    }
    footballer.birthDateNumber = [self parseBirthDateNumber:birthDateString];
    if (footballer.birthDateNumber == 0) {
        if ([nameString containsString:@"Harvey Lintott"]) footballer.birthDateNumber = 20030305;
        else if ([nameString containsString:@"Brian Woodall"]) footballer.birthDateNumber = 19871228;
        else if ([nameString containsString:@"Lee Pugh"]) footballer.birthDateNumber = 19921130;
        else {
            // NSLog(@"Missing footballer birthdate: %@", nameString);
            //
            // Fudge date of birth
            footballer.birthDateNumber = (seasonID - 25) * 10000 + 101;
        }
    }
    if (deathDateString != nil) footballer.deathDateNumber = [self parseDeathDateNumber:deathDateString];
    [footballer addAttribute:@"Birthplace" value:birthPlaceString];
    [footballer addAttribute:@"Position" value:positionString];
    
    
    
    //
    // Add to model
    footballer = [model addFootballer:footballer season:seasonID];
    
    return footballer;
    
}

- (Manager*)parseManager:(NSString*)idString teamID:(int)teamID urlString:(NSString*)urlString seasonID:(int)seasonID  {
    
    //
    // Get identifier
    int identInt = [idString intValue];
    if (identInt == 173495) {
        int ian = 10;;
    }
    
    //
    // Lookup manager in model
    Manager* manager = [model checkManager:identInt identKey:@"parseIndexTM" teamID:teamID season:seasonID];
    if (manager != nil) return manager;
    
    //
    // Add manager
    NSString* webPageString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:ManagersFilePath dataKey:idString];
    if (webPageString == nil) {
        NSString* fullURL = [NSString stringWithFormat:@"%@%@", baseURL, urlString];
        webPageString = [webPageManager getWebPage:fullURL];
        if ([webPageString containsString:@"<title>500 Internal Server Error"]) {
            webPageString = [webPageManager getWebPage:fullURL];
        }
        if (![webPageString containsString:@"<title>500 Internal Server Error"]) {
            [fileManager savePersistedString:dataPath dataName:FilePath dataType:ManagersFilePath dataKey:idString dataString:webPageString];
        }
        else {
            return nil;
        }
    }
    if (webPageString == nil) {
        return nil;
    }
    
    
    NSString* nationalityString = nil;
    NSString* positionString = nil;
    NSString* birthPlaceString = nil;
    NSString* birthDateString = nil;
    NSString* deathDateString = nil;
    StringSearch* stringSearch = [StringSearch initWithString:webPageString];
    NSString* nameString = [stringSearch getSectionWithKey:@"<h1 itemprop=\"name" startKey:@"<h1" endKey:@"</h1>"];
    if (nameString != nil) {
        nameString = [StringSearch stringByStrippingHTML:nameString];
        NSString* tableData = [stringSearch getSectionWithKey:@"<div class=\"dataBottom" startKey:@"<div" endKey:@"</div"];
        StringSearch* stringSearchInfo = [StringSearch initWithString:tableData];
        birthDateString = [[stringSearchInfo getTextWithKey:@"birthDate\"" startKey:@">" endKey:@"<"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        birthPlaceString = [stringSearchInfo getTextWithKey:@"birthPlace\"" startKey:@">" endKey:@"<"];
        nationalityString = [stringSearchInfo getTextWithKey:@"nationality\"" startKey:@">" endKey:@"<"];
        deathDateString = [[stringSearchInfo getTextWithKey:@"deathDate\"" startKey:@">" endKey:@"("] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        positionString = [[stringSearchInfo getTextWithKey:@"Position:" startKey:@"dataValue\">" endKey:@"<"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    }
    else {
        nameString = [stringSearch getSectionWithKey:@"<h1 " startKey:@"<h1" endKey:@"</h1>"];
        StringSearch* stringSearch2 = [StringSearch initWithString:nameString];
        if ([nameString containsString:@"</span>"]) {
            nameString = [stringSearch2 getText:@"</span>" endKey:@"</h1>"];
        }
        nameString = [StringSearch stringByStrippingHTML:nameString];
        nameString = [nameString stringByTrimmingCharactersInSet:
                      [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        NSString* tableData = [stringSearch getSectionWithKey:@"<div class=\"data-header__info-box" startKey:@"<div" endKey:@"</div"];
        StringSearch* stringSearchInfo = [StringSearch initWithString:tableData];
        birthDateString = [[stringSearchInfo getTextWithKey:@"birthDate\"" startKey:@">" endKey:@"<"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        birthPlaceString = [stringSearchInfo getTextWithKey:@"birthPlace\"" startKey:@">" endKey:@"<"];
        nationalityString = [stringSearchInfo getTextWithKey:@"nationality\"" startKey:@"title=\"" endKey:@"\""];
        deathDateString = [[stringSearchInfo getTextWithKey:@"deathDate\"" startKey:@">" endKey:@"("] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        positionString = [[stringSearchInfo getTextWithKey:@"Position:" startKey:@"data-header__content\">" endKey:@"<"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        int ian = 10;
    }
    
    
    
    //
    // Create new footballer
    manager = [[Manager alloc] init];
    manager.name = nameString;
    manager.parseIndexTM = identInt;
    manager.teamID = teamID;
    manager.countryID = [Team getCountryID:nationalityString];
    manager.birthDateNumber = [self parseBirthDateNumber:birthDateString];
    if (manager.birthDateNumber == 0) {
        if ([nameString containsString:@"Missing Name"]) manager.birthDateNumber = 20030305;
        else {
            // NSLog(@"Missing footballer birthdate: %@", nameString);
            //
            // Fudge date of birth
            manager.birthDateNumber = (seasonID - 25) * 10000 + 0101;
        }
    }
    if (deathDateString != nil) manager.deathDateNumber = [self parseDeathDateNumber:deathDateString];
    [manager addAttribute:@"Birthplace" value:birthPlaceString];
    
    
    
    //
    // Add to model
    manager = [model addManager:manager season:seasonID];
    return manager;
}


- (NSString*)generateDataKey:(NSString*)urlString {
    //
    // URL: /spielbericht/index/spielbericht/1019745
    NSString* idString = [urlString lastPathComponent];
    return idString;
}

- (int)parseTimeString:(NSString*)timeString {
    if (timeString == nil) return 0;
    bool pm = [timeString containsString:@"PM"];
    NSArray* timeComponents = [timeString componentsSeparatedByString:@":"];
    int hours = [[timeComponents firstObject] intValue];
    if (hours < 11 && pm) hours += 12;
    return hours;
}

- (int)parseTimeNumber:(NSString*)timeString dateNumber:(int)dateNumber {
    if (timeString == nil) return 0;
    
    //
    // Need to convert to UTC from NY time
    //
    // 3:00 PM
    bool pm = [timeString containsString:@"PM"];
    NSArray* timeComponents = [timeString componentsSeparatedByString:@":"];
    int hours = [[timeComponents firstObject] intValue];
    if (hours < 11 && pm) hours += 12;
    // hours += TM_TIMEDIFFERENCE;
    int minutes = [[timeComponents lastObject] intValue];
    if (hours < 0 || hours > 23 || minutes < 0 || minutes > 59) {
        raise(SIGSTOP);
    }
    int day = dateNumber % 100;
    int month = (dateNumber / 100) % 100;
    int year = (dateNumber / 10000);

    
    static NSCalendar *calendarNY = nil;
    if (calendarNY == nil) {
        calendarNY = [NSCalendar currentCalendar];
        [calendarNY setTimeZone:[NSTimeZone timeZoneWithName:@"America/New_York"]]; // Cannot use EST
    }
    static NSCalendar *calendarUTC = nil;
    if (calendarUTC == nil) {
        calendarUTC = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        calendarUTC.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    }
    //
    // Create date
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.minute = minutes;
    components.hour = hours;
    components.day = day;
    components.month = month;
    components.year = year;
    NSDate * matchDate = [calendarNY dateFromComponents:components];

    //
    // Get time from date
    NSDateComponents* dateComponents = [calendarUTC components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:matchDate];
    NSInteger hourUTC = [dateComponents hour];
    NSInteger minuteUTC = [dateComponents minute];
    return TIME_NUMBER_CREATE((int)hourUTC, (int)minuteUTC);
}


- (int)parseDateNumber:(NSString*)dateString season:(int)season teamString1:(NSString*)teamString1 teamString2:(NSString*)teamString2 hourNumber:(int)hourNumber {
    //
    // Need to add 1 day to the date
    // 2019-08-02
    NSString* dateNumberString = [dateString stringByReplacingOccurrencesOfString:@"-" withString:@""];
    int dateNumber = [dateNumberString intValue];
    if (dateNumber == 20240304) {
        int ian = 10;
    }
    if (dateNumber == 20240305) {
        int ian = 10;
    }

    int year = dateNumber / 10000;
    if (season == 1993) {
        if (dateNumber == 19930824 && [teamString1 containsString:@"Liverpool"]) return 19930825;
        if (dateNumber == 19930824 && [teamString1 containsString:@"Southampton"]) return 19930825;
        if (dateNumber == 19931121 && [teamString1 containsString:@"Chelsea"]) return 19931122;
        if (dateNumber == 19931127 && [teamString1 containsString:@"Liverpool"]) return 19931128;
        if (dateNumber == 19931128 && [teamString1 containsString:@"Southampton"]) return 19931129;
        if (dateNumber == 19931210 && [teamString1 containsString:@"Southampton"]) return 19931211;
        if (dateNumber == 19931217 && [teamString1 containsString:@"Wimbledon"]) return 19931218;
        if (dateNumber == 19931231 && [teamString1 containsString:@"Aston Villa"]) return 19940101;
        if (dateNumber == 19940226 && [teamString1 containsString:@"Tottenham"]) return 19940302;
        if (dateNumber == 19940226 && [teamString1 containsString:@"Ipswich"]) return 19940222;
        if (dateNumber == 19940322 && [teamString1 containsString:@"Newcastle"]) return 19940323;
        if (dateNumber == 19940327 && [teamString1 containsString:@"Wimbledon"]) return 19940326;
        if (dateNumber == 19930921 && [teamString1 containsString:@"Fulham"]) return 19930922;
    }

    if (season == 2003) {
        if (dateNumber == 20040411 && [teamString1 containsString:@"Wolverhampton Wanderers"]) return 20040412;
    }
    if (season == 2004) {
        if (dateNumber == 20041029) return 20041030;
        if (dateNumber == 20041030 && [teamString1 containsString:@"Bolton Wanderers"]) return 20041031;
        if (dateNumber == 20041031 && [teamString1 containsString:@"Manchester City"]) return 20041101;
        if (dateNumber == 20050226 && [teamString1 containsString:@"Middlesbrough"]) return 20050227;
        if (dateNumber == 20050226 && [teamString1 containsString:@"Newcastle"]) return 20050227;
        if (dateNumber == 20050319 && [teamString1 containsString:@"Liverpool"]) return 20050320;
        if (dateNumber == 20050319 && [teamString1 containsString:@"Birmingham City"]) return 20050320;
        if (dateNumber == 20050319 && [teamString1 containsString:@"Middlesbrough"]) return 20050320;
        if (dateNumber == 20050423 && [teamString1 containsString:@"Portsmouth"]) return 20050424;
        if (dateNumber == 20050423 && [teamString1 containsString:@"Arsenal"]) return 20050425;
        if (dateNumber == 20050423 && [teamString1 containsString:@"Manchester United"]) return 20050424;
    }
    if (season == 2005) {
        if (dateNumber == 20060131 && [teamString1 containsString:@"Liverpool"]) return 20060201;
        if (dateNumber == 20060131 && [teamString1 containsString:@"Portsmouth"]) return 20060201;
        if (dateNumber == 20060311 && [teamString1 containsString:@"Charlton Athletic"]) return 20060312;
        if (dateNumber == 20060325 && [teamString1 containsString:@"Charlton Athletic"]) return 20060326;
        if (dateNumber == 20060325 && [teamString1 containsString:@"Portsmouth"]) return 20060412;
        if (dateNumber == 20060401 && [teamString1 containsString:@"Manchester City"]) return 20060402;
        if (dateNumber == 20060401 && [teamString1 containsString:@"West Ham United"]) return 20060402;
        if (dateNumber == 20060408 && [teamString1 containsString:@"Chelsea"]) return 20060409;
        if (dateNumber == 20060415 && [teamString1 containsString:@"Aston Villa"]) return 20060416;
        if (dateNumber == 20060415 && [teamString1 containsString:@"Blackburn Rovers"]) return 20060416;
        if (dateNumber == 20060415 && [teamString1 containsString:@"Manchester United"]) return 20060414;
        if (dateNumber == 20060429 && [teamString1 containsString:@"Sunderland"]) return 20060501;
        if (dateNumber == 20060429 && [teamString1 containsString:@"Tottenham"]) return 20060430;
        if (dateNumber == 20060429 && [teamString1 containsString:@"West Bromwich Albion"]) return 20060501;
        // Ch
        if (dateNumber == 20060114 && [teamString1 containsString:@"Watford"]) return 20060116;
        if (dateNumber == 20060408 && [teamString1 containsString:@"Watford"]) return 20060409;

    }
    if (season == 2006) {
        if (dateNumber == 20061216 && [teamString1 containsString:@"Manchester City"]) return 20061217;
        if (dateNumber == 20070101 && [teamString1 containsString:@"Arsenal"]) return 20070102;
        if (dateNumber == 20070113 && [teamString1 containsString:@"Everton"]) return 20070114;
        if (dateNumber == 20070113 && [teamString1 containsString:@"Tottenham"]) return 20070114;
        if (dateNumber == 20070120 && [teamString1 containsString:@"Arsenal"]) return 20070121;
        if (dateNumber == 20070120 && [teamString1 containsString:@"Wigan"]) return 20070121;
        if (dateNumber == 20070130 && [teamString1 containsString:@"Bolton"]) return 20070131;
        if (dateNumber == 20070131 && [teamString1 containsString:@"West Ham"]) return 20070130;
        if (dateNumber == 20070203 && [teamString1 containsString:@"Tottenham"]) return 20070204;
    }

    if (season == 1992 || season == 1994 || season == 1995 || season == 1996 || season == 1997 || season == 1998 || season == 1999) {
        // 1992
        if (dateNumber == 19920926 && [teamString1 containsString:@"Liverpool"]) return 19920926;
        if (dateNumber == 19921107 && [teamString1 containsString:@"Oldham Athletic"]) return 19921109;
        if (dateNumber == 19921204 && [teamString1 containsString:@"Manchester United"]) return 19921206;
        if (dateNumber == 19921204 && [teamString1 containsString:@"Everton"]) return 19921207;
        if (dateNumber == 19921213 && [teamString1 containsString:@"Liverpool"]) return 19921213;
        if (dateNumber == 19930207 && [teamString1 containsString:@"Ipswich Town"]) return 19930209;
        if (dateNumber == 19930403 && [teamString1 containsString:@"Norwich City"]) return 19930405;
        if (dateNumber == 19930507 && [teamString1 containsString:@"Queens Park Rangers"]) return 19930509;
        if (dateNumber == 19930507 && [teamString1 containsString:@"Wimbledon"]) return 19930509;

        // 1994
        if (dateNumber == 19940820 && season == 1994) return 19940820;
        if (dateNumber == 19940821 && [teamString1 containsString:@"Leicester"]) return 19940821;
        if (dateNumber == 19941211 && [teamString1 containsString:@"Liverpool"]) return 19941211;
        if (dateNumber == 19941218 && [teamString1 containsString:@"Chelsea"]) return 19941218;
        if (dateNumber == 19941226 && [teamString1 containsString:@"Leicester"]) return 19941226;
        if (dateNumber == 19941228 && [teamString1 containsString:@"Liverpool"]) return 19941228;
        if (dateNumber == 19950418 && [teamString1 containsString:@"Blackburn Rovers"]) return 19950420;
        
        // 1995
        if (dateNumber == 19951105 && [teamString1 containsString:@"Newcastle"]) return 19951108;
        if (dateNumber == 19951230 && [teamString1 containsString:@"Aston Villa"]) return 19960131;
        if (dateNumber == 19960204 && [teamString1 containsString:@"Chelsea"]) return 19960204;
        if (dateNumber == 19960301 && [teamString1 containsString:@"Liverpool"]) return 19960303;
        if (dateNumber == 19960322 && [teamString1 containsString:@"Southampton"]) return 19960325;

        // 1996
        if (dateNumber == 19961020 && [teamString1 containsString:@"Newcastle"]) return 19961020;
        if (dateNumber == 19961217 && [teamString1 containsString:@"Liverpool"]) return 19961217;
        if (dateNumber == 19970327 && [teamString1 containsString:@"Wimbledon"]) return 19970318;
        if (dateNumber == 19970101 && [teamString1 containsString:@"Chelsea"]) return 19970101;
        
        // 1997
        if (dateNumber == 19980116 && [teamString1 containsString:@"Everton"]) return 19980118;
        if (dateNumber == 19980131 && [teamString1 containsString:@"West Ham United"]) return 19980131;
        if (dateNumber == 19980207 && [teamString1 containsString:@"Newcastle"]) return 19980207;
        if (dateNumber == 19980214 && [teamString1 containsString:@"Manchester United"]) return 19980214;

        // 1998
        if (dateNumber == 19980815 && season == 1998) return 19980815;
        if (dateNumber == 19980816 && season == 1998) return 19980816;
        if (dateNumber == 19980817 && season == 1998) return 19980817;
        if (dateNumber == 19980907 && [teamString1 containsString:@"Manchester United"]) return 19980909;

        // 1999
        if (dateNumber == 19990828 && [teamString1 containsString:@"Manchester United"]) return 19990830;
        if (dateNumber == 20000130 && [teamString1 containsString:@"Chelsea"]) return 20000130;
        if (dateNumber == 20000130 && [teamString1 containsString:@"Aston Villa"]) return 20000130;
       //if (dateNumber >= 20000318 && season == 1999) return dateNumber;


    }

    
    
    if (season < 1992) {
        if (season == 1991) {
            // 1991
            if (dateNumber == 19910918 && [teamString1 containsString:@"Manchester City"]) return 19910917;
        }
        if (season == 1990) {
            // 1990
            if (dateNumber == 19910104 && [teamString1 containsString:@"Manchester United"]) return 19910107;
        }
        if (season == 1985) {
            // 1985
            if (dateNumber == 19851007 && [teamString1 containsString:@"Notts County"]) return 19851008;
        }

        if (season == 1984) {
            // 1984
            if (dateNumber == 19840929 && [teamString1 containsString:@"Chelsea"]) return 19840929;
            if (dateNumber == 19840930 && [teamString1 containsString:@"West Bromwich"]) return 19840929;
            if (dateNumber == 19841001 && [teamString1 containsString:@"Watford"]) return 19840929;
            if (dateNumber == 19841002 && [teamString1 containsString:@"Tottenham"]) return 19840929;
            if (dateNumber == 19841003 && [teamString1 containsString:@"Stoke"]) return 19840929;
            if (dateNumber == 19841004 && [teamString1 containsString:@"Nottingham"]) return 19840929;
            if (dateNumber == 19841005 && [teamString1 containsString:@"Newcastle"]) return 19840929;
            if (dateNumber == 19841007 && [teamString1 containsString:@"Liverpool"]) return 19840929;
            if (dateNumber == 19841007 && [teamString1 containsString:@"Ipswich"]) return 19840929;
            if (dateNumber == 19841008 && [teamString1 containsString:@"Coventry"]) return 19840929;
            if (dateNumber == 19841215 && [teamString1 containsString:@"Coventry"]) return 19841215;
            if (dateNumber == 19841227 && [teamString1 containsString:@"Sunderland"]) return 19841226;
            if (dateNumber == 19841228 && [teamString1 containsString:@"Stoke"] && [teamString2 containsString:@"Manchester"]) return 19841226;
            if (dateNumber == 19841228 && [teamString1 containsString:@"Stoke"] && [teamString2 containsString:@"Queens"]) return 19841229;
            if (dateNumber == 19841229 && [teamString1 containsString:@"Southampton"]) return 19841226;
            if (dateNumber == 19841230 && [teamString1 containsString:@"Sheffield Wednesday"]) return 19841226;
            if (dateNumber == 19850101 && [teamString1 containsString:@"Nottingham"]) return 19841226;
            if (dateNumber == 19850102 && [teamString1 containsString:@"Norwich"]) return 19841226;
            if (dateNumber == 19850103 && [teamString1 containsString:@"Luton"]) return 19841226;
            if (dateNumber == 19850104 && [teamString1 containsString:@"West Bromwich"]) return 19841226;
        }
        if (season == 1983) {
            // 1983.
            if (dateNumber == 19840121 && [teamString1 containsString:@"Wolverhampton"]) return 19840121;
            if (dateNumber == 19840122 && [teamString1 containsString:@"West Ham"]) return 19840121;
            if (dateNumber == 19840123 && [teamString1 containsString:@"Watford"]) return 19840121;
            if (dateNumber == 19840124 && [teamString1 containsString:@"Manchester United"]) return 19840121;
            if (dateNumber == 19840125 && [teamString1 containsString:@"Ipswich"]) return 19840121;
            if (dateNumber == 19840126 && [teamString1 containsString:@"Everton"]) return 19840121;
            if (dateNumber == 19840127 && [teamString1 containsString:@"Arsenal"]) return 19840121;
            if (dateNumber == 19840128 && [teamString1 containsString:@"Leicester"]) return 19840121;
            if (dateNumber == 19840207 && [teamString1 containsString:@"Queens Park"]) return 19840207;
            if (dateNumber == 19840208 && [teamString1 containsString:@"West Bromwich"]) return 19840208;
            if (dateNumber == 19840313 && [teamString1 containsString:@"Luton"]) return 19840313;
            if (dateNumber == 19840313 && [teamString1 containsString:@"Norwich"]) return 19840313;
        }
        if (season == 1982) {
            // 1982.
            if (dateNumber == 19821230 && [teamString1 containsString:@"Notts County"]) return 19821228;
            if (dateNumber == 19821230 && [teamString1 containsString:@"Watford"]) return 19821229;
            if (dateNumber == 19821231 && [teamString1 containsString:@"Norwich"]) return 19821228;
            if (dateNumber == 19830101 && [teamString1 containsString:@"Manchester City"]) return 19821228;
            if (dateNumber == 19830102 && [teamString1 containsString:@"Everton"]) return 19821228;
            if (dateNumber == 19830103 && [teamString1 containsString:@"Coventry"]) return 19821228;
            if (dateNumber == 19830311 && [teamString1 containsString:@"Birmingham"]) return 19830315;
        }
        if (season == 1979) {
            // 1976.
            if (dateNumber == 19790915 && [teamString1 containsString:@"Fulham"]) return 19790905;
        }
        if (season == 1976) {
            // 1976.
            if (dateNumber == 19770105 && [teamString1 containsString:@"Liverpool"]) return 19770101;
            if (dateNumber == 19761005 && [teamString1 containsString:@"Bolton Wanderer"]) return 19761004;
        }
        if (season == 1975) {
            // 1975.
            if (dateNumber == 19760323 && [teamString1 containsString:@"West Ham"]) return 19760223;
        }
 
    }
    
    
    //
    // Seems that matches labelled 2:30pm or earlier are the correct date.
    // Those labelled later 6:00pm are a day behind
    if (hourNumber > 18) {
        //
        // Need to add a day, except in a few cases
        static NSDateFormatter *dateFormatter = nil;
        if (dateFormatter == nil) {
            dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        }
        NSDate* matchDate = [dateFormatter dateFromString:dateString];
        if (matchDate == nil) {
            int ian = 10;
        }
        NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
        dayComponent.day = 1;
        NSCalendar *theCalendar = [NSCalendar currentCalendar];
        NSDate *nextDate = [theCalendar dateByAddingComponents:dayComponent toDate:matchDate options:0];
        static NSCalendar* gregorianCalandar = nil;
        if (gregorianCalandar == nil) {
            gregorianCalandar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        }
        NSDateComponents *components = [gregorianCalandar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:nextDate];
        
        dateNumber = (int)([components year] * 10000 + [components month] * 100 + [components day]);
    }
    else {
        //
        // Do nothing
    }

    
    return dateNumber;
}

- (int)parseDeathDateNumber:(NSString*)dateString {
    //
    // Dates come in as dd.MM.yyyy
    NSString* dateNumberString = [dateString stringByReplacingOccurrencesOfString:@"." withString:@""];
    int dateNumberIn = [dateNumberString intValue];
    int year = (dateNumberIn % 10000);
    int month = ((dateNumberIn / 10000) % 100);
    int day = dateNumberIn / 1000000;
    int dateNumberOut = (int)(year * 10000 + month * 100 + day);
    
    if (year < 1870 || year > 2030) {
        NSLog(@"Failed to parse date string: %@", dateString);
    }
        
    return dateNumberOut;
}

- (int)parseBirthDateNumber:(NSString*)dateString {
    // Mar 17, 1993                                                                            (28)
    dateString = [dateString stringByReplacingOccurrencesOfString:@"\\(.*\\)"
                                                                 withString:@""
                                                                    options:NSRegularExpressionSearch range:NSMakeRange(0, dateString.length)];

    static NSDateFormatter *dateFormatter = nil;
    if (dateFormatter == nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"MMM d, yyyy"];
    }
    static NSCalendar* gregorianCalandar = nil;
    if (gregorianCalandar == nil) {
        gregorianCalandar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    }
    @try {
        dateString = [dateString stringByReplacingOccurrencesOfString:@"\t" withString:@" "];
        NSRange range = [dateString rangeOfString:@"  "];
        if (range.length != 0) {
            dateString = [dateString substringToIndex:range.location];
        }
        if ([dateString compare:@"-"] == NSOrderedSame) return 0;
        if (dateString.length == 0) return 0;
        NSDate* date = [dateFormatter dateFromString:dateString];
        NSDateComponents *components = [gregorianCalandar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:date];
        int birthDateNumber = (int)([components year] * 10000 + [components month] * 100 + [components day]);
        return birthDateNumber;
    }
    
    @catch ( NSException *e ) {
        NSLog(@"Failed to parse date: %@", dateString);
       //  [self parseBirthDateNumber:dateString];
        return 0;
    }
}

- (int)addTeam:(NSString*)teamName {
    //
    // Remove " FC"
    teamName = [teamName stringByReplacingOccurrencesOfString:@" FC" withString:@""];
    teamName = [teamName stringByReplacingOccurrencesOfString:@" AFC" withString:@""];
    teamName = [teamName stringByReplacingOccurrencesOfString:@" &amp; " withString:@" & "];
    if ([teamName containsString:@"Wimbledon (- 2004)"]) teamName = @"Wimbledon";
    else if ([teamName containsString:@"Scarborough"]) teamName = @"Scarborough";
    else if ([teamName containsString:@"Telford United"]) teamName = @"Telford United";
    else if ([teamName containsString:@"Rushden & Diamonds"]) teamName = @"Rushden & Diamonds";
    else if ([teamName containsString:@"Darlington"]) teamName = @"Darlington";
    else if ([teamName containsString:@"Chester City"]) teamName = @"Chester City";
    else if ([teamName containsString:@"Hereford United"]) teamName = @"Hereford United";
    else if ([teamName containsString:@"Billericay Town"]) teamName = @"Billericay";
    else if ([teamName containsString:@"Ilkeston Town"]) teamName = @"Ilkeston Town";
    else if ([teamName containsString:@"Tooting & Mitcham"]) teamName = @"Tooting & Mitcham United";
    else if ([teamName containsString:@"Hinckley United"]) teamName = @"Hinckley United";
    else if ([teamName containsString:@"Salisbury City"]) teamName = @"Salisbury City";
    else if ([teamName containsString:@"Forest Green Rovers"]) teamName = @"Forest Green Rovers";
    else if ([teamName containsString:@"Farnborough"]) teamName = @"Farnborough Town";
    else if ([teamName containsString:@"Halifax Town"]) teamName = @"Halifax Town";
    else if ([teamName containsString:@"Droylsden"]) teamName = @"Droylsden";
    else if ([teamName containsString:@"Marlow"]) teamName = @"Marlow";
    else if ([teamName containsString:@"United of Manchester"]) teamName = @"United of Manchester";

    else if ([teamName containsString:@"Guiseley"]) teamName = @"Guiseley AFC";
    else if ([teamName containsString:@"Hendon"]) teamName = @"Hendon";
    else if ([teamName containsString:@"Bishop&#039;s"]) teamName = @"Bishop's Stortford";
    else if ([teamName containsString:@"Weston-super"]) teamName = @"Weston-Super-Mare";
    else if ([teamName containsString:@"North Ferriby United"]) teamName = @"North Ferriby United";
    else if ([teamName containsString:@"Westfields"]) teamName = @"Westfields";
    else if ([teamName compare:@"Hereford"] == NSOrderedSame) teamName = @"Westfields";
    else if ([teamName containsString:@"Shaw Lane"]) teamName = @"Shaw Lane";
    else if ([teamName containsString:@"Hampton & Richmond"]) teamName = @"Hampton & Richmond";
    else if ([teamName containsString:@"Chippenham Town"]) teamName = @"Chippenham Town";
    else if ([teamName containsString:@"Tonbridge Angels"]) teamName = @"Tonbridge";
    else if ([teamName containsString:@"King&#039;s Lynn Town"]) teamName = @"King's Lynn";
    else if ([teamName containsString:@"Aldershot (- 1992)"]) teamName = @"Aldershot";
    else if ([teamName containsString:@"Maidstone United (- 1992)"]) teamName = @"Maidstone United";
    else if ([teamName containsString:@"Merthyr Tydfil"]) teamName = @"Merthyr Tydfil";
    else if ([teamName containsString:@"Yeovil Town"]) teamName = @"Yeovil";
    else if ([teamName containsString:@"Yeading (- 2007)"]) teamName = @"Yeading";
    else if ([teamName containsString:@"Runcorn"]) teamName = @"Runcorn";
    else if ([teamName containsString:@"Newport (IOW)"]) teamName = @"Newport IOW";
    else if ([teamName containsString:@"Cheltenham Town"]) teamName = @"Cheltenham";

   //  else if ([teamName containsString:@"FC Barcelona"]) teamName = @"Barcelona";

    
    return [model addTeam:teamName];
}

- (Team*)addTeamEurope:(NSString*)teamName teamParseID:(int)teamParseID {
    if ([teamName containsString:@"Partick Thistle"]) {
        int ina = 10;
    }
    teamName = [teamName stringByReplacingOccurrencesOfString:@"\\s+$" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, teamName.length)];
    teamName = [teamName stringByReplacingOccurrencesOfString:@" FC" withString:@""];
    return [model getTeam:teamName identInt:teamParseID identKey:@"parseIndexTM"];

}

- (int)getCompetitionFlag:(NSString*)roundString {
    if (roundString == nil) return 0;
    if ([roundString containsString:@"semi"]||
        [roundString containsString:@"Semi"] ||
        [roundString containsString:@"SF"]) {
        return COMPETITION_SEMI_FINAL_FLAG;
    }
    else if ([roundString containsString:@"quarter-final"] ||
             [roundString containsString:@"Quarter"]) {
        return COMPETITION_QUARTER_FINAL_FLAG;
    }
    else if ([roundString containsString:@"Final"] || [roundString containsString:@"final"]) {
        return COMPETITION_FINAL_FLAG;
    }
    else if ([roundString containsString:@"Group A"]) {
        return COMPETITION_GroupA_FLAG;
    }
    else if ([roundString containsString:@"Group B"]) {
        return COMPETITION_GroupB_FLAG;
    }
    else if ([roundString containsString:@"Group C"]) {
        return COMPETITION_GroupC_FLAG;
    }
    else if ([roundString containsString:@"Group D"]) {
        return COMPETITION_GroupD_FLAG;
    }
    else if ([roundString containsString:@"Group E"]) {
        return COMPETITION_GroupE_FLAG;
    }
    else if ([roundString containsString:@"Group F"]) {
        return COMPETITION_GroupF_FLAG;
    }
    else if ([roundString containsString:@"Group G"]) {
        return COMPETITION_GroupG_FLAG;
    }
    else if ([roundString containsString:@"Group H"]) {
        return COMPETITION_GroupH_FLAG;
    }
    else if ([roundString containsString:@"roup I"]) {
        return COMPETITION_GroupI_FLAG;
    }
    else if ([roundString containsString:@"roup J"]) {
        return COMPETITION_GroupJ_FLAG;
    }
    else if ([roundString containsString:@"roup K"]) {
        return COMPETITION_GroupJ_FLAG;
    }
    else if ([roundString containsString:@"roup L"]) {
        return COMPETITION_GroupJ_FLAG;
    }
    else if ([roundString containsString:@"First Round"]) {
        return 1; // replay: https://www.transfermarkt.us/spielbericht/index/spielbericht/1019704
    }
    else if ([roundString containsString:@"Second Round"]) {
        return 2;
    }
    else if ([roundString containsString:@"Third Round"]) {
        return 3;
    }
    else if ([roundString containsString:@"Fourth Round"]) {
        return 4;
    }
    else if ([roundString containsString:@"Fifth Round"]) {
        return 5;
    }
    else if ([roundString containsString:@"last 16"] ||
             [roundString containsString:@"Round of 16"]) {
        return COMPETITION_R_16_FLAG;
    }
    else if ([roundString containsString:@"Qualifying"]) {
        return COMPETITION_PRELIM_FLAG;
    }
    else if ([roundString containsString:@"intermediate"]) {
        return COMPETITION_PRELIM_FLAG;
    }
    else {
        int roundValue = [[roundString extractNumberFromText] intValue];
        if (roundValue == 0) {
            int ian = 10;
        }
        return roundValue;
    }
    
}


@end
