//
//  LFCSeasonParser.h
//  FootballData
//
//  Created by Ian Copeman on 10/11/2020.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

// #define LIVERPOOL_TEAM_ID (1)
#define LIVERPOOL_PARSER_FIRST_SEASON (1893)


@interface LFCSeasonParser : NSObject

- (void)parseAll;
- (void)parseSeason:(NSInteger)season;
- (void)parse:(NSString*)webData;


@end

NS_ASSUME_NONNULL_END
