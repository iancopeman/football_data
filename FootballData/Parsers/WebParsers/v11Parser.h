//
//  v11Parser.h
//  FootballData
//
//  Created by Ian Copeman on 13/09/2021.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface v11Parser : NSObject
- (void)parseAll;
- (void)parseCompetition:(int)competition season:(NSInteger)season;
- (NSArray*)getCompetionArray;
- (void)test;
@end

NS_ASSUME_NONNULL_END
