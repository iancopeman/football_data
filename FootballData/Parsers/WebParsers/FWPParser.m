//
//  FWPParser.m
//  FootballData
//
//  Created by Ian Copeman on 15/11/2021.
//

#import "FWPParser.h"
#import "FootballModel.h"
#import "StringSearch.h"
#import "WebPageManager.h"

#define FilePath (@"/FootballWebPages/")

// https://www.footballwebpages.co.uk/national-league/match-grid/2012-2013

//
// https://www.footballwebpages.co.uk/match/2013-2014/europa-league/tottenham-hotspur/dinamo-tbilisi/79647
// https://www.footballwebpages.co.uk/match/2013-2014/europa-league/dinamo-tbilisi/tottenham-hotspur/79497

@implementation FWPParser {
    WebPageManager* webPageManager;
    FootballModel* model;
    FileManager* fileManager;
    NSString* dataPath;
    NSString* baseURL;
    
}

- (void)parseAll {
    model = [FootballModel getModel];
    webPageManager = [WebPageManager getWebPageManager];
    fileManager = [[FileManager alloc] init];
    dataPath  = [[NSUserDefaults standardUserDefaults] objectForKey:@"dataPath"];
    baseURL = @"https://www.footballwebpages.co.uk";
    
    int season = 2012;
    bool ok = true;
    int count = 0;
    //NSArray* matchFormatList = @[@"%d", @"%dFA", @"%dLC", @"%dL2", @"national-league" ];
    NSArray* matchFormatList = @[ @"national-league"];

    do {
        // NSString* fullURL = [NSString stringWithFormat:@"%@/%d/matches", baseURL, seasonIndex + 1];
        NSLog(@"FWP parsing season: %d", season);
        for (NSString* dataKeyFormat in matchFormatList) {
            CompetitionType competitionType = [self getCompetition:dataKeyFormat season:season];
            NSString* dataKey = [NSString stringWithFormat:@"%d_%@", season, dataKeyFormat];
            NSString* webPageString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:SeasonFilePath dataKey:dataKey];
            if (webPageString == nil) {
                NSString* matchURL = [NSString stringWithFormat:@"%@/%@/match-grid/%d-%d", baseURL, dataKeyFormat, season, season + 1];
                webPageString = [webPageManager getWebPage:matchURL];
                [fileManager savePersistedString:dataPath dataName:FilePath dataType:SeasonFilePath dataKey:dataKey dataString:webPageString];
            }
            
            if (webPageString != nil) {
                if ([webPageString containsString:@"Server Error"]) {
                    break;
                }
                [self parseSeason:webPageString seasonID:season competitionType:competitionType];
            }
            else {
                ok = false;
            }
        }
        season++;
        
        if (season == 2021) break;
        // if (++count == 1) break;

    } while (ok);


    
}

- (void)parseSeason:(NSString*)webData seasonID:(int)seasonID competitionType:(CompetitionType)competitionType {
    StringSearch* stringSearch = [StringSearch initWithString:webData];

    NSString* tableData = [stringSearch getSectionWithKey:@"<table class=\"table match-grid" startKey:@"<table" endKey:@"</table>"];
    StringSearch* stringSearchTable = [StringSearch initWithString:tableData];

    //
    // Get row
    NSString* tableRow = [stringSearchTable getText:@"<tr>" endKey:@"</tr>"];
    while (tableRow != nil) {
        tableRow = [stringSearchTable getText:@"<tr>" endKey:@"</tr>"];
        StringSearch* stringSearchRow = [StringSearch initWithString:tableRow];
        
        NSString* match = [stringSearchRow getText:@"href=\"" endKey:@"\""];
        while (match != nil) {
            [self parseMatch:match seasonID:seasonID competitionType:competitionType];
            match = [stringSearchRow getText:@"href=\"" endKey:@"\""];
        }
    }

}

- (bool)parseMatch:(NSString*)matchURL seasonID:(int)seasonID competitionType:(CompetitionType)competitionType {
    NSString* dataKey = [self generateDataKey:matchURL];
    NSString* dataType = [NSString stringWithFormat:@"%@%d/", MatchFilePath, seasonID];
    NSString* webPageString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:dataType dataKey:dataKey];
    if (webPageString == nil) {
        NSString* matchURLFull = [NSString stringWithFormat:@"%@/%@", baseURL, matchURL];
        webPageString = [webPageManager getWebPage:matchURLFull];
        [fileManager savePersistedString:dataPath dataName:FilePath dataType:dataType dataKey:dataKey dataString:webPageString];
    }
    if (webPageString == nil) {
        return NO;
    }
    
    StringSearch* stringSearch = [StringSearch initWithString:webPageString];
    NSString* tableData = [stringSearch getSectionWithKey:@"<div id=\"match-refreshable" startKey:@"<div" endKey:@"</div"];
    StringSearch* stringSearchTable = [StringSearch initWithString:tableData];

    //
    // Date
    NSString* dateString = [stringSearchTable getTextWithKey:@"<p class=\"match-heading" startKey:@">" endKey:@"<"];

    //
    // Teams
    NSString* homeTeamString = [stringSearchTable getTextWithKey:@"<li class=\"team home-team" startKey:@"<h4>" endKey:@"/h4>"];
    StringSearch* stringSearchTeam = [StringSearch initWithString:homeTeamString];
    NSString* scoreString1 = [stringSearchTeam getText:@">" endKey:@"<"];
    int score1 = [scoreString1 intValue];
    NSString* nameString1 = [stringSearchTeam getText:@">" endKey:@"<"];
    NSString* awayTeamString = [stringSearchTable getTextWithKey:@"<li class=\"team away-team" startKey:@"<h4>" endKey:@"/h4>"];
    stringSearchTeam = [StringSearch initWithString:awayTeamString];
    NSString* scoreString2 = [stringSearchTeam getText:@">" endKey:@"<"];
    int score2 = [scoreString2 intValue];
    NSString* nameString2 = [stringSearchTeam getText:@">" endKey:@"<"];

    
    
    //
    // Parse Date
    NSDate* matchDate = [self parseDate:dateString];

    
    //
    // Add Teams
    int teamID1 = [self addTeam:nameString1];
    int teamID2 = [self addTeam:nameString2];
    NSString* matchKey = [Match makeMatchKeyFromDate:matchDate team1:teamID1 team2:teamID2];

    
    //
    // Check match
    Match* match = [model getMatch:matchKey];
    if (!match) {
        match = [[Match alloc] init];
        [match setMatchTeamsAndDate:matchDate team1:teamID1 team2:teamID2];
        match.competition = competitionType;
        [match setSeason:seasonID];
        [match setHomeTeamID:teamID1]; // Home team is first team
        [match setScore:score1 teamID:teamID1];
        [match setScore:score2 teamID:teamID2];

        [model addMatch:match];
    }
    /*
    if (![model checkMatch:match.matchKey matchState:MatchStateEvents]) {
        //
        // Info
        NSString* infoBlock = [stringSearchTable getText:@"<li class=\"info" endKey:@"</li>"];
        
        //
        // Match events
        NSString* eventsBlock = [stringSearchTable getText:@"<ul class=\"match-events" endKey:@"</ul>"];
        
        //
        // Home team
        NSString* homeTeam =[stringSearchTable getSectionWithKey:@"<ul class=\"match-line-up" startKey:@"<ul" endKey:@"</ul>"];
        NSString* awayTeam =[stringSearchTable getSectionWithKey:@"<ul class=\"match-line-up" startKey:@"<ul" endKey:@"</ul>"];
        

        [self processSquad:homeTeam teamID:teamID1];

    }
    */
    return true;

}

- (NSString*)generateDataKey:(NSString*)urlString {
    //
    // URL: match/2018-2019/national-league/afc-fylde/aldershot-town/11075
    NSString* idString = [urlString lastPathComponent];
    return idString;
}

- (NSDate*)parseDate:(NSString*)dateString {
    static NSDateFormatter *dateFormatter = nil;
    if (dateFormatter == nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"EEEE d MMMM yyyy"];
    }
   /*
    //regular expression using positive look behind. Checks for a digit, than a two letter combination that will match below
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"(?<=\\d)[snrt][tdh]" options:nil error:nil];
    [regex replaceMatchesInString:dateString options: nil range: NSMakeRange(0, 4) withTemplate:@""];

    dateString = dateString.replacingOccurrences(of: "(?<=\\d)[snrt][tdh]", with: "", options: String.CompareOptions.regularExpression, range: nil)
*/
    dateString = [dateString stringByReplacingOccurrencesOfString:@"th " withString:@" "];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"st " withString:@" "];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"Augu " withString:@"August "];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"nd " withString:@" "];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"rd " withString:@" "];
    NSDate* date = [dateFormatter dateFromString:dateString];
    if (date == nil) {
        NSLog(@"Cannot parse date string: %@", dateString);
    }
    return date;
}

- (int)addTeam:(NSString*)teamName {
    teamName = [teamName stringByReplacingOccurrencesOfString:@" &amp; " withString:@" & "];
    if ([teamName compare:@"Chester"] == NSOrderedSame) teamName = @"Chester City";
    if ([teamName compare:@"Cheltenham Town"] == NSOrderedSame) teamName = @"Cheltenham";
    if ([teamName compare:@"Guiseley"] == NSOrderedSame) teamName = @"Guiseley AFC";
    if ([teamName compare:@"Yeovil Town"] == NSOrderedSame) teamName = @"Yeovil";
    if ([teamName compare:@"FC Halifax Town"] == NSOrderedSame) teamName = @"Halifax Town";
    if ([teamName compare:@"King's Lynn Town"] == NSOrderedSame) teamName = @"King's Lynn";
    
    return [model addTeam:teamName];
}

- (CompetitionType)getCompetition:(NSString*)fileName season:(int)season{
    if ([fileName containsString:@"national-league"]) {
        return CompetitionNationalLeague;
    }
    NSLog(@"Unknown competition: %@", fileName);
    return CompetitionMinor;
}


- (void)processSquad:(NSString*)squad teamID:(int)teamID {
    //
    // first 11 are plying, the others subs
    StringSearch* stringSearch = [StringSearch initWithString:squad];
    NSString* playerText = [stringSearch getSectionWithKey:@"<li" startKey:@"<li" endKey:@"</li"];

    while (playerText != nil) {
        StringSearch* stringSearchPlayer = [StringSearch initWithString:playerText];
        NSString* urlString = [stringSearchPlayer getText:@"a href=\"" endKey:@"\""];
        NSString* nameString = [stringSearchPlayer getTextWithKey:@"player" startKey:@">"  endKey:@"<"];
        nameString = [nameString stringByTrimmingCharactersInSet:
                      [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString* idString = [urlString lastPathComponent];
        int ident = [idString intValue];
        /*
        //
        // Lookup player in model
        Footballer* footballer = [model checkFootballer:ident teamID:teamID];
        if (footballer == nil) {
            //
            // Create new footballer
            footballer = [[Footballer alloc] init];
            footballer.name = nameString;
            footballer.parseIndexFWP = ident;
            footballer.teamID = teamID;

        }
*/
        //
        // find player
        //Footballer* footballer = [self parsePlayer:nameString teamID:teamID idString:idString urlString:urlString];
        
        playerText = [stringSearch getSectionWithKey:@"<li" startKey:@"<li" endKey:@"</li"];


    }

    int ian = 10;
}


@end
