//
//  StringSearch.m
//  FootballData
//
//  Created by Ian Copeman on 10/11/2020.
//

#import "StringSearch.h"

@interface StringSearch () {
    NSString* seachString;
    NSUInteger pos;
}
@end

@implementation StringSearch

+ (StringSearch*)initWithString:(NSString*)seachString {
    StringSearch* stringSearch = [[StringSearch alloc] init];
    stringSearch->seachString = seachString;
    return stringSearch;
}

+ (StringSearch*)initWithString:(NSString*)seachString key:(NSString*)key {
    if (seachString == nil) return nil;
    
    NSRange rangeSearch = [seachString
                           rangeOfString:key
                           options:NSLiteralSearch];
    if (rangeSearch.length == 0) return nil;
    rangeSearch.location = rangeSearch.location + key.length;
    rangeSearch.length = seachString.length - rangeSearch.location;
    NSString* foundString = [seachString substringWithRange:rangeSearch];
    return [StringSearch initWithString:foundString];
}

+ (StringSearch*)initWithString:(NSString*)seachString key:(NSString*)key startKey:(NSString*)startKey endKey:(NSString*)endKey {
    if (seachString == nil) return nil;
    
    NSRange rangeSearch = [seachString
                           rangeOfString:key
                           options:NSLiteralSearch];
    if (rangeSearch.length == 0) return nil;
    rangeSearch.location = rangeSearch.location + key.length;
    rangeSearch.length = seachString.length - rangeSearch.location;
    
    NSRange rangeStart = [seachString
                          rangeOfString:startKey
                          options:NSLiteralSearch
                          range:rangeSearch];
    if (rangeStart.length == 0) return nil;
    
    rangeSearch.location = rangeStart.location + startKey.length;
    rangeSearch.length = seachString.length - rangeSearch.location;
    NSRange rangeEnd = [seachString
                        rangeOfString:endKey
                        options:NSLiteralSearch
                        range:rangeSearch];
    if (rangeEnd.length == 0) return nil;
    
    rangeSearch.length = rangeEnd.location - rangeSearch.location;
    NSString* foundString = [seachString substringWithRange:rangeSearch];
    
    return [StringSearch initWithString:foundString];
}

+(NSString*)stringByStrippingHTML:(NSString*)s {
    return [self stringByStrippingRegEx:s regex:@"<[^>]+>"];
}

+(NSString*)stringByStrippingRegEx:(NSString*)s regex:(NSString*)regex {
    if (s == nil) return nil;
    NSRange r;
    while ((r = [s rangeOfString:regex options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}


- (void)setString:(NSString*)string {
    seachString = string;
    pos = 0;
}



- (NSString*)getString {
    return seachString;
}
- (void)setStringPos:(int)seachPos {
    pos = seachPos;
}
- (int)getStringPos {
    return (int)pos;
}

- (NSString*)getText:(nullable NSString*)startKey  endKey:(NSString*)endKey {
    return [self getTextWithKey:nil startKey:startKey endKey:endKey];
}

- (NSString*)getTextWithKey:(nullable NSString*)key startKey:(nullable NSString*)startKey  endKey:(NSString*)endKey {
    NSRange rangeSearch;
    rangeSearch.location = pos;
    rangeSearch.length = seachString.length - pos;
    if (key != nil) {
        rangeSearch = [seachString
                       rangeOfString:key
                       options:NSLiteralSearch
                       range:rangeSearch];
        if (rangeSearch.length == 0) return nil;
        rangeSearch.location = rangeSearch.location + key.length;
        rangeSearch.length = seachString.length - rangeSearch.location;
    }
    
    NSRange rangeStart = NSMakeRange(pos, rangeSearch.length);
    if (startKey != nil) {
        rangeStart = [seachString
                              rangeOfString:startKey
                              options:NSLiteralSearch
                              range:rangeSearch];
        if (rangeStart.length == 0) return nil;
    }
    
    rangeSearch.location = rangeStart.location + startKey.length;
    rangeSearch.length = seachString.length - rangeSearch.location;
    NSRange rangeEnd = [seachString
                        rangeOfString:endKey
                        options:NSLiteralSearch
                        range:rangeSearch];
    if (rangeEnd.length == 0) return nil;
    
    
    rangeSearch.length = rangeEnd.location - rangeSearch.location;
    NSString* foundString = [seachString substringWithRange:rangeSearch];
    pos = rangeEnd.location;
    
    return foundString;
}

- (NSString*)getSectionWithKey:(nullable NSString*)key startKey:(NSString*)startKey  endKey:(NSString*)endKey {
    
    NSRange rangeSearch;
    rangeSearch.location = pos;
    rangeSearch.length = seachString.length - pos;
    if (key != nil) {
        rangeSearch = [seachString
                       rangeOfString:key
                       options:NSLiteralSearch
                       range:rangeSearch];
        if (rangeSearch.length == 0) return nil;
    }
    
    NSUInteger startPos = rangeSearch.location;
    rangeSearch.location = rangeSearch.location + key.length;
    rangeSearch.length = seachString.length - rangeSearch.location;
    
    int startCount = [key containsString:startKey] ? 1 : 0;
    int endCount = 0;
    NSRange rangeSearchInner;
    NSUInteger startInnerSearch = rangeSearch.location;
    while (rangeSearch.length > 0) {
        rangeSearch = [seachString
                       rangeOfString:endKey
                       options:NSLiteralSearch
                       range:rangeSearch];
        if (rangeSearch.length == 0) return nil;
        endCount++;
        rangeSearchInner.location = startInnerSearch;
        rangeSearchInner.length = rangeSearch.location - rangeSearchInner.location;
        while (rangeSearchInner.location < rangeSearch.location) {
            rangeSearchInner = [seachString
                                rangeOfString:startKey
                                options:NSLiteralSearch
                                range:rangeSearchInner];
            if (rangeSearchInner.length == 0) break;
            startCount++;
            rangeSearchInner.location += startKey.length;
            rangeSearchInner.length = rangeSearch.location - rangeSearchInner.location + endKey.length - startKey.length;
        }
        rangeSearch.location += endKey.length;
        if (startCount == endCount) {
            rangeSearch.length = rangeSearch.location - startPos;
            rangeSearch.location = startPos;
            NSString* foundString = [seachString substringWithRange:rangeSearch];
            pos = rangeSearch.location + rangeSearch.length;
            return foundString;
        }
        startInnerSearch = rangeSearch.location;
        rangeSearch.length = seachString.length - rangeSearch.location;
        
    }
    return nil;
    
}

@end

