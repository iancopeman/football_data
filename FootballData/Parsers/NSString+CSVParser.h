//
//  NSString+CSVParser.h
//  FootballData
//
//  Created by Ian Copeman on 11/08/2021.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (CSVParser)
- (NSArray*)csvRows;
- (NSString*)extractNumberFromText;
@end

NS_ASSUME_NONNULL_END
