//
//  LFCViewController.m
//  FootballData
//
//  Created by Ian Copeman on 09/08/2021.
//

#import "LFCViewController.h"
#import "LFCSeasonParser.h"

@interface LFCViewController ()

@end

@implementation LFCViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
}
- (IBAction)buttonParseLFCPush:(id)sender {
    LFCSeasonParser* seasonParser = [[LFCSeasonParser alloc] init];
    [seasonParser parseAll];
}

@end
