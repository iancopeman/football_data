//
//  FAViewController.m
//  FootballData
//
//  Created by Ian Copeman on 04/08/2021.
//

#import "FAViewController.h"
#import "FAParser.h"

@interface FAViewController ()

@end

@implementation FAViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
}

- (IBAction)logonFAPush:(id)sender {
    FAParser* faParser = [[FAParser alloc] init];
    [faParser logon];
}

- (IBAction)parseFAPush:(id)sender {
    FAParser* faParser = [[FAParser alloc] init];
    [faParser parseAll];
}


@end
