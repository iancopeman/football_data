//
//  FAParser.h
//  FootballData
//
//  Created by Ian Copeman on 20/07/2021.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FAParser : NSObject
- (void)logon;
- (void)parseAll;

@end

NS_ASSUME_NONNULL_END
