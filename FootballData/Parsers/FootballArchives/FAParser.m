//
//  FAParser.m
//  FootballData
//
//  Created by Ian Copeman on 20/07/2021.
//

#import "FAParser.h"
#import "FootballModel.h"
#import "StringSearch.h"
#import "WebPageManager.h"

#define FilePath (@"/TM/")

// https://www.transfermarkt.us/uefa-champions-league/gesamtspielplan/pokalwettbewerb/CL/saison_id/1992

@implementation FAParser {
    WebPageManager* webPageManager;
    FootballModel* model;
    FileManager* fileManager;
    NSString* dataPath;

}





- (void)parseAll {
    model = [FootballModel getModel];
    webPageManager = [WebPageManager getWebPageManager];
    fileManager = [[FileManager alloc] init];
    dataPath  = [[NSUserDefaults standardUserDefaults] objectForKey:@"dataPath"];

    
    //NSArray* matchFormatList = @[@"%d", @"%dFA", @"%dLC", @"%dCH", @"%dL1", @"%dL2" ];
    NSArray* matchFormatList = @[@"CL" ];
    NSString* competitionString = @"CL";
    
    NSString* baseURL = @"https://www.transfermarkt.us/uefa-champions-league/gesamtspielplan/pokalwettbewerb";

    int seasonIndex = 1992;
    bool ok = true;
    int count = 0;
    do {
        NSString* fullURL = [NSString stringWithFormat:@"%@/%@/%d", baseURL, competitionString, seasonIndex];
        NSString* dataKey = [[NSNumber numberWithInt:seasonIndex] stringValue];
        NSString* webPageString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:SeasonFilePath dataKey:dataKey];
        if (webPageString == nil) {
            webPageString = [webPageManager getWebPage:fullURL];
            [fileManager savePersistedString:dataPath dataName:FilePath dataType:SeasonFilePath dataKey:dataKey dataString:webPageString];
        }
        
        if (webPageString != nil) {
            if ([webPageString containsString:@"Server Error"]) {
                break;
            }
            //[self parseSeason:webPageString seasonID:seasonIndex];
        }
        else {
            ok = false;
        }
        seasonIndex++;
        
        if (++count == 1) break;
        
    } while (ok);

    
    
    // http://www.thefootballarchives.com/network/competition.php?CID=54&Season=1992-1993
    
    // Liverpool   4:3      Leeds United 1141023
    // Crystal Palace 1:0       Southampton 1141022
    //Match* match = [[Match alloc] init];
    //NSString* matchURL = @"http://www.thefootballarchives.com/network/match.php?MID=1141023";

    //if ([self parseMatch:matchURL match:match]) {
   // }

    
}

- (void)parseSeason:(NSString*)webData seasonID:(int)seasonID {
    NSLog(@"FA parsing season: %d", seasonID);
    
    StringSearch* stringSearch = [StringSearch initWithString:webData];
    NSString* matchSection = [stringSearch getSectionWithKey:@"<div class=\"profile_content\">" startKey:@"<div" endKey:@"</div"];
    
    stringSearch = [StringSearch initWithString:matchSection];
    NSString* matchText = [stringSearch getText:@"match.php?MID=" endKey:@"\""];
    NSString* baseURL = @"http://www.thefootballarchives.com/network/match.php?MID=";
    int count = 0;
    while (matchText != nil) {
        //
        // Get match
        NSString* matchURL = [NSString stringWithFormat:@"%@%@", baseURL, matchText];
        Match* match = [[Match alloc] init];
        
        if (![self parseMatch:matchURL match:match seasonID:seasonID]) {
            break;
        }
            /*
        //
        // Check match
        if ([model checkMatch:ParseUEFA dataKey:dataKeyNumber]) {
            //
            // Already parsed
        }
        else {
            if (![self parseMatch:matchURL dataKey:dataKey seasonID:seasonID]) {
             
        }
        */
        if (++count == 1000) break;
        
        matchText = [stringSearch getText:@"match.php?MID=" endKey:@"\""];
        if (matchText == nil) {
            NSLog(@"Last match url: %@", matchURL);
        }


        
    }
    NSLog(@"FA parse complete, %d matches", count);
    // int length = webData.length;// NSString* test = [webData substringFromIndex:[stringSearch getStringPos]];
    
    int ian = 10;
    
}



- (bool)parseMatch:(NSString*)matchURL match:(Match*)match seasonID:(int)seasonID {
    
    NSString* dataKey = [self generateDataKey:MatchPage urlString:matchURL];

    
    NSString* dataType = [NSString stringWithFormat:@"/Matches/%d/", seasonID];
    NSString* webPageString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:dataType dataKey:dataKey];
    if (webPageString == nil) {
        webPageString = [webPageManager getWebPage:matchURL];
        if ([webPageString containsString:@"Registered users only"]) {
            NSLog(@"Not registered!");
            return NO;
        }
        [fileManager savePersistedString:dataPath dataName:FilePath dataType:dataType dataKey:dataKey dataString:webPageString];
    }
    if (webPageString == nil) {
        return NO;
    }

    
    StringSearch* stringSearch = [StringSearch initWithString:webPageString];

    //
    // Match details
    NSString* matchDateString = [stringSearch getTextWithKey:@"Match Date:" startKey:@"<br>" endKey:@"<br"];
    NSString* matchTimeString = [stringSearch getTextWithKey:@"Kick off:" startKey:@"<br>" endKey:@"<br"];
    NSString* matchLocationString = [stringSearch getTextWithKey:@"Ground:" startKey:@"<br>" endKey:@"<br"];
    matchLocationString = [StringSearch stringByStrippingHTML:matchLocationString];
    
    //
    // Match type and season
    NSString* matchTypeString = [stringSearch getTextWithKey:@"competition.php" startKey:@">" endKey:@"<"];
    NSRange rangeEnd = [matchTypeString
                        rangeOfString:@"-"
                        options:NSLiteralSearch];
    if (rangeEnd.length == 0) {
        NSLog(@"Can't find year in webpage");
        return NO;
    }
    NSString* stringSearchYear = [matchTypeString substringToIndex:rangeEnd.location];
    int year = [stringSearchYear intValue];


    //
    // Match events
    NSString* matchEvents = [stringSearch getSectionWithKey:@"<div id=\"profile_profile" startKey:@"<div" endKey:@"</div"];

    stringSearch = [StringSearch initWithString:matchEvents];

    NSString* matchTeam1String = [stringSearch getTextWithKey:@"<td class=\"header2" startKey:@">" endKey:@"<"];
    NSString* matchTeam2String = [stringSearch getTextWithKey:@"<td class=\"header2" startKey:@">" endKey:@"<"];

    
    //
    // Add Teams
    int teamID1 = [model addTeam:matchTeam1String];
    int teamID2 = [model addTeam:matchTeam2String];

    [match setMatchTeamsAndDateString:matchDateString team1:teamID1 team2:teamID2];
    
    //
    // Check match
    if (![model checkMatch:match.matchKey]) {
        //
        // Add match
        match.locationID = [model addLocation:matchLocationString];
        match.competition = [model addCompetition:matchTypeString];
        if (match.competition == CompetitionMinor) return YES;
        [match setSeason:year];
        [model addMatch:match];
        
        //
        // Squad 1
        NSString* squad1 = [stringSearch getSectionWithKey:@"<td style=\"padding" startKey:@"<td" endKey:@"</td"];
        NSString* squad2 = [stringSearch getSectionWithKey:@"<td style=\"padding" startKey:@"<td" endKey:@"</td"];
        
        [self processSquad:squad1 teamID:teamID1];
        
        int ian = 10;

    }
    /*
    if (savePage) {
        [webPageManager saveWebPage:webPage];
    }
*/
    return true;
}

- (void)processSquad:(NSString*)squad teamID:(int)teamID {
    StringSearch* stringSearch = [StringSearch initWithString:squad];
    NSString* player = [stringSearch getText:@"<span" endKey:@"br"];
    bool processSubs = NO;
    while (player != nil) {
        StringSearch* stringSearchPlayer = [StringSearch initWithString:player];
        if (!processSubs) {
            NSString* roleString = [stringSearchPlayer getText:@">" endKey:@"<"];
        }
        else {
            
        }
        NSString* idString = [stringSearchPlayer getText:@"?ID=" endKey:@"\""];
        NSString* nameString = [stringSearchPlayer getText:@">" endKey:@"<"];
        int bookingTime = 0;
        int subTime = 0;
        int goalTime = 0;
        if ([player containsString:@"Booking"]) {
            NSString* bookingTimeString = [stringSearchPlayer getTextWithKey:@"Booking" startKey:@">" endKey:@"<"];;
            bookingTime = [bookingTimeString intValue];
        }
        if ([player containsString:@"Subst"]) {
            NSString* subTimeString = [stringSearchPlayer getTextWithKey:@"Subst" startKey:@">" endKey:@"<"];
            subTime = [subTimeString intValue];
        }
        if ([player containsString:@"Goal"]) {
            NSString* goalTimeString = [stringSearchPlayer getTextWithKey:@"Goal" startKey:@">" endKey:@"<"];
            goalTime = [goalTimeString intValue];
        }
        
        //
        // find player
        Footballer* footballer = [self parsePlayer:nameString teamID:teamID idString:idString];

        
        if (!processSubs) {
            player = [stringSearch getText:@"<span" endKey:@"br"];
            if (player == nil) {
                processSubs = YES;
            }
        }
        if (processSubs) {
            player = [stringSearch getText:@"<a href" endKey:@"br"];
        }
        int ian = 10;

    }
    NSString* player2 = [StringSearch stringByStrippingHTML:player];



}

- (Footballer*)parsePlayer:(NSString*)playerName teamID:(int)teamID idString:(NSString*)idString  {
    
    //
    // Get footballer identifier
    int identInt = idString.intValue;
    if (identInt == 0) {
        return nil;
    }
    
    //
    // Lookup player in model
    Footballer* footballer = nil; // [model checkFootballerFA:identInt teamID:teamID];
    if (footballer != nil) return footballer;

    //
    // Add footballer
    NSString* fullURL = [NSString stringWithFormat:@"http://www.thefootballarchives.com/network/player.php?ID=%d", identInt];
    NSString* webPageString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:FootballerFilePath dataKey:idString];
    if (webPageString == nil) {
        webPageString = [webPageManager getWebPage:fullURL];
        if ([webPageString containsString:@"Registered users only"]) {
            NSLog(@"Not registered!");
            return nil;
        }
        [fileManager savePersistedString:dataPath dataName:FilePath dataType:FootballerFilePath dataKey:idString dataString:webPageString];
    }
    if (webPageString == nil) {
        return nil;
    }

    //
    // Create new footballer
    footballer = [[Footballer alloc] init];
    footballer.name = playerName;
    // footballer.parseIndexFA = identInt;
    footballer.teamID = teamID;


    //
    // Find attributes
    StringSearch* stringSearchProfile = [StringSearch initWithString:webPageString];
    NSString* section = [stringSearchProfile getText:@"<td class=\"profile\"" endKey: @"</td"];
    StringSearch* rowSearch = [StringSearch initWithString:section];
    while (rowSearch != nil) {
        NSString* profileAttribute = [rowSearch getText:@"b>" endKey: @"</b"];
        NSString* profileValue = [rowSearch getText:@"br>" endKey:@"<br"];
        if (profileAttribute == nil || profileValue == nil) break;
        [footballer addAttribute:profileAttribute value:profileValue];
    }

    


    //
    // Add attributes

    //
    // Add to model
    footballer = [model addFootballer:footballer season:0];

    return footballer;

}

- (void)doLogin {
    
    
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://www.thefootballarchives.com/network/login.php"]];
    
    NSString * parameterString = @"email=ian%40devlex.com&password=1an1an&task=dologin&ip=86.132.67.1";
    NSLog(@"%@",parameterString);

    //create the Method "GET" or "POST"
    [urlRequest setHTTPMethod:@"POST"];
    
    //Convert the String to Data
    NSData *data1 = [parameterString dataUsingEncoding:NSUTF8StringEncoding];
    
    //Apply the data to the body
    [urlRequest setHTTPBody:data1];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        if(httpResponse.statusCode == 200)
        {
            for (NSHTTPCookie *cookie in [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies])
            {
                NSLog(@"name: '%@'\n",   [cookie name]);
                NSLog(@"value: '%@'\n",  [cookie value]);
                NSLog(@"domain: '%@'\n", [cookie domain]);
                NSLog(@"path: '%@'\n",   [cookie path]);
            }
            
            NSURL* url = [NSURL URLWithString:@"http://www.thefootballarchives.com/network/match.php?MID=1141022"];
            //NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];

            NSError* error;
            NSString* cookieString = @"PHPSESSID=44nrdp3oc6o8bctt520k2n95d5; se_language_autodetected=1; prev_email=ian%40devlex.com; se_auth_token=d3ad1bb35bc318e6ca041d9c48463ff6a42238aa";
            // [url setValue:cookieString forHTTPHeaderField:@"Cookie"];

            NSStringEncoding* encoding = NULL;
            NSString* webData = [NSString stringWithContentsOfURL:url usedEncoding:encoding error:&error];
            int ian = 10;
            /*
            NSDictionary *fields = [httpResponse allHeaderFields];
            NSDictionary *cookie = [fields valueForKey:@"Set-Cookie"];
            NSString* auth_token = [cookie valueForKey:@"se_auth_token"];
            int ian = 10;
             */
            
            /*
            NSError *parseError = nil;
            NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:response options:0 error:&parseError];
            NSLog(@"The response is - %@",responseDictionary);
            NSInteger success = [[responseDictionary objectForKey:@"success"] integerValue];
            if(success == 1)
            {
                NSLog(@"Login SUCCESS");
            }
            else
            {
                NSLog(@"Login FAILURE");
            }
             */
        }
        else
        {
            NSLog(@"Error");
        }
    }];
    [dataTask resume];
    
    
   /*
    NSError *error;
    NSString *urlString = @"http://www.thefootballarchives.com/network/login.php";
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    NSString* username = @"ian@devlex.com";
    NSString* password = @"1an1an";
    NSString* task = @"dologin";
    NSString* ip = @"86.132.67.1";

    NSString * parameterString = @"email=ian%40devlex.com&password=1an1an&task=dologin&ip=86.132.67.1";
    NSLog(@"%@",parameterString);
    
    [request setHTTPMethod:@"POST"];
    
    [request setURL:url];
    
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    
    NSData *postData = [parameterString dataUsingEncoding:NSUTF8StringEncoding];
    
    [request setHTTPBody:postData];
    
    NSData *finalDataToDisplay = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&error];
    */
    /*
    WebPage* webpage = [[WebPage alloc] init];
    webpage.dataKey = @"Login";
    webpage.webPageType = FAMatchPage;
    NSString* webDataString = [[NSString alloc] initWithData:finalDataToDisplay encoding:NSASCIIStringEncoding];
    [webpage setWebData:webDataString isDownloaded:YES];
    [webPageManager saveWebPage:webpage];
*/
 //   NSLog(@"%@",abc);
}


- (NSString*)generateDataKey:(FileType)pageType urlString:(NSString*)urlString {
    if (pageType == MatchPage) {
        //
        // URL: http://www.thefootballarchives.com/network/match.php?MID=1141022
        NSString* key = @"MID=";
        NSRange range = [urlString rangeOfString:key];
        if (range.length == 0) {
            NSLog(@"Cant find key in URL: %@", urlString);
            return nil;
        }
        return [urlString substringFromIndex:range.location + key.length];
    }
    if (pageType == FootballerPage) {
        //
        // URL: http://www.thefootballarchives.com/network/player.php?ID=314512
        NSString* key = @"ID=";
        NSRange range = [urlString rangeOfString:key];
        if (range.length == 0) {
            NSLog(@"Cant find key in URL: %@", urlString);
            return nil;
        }
        return [urlString substringFromIndex:range.location + key.length];
    }
    return nil;
}


@end
