//
//  APIFootballParser.m
//  FootballData
//
//  Created by Ian Copeman on 26/01/2023.
//

#import "APIFootballParser.h"
#import "FootballModel.h"
#import "FileManager.h"
#import "MatchStats.h"
#import "StringSearch.h"
#import <Cocoa/Cocoa.h>

#define FilePathStub (@"/APIFootball/")
#define TeamsFilePath (@"Teams/")

#define APIF_BASE_URL (@"https://v3.football.api-sports.io/")
#define APIF_LIVE_SCORE (@"fixtures?id=")

// #define DataType (@"/2022/")

//
// To Get an actual match
//
// Get a list of fixtures for a competition in a season:
// https://api-football-v1.p.rapidapi.com/v3/fixtures?league=3&season=2020
//
// For each Fixture:
// https://api-football-v1.p.rapidapi.com/v3/fixtures?id=606561
//
// Need to map APIFootball playerIds to FSN playerIDs

@implementation APIFootballParser {
    FootballModel* model;
    FileManager* fileManager;
    NSString* dataPath;
    NSArray* parseArray;
}

- (id) init {
    self = [super init];
    if (self != nil) {
        self->model = [FootballModel getModel];
        self->fileManager = [[FileManager alloc] init];
        self->dataPath  = [[NSUserDefaults standardUserDefaults] objectForKey:@"dataPath"];
        self->parseArray = @[
            @(CompetitionPremierLeague), @(39),
            @(CompetitionChampionship), @(40),
            @(CompetitionLeagueOne), @(41),
            @(CompetitionLeagueTwo), @(42),
            @(CompetitionNationalLeague), @(43),
            @(CompetitionFACup), @(45),
            @(CompetitionLeagueCup), @(48),
            
            @(CompetitionEuropeanCup), @(2),
            @(CompetitionEuropaLeague), @(3), // First season 2014
            @(CompetitionEuropaConferenceLeague), @(848),
            
        ];
    }
    return self;
}

#pragma mark - Player Parser

- (void)parsePlayers:(int)competition {
    int season = [model getCurrentSeason];
    
    NSString* dataName = [NSString stringWithFormat:@"%@%@%d", FilePathStub, TeamsFilePath, season];
    for (int i = 0; i < parseArray.count / 2; i++) {
        if ([parseArray[i * 2] intValue] == competition) {
            NSNumber* competitionID = parseArray[i * 2 + 1];
            NSString* dataKey = [NSString stringWithFormat:@"%@_Teams", [Match getCompetitionShortName:competition]];
            NSString* jsonString = [fileManager loadPersistedString:dataPath dataName:dataName dataType:[Match getCompetitionShortName:competition] dataKey:dataKey];
            if (jsonString == nil) {
                NSString* fullURL = [NSString stringWithFormat:@"https://v3.football.api-sports.io/teams?league=%@&season=%d", competitionID, season];
                jsonString = [self getRemoteJSONString:fullURL];
                if (jsonString != nil) {
                    [self->fileManager savePersistedString:dataPath dataName:dataName dataType:[Match getCompetitionShortName:competition] dataKey:dataKey dataString:jsonString];
                }
            }
            if (jsonString != nil) {
                [self parseTeams:jsonString competition:competition season:season];
            }
        }
    }
    
}

- (void)parseTeams:(NSString*)jsonString competition:(int)competition season:(int)season {
    NSError *error;
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary* teamDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
    if (teamDictionary == nil) {
        NSLog(@"Failed to load JSON string: %@", error.description);
        return;
    }
    
    NSArray* teams = [teamDictionary objectForKey:@"response"];
    for (NSDictionary* dictionary in teams) {
        NSDictionary* value = [dictionary objectForKey:@"team"];
        NSNumber* apifTeamID = [value objectForKey:@"id"];
        NSString* teamName = [value objectForKey:@"name"];
        //  if (![teamName containsString:@"Watford"]) continue;
        int teamID = [self getTeamID:[apifTeamID intValue]];
        if (teamID == 0) {
            int ian = 10;
        }
        [self parsePlayersForTeam:[apifTeamID intValue] teamName:teamName competition:competition teamID:teamID season:season];
        //  break;
    }
}

- (void)parsePlayersForTeam:(int)apifTeamID teamName:(NSString*)teamName competition:(int)competition  teamID:(int)teamID season:(int)season  {
    NSString* dataKey = [NSString stringWithFormat:@"%@_Players", teamName];
    NSString* dataType = [NSString stringWithFormat:@"%@/%@", [Match getCompetitionShortName:competition], teamName];
    NSString* dataName = [NSString stringWithFormat:@"%@%@%d", FilePathStub, TeamsFilePath, season];
    NSString* jsonString = [fileManager loadPersistedString:dataPath dataName:dataName dataType:dataType dataKey:dataKey];
    if (jsonString == nil) {
        
        NSString* fullURL = [NSString stringWithFormat:@"https://v3.football.api-sports.io/players/squads?team=%d", apifTeamID];
        jsonString = [self getRemoteJSONString:fullURL];
        if (jsonString != nil) {
            [self->fileManager savePersistedString:dataPath dataName:dataName dataType:dataType dataKey:dataKey dataString:jsonString];
            
        }
    }
    if (jsonString != nil) {
        [self parsePlayersFromJSON:jsonString apifTeamID:apifTeamID dirPath:dataType teamID:teamID season:season];
    }
}

- (void)parsePlayersFromJSON:(NSString*)jsonString apifTeamID:(int)apifTeamID dirPath:(NSString*)dirPath teamID:(int)teamID season:(int)season{
    NSError *error;
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary* teamDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
    if (teamDictionary == nil) {
        NSLog(@"Failed to load JSON string: %@", error.description);
        return;
    }
    
    NSArray* teams = [teamDictionary objectForKey:@"response"];
    for (NSDictionary* dictionary in teams) {
        NSArray* players = [dictionary objectForKey:@"players"];
        for (NSDictionary* playerDictionary in players) {
            NSNumber* playerID = [playerDictionary objectForKey:@"id"];
            NSString* playerName = [playerDictionary objectForKey:@"name"];
            [self addPlayerForTeam:apifTeamID playerID:[playerID intValue] playerName:playerName dirPath:dirPath teamID:teamID season:season];
        }
        break;
    }
}

#pragma mark - Player Parser

- (Footballer*)addPlayerForTeam:(int)apifTeamID playerID:(int)playerID playerName:(NSString*)playerName dirPath:(NSString*)dirPath teamID:(int)teamID season:(int)season {
    //
    // Look up player
    int tmp = playerID;
    if (apifTeamID == 47) {
        //playerID = 0;
    }
    if (playerID == 1566) {
        int ian = playerID;
        //    playerID = 0;
    }
    
    Footballer* footballer = [model checkFootballer:playerID identKey:@"parseIndexAPIFootballer" teamID:apifTeamID season:season];
    if (footballer != nil) return footballer;
    
    if (playerID == 0) {
        //        playerID = tmp;
    }
    //
    // Add footballer
    return [self parsePlayer:playerID dirPath:dirPath playerName:playerName teamID:teamID season:season];
}

- (Footballer*)parsePlayer:(int)playerID dirPath:(NSString*)dirPath playerName:(NSString*)playerName teamID:(int)teamID season:(int)season {
    NSString* dataKey = [NSString stringWithFormat:@"%d_%@", playerID, playerName];
    NSString* jsonString = [fileManager loadPersistedString:dataPath dataName:FilePathStub dataType:FootballerFilePath dataKey:dataKey];
    bool saveFile = NO;
    if (jsonString == nil) {
        if (playerID == 65420) season = 2018;
        NSString* fullURL = [NSString stringWithFormat:@"https://v3.football.api-sports.io/players?id=%d&season=%d", playerID, season];
        jsonString = [self getRemoteJSONString:fullURL];
        if (jsonString != nil) {
            saveFile = YES;
        }
    }
    if (jsonString != nil) {
        NSError *error;
        NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary* responseDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
        if (responseDictionary == nil) {
            NSLog(@"Failed to load JSON string: %@", error.description);
            return nil;
        }
        
        NSArray* players = [responseDictionary objectForKey:@"response"];
        if (players.count == 0) {
            return nil;
        }
        NSDictionary* dictionaryPlayer = [players firstObject];
        if (dictionaryPlayer != nil) {
            NSDictionary* player = [dictionaryPlayer objectForKey:@"player"];
            // NSNumber* playerID = [player objectForKey:@"id"];
            // NSString* lastName = [player objectForKey:@"lastname"];
            
            
            //
            // Check footballer with key
            NSDictionary* birth = [player objectForKey:@"birth"];
            NSString* playerBirth = [birth objectForKey:@"date"];
            int birthDateNumber = [self parseBirthDateNumber:playerBirth];
            if (birthDateNumber == 0) {
                NSLog(@"Failed to find birthdate: %@", playerName);
                birthDateNumber = 20020101;
            }
            NSString* lastName = [[playerName componentsSeparatedByString:@" "] lastObject];
            NSString* footballerKey = [Footballer generateKey:birthDateNumber lastName:lastName];
            Footballer* footballer = [model checkFootballerWithKey:footballerKey season:season teamID:teamID parseID:playerID];
            if (footballer != nil) {
                if (saveFile) {
                    [self->fileManager savePersistedString:dataPath dataName:FilePathStub dataType:FootballerFilePath dataKey:dataKey dataString:jsonString];
                }
                return footballer;
            }
            
            if ([playerName containsString:@"Kverkvelia"]) {
                int ian = 10;
            }
            
            //
            // Create new footballer
            NSLog(@"Adding footballer: %@, %@", playerName, dirPath);
            footballer = [[Footballer alloc] init];
            footballer.name = playerName;
            footballer.parseIndexAPIFootballer = playerID;
            footballer.teamID = teamID;
            footballer.birthDateNumber = birthDateNumber;
            NSObject* nationalityObject = [player objectForKey:@"nationality"];
            if ([nationalityObject isKindOfClass:[NSString class]]) {
                footballer.countryID = [Team getCountryID:(NSString*)nationalityObject];
            }
            NSObject* birthPlaceObject = [birth objectForKey:@"place"];
            if ([birthPlaceObject isKindOfClass:[NSString class]]) {
                NSString* birthPlace = [(NSString*)birthPlaceObject stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                if (birthPlace.length != 0) {
                    [footballer setBirthPlace:(NSString*)birthPlaceObject];
                }
            }
            footballer = [model addFootballer:footballer season:season];
            
            if (saveFile) {
                [self->fileManager savePersistedString:dataPath dataName:FilePathStub dataType:FootballerFilePath dataKey:dataKey dataString:jsonString];
            }
            
            return footballer;
        }
    }
    return nil;
}

- (Manager*)parseManager:(int)coachID apifTeamID:(int)apifTeamID coachName:(NSString*)coachName teamID:(int)teamID season:(int)season  {
    if (coachID == 0) return nil;
    
    //
    // Lookup manager in model
    Manager* manager = [model checkManager:coachID identKey:@"parseIndexAPIFootballer" teamID:teamID season:season];
    if (manager != nil) return manager;
    
    
    NSString* dataKey = [NSString stringWithFormat:@"%d_%@", coachID, coachName];
    NSString* jsonString = [fileManager loadPersistedString:dataPath dataName:FilePathStub dataType:ManagersFilePath dataKey:dataKey];
    bool saveFile = NO;
    if (jsonString == nil) {
        NSString* fullURL = [NSString stringWithFormat:@"https://v3.football.api-sports.io/coachs?id=%d", coachID];
        jsonString = [self getRemoteJSONString:fullURL];
        if (jsonString != nil) {
            saveFile = YES;
        }
    }
    
    if (jsonString != nil) {
        NSError *error;
        NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary* responseDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
        if (responseDictionary == nil) {
            NSLog(@"Failed to load JSON string: %@", error.description);
            return nil;
        }
        NSArray* players = [responseDictionary objectForKey:@"response"];
        NSDictionary* dictionaryPlayer = [players firstObject];
        if (dictionaryPlayer != nil) {
            NSDictionary* birth = [dictionaryPlayer objectForKey:@"birth"];
            NSString* playerBirth = [birth objectForKey:@"date"];
            int birthDateNumber = [self parseBirthDateNumber:playerBirth];
            if (birthDateNumber == 0) {
                NSLog(@"Failed to find birthdate: %@", coachName);
                birthDateNumber = 20020101;
            }
            NSString* lastName = [[coachName componentsSeparatedByString:@" "] lastObject];
            NSString* footballerKey = [Footballer generateKey:birthDateNumber lastName:lastName];
            manager = [model checkManagerWithKey:footballerKey season:season teamID:teamID parseID:coachID];
            if (manager != nil) {
                if (saveFile) {
                    [self->fileManager savePersistedString:dataPath dataName:FilePathStub dataType:ManagersFilePath dataKey:dataKey dataString:jsonString];
                }
                return manager;
            }
            
            //
            // Create new Manager
            manager = [[Manager alloc] init];
            manager.name = coachName;
            manager.parseIndexAPIFootballer = coachID;
            manager.teamID = teamID;
            manager.birthDateNumber = birthDateNumber;
            NSObject* nationalityObject = [dictionaryPlayer objectForKey:@"nationality"];
            if ([nationalityObject isKindOfClass:[NSString class]]) {
                manager.countryID = [Team getCountryID:(NSString*)nationalityObject];
            }
            NSObject* birthPlaceObject = [birth objectForKey:@"place"];
            if ([birthPlaceObject isKindOfClass:[NSString class]]) {
                [manager setBirthPlace:(NSString*)birthPlaceObject];
            }
            manager = [model addManager:manager season:season];
            
            if (saveFile) {
                [self->fileManager savePersistedString:dataPath dataName:FilePathStub dataType:ManagersFilePath dataKey:dataKey dataString:jsonString];
            }
            
            return manager;
        }
    }
    return nil;
}

#pragma mark - Match Parser
- (void)parseTest {
    NSString* fullURL = [NSString stringWithFormat:@"%@%@%@", APIF_BASE_URL, APIF_LIVE_SCORE, @(1208252)];
    NSString* jsonString = [self getRemoteJSONString:fullURL];
    [self parseMatch:jsonString season:2024 competitionType:CompetitionPremierLeague ];

}

- (void)parseCompetition:(int)competition season:(int)season {
    
    NSUInteger matchCount = [model getMatchCount];
    NSUInteger playerCount = [model getFootballerCount];
    NSUInteger managerCount = [model getManagerCount];
    
    
    //
    // Get matches in competition
    // https://api-football-v1.p.rapidapi.com/v3/fixtures?league=3&season=2020
    NSString* dataName = [NSString stringWithFormat:@"%@%@%d", FilePathStub, MatchFilePath, season];
    for (int i = 0; i < parseArray.count / 2; i++) {
        if ([parseArray[i * 2] intValue] == competition) {
            NSNumber* competitionID = parseArray[i * 2 + 1];
            
            NSString* dataKey = [NSString stringWithFormat:@"%d_%@_Matches", season, [Match getCompetitionShortName:competition]];
            NSString* jsonString = [fileManager loadPersistedString:dataPath dataName:dataName dataType:nil dataKey:dataKey];
            int test = jsonString.length;
            if (jsonString == nil) {
                NSString* fullURL = [NSString stringWithFormat:@"https://v3.football.api-sports.io/fixtures?league=%@&season=%d", competitionID, season];
                jsonString = [self getRemoteJSONString:fullURL];
                if (jsonString != nil) {
                    if ([model getCurrentSeason] != season) {
                        [self->fileManager savePersistedString:dataPath dataName:dataName dataType:nil dataKey:dataKey dataString:jsonString];
                    }
                }
            }
            if (jsonString != nil) {
                [self parseMatches:jsonString competition:competition season:season];
            }
            // break;
        }
    }
    
    bool matchesChanged = [model areMatchesModified];
    if (matchesChanged) {
        NSLog(@"Matches are modified");
    }
    else {
        NSUInteger newMatchCount = [model getMatchCount];
        NSLog(@"Added %lu matches", newMatchCount - matchCount);
    }
    NSUInteger newPlayerCount = [model getFootballerCount];
    NSLog(@"Added %lu footballers", newPlayerCount - playerCount);
    NSUInteger newManagerCount = [model getManagerCount];
    NSLog(@"Added %lu managers", newManagerCount - managerCount);
    
    
}

- (void)parseMatches:(NSString*)jsonMatches competition:(int)competition season:(int)season {
    NSError *error;
    NSData *jsonData = [jsonMatches dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary* matchDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
    if (matchDictionary == nil) {
        NSLog(@"Failed to load JSON string: %@", error.description);
        return;
    }
    
    NSInteger dateNumberNow = [model getCurrentDateNumber];
    int count = 0;
    NSArray* matches = [matchDictionary objectForKey:@"response"];
    for (NSDictionary* dictionary in matches) {
        //
        // Skip early rounds
        NSDictionary* league = [dictionary objectForKey:@"league"];
        NSString* roundString = [league objectForKey:@"round"];
        int competitionFlag = [self getCompetitionFlag:roundString competition:competition];
        
        if (!IS_LEAGUE_COMPETITION(competition)) {
            if (competition == CompetitionFACup && (competitionFlag == 1 || competitionFlag == 2)) {
                continue;
            }
            if (competition == CompetitionLeagueCup && (competitionFlag == 1)) {
                continue;
            }
            if (competitionFlag == COMPETITION_PRELIM_FLAG) {
                continue;
            }
        }

        NSDictionary* fixture = [dictionary objectForKey:@"fixture"];
        NSNumber* apifMatchID = [fixture objectForKey:@"id"];
        NSString* dateString = [fixture objectForKey:@"date"];
        int matchDateNumber = [self parseDateString:dateString];
        if (matchDateNumber > dateNumberNow) {
            [self parseFutureMatch:dictionary season:season competitionType:competition matchDateNumber:matchDateNumber];
            continue;
        }
        
        //
        // Check for english team in playoff
        if (competitionFlag == COMPETITION_PlayOff_FLAG && competition == CompetitionEuropaConferenceLeague) {
            NSDictionary* teams = [dictionary objectForKey:@"teams"];
            NSDictionary* teamHome = [teams objectForKey:@"home"];
            int teamAPIID1 = [[teamHome objectForKey:@"id"] intValue];
            NSString* teamName1 = [teamHome objectForKey:@"name"];
            NSDictionary* teamAway = [teams objectForKey:@"away"];
            int teamAPIID2 = [[teamAway objectForKey:@"id"] intValue];
            NSString* teamName2 = [teamAway objectForKey:@"name"];
            int teamID1 = [self getTeamID:teamAPIID1];
            int teamID2 = [self getTeamID:teamAPIID2];
            bool englishTeam = NO;
            if (teamID1 != 0) {
                Team* team = [model getTeam:teamID1];
                if (team.countryID == 1) {
                    englishTeam = YES;
                }
            }
            else {
                NSLog(@"@\"%@\", @(%d),", teamName1, teamAPIID1);
            }

            if (teamID2 != 0) {
                Team* team = [model getTeam:teamID2];
                if (team.countryID == 1) {
                    englishTeam = YES;
                }
            }
            else {
                NSLog(@"@\"%@\", @(%d),", teamName2, teamAPIID2);
            }

            if (!englishTeam) {
                continue;
            }
        }

        
        NSString* filePathMatches = [NSString stringWithFormat:@"%@%@%ld/", FilePathStub, MatchFilePath, (long)season];
        NSString* dataKey = [NSString stringWithFormat:@"%@", apifMatchID];
        NSString* jsonString = [fileManager loadPersistedString:dataPath dataName:filePathMatches dataType:[Match getCompetitionShortName:competition] dataKey:dataKey];
        bool downloaded = NO;
        if (jsonString == nil) {
            NSString* fullURL = [NSString stringWithFormat:@"%@%@%@", APIF_BASE_URL, APIF_LIVE_SCORE, apifMatchID];
            jsonString = [self getRemoteJSONString:fullURL];
            downloaded = YES;
        }

        if ([self parseMatch:jsonString season:season competitionType:competition ]) {
            if (downloaded) {
                [self->fileManager savePersistedString:dataPath dataName:filePathMatches dataType:[Match getCompetitionShortName:competition] dataKey:dataKey dataString:jsonString];
            }
        }
        count++;
        // if (count == 2) break;
    }
    
    
    
    //
    // https://api-football-v1.p.rapidapi.com/v3/fixtures?id=606561
    int fixtureID = 606561;
    
    
    
}

- (void)parseFutureMatch:(NSDictionary*)matchDictionary season:(int)season competitionType:(CompetitionType)competitionType matchDateNumber:(NSInteger)matchDateNumber {
    NSDictionary* fixture = [matchDictionary objectForKey:@"fixture"];
    NSDictionary* teams = [matchDictionary objectForKey:@"teams"];
    NSDictionary* league = [matchDictionary objectForKey:@"league"];

    NSDictionary* teamHome = [teams objectForKey:@"home"];
    int teamAPIID1 = [[teamHome objectForKey:@"id"] intValue];
    NSString* teamName1 = [teamHome objectForKey:@"name"];
    NSDictionary* teamAway = [teams objectForKey:@"away"];
    int teamAPIID2 = [[teamAway objectForKey:@"id"] intValue];
    NSString* teamName2 = [teamAway objectForKey:@"name"];

    int teamID1 = [self getTeamID:teamAPIID1];
    int teamID2 = [self getTeamID:teamAPIID2];
    if (teamID1 == 0) {
        Team* team = [model getTeamFromName:teamName1];
        Team* team2 = [model getTeamFromName:@"Plymouth Argyle"];
        //NSLog(@"Failed to find team: %@", teamName1);
        NSLog(@"@\"%@\", @(%d),", teamName1, teamAPIID1);

        raise(SIGSTOP);
    }
    if (teamID2 == 0) {
        Team* team = [model getTeamFromName:teamName2];
        // NSLog(@"Failed to find team: %@", teamName2);
       NSLog(@"@\"%@\", @(%d),", teamName2, teamAPIID2);

         raise(SIGSTOP);
    }

    NSString* dateString = [fixture objectForKey:@"date"];
    int timeNumber = [self parseTimeNumber:dateString];

    
    
    NSString* matchKey = [Match makeMatchKeyFromDateNumber:(int)matchDateNumber team1:teamID1 team2:teamID2];
    Match* match = [model getMatch:matchKey];
    if (match == nil) {
        match = [[Match alloc] init];
        [match setMatchTeamsAndDateNumber:matchDateNumber team1:teamID1 team2:teamID2];
        match.competition = competitionType;
        [match setSeason:season];
        
        NSString* dateString = [fixture objectForKey:@"date"];
        int timeNumber = [self parseTimeNumber:dateString];
        match.timeNumber = timeNumber;
        
        NSDictionary* venue = [fixture objectForKey:@"venue"];
        NSString* venueName = nil;
        NSObject* venueObject = [venue objectForKey:@"name"];
        if ([venueObject isKindOfClass:[NSString class]]) {
            venueName = (NSString*)venueObject;
            [match setLocation:venueName];
        }
        NSString* roundString = [league objectForKey:@"round"];
        match.competitionFlag = [self getCompetitionFlag:roundString competition:competitionType];
        match.matchState = MatchStateFutureMatch;
        [model matchIsModified:match];
        [model addMatch:match];
    }
    else {
        if (match.timeNumber != timeNumber) {
            [model matchIsModified:match];
            match.timeNumber = timeNumber;
        }
    }
}


- (bool)parseMatch:(NSString*)jsonString season:(int)season competitionType:(CompetitionType)competitionType {
    NSError *error;
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary* matchDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
    if (matchDictionary == nil) {
        NSLog(@"Failed to load JSON string: %@", error.description);
        return NO;
    }
    
    NSArray* response = [matchDictionary objectForKey:@"response"];
    NSDictionary* dictionary = [response firstObject];
    bool returnValue = NO;
    if (dictionary != nil) {
        NSDictionary* fixture = [dictionary objectForKey:@"fixture"];
        NSString* dateString = [fixture objectForKey:@"date"];

        int matchDateNumber = [self parseDateString:dateString];
        NSDictionary* teams = [dictionary objectForKey:@"teams"];
        NSDictionary* teamHome = [teams objectForKey:@"home"];
        int teamAPIID1 = [[teamHome objectForKey:@"id"] intValue];
        NSString* teamName1 = [teamHome objectForKey:@"name"];
        NSDictionary* teamAway = [teams objectForKey:@"away"];
        int teamAPIID2 = [[teamAway objectForKey:@"id"] intValue];
        NSString* teamName2 = [teamAway objectForKey:@"name"];

        bool testNames = NO;
        bool matchHasEnglishTeam = NO;
        int teamID1 = [self getTeamID:teamAPIID1];
        int teamID2 = [self getTeamID:teamAPIID2];
        if (teamID1 == 0) {
            Team* team = [model getTeamFromName:teamName1];
            Team* team2 = [model getTeamFromName:@"Heidenheim"];
            //NSLog(@"Failed to find team: %@", teamName1);
            NSLog(@"@\"%@\", @(%d),", teamName1, teamAPIID1);

            if (!testNames) raise(SIGSTOP);
        }
        if (teamID2 == 0) {
            Team* team = [model getTeamFromName:teamName2];
            // NSLog(@"Failed to find team: %@", teamName2);
           NSLog(@"@\"%@\", @(%d),", teamName2, teamAPIID2);

            if (!testNames) raise(SIGSTOP);
        }
        if (testNames) {
            return NO;
        }
        
        
        /*
        static NSMutableArray* teamArray = nil;
        if (teamArray == nil) {
            teamArray = [NSMutableArray arrayWithCapacity:100];
        }
        {
            Team* team = [model getTeam:teamID1];
            if (team.countryID != 1) {
                if (![teamArray containsObject:@(team.index)]) {
                    [teamArray addObject:@(team.index)];
                    NSLog(@"%@", team.name);
                }
            }
            team = [model getTeam:teamID2];
            if (team.countryID != 1) {
                if (![teamArray containsObject:@(team.index)]) {
                    [teamArray addObject:@(team.index)];
                    NSLog(@"%@", team.name);
                }
            }
            return NO;
        }
         */
        
        bool futureMatch = false;
        if (teamID1 != 0 || teamID2 != 0) {
            //
            // Match with an english club
            Team* team1 = [model getTeam:teamID1];
            Team* team2 = [model getTeam:teamID2];
            matchHasEnglishTeam = team1.countryID == CountryEngland || team2.countryID == CountryEngland;
            
            if (team1 == nil || team2 == nil) {
                int ian = 10;
            }
            
            NSString* matchKey = [Match makeMatchKeyFromDateNumber:matchDateNumber team1:teamID1 team2:teamID2];
            
            
            NSLog(@"Parsing Match: %@ v %@", teamName1, teamName2);
            if ([teamName1 containsString:@"Tottenham"]) {
                int ian = 10;
            }
            if ([teamName2 containsString:@"Stuttgart"]) {
                int ian = 10;
            }
            else {
               //   return NO;
            }

            
            //
            // Check match
            bool createMatch = NO;
            Match* match = [model getMatch:matchKey];
            if (match == nil) {
                createMatch = YES;
            }
            if (match.matchParseState == MatchParseStateTeams) createMatch = YES;
            if (createMatch) {
                //
                // New match
                if (match == nil) {
                    match = [[Match alloc] init];
                }
                [match setMatchTeamsAndDateNumber:matchDateNumber team1:teamID1 team2:teamID2];
                match.competition = competitionType;
                [match setSeason:season];
                
                int timeNumber = [self parseTimeNumber:dateString];
                match.timeNumber = timeNumber;

                
                NSDictionary* venue = [fixture objectForKey:@"venue"];
                NSString* venueName = nil;
                NSObject* venueObject = [venue objectForKey:@"name"];
                if ([venueObject isKindOfClass:[NSString class]]) {
                    venueName = (NSString*)venueObject;
                }
                NSString* refereeString = [fixture objectForKey:@"referee"];
                NSDictionary* status = [fixture objectForKey:@"status"];
                NSObject* longObject = [status objectForKey:@"long"];
                bool penalties = false;
                if ([longObject isKindOfClass:[NSString class]]) {
                    NSString* longString = (NSString*)longObject;
                    if ([longString containsString:@"Not Started"]) {
                        futureMatch = true;
                        match.matchState = MatchStateFutureMatch;
                        returnValue = NO;
                    }
                    else if ([longString containsString:@"Time to be defined"]) {
                        futureMatch = true;
                        match.matchState = MatchStateFutureMatch;
                        returnValue = NO;
                    }
                    else if ([longString containsString:@"Match Finished"]) {
                        futureMatch = false;
                        match.matchState = MatchStateFutureMatch;
                        returnValue = NO;
                        NSObject* shortObject = [status objectForKey:@"short"];
                        if ([shortObject isKindOfClass:[NSString class]]) {
                            NSString* shortString = (NSString*)shortObject;
                            if ([shortString containsString:@"PEN"]) {
                                penalties = true;
                            }
                        }

                    }
                    else {
                        futureMatch = true;
                    }
                }
                
                if (!futureMatch) {
                    int ian = 10;
                }
                
                NSDictionary* league = [dictionary objectForKey:@"league"];
                NSString* roundString = [league objectForKey:@"round"];
                match.competitionFlag = [self getCompetitionFlag:roundString competition:competitionType];
                if (![Match isNeutralVenue:match.competition competitionFlag:match.competitionFlag]) {
                    [match setHomeTeamID:teamID1]; // Home team is first team unless final
                }
                
                int goals1 = 0;
                int goals2 = 0;
                int goalMask = 0;
                int penalties1 = 0;
                int penalties2 = 0;
                Manager* manager1 = nil;
                Manager* manager2 = nil;
                NSMutableArray* teamFootballers1 = nil;
                NSMutableArray* teamEvents1 = nil;
                NSMutableArray* teamFootballers2 = nil;
                NSMutableArray* teamEvents2 = nil;
                if (!futureMatch) {
                    returnValue = YES;
                    NSDictionary* goals = [dictionary objectForKey:@"goals"];
                    goals1 = [[goals objectForKey:@"home"] intValue];
                    goals2 = [[goals objectForKey:@"away"] intValue];
                    [match setScore:goals1 teamID:teamID1];
                    [match setScore:goals1 teamID:teamID2];
                    
                    //
                    // Penalties
                    if (penalties) {
                        NSDictionary* score = [dictionary objectForKey:@"score"];
                        if (score != nil) {
                            NSDictionary* penalty = [score objectForKey:@"penalty"];
                            penalties1 = [[penalty objectForKey:@"home"] intValue];
                            penalties2 = [[penalty objectForKey:@"away"] intValue];
                        }
                    }
                    
                    //
                    // Calculate winner
                    EventResultType resultType1 = EventResultDraw;
                    EventResultType resultType2 = EventResultDraw;
                    if (goals1 > goals2) {
                        resultType1 = EventResultWin;
                        resultType2 = EventResultLose;
                    }
                    if (goals1 < goals2) {
                        resultType1 = EventResultLose;
                        resultType2 = EventResultWin;
                    }
                    
                    if (matchHasEnglishTeam) {
                        //
                        // Lineups
                        teamFootballers1 = [NSMutableArray arrayWithCapacity:20];
                        teamEvents1 = [NSMutableArray arrayWithCapacity:20];
                        teamFootballers2 = [NSMutableArray arrayWithCapacity:20];
                        teamEvents2 = [NSMutableArray arrayWithCapacity:20];
                        NSArray* lineups = [dictionary objectForKey:@"lineups"];
                        NSDictionary* lineup1 = [lineups firstObject];
                        NSDictionary* lineup2 = [lineups lastObject];
                        if (lineup1 != nil && lineup2 != nil) {
                            NSArray* lineupArray = @[lineup1, lineup2];
                            for (NSDictionary* lineup in lineupArray) {
                                NSDictionary* teamDetail  = [lineup objectForKey:@"team"];
                                int apifTeamID = [[teamDetail objectForKey:@"id"] intValue];
                                NSDictionary* coachDetail  = [lineup objectForKey:@"coach"];
                                
                                int coachID = 0;
                                NSString* coachIDObject = [coachDetail objectForKey:@"id"];
                                if ([coachIDObject class] == [NSNumber class]) {
                                    coachID = [[coachDetail objectForKey:@"id"] intValue];
                                }
                                NSString* coachName = [coachDetail objectForKey:@"name"];
                                
                                if (apifTeamID == teamAPIID1) {
                                    manager1 = [self parseManager:coachID apifTeamID:apifTeamID coachName:coachName teamID:teamID1 season:season];
                                    [self setMatchLineups:lineup1 apifTeamID:apifTeamID teamID:teamID1 season:season footballers:teamFootballers1 events:teamEvents1 cleanSheet:goals2 == 0 resultType:resultType1];
                                }
                                else {
                                    manager2 = [self parseManager:coachID apifTeamID:apifTeamID coachName:coachName teamID:teamID2 season:season];
                                    [self setMatchLineups:lineup2 apifTeamID:apifTeamID teamID:teamID2 season:season footballers:teamFootballers2 events:teamEvents2 cleanSheet:goals1 == 0 resultType:resultType2];
                                }
                            }
                        }
                        
                        
                        //
                        // Events
                        NSArray* events = [dictionary objectForKey:@"events"];
                        int goalCount = 0;
                        for (NSDictionary* event in events) {
                            NSDictionary* time = [event objectForKey:@"time"];
                            NSNumber* elapsed = [time objectForKey:@"elapsed"];
                            NSDictionary* team = [event objectForKey:@"team"];
                            NSNumber* teamID = [team objectForKey:@"id"];
                            NSDictionary* player = [event objectForKey:@"player"];
                            NSNumber* playerNumber = [player objectForKey:@"id"];
                            NSDictionary* assist = [event objectForKey:@"assist"];
                            NSString* type = [event objectForKey:@"type"];
                            NSString* detail = [event objectForKey:@"detail"];
                            int assistID = 0;
                            bool isTeam1 = [teamID intValue] == teamAPIID1;
                            EventType eventType = 0;
                            if ([type compare:@"Goal"] == NSOrderedSame) {
                                //
                                // Need to check for Penalty shootout goals
                                // Ignore these
                                // comments = "Penalty Shootout";
                                NSObject* commentObject = [event objectForKey:@"comments"];
                                if ([commentObject isKindOfClass:[NSString class]]) {
                                    NSString* commentString = (NSString*)commentObject;
                                    if ([commentString containsString:@"Shootout"]) {
                                        continue;
                                    }
                                }
                                
                                //
                                // Goal Mask - Set before switching isTeam1 in owngoal
                                if (isTeam1) {
                                    goalMask |= 1 << goalCount;
                                }
                                goalCount++;
                                
                                bool penalty = detail != nil && [detail compare:@"Penalty"] == NSOrderedSame;
                                bool ownGoal = detail != nil && [detail compare:@"Own Goal"] == NSOrderedSame;
                                eventType = EventGoal;
                                if (penalty) eventType = EventPenalty;
                                if (ownGoal) {
                                    eventType = EventOwnGoal;
                                    isTeam1 = !isTeam1; // Switch teams for own goal
                                }
                                if (!penalty && !ownGoal && assist != nil) {
                                    NSObject* assistObject = [assist objectForKey:@"id"];
                                    if ([assistObject isKindOfClass:[NSNumber class]]) {
                                        NSNumber* assistNumber = (NSNumber*)assistObject;
                                        assistID = [assistNumber intValue];
                                        
                                    }
                                }
                            }
                            if ([type compare:@"Card"] == NSOrderedSame) {
                                bool redCard = detail != nil && [detail compare:@"Red Card"] == NSOrderedSame;
                                eventType = redCard ? EventRedCard : EventYellowCard;
                            }
                            if ([type compare:@"subst"] == NSOrderedSame) {
                                eventType = EventSub;
                                NSNumber* assistNumber = [assist objectForKey:@"id"];
                                assistID = [assistNumber intValue];
                            }
                            if (eventType != 0) {
                                bool cleanSheet = isTeam1 ? goals2 == 0 : goals1 == 0;
                                EventResultType resultType = isTeam1 ? resultType1 : resultType2;
                                [self processEvent:eventType id1:[playerNumber intValue] id2:assistID time:[elapsed intValue] teamFootballers:isTeam1 ? teamFootballers1 : teamFootballers2 teamEvents:isTeam1 ? teamEvents1 : teamEvents2 cleanSheet:cleanSheet resultType:resultType];
                            }
                            
                        }
                    }
                }
                
                [model matchIsModified:match];
                [model addMatch:match];
                if (venueName != nil) {
                    [match setLocation:venueName];
                }
                
                if (!futureMatch) {
                    [match setReferee:refereeString];
                    [match setManager:manager1 teamID:teamID1];
                    [match setManager:manager2 teamID:teamID2];
                    //  match.attendance = matchAttendance; Can't get attendance
                    [match setScore:goals1 teamID:teamID1];
                    [match setScore:goals2 teamID:teamID2];
                    [match setGoalMask:goalMask];
                    [match setPenalties:penalties1 teamID:teamID1];
                    [match setPenalties:penalties2 teamID:teamID2];
                    [match setFootballers:teamFootballers1 teamID:teamID1];
                    [match setFootballers:teamFootballers2 teamID:teamID2];
                    [match addMatchEvents:teamEvents1 teamID:teamID1 hasAssists:true];
                    [match addMatchEvents:teamEvents2 teamID:teamID2 hasAssists:true];
                    match.matchState = MatchStateResult;
                }
            }
            
            //
            // Parse statistics
            if (!futureMatch) {
                bool processStatistics = NO;
                if (competitionType == CompetitionPremierLeague) processStatistics = YES;
                if (competitionType == CompetitionEuropeanCup) processStatistics = YES;
                if (competitionType == CompetitionEuropaLeague) processStatistics = YES;
                if (competitionType == CompetitionLeagueCup) processStatistics = YES;

                if (competitionType == CompetitionChampionship && (teamID1 == 400 || teamID2 == 400)) { // LeicesterCity
                    processStatistics = YES;
                }
                if (competitionType == CompetitionChampionship && (teamID1 == 380 || teamID2 == 380)) { // Ipswich
                    processStatistics = YES;
                }
                if (competitionType == CompetitionChampionship && (teamID1 == 611 || teamID2 == 611)) { // Southampton
                    processStatistics = YES;
                }
                
                if (processStatistics) {
                    NSArray* statistics = [dictionary objectForKey:@"statistics"];
                    if (statistics.count > 0 && match.index != 0) {
                        if (![model checkMatchStats:match.index]) {
                            MatchStats* matchStats = [MatchStats initWithMatchID:match.index seasonNumber:@(season) dateNumber:match.dateNumber teamName1:team1.name teamName2:team2.name];
                            if ([self parseStatistics:statistics apifTeamID1:teamAPIID1 teamID1:teamID1 apifTeamID2:teamAPIID2 teamID2:teamID2 matchStats:matchStats]) {
                                [model addMatchStats:matchStats];
                                returnValue = YES;
                                
                            }
                            
                        }
                    }
                }
            }
            
        }
        
        
    }
    return returnValue;
    
}

- (int)getSafeIntValue:(NSDictionary*)dic key:(NSString*)key {
    NSObject* object =  [dic objectForKey:key];
    if ([object respondsToSelector:@selector(intValue)]) {
        return [((NSNumber*)object) intValue];
    }
    
    return 0;
}

- (bool)parseStatistics:(NSArray*)statistics apifTeamID1:(int)apifTeamID1 teamID1:(int)teamID1 apifTeamID2:(int)apifTeamID2 teamID2:(int)teamID2 matchStats:(MatchStats*)matchStats {
    if (statistics.count != 2) return false;
    
    NSDictionary* statsTeam1 = statistics.firstObject;
    NSDictionary* statsTeam2 = statistics.lastObject;
    
    // NSDictionary* statsTeam = statsTeam1;
    NSDictionary* team1 = [statsTeam1 objectForKey:@"team"];
    int statTeamID1 = [[team1 objectForKey:@"id"] intValue];
    NSArray* statArray1 = [statsTeam1 objectForKey:@"statistics"];

    NSDictionary* team2 = [statsTeam2 objectForKey:@"team"];
    int statTeamID2 = [[team2 objectForKey:@"id"] intValue];
    NSArray* statArray2 = [statsTeam2 objectForKey:@"statistics"];
    
    
    for (int i = 0; i < 2; i++) {
        bool isPass1 = i == 0;
        int statTeamID = isPass1 ? statTeamID1 :  statTeamID2;
        NSArray* stats = isPass1 ? statArray1 : statArray2;
        bool isTeam1 = statTeamID == apifTeamID1;
        MatchStatsTeam* matchStatsTeam = isTeam1 ? matchStats.teamStats1 :  matchStats.teamStats2;
        for (NSDictionary* dic in stats) {
            NSString* type = [dic objectForKey:@"type"];
            if ([type isEqual:@"Shots on Goal"]) {
                matchStatsTeam.shotsOn = [self getSafeIntValue:dic key:@"value"];
            }
            if ([type isEqual:@"Total Shots"]) {
                matchStatsTeam.shotsTotal = [self getSafeIntValue:dic key:@"value"];
            }
            if ([type isEqual:@"Fouls"]) {
                matchStatsTeam.fouls = [self getSafeIntValue:dic key:@"value"];
            }
            if ([type isEqual:@"Corner Kicks"]) {
                matchStatsTeam.corners = [self getSafeIntValue:dic key:@"value"];
            }
            if ([type isEqual:@"Ball Possession"]) {
                matchStatsTeam.possession = [self getSafeIntValue:dic key:@"value"];
            }
            if ([type isEqual:@"Total passes"]) {
                matchStatsTeam.passes = [self getSafeIntValue:dic key:@"value"];
            }
            if ([type isEqual:@"Passes accurate"]) {
                matchStatsTeam.passesGood = [self getSafeIntValue:dic key:@"value"];
            }
            if ([type isEqual:@"Goalkeeper Saves"]) {
                matchStatsTeam.saves = [self getSafeIntValue:dic key:@"value"];
            }
            if ([type isEqual:@"Shots insidebox"]) {
                matchStatsTeam.shotsIn = [self getSafeIntValue:dic key:@"value"];
            }
            if ([type isEqual:@"Shots outsidebox"]) {
                matchStatsTeam.shotsOut = [self getSafeIntValue:dic key:@"value"];
            }
        }
    }
    
    return true;
}

- (void)processEvent:(EventType)eventType id1:(int)id1 id2:(int)id2 time:(int)time teamFootballers:(NSMutableArray*)teamFootballers teamEvents:(NSMutableArray*)teamEvents cleanSheet:(bool)cleanSheet resultType:(EventResultType)resultType {
    
    int playerID1 = 0;
    int playerID2 = 0;
    for (Footballer* footballer in teamFootballers) {
        if (footballer.parseIndexAPIFootballer == id1) {
            playerID1 = footballer.index;
        }
        if (footballer.parseIndexAPIFootballer == id2) {
            playerID2 = footballer.index;
        }
        if (playerID1 != 0 && (playerID2 != 0 || id2 == 0)) break;
    }
    
    if (eventType == EventGoal) {
        [teamEvents addObject:[Event createEventGoalNumber:playerID1 playerAssistID:playerID2 time:time]];
    }
    else if (eventType == EventPenalty) {
        [teamEvents addObject:[Event createEventPenaltyNumber:playerID1 time:time]];
    }
    else if (eventType == EventOwnGoal) {
        [teamEvents addObject:[Event createEventOwnGoalNumber:playerID1 time:time]];
    }
    else if (eventType == EventSub) {
        //
        // playerID1 - on field player, playerID2 - sub coming on
        [teamEvents addObject:[Event createEventSubNumber:playerID2 playerIDOut:playerID1 time:time cleanSheet:cleanSheet resultType:resultType]];
    }
    else if (eventType == EventRedCard) {
        [teamEvents addObject:[Event createEventRedCardNumber:playerID1 time:time]];
    }
    else if (eventType == EventYellowCard) {
        [teamEvents addObject:[Event createEventYellowCardNumber:playerID1 time:time]];
    }
    
}

- (void)setMatchLineups:(NSDictionary*)lineup apifTeamID:(int)apifTeamID teamID:(int)teamID season:(int)season footballers:(NSMutableArray*)footballers events:(NSMutableArray*)events cleanSheet:(bool)cleanSheet resultType:(EventResultType)resultType {
    NSArray* startXI = [lineup objectForKey:@"startXI"];
    if (startXI.count != 0) {
        int position = 1;
        for (NSDictionary* appearance in startXI) {
            NSDictionary* appearanceDetails = [appearance objectForKey:@"player"];
            NSObject* playerIDObject  = [appearanceDetails objectForKey:@"id"];
            if ([playerIDObject isKindOfClass:[NSNumber class]]) {
                NSNumber* playerID = (NSNumber*)playerIDObject;
                NSString* playerName = [appearanceDetails objectForKey:@"name"];
                bool isGoalie = NO;
                NSNumber* playerNumber = [appearanceDetails objectForKey:@"number"];
                int shirtNumber = [playerNumber intValue];
                NSString* playerPos = [appearanceDetails objectForKey:@"pos"];
                if ([playerPos class] != [NSNull class]) {
                    isGoalie = ([playerPos compare:@"G"] == NSOrderedSame);
                }
                else {
                    NSString* grid = [appearanceDetails objectForKey:@"grid"];
                    if ([grid class] != [NSNull class]) {
                        isGoalie = ([grid compare:@"1:1"] == NSOrderedSame);
                    }
                    else {
                        isGoalie = shirtNumber == 1;
                    }
                }
                Footballer* footballer = [self addPlayerForTeam:apifTeamID playerID:[playerID intValue] playerName:playerName dirPath:FootballerFilePath teamID:teamID season:season];
                if (footballer != nil) {
                    [footballers addObject:footballer];
                    if (isGoalie) {
                        NSNumber* eventNumber = [Event createEventGoalkeeperApperance:footballer.index position:position++ shirt:shirtNumber cleanSheet:cleanSheet resultType:resultType];
                        [events addObject:eventNumber];
                        
                        if ([Event isGoalieApperance:eventNumber])  {
                            int ian = 10;
                        }
                        else {
                            [Event isGoalieApperance:eventNumber];
                        }

                    }
                    else {
                        [events addObject:[Event createEventAppearanceNumber:footballer.index position:position++ shirt:shirtNumber cleanSheet:cleanSheet resultType:resultType]];
                    }
                }
            }
        }
    }
    NSArray* subs = [lineup objectForKey:@"substitutes"];
    if (subs.count != 0) {
        for (NSDictionary* appearance in subs) {
            NSDictionary* appearanceDetails = [appearance objectForKey:@"player"];
            NSObject* playerIDObject  = [appearanceDetails objectForKey:@"id"];
            if ([playerIDObject isKindOfClass:[NSNumber class]]) {
                NSNumber* playerID = (NSNumber*)playerIDObject;
                NSString* playerName = [appearanceDetails objectForKey:@"name"];
                Footballer* footballer = [self addPlayerForTeam:apifTeamID playerID:[playerID intValue] playerName:playerName dirPath:FootballerFilePath teamID:teamID season:season];
                if (footballer != nil) {
                    [footballers addObject:footballer];
                }
                else {
                    int ian = 10;
                }
            }
        }
    }
}



#pragma mark - Helper Functions
/*
- (Team*)getTeam:(NSString*)teamName identInt:(int)identInt {
    Team* team = [model getTeam:teamName identInt:identInt identKey:@"parseIndexAPI"];
    if (team == nil) {
        NSLog(@"Failed to find Team: %@: %d", teamName, identInt);
    }
    return team;
}
*/

- (int)parseDateString:(NSString*)dateString {
    if ([dateString class] == [NSNull class]) {
        return 0;
    }
    // "2020-08-25T16:00:00+00:00";
    dateString = [dateString substringToIndex:10];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"-" withString:@""];
    int dateNumber = [dateString intValue];
    return dateNumber;
}

- (int)parseTimeNumber:(NSString*)dateString {
    if ([dateString class] == [NSNull class]) {
        return 0;
    }
    // "2020-08-25T16:00:00+00:00";
    dateString = [dateString substringWithRange:NSMakeRange(11, 5)];
    dateString = [dateString stringByReplacingOccurrencesOfString:@":" withString:@""];
    int dateNumber = [dateString intValue];
    return dateNumber;
}



- (int)parseBirthDateNumber:(NSString*)dateString {
    if ([dateString class] == [NSNull class]) {
        return 0;
    }
    // 1994-12-04
    dateString = [dateString stringByReplacingOccurrencesOfString:@"-" withString:@""];
    int dateNumber = [dateString intValue];
    return dateNumber;
}

- (NSString*)getRemoteJSONString:(NSString*)urlString {
    NSDictionary *headers = @{ @"X-RapidAPI-Key": @"fdcc8c32de04e730d47469fb2ffe88c6",
                               @"X-RapidAPI-Host": @"v3.football.api-sports.io" };
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    dispatch_semaphore_t    sem;
    sem = dispatch_semaphore_create(0);
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    __block NSString* strResponse = nil;
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"%@", error);
        } else {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            // NSLog(@"%@", httpResponse);
            strResponse = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            // return @"ian";
        }
        dispatch_semaphore_signal(sem);
        
    }];
    [dataTask resume];
    dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);

    //
    // Check for error in returned file
    if ([strResponse containsString:@"Too many requests"]) {
        raise(SIGSTOP);
    }
    
    return strResponse;
    
}


- (void)getRemoteJSONString:(NSString*)urlString completionHandler:(void (^)(NSString* _Nullable stringResponse))completionHandler {
    
    NSDictionary *headers = @{ @"X-RapidAPI-Key": @"fdcc8c32de04e730d47469fb2ffe88c6",
                               @"X-RapidAPI-Host": @"v3.football.api-sports.io" };
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"%@", error);
            completionHandler(nil);
        } else {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            NSLog(@"%@", httpResponse);
            NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            completionHandler(str);
            // return @"ian";
        }
    }];
    [dataTask resume];
}

- (int)getTeamID:(int)apifTeamID {
    Team* team =  [model getTeam:apifTeamID identKey:@"parseIndexAPI"];
    return team.index;
}

// APIFID, DevlexID
static int teamIDArray[] = {
    42, 28,  // Arsenal
    66, 35,  // AstonVilla
    35, 8,   // Bournemouth
    55, 103, // Brentford
    51, 112, // BrightonHoveAlbion
    49, 151, // Chelsea
    52, 199, // CrystalPalace
    45, 257, // Everton
    36, 282, // Fulham
    63, 398, // LeedsUnited
    46, 400, // LeicesterCity
    40, 415, // Liverpool
    50, 443, // ManchesterCity
    33, 444, // ManchesterUnited
    34, 483, // NewcastleUnited
    65, 496, // NottinghamForest
    41, 611, // Southampton
    47, 669, // TottenhamHotspur
    48, 710, // WestHamUnited
    39, 738, // WolverhamptonWanderers
    57, 380, // Ipswich

    54,   70,   // Birmingham City
    67,   80,   // Blackburn Rovers
    1356, 82,   // Blackpool
    56,   113,  // Bristol City
    44,   121,  // Burnley
    43,   141,  // Cardiff City
    1346, 187,  // Coventry City
    37,   369,  // Huddersfield Town
    64,   370,  // Hull City
    1359, 432,  // Luton Town
    70,   457,  // Middlesbrough
    58,   459,  // Millwall
    71,   495,  // Norwich City
    59,   539,  // Preston North End
    72,   541,  // Queens Park Rangers
    53,   548,  // Reading
    73,   567,  // Rotherham United
    62,   586,  // Sheffield United
    75,   639,  // Stoke City
    746,  644,  // Sunderland
    76,   650,  // Swansea City
    38,   694,  // Watford
    60,   708,  // West Bromwich Albion
    61,   723,  // Wigan Athletic

    74,   587,  // Sheffield Wednesday
    1357, 534,  // Plymouth Argyle
    
    1352, 592,  // ShrewsburyTown
    68, 89,     // BoltonWanderers
    1837, 748,     // Wrexham
    1350, 531,     // PeterboroughUnited
    1370, 137,     // CambridgeUnited
    1338, 522,     // OxfordUnited
    1333, 14,      // AFCWimbledon
    1368, 636,     // Stevenage
    1367, 485,     // NewportCounty
    1334, 114,     // BristolRovers
    1354, 223,     // DoncasterRovers
    1381, 671,     // TranmereRovers
    1362, 190,      // CrawleyTown

    1373, 405,      // LeytonOrient
    747, 47,      // Barnsley
    1336, 272,      // FleetwoodTown
    1342, 686,      // Walsall
    1365, 312,      // GrimsbyTown
    1819, 49,      // Barrow
    69, 213,      // DerbyCounty
    1842, 325,      // HarrogateTown
    1358, 749,      // WycombeWanderers
    1361, 179,      // ColchesterUnited
    1355, 537,      // Portsmouth

    
    1345, 158,     // Chesterfield
    1822, 241,     // Eastleigh
    1825, 439,     // Maidstone United

    
    //
    // Champions league
    496, 1160,     // Juventus
    197, 1093,     // PSVEindhoven
    565, 1135,     // BSCYoungBoys
    157, 864,      // BayernMunich
    620, 1337,     // DinamoZagreb
    489, 1150,     // ACMilan
    228, 777,      // SportingCP
    79, 1034,      // LilleOSC
    541, 1123,     // RealMadrid
    172, 905,      // VfBStuttgart
    500, 1152,     // Bologna
    550, 1537,     // ShakhtarDonetsk
    628, 1317,     // ACSpartaPraha
    571, 1071,     // Salzburg
    247, 911,      // Celtic
    656, 1267,     // SKSlovanBratislava
    569, 836,      // ClubBrugge
    165, 867,      // BorussiaDortmund
    505, 1159,     // InterMilan
    85, 1045,      // ParisSaintGermain
    547, 1112,     // Girona
    598, 796,      // RedStarBelgrade
    211, 760,      // Benfica
    209, 1085,     // Feyenoord
    168, 863,      // BayerLeverkusen
    499, 1151,     // Atalanta
    530, 1106,     // AtleticoMadrid
    173, 904,      // VfBLeipzig
    106, 1026,     // Brest
    637, 1076,     // SturmGraz
    91, 1023,      // ASMonaco
    529, 1107,     // Barcelona

    //
    // Europa League
    201, 1081,     // AZ67Alkmaar United
    372, 933,      // Elfsborg
    327, 1412,     // BodøGlimt
    212, 767,      // FCPorto
    572, 1526,     // DynamoKiev
    487, 1161,     // Lazio
    397, 1010,     // Midtjylland
    167, 900,      // TSG1899Hoffenheim
    645, 1240,     // Galatasaray
    619, 1409,     // PAOK
    566, 1283,     // LudogoretsRazgrad
    560, 1330,     // SlaviaPraha
    415, 1098,     // Twente
    84, 1043,      // OGCNice
    548, 1125,     // RealSociedad
    554, 831,      // Anderlecht
    651, 814,      // Ferencvaros
    611, 1239,     // Fenerbahce
    1393, 852,     // RoyalUnionSaintGilloise
    375, 945,      // Malmo
    257, 924,      // Rangers
    194, 1079,     // Ajax
    549, 1234,     // Besiktas
    169, 877,      // EintrachtFrankfurt
    567, 1334,     // ViktoriaPlzen
    559, 1219,     // SteauaBucuresti
    4160, 1567,    // RFS
    80, 1044,      // OlympiqueLyonnais
    553, 1404,     // Olympiacos
    497, 1166,     // Roma
    531, 1105,     // AthleticBilbao
    217, 776,      // SportingBraga
    604, 1742,     // MaccabiTelAviv
    556, 1782,     // QarabagFK
    
    //
    // European conference league
    564, 1244, // İstanbulBaşakşehir
    781, 1069, // RapidWien
    224, 780, // VitoriaGuimaraes
    4360, 1743, // Celje
    677, 1752, // OlimpijaLjubljana
    741, 834, // CercleBrugge
    1011, 1146, // StGallen
    562, 1684, // Astana
    2646, 781, // BackaTopola
    394, 1619, // DinamoMinsk
    254, 916, // Hearts
    3684, 1705, // Noah
    640, 1326, // MladaBoleslav
    339, 966, // LegiaWarsaw
    543, 1122, // RealBetis
    329, 1425, // MoldeFK
    3402, 1480, // OmoniaNicosia
    278, 1498, // VikingurReykjavik
    502, 1156, // Fiorentina
    354, 1612, // TotalNetworkSolutions
    631, 839, // Gent
    400, 996, // FCCopenhagen
    336, 960, // Jagiellonia
    606, 1142, // Lugano
    649, 1369, // HJKHelsinki
    2271, 1673, // Petrocub
    3364, 1542, // BoracBanjaLuka
    617, 1407, // Panathinaikos
    1026, 1066, // LinzerASK
    364, 932, // DjurgardensIF
    652, 1311, // ShamrockRovers
    2247, 1472, // APOELNikosia
    180, 886, // Heidenheim
    5354, 1860, // Larne
    3403, 1861, // Pafos
    2184, 1144, // Servette FC

};


- (int)getTeamID:(int)apifTeamID reverseLookup:(bool)reverseLookup {
    
    int arraySize = sizeof(teamIDArray) / sizeof(teamIDArray[0]);
    if (reverseLookup) {
        for (int i = 0; i < arraySize / 2; i++) {
            if (teamIDArray[i * 2 + 1] == apifTeamID) return teamIDArray[i * 2];
        }
    }
    else {
        for (int i = 0; i < arraySize / 2; i++) {
            if (teamIDArray[i * 2] == apifTeamID) return teamIDArray[i * 2 + 1];
        }
    }
    
    
    return 0;
}

- (int)getCompetitionFlag:(NSString*)roundString competition:(int)competition  {
    if (roundString == nil) return 0;
    if ([roundString containsString:@"Regular Season"]) {
        NSCharacterSet *alphaNums = [NSCharacterSet decimalDigitCharacterSet];
        NSRange numberRange = [roundString rangeOfCharacterFromSet:alphaNums];
        NSString* numbers = [roundString substringFromIndex:numberRange.location];
        int test  = [numbers intValue];
        return test;
    }
    else if ([roundString containsString:@"League Stage"]) {
        // "League Stage - 1"
        return COMPETITION_League_FLAG;
        /*
        NSCharacterSet *alphaNums = [NSCharacterSet decimalDigitCharacterSet];
        NSRange numberRange = [roundString rangeOfCharacterFromSet:alphaNums];
        NSString* numbers = [roundString substringFromIndex:numberRange.location];
        int test  = [numbers intValue];
        return test;
         */
    }
    else if ([roundString containsString:@"Knockout Round Play-offs"]) {
        return COMPETITION_PlayOff_FLAG;
    }
    else if ([roundString containsString:@"Play-offs"] && competition == CompetitionEuropaConferenceLeague) {
        return COMPETITION_PlayOff_FLAG;
    }
    else if ([roundString containsString:@"Qualifying"] || [roundString containsString:@"Preliminary"]) {
        return COMPETITION_PRELIM_FLAG;
    }
    else if ([roundString containsString:@"Semi-finals"]) {
        return COMPETITION_SEMI_FINAL_FLAG;
    }
    else if ([roundString containsString:@"Quarter-finals"]) {
        return COMPETITION_QUARTER_FINAL_FLAG;
    }
    else if ([roundString containsString:@"Final"]) {
        return COMPETITION_FINAL_FLAG;
    }
    else if ([roundString containsString:@"Play-offs"]) {
        return COMPETITION_PRELIM_FLAG;
    }
    else if ([roundString containsString:@"16"]) {
        return COMPETITION_R_16_FLAG;
    }
    else if ([roundString containsString:@"8th Finals"]) {
        return COMPETITION_QUARTER_FINAL_FLAG;
    }
    else if ([roundString containsString:@"Group Stage"] || [roundString containsString:@"Group stage"]) {
        return COMPETITION_2_GroupA_FLAG;
    }
    else if ([roundString containsString:@"1st Round"]) {
        return 1;
    }
    else if ([roundString containsString:@"2nd Round"]) {
        return 2;
    }
    else if ([roundString containsString:@"3rd Round"]) {
        return 3;
    }
    else if ([roundString containsString:@"4th Round"]) {
        return 4;
    }
    else if ([roundString containsString:@"5th Round"]) {
        return 5;
    }
    else if ([roundString containsString:@"6th Round"]) {
        return 6;
    }
    else if ([roundString containsString:@"Group A"]) {
        return COMPETITION_GroupA_FLAG;
    }
    else if ([roundString containsString:@"Group B"]) {
        return COMPETITION_GroupB_FLAG;
    }
    else if ([roundString containsString:@"Group C"]) {
        return COMPETITION_GroupC_FLAG;
    }
    else if ([roundString containsString:@"Group D"]) {
        return COMPETITION_GroupD_FLAG;
    }
    else if ([roundString containsString:@"Group E"]) {
        return COMPETITION_GroupE_FLAG;
    }
    else if ([roundString containsString:@"Group F"]) {
        return COMPETITION_GroupF_FLAG;
    }
    else if ([roundString containsString:@"Group G"]) {
        return COMPETITION_GroupG_FLAG;
    }
    else if ([roundString containsString:@"Group H"]) {
        return COMPETITION_GroupH_FLAG;
    }
    NSLog(@"Unsupported competition flag: %@", roundString);
    return 0;
}

#pragma mark - Test Functions

- (NSDictionary*)getAPIPlayersForTeam:(int)teamID {
    int apifTeamID = [self getTeamID:teamID reverseLookup:true];
    NSString* fullURL = [NSString stringWithFormat:@"https://v3.football.api-sports.io/players/squads?team=%d", apifTeamID];
    NSString* jsonString = [self getRemoteJSONString:fullURL];
    
    NSError *error;
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary* teamDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
    if (teamDictionary == nil) {
        NSLog(@"Failed to load JSON string: %@", error.description);
        return nil;
    }
    
    NSMutableDictionary* teamFootballers = [NSMutableDictionary dictionaryWithCapacity:50];
    NSArray* teams = [teamDictionary objectForKey:@"response"];
    for (NSDictionary* dictionary in teams) {
        NSArray* players = [dictionary objectForKey:@"players"];
        for (NSDictionary* playerDictionary in players) {
            NSNumber* playerID = [playerDictionary objectForKey:@"id"];
            NSString* playerName = [playerDictionary objectForKey:@"name"];
            [teamFootballers setObject:playerName forKey:playerID];
        }
        break;
    }
    return teamFootballers;
}

+ (void)setAPITeamID:(Team*)team {
    int arraySize = sizeof(teamIDArray) / sizeof(teamIDArray[0]);
    for (int i = 0; i < arraySize / 2; i++) {
        if (teamIDArray[i * 2 + 1] == team.index) {
            team.parseIndexAPI = teamIDArray[i * 2];
            return;
        }
    }
}

#define UIColorFromRGB(rgbValue) @(rgbValue)

NSArray* teamColours = @[
    // 2024 Europa Conference League
    @"Servette Geneve", UIColorFromRGB(0x860C26),
    @"Vitoria Guimaraes", UIColorFromRGB(0xFAFAFA),
    @"Celje", UIColorFromRGB(0x152657),
    @"Istanbulspor AS", UIColorFromRGB(0xFEEE00),
    @"Rapid Wien", UIColorFromRGB(0x009150),
    @"Fiorentina", UIColorFromRGB(0x61328D),
    @"Total Network Solutions", UIColorFromRGB(0x0DB357),
    @"Dinamo Minsk", UIColorFromRGB(0x1B55B0),
    @"Hearts", UIColorFromRGB(0x48162A),
    @"Cercle Brugge", UIColorFromRGB(0x02D919),
    @"St. Gallen", UIColorFromRGB(0x288647),
    @"Omonia Nicosia", UIColorFromRGB(0x006C34),
    
    @"Vikingur Reykjavik", UIColorFromRGB(0xDB2F25),
    @"Astana", UIColorFromRGB(0xFEEE00),
    @"Backa Topola", UIColorFromRGB(0x146496),
    @"Noah", UIColorFromRGB(0x080808),
    @"Mlada Boleslav", UIColorFromRGB(0x028BC6),
    @"F.C. Copenhagen", UIColorFromRGB(0xF5F5F5),
    
    @"Jagiellonia", UIColorFromRGB(0xFEF300),
    @"FC Heidenheim", UIColorFromRGB(0xE30312),
    @"Olimpija Ljubljana", UIColorFromRGB(0x029036),
    @"Legia Warsaw", UIColorFromRGB(0x008635),
    @"Real Betis", UIColorFromRGB(0x00974B),
    @"Linzer ASK", UIColorFromRGB(0x080808),
    
    @"Djurgardens IF", UIColorFromRGB(0xFFE700),
    @"Gent", UIColorFromRGB(0x024B9E),
    @"Borac Banja Luka", UIColorFromRGB(0xDA2727),
    @"Panathinaikos", UIColorFromRGB(0x017A3D),
    @"Petrocub", UIColorFromRGB(0x565656),
    @"Pafos", UIColorFromRGB(0x80BFFF),
    
    @"Molde FK", UIColorFromRGB(0x0169B0),
    @"Larne", UIColorFromRGB(0xCD1617),
    @"Lugano", UIColorFromRGB(0x000000),
    @"HJK Helsinki", UIColorFromRGB(0x0C41B5),
    @"Shamrock Rovers", UIColorFromRGB(0x0F882A),
    @"APOEL Nikosia", UIColorFromRGB(0xFDC529),

    // 2024 Europa League
    @"Galatasaray", UIColorFromRGB(0xFCa400),
    @"PAOK", UIColorFromRGB(0x191919),
    @"Anderlecht", UIColorFromRGB(0x5C2195),
    @"Ferencvaros", UIColorFromRGB(0x0A7346),
    @"AZ 67 Alkmaar", UIColorFromRGB(0xDC031B),
    @"Elfsborg", UIColorFromRGB(0xF7DD0B),
    @"Ludogorets Razgrad", UIColorFromRGB(0x00632D),
    @"Slavia Praha", UIColorFromRGB(0xED0F28),
    @"Midtjylland", UIColorFromRGB(0xE10309),
    @"TSG 1899 Hoffenheim", UIColorFromRGB(0x1361B6),
    @"Bodø/Glimt", UIColorFromRGB(0xF9DE00),
    @"FC Porto", UIColorFromRGB(0x2200FF),
    @"OGC Nice", UIColorFromRGB(0xEE151F),
    @"Real Sociedad", UIColorFromRGB(0x0D398D),
    @"Dynamo Kiev", UIColorFromRGB(0x1070C2),
    @"Lazio", UIColorFromRGB(0x86D9F8),
    @"Twente", UIColorFromRGB(0xE70214),
    @"Sporting Braga", UIColorFromRGB(0xC9261B),
    @"Maccabi Tel Aviv", UIColorFromRGB(0xFEFA04),
    @"Olympique Lyonnais", UIColorFromRGB(0xF5F5F5),
    @"Olympiacos", UIColorFromRGB(0xE3171F),
    @"Steaua Bucuresti", UIColorFromRGB(0xEE2A1F),
    @"RFS", UIColorFromRGB(0x2200FF),
    @"Roma", UIColorFromRGB(0x980528),
    @"Athletic Bilbao", UIColorFromRGB(0xEF1F1D),
    @"Malmo", UIColorFromRGB(0x80BFFF),
    @"Rangers", UIColorFromRGB(0x2100FD),
    @"Fenerbahce", UIColorFromRGB(0xFDFF00),
    @"Royal Union Saint-Gilloise", UIColorFromRGB(0xFEF200),
    @"Ajax", UIColorFromRGB(0xEF0200),
    @"Besiktas", UIColorFromRGB(0xEDEDED),
    @"Eintracht Frankfurt", UIColorFromRGB(0xE1040B),
    @"Viktoria Plzen", UIColorFromRGB(0xFF0300),
    @"Qarabag FK", UIColorFromRGB(0x252525),
    
    // 2024 European Cup
    @"AC Milan", UIColorFromRGB(0xF6080E),
    @"BSC Young Boys", UIColorFromRGB(0xF9CD08),
    @"Sporting CP", UIColorFromRGB(0x018157),
    @"Lille OSC", UIColorFromRGB(0xC2071B),
    @"Real Madrid", UIColorFromRGB(0xF9F9F9),
    @"VfB Stuttgart", UIColorFromRGB(0xFFFFFF),
    @"Juventus", UIColorFromRGB(0x141414),
    @"PSV Eindhoven", UIColorFromRGB(0xFF0301),
    @"Bayern Munich", UIColorFromRGB(0xFF0300),
    @"Dinamo Zagreb", UIColorFromRGB(0x397DFF),
    @"AC Sparta Praha", UIColorFromRGB(0x910A0A),
    @"Salzburg", UIColorFromRGB(0xEEEEEE),
    @"Paris Saint-Germain", UIColorFromRGB(0x033F71),
    
    @"Girona", UIColorFromRGB(0xD10624),
    @"Inter Milan", UIColorFromRGB(0x0E1D9E),
    @"Club Brugge", UIColorFromRGB(0x0079C0),
    @"Borussia Dortmund", UIColorFromRGB(0xFFDA01),
    @"Bologna", UIColorFromRGB(0xA01930),
    @"Shakhtar Donetsk", UIColorFromRGB(0xEC6A04),
    @"Celtic", UIColorFromRGB(0x008847),
    @"SK Slovan Bratislava", UIColorFromRGB(0x28B387),
    @"Feyenoord", UIColorFromRGB(0xFF0300),
    @"Bayer Leverkusen", UIColorFromRGB(0xE32220),
    @"Atalanta", UIColorFromRGB(0x295CAF),
    @"Red Star Belgrade", UIColorFromRGB(0xEE151F),
    @"Benfica", UIColorFromRGB(0xE92C2C),
    @"Brest", UIColorFromRGB(0xEE151F),
    @"Sturm Graz", UIColorFromRGB(0xEEEEEE),
    @"Atletico Madrid", UIColorFromRGB(0xE80E17),
    @"VfB Leipzig", UIColorFromRGB(0xFFDD00),
    @"AS Monaco", UIColorFromRGB(0xFF022B),
    @"Barcelona", UIColorFromRGB(0xA60342),

    // 2024 FA Cup
    @"Exeter City", UIColorFromRGB(0xFF0300),
    @"Salford City", UIColorFromRGB(0xEF0200),
    @"Dagenham & Redbridge", UIColorFromRGB(0x0F66EB),
    @"Accrington Stanley", UIColorFromRGB(0xFF0300),
    @"Bristol City", UIColorFromRGB(0xEE0200),
    @"Charlton Athletic", UIColorFromRGB(0xEF0200),
    @"Morecambe", UIColorFromRGB(0xFF0300),
    @"West Bromwich Albion", UIColorFromRGB(0x040C52),
    @"Mansfield Town", UIColorFromRGB(0xFFC001),
    @"Wigan Athletic", UIColorFromRGB(0x1544DD),
    @"Tamworth", UIColorFromRGB(0xE80301),
    @"Hull City", UIColorFromRGB(0xFF9001),
    @"Sunderland", UIColorFromRGB(0xDA0200),
    @"Bromley", UIColorFromRGB(0xEEEEEE),
    @"Peterborough United", UIColorFromRGB(0x0D73FF),
    @"Portsmouth", UIColorFromRGB(0x2200FF),
    @"Lincoln City", UIColorFromRGB(0xFF0300),
    @"Bristol Rovers", UIColorFromRGB(0x2200FF),
    @"Stockport County", UIColorFromRGB(0x1C44FF),
    
    // 2024 League Cup
    @"Birmingham City", UIColorFromRGB(0x2200FF),
    @"Harrogate Town", UIColorFromRGB(0xFEF701),
    @"Preston North End", UIColorFromRGB(0xF2F2F2),
    @"Queens Park Rangers", UIColorFromRGB(0x1A00D3),
    @"Shrewsbury Town", UIColorFromRGB(0xF2A900),
    @"Bolton Wanderers", UIColorFromRGB(0xEEEEEE),
    @"Blackburn Rovers", UIColorFromRGB(0x1BDA00),
    @"Fleetwood Town", UIColorFromRGB(0xFF0300),
    @"Rotherham United", UIColorFromRGB(0xFF0300),
    @"Tranmere Rovers", UIColorFromRGB(0xF2F2F2),
    @"Crawley Town", UIColorFromRGB(0xFF2220),
    @"Barnsley", UIColorFromRGB(0xFF0300),
    @"Doncaster Rovers", UIColorFromRGB(0xFF0300),
    @"Watford", UIColorFromRGB(0xFDF201),
    @"Plymouth Argyle", UIColorFromRGB(0x009150),
    @"Walsall", UIColorFromRGB(0xE40300),
    @"Huddersfield Town", UIColorFromRGB(0x0272CE),
    @"Grimsby Town", UIColorFromRGB(0x080808),
    @"Sheffield Wednesday", UIColorFromRGB(0x0200FF),
    @"Barrow", UIColorFromRGB(0x0D73FF),
    @"Derby County", UIColorFromRGB(0xF0F0F0),
    @"Coventry City", UIColorFromRGB(0x87BEEF),
    @"Oxford United", UIColorFromRGB(0xF9E758),
    @"Millwall", UIColorFromRGB(0x010E36),
    @"Leyton Orient", UIColorFromRGB(0xF00200),
    @"Middlesbrough", UIColorFromRGB(0xFF0300),
    @"Stoke City", UIColorFromRGB(0xF20201),
    @"Norwich City", UIColorFromRGB(0xFEF002),
    @"Swansea City", UIColorFromRGB(0xEFEFEF),
    @"Wycombe Wanderers", UIColorFromRGB(0x76C3EE),
    @"Colchester United", UIColorFromRGB(0x2200FF),
    @"AFC Wimbledon", UIColorFromRGB(0x114BD4),

     ];

+ (void)setTeamColour:(Team*)team {
    for (int i = 0; i < teamColours.count / 2; i++) {
        NSString* teamName = teamColours[i * 2];
        if ([team.name compare:teamName] == NSOrderedSame) {
            int colourValue = [teamColours[i * 2 + 1] intValue];
            team.colour = colourValue;
        }
    }
}




@end
