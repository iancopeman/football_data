//
//  APIFootballParser.h
//  FootballData
//
//  Created by Ian Copeman on 26/01/2023.
//

#import <Foundation/Foundation.h>
#import "Team.h"

NS_ASSUME_NONNULL_BEGIN

@interface APIFootballParser : NSObject
- (void)parsePlayers:(int)competition;
- (void)parseCompetition:(int)competition season:(int)season;
- (NSDictionary*)getAPIPlayersForTeam:(int)teamID;
- (void)parseTest;

// Test function to set API ID into csv file
+ (void)setAPITeamID:(Team*)team;
+ (void)setTeamColour:(Team*)team;
@end

NS_ASSUME_NONNULL_END
