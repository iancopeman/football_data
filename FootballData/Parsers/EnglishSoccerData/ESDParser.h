//
//  ESDParser.h
//  FootballData
//
//  Created by Ian Copeman on 04/08/2021.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ESDParser : NSObject
- (void)parseAll;
+ (NSString*)modifyTeamName:(NSString*)originalName;
- (void)exportCSVFiles;
@end

NS_ASSUME_NONNULL_END
