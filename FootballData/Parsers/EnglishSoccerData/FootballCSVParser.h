//
//  FootballCSVParser.h
//  FootballData
//
//  Created by Ian Copeman on 04/11/2021.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface FootballCSVParser : NSObject
- (void)parseAll;

@end

NS_ASSUME_NONNULL_END
