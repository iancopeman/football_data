//
//  ESDViewController.m
//  FootballData
//
//  Created by Ian Copeman on 04/08/2021.
//

#import "ESDViewController.h"
#import "ESDParser.h"
#import "FootballCSVParser.h"

@interface ESDViewController ()

@end

@implementation ESDViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
}

- (IBAction)pushESDParse:(id)sender {
    ESDParser* parser = [[ESDParser alloc] init];
    [parser parseAll];
}

- (IBAction)pushFootballCSVParse:(id)sender {
    FootballCSVParser* parser = [[FootballCSVParser alloc] init];
    [parser parseAll];
}

- (IBAction)pushTestButton:(id)sender {
    ESDParser* parser = [[ESDParser alloc] init];
    [parser exportCSVFiles];
}
@end
