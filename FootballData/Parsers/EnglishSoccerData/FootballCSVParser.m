//
//  FootballCSVParser.m
//  FootballData
//
//  Created by Ian Copeman on 04/11/2021.
//

#import "FootballCSVParser.h"
#import "FootballModel.h"
#import "WebPageManager.h"
#import "StringSearch.h"
#import "NSString+CSVParser.h"


#define FilePath (@"/FootballCSV/")


@interface FootballCSVParser ()

@end

@implementation FootballCSVParser {
    WebPageManager* webPageManager;
    FootballModel* model;
    FileManager* fileManager;
    NSString* dataPath;
    
}

- (void)parseAll {
    model = [FootballModel getModel];
    fileManager = [[FileManager alloc] init];
    dataPath  = [[NSUserDefaults standardUserDefaults] objectForKey:@"dataPath"];
    webPageManager = [WebPageManager getWebPageManager];
    NSUInteger matchCount = [model getMatchCount];
    
    //
    // Get file, don't download
    //
    // Start at year 2000
    int decade = 2010;
    int year = 0;
    //NSArray* fileNameArray = @[@"eng.1.csv", @"eng.2.csv", @"eng.3.csv", @"eng.4.csv", @"eng.5.csv"];
    NSArray* fileNameArray = @[@"eng.2.csv", @"eng.3.csv", @"eng.4.csv", @"eng.5.csv"];
    int season = decade + year;

    while (season < 2022) {
        season = decade + year;
        NSLog(@"*** Season %d ***", season);
        int nextSeason = decade + year + 1 - 2000;
        NSString* seasonString = [NSString stringWithFormat:@"%ds/%d-%02d", decade, decade + year, nextSeason];
        for (NSString* fileName in fileNameArray) {
            NSString* webPageString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:seasonString dataKey:fileName];
            if (webPageString == nil) continue;
            NSArray* rows = [webPageString csvRows];
            int count = 0;
            for (NSArray *row in rows){
                if (row.count != 5) {
                    break;
                }
                NSString* dateString = row[1];
                if ([dateString containsString:@"Date"]) continue;
                NSString* homeString = row[2];
                NSString* scoreString = row[3];
                NSString* awayString = row[4];
                if (![scoreString containsString:@"-"]) {
                    continue;
                }
                //
                // Parse Date
                NSDate* matchDate = [self parseDate:dateString];
                
                //
                // Get match key
                int team1ID = [self addTeam:homeString];
                int team2ID = [self addTeam:awayString];
                NSString* matchKey = [Match makeMatchKeyFromDate:matchDate team1:team1ID team2:team2ID];
                
                //
                // Check match
                Match* match = [model getMatch:matchKey];
                if (!match) {
                    match = [[Match alloc] init];
                    
                    match.competition = [self getCompetition:fileName season:season];
                    [match setMatchTeamsAndDate:matchDate team1:team1ID team2:team2ID];
                    if ([model checkMatch:match.matchKey]) {
                        continue;
                    }
                    
                    //
                    // Score
                    NSArray* scoreArray = [scoreString componentsSeparatedByString:@"-"];
                    int score1 = [scoreArray[0] intValue];
                    int score2 = [scoreArray[1] intValue];
                    [match setScore:score1 teamID:team1ID];
                    [match setScore:score2 teamID:team2ID];
                    
                    match.homeTeamID = team1ID; // League match, so location is the home team
                    [match setSeason:season];
                    [model addMatch:match];
                    [model matchIsModified:match];
                    
                }
                /*
                 if (count == 1500) {
                 NSLog(@"Last match parsed key: %@", match.matchKey);
                 
                 break;
                 }
                 */
            }
            
        }
        year++;
        if (year == 10) {
            decade += 10;
            year = 0;
            break;
        }
        // break;
    }
    NSUInteger newMatchCount = [model getMatchCount];
    NSLog(@"Added %lu matches", newMatchCount - matchCount);

}

- (CompetitionType)getCompetition:(NSString*)fileName season:(int)season{
    if ([fileName containsString:@".1."]) {
        return CompetitionPremierLeague;
    }
    if ([fileName containsString:@".2."]) {
        if (season < 2004) {
            return CompetitionFirstDivision;
        }
        return CompetitionChampionship;
    }
    if ([fileName containsString:@".3."]) {
        if (season < 2004) {
            return CompetitionSecondDivision;
        }
        return CompetitionLeagueOne;
    }
    if ([fileName containsString:@".4."]) {
        if (season < 2004) {
            return CompetitionThirdDivision;
        }
        return CompetitionLeagueTwo;
    }
    if ([fileName containsString:@".5."]) {
        return CompetitionNationalLeague;
    }
    NSLog(@"Unknown competition: %@", fileName);
    return CompetitionMinor;
}

- (NSDate*)parseDate:(NSString*)dateString {
    static NSDateFormatter *dateFormatter = nil;
    if (dateFormatter == nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"EEEE MMM dd yyyy"];
    }
    return [dateFormatter dateFromString:dateString];
}

- (int)addTeam:(NSString*)teamName {
    teamName = [teamName stringByReplacingOccurrencesOfString:@" FC" withString:@""];
    teamName = [teamName stringByReplacingOccurrencesOfString:@" AFC" withString:@""];
    if ([teamName compare:@"Yeovil Town"] == NSOrderedSame) teamName = @"Yeovil";
    if ([teamName compare:@"Cheltenham Town"] == NSOrderedSame) teamName = @"Cheltenham";
    if ([teamName compare:@"FC Halifax Town"] == NSOrderedSame) teamName = @"Halifax Town";
    if ([teamName containsString:@"Rushden & Diamonds"]) teamName = @"Rushden & Diamonds";
    if ([teamName containsString:@"Hereford"]) teamName = @"Hereford United";
    return [model addTeam:teamName];
}


@end
