//
//  ESDParser.m
//  FootballData
//
//  Created by Ian Copeman on 04/08/2021.
//

#import "ESDParser.h"
#import "FootballModel.h"
#import "WebPageManager.h"
#import "StringSearch.h"
#import "NSString+CSVParser.h"

#define FilePath (@"/ESD/")
#define FileNameLeague (@"england.csv")
#define FileNamePlayoffs (@"englandplayoffs.csv")
#define FileNameFACup (@"facup.csv")
#define FileNameLeagueCup (@"leaguecup.csv")
#define FileNameChampionsLeague (@"champs.csv")


//
// Files taken from: https://github.com/jalapic/engsoccerdata/tree/master/data-raw

@implementation ESDParser {
    WebPageManager* webPageManager;
    FootballModel* model;
    FileManager* fileManager;
    NSString* dataPath;

}

- (void)parseAll {
    model = [FootballModel getModel];
    fileManager = [[FileManager alloc] init];
    dataPath  = [[NSUserDefaults standardUserDefaults] objectForKey:@"dataPath"];
    webPageManager = [WebPageManager getWebPageManager];
    NSUInteger matchCount = [model getMatchCount];

    //
    // Parse data files
    //[self parseFACup];
    // [self parseLeague];
    //[self parsePlayoffs];
    //[self parseLeagueCup];
    [self parseChampionsLeague];

    //
    // Report new matches
    NSUInteger matchCount2 = [model getMatchCount];
    if (matchCount == matchCount2) {
        NSLog(@"No matches found");
    }
    else {
        NSLog(@"Found %lu new matches", matchCount2 - matchCount);
    }

    
    
    int ian = 10;
}

- (void)parseLeague {
    //
    // First leage 1888-89
    NSLog(@"League");
    
    //
    // Get file, don't download
    NSString* webPageString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:nil dataKey:FileNameLeague];
    NSArray* rows = [webPageString csvRows];
    int season = 0;
    int count = 0;
    for (NSArray *row in rows){
        ++count;
        if (row.count != 12) {
            break;
        }
        NSString* dateString = row[0];
        if ([dateString containsString:@"Date"]) continue;
        NSString* seasonString = row[1];
        NSString* homeString = row[2];
        NSString* awayString = row[3];
        NSString* scoreString = row[4];
        NSString* divisionString = row[7];
        
        int team1ID = [self addTeam:homeString];
        int team2ID = [self addTeam:awayString];
        
        //
        // Get match key
        NSString* matchKey = [Match makeMatchKey:dateString team1:team1ID team2:team2ID];
        
        //
        // Check match
        Match* match = [model getMatch:matchKey];
        if (!match) {
            match = [[Match alloc] init];
            
            season = [seasonString intValue];
            divisionString = [divisionString stringByReplacingOccurrencesOfString:@"\"" withString:@""];
            if ([divisionString containsString:@"3S"]) {
                match.competition = CompetitionThirdDivisionSouth;
            }
            else if ([divisionString containsString:@"3N"]) {
                match.competition = CompetitionThirdDivisionNorth;
            }
            else {
                int division = [divisionString intValue];
                match.competition = [self getCompetition:division season:season];
            }
            [match setMatchTeamsAndDateString:dateString team1:team1ID team2:team2ID];
            if ([model checkMatch:match.matchKey]) {
                continue;
            }
            
            //
            // Score
            NSArray* scoreArray = [scoreString componentsSeparatedByString:@"-"];
            int score1 = [scoreArray[0] intValue];
            int score2 = [scoreArray[1] intValue];
            [match setScore:score1 teamID:team1ID];
            [match setScore:score2 teamID:team2ID];
            
            match.homeTeamID = team1ID; // League match, so location is the home team
            [match setSeason:season];
            [model addMatch:match];
        }
        /*
        if (count == 1500) {
            NSLog(@"Last match parsed key: %@", match.matchKey);
            
            break;
        }
         */
    }
    NSLog(@"Last season parsed: %d", season);
}

- (void)parsePlayoffs {
    NSLog(@"Playoffs");
    //
    // Get file, don't download
    NSString* webPageString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:nil dataKey:FileNamePlayoffs];
    NSArray* rows = [webPageString csvRows];
    int count = 0;
    for (NSArray *row in rows){
        ++count;
        if (row.count < 17) {
            break;
        }
        NSString* dateString = row[0];
        if ([dateString containsString:@"Date"]) continue;
        NSString* seasonString = row[1];
        NSString* homeString = row[2];
        NSString* awayString = row[3];
        NSString* scoreString = row[4];
        NSString* divisionString = row[7];
        NSString* venueString = row[15];
        NSString* neutralString = row[16];
        
        int team1ID = [self addTeam:homeString];
        int team2ID = [self addTeam:awayString];
        
        //
        // Get match key
        NSString* matchKey = [Match makeMatchKey:dateString team1:team1ID team2:team2ID];
        if (matchKey == nil) continue;
        
        //
        // Check match
        Match* match = [model getMatch:matchKey];
        if (!match) {
            match = [[Match alloc] init];
            
            int season = [seasonString intValue];
            divisionString = [divisionString stringByReplacingOccurrencesOfString:@"\"" withString:@""];
            if ([divisionString containsString:@"3S"]) {
                match.competition = CompetitionThirdDivisionSouth;
            }
            else if ([divisionString containsString:@"3N"]) {
                match.competition = CompetitionThirdDivisionNorth;
            }
            else if ([divisionString containsString:@"test match"]) {
                match.competition = CompetitionLeagueTestMatch;
            }
            else {
                int division = [divisionString intValue];
                match.competition = [self getCompetition:division season:season];
            }
            [match setMatchTeamsAndDateString:dateString team1:team1ID team2:team2ID];
            if ([model checkMatch:match.matchKey]) {
                continue;
            }
            
            //
            // Score
            NSArray* scoreArray = [scoreString componentsSeparatedByString:@"-"];
            int score1 = [scoreArray[0] intValue];
            int score2 = [scoreArray[1] intValue];
            [match setScore:score1 teamID:team1ID];
            [match setScore:score2 teamID:team2ID];

            //
            // Location
            if ([neutralString containsString:@"yes"]) {
                //
                // Played at a neutral ground
                match.locationID = [model addLocation:venueString];
            }
            else {
                match.homeTeamID = team1ID; // Location is the home team
            }
            [match setSeason:season];
            [model addMatch:match];
        }
        /*
        if (count == 1500) {
            NSLog(@"Last match parsed key: %@", match.matchKey);
            
            break;
        }
         */
        int ian = 10;
    }
}

- (void)parseFACup {
    NSLog(@"FA Cup");
   //
    // Get file, don't download
    NSString* webPageString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:nil dataKey:FileNameFACup];
    NSArray* rows = [webPageString csvRows];
    int count = 0;
    for (NSArray *row in rows){
        ++count;
        if (row.count < 19) {
            break;
        }
        NSString* dateString = row[0];
        if ([dateString containsString:@"Date"]) continue;
        if ([dateString compare:@"NA"] == NSOrderedSame) continue;

        NSString* seasonString = row[1];
        int season = [seasonString intValue];
        
        if (season < 1991) continue;
        if (season == 1992) break;

        NSString* homeString = row[2];
        NSString* awayString = row[3];
        NSString* scoreString = row[4];
        NSString* roundString = row[7];
        NSString* venueString = row[14];
        NSString* neutralString = row[18];
  
        
        //
        // Parse Date
        int matchDateNumber = [self parseDateNumber:dateString];
        if (matchDateNumber == 0) continue;

        //
        // Add Team
        int team1ID = [self addTeam:homeString];
        int team2ID = [self addTeam:awayString];
        
        //
        // Check match
        Match* match = [model getMatch:matchDateNumber teamID1:team1ID teamID2:team2ID];
        if (!match) {
            match = [[Match alloc] init];
            
            //
            // Set competition and flag
            match.competition = CompetitionFACup;
            if ([roundString containsString:@"s"]) {
                match.competitionFlag = COMPETITION_SEMI_FINAL_FLAG;
            }
            else if ([roundString containsString:@"f"]) {
                match.competitionFlag = COMPETITION_FINAL_FLAG;
            }
            else {
                match.competitionFlag = [roundString intValue];
            }
            [match setMatchTeamsAndDateNumber:matchDateNumber team1:team1ID team2:team2ID];
            /*if ([model checkMatch:match.matchKey]) {
                continue;
            }*/
            
            //
            // Score
            if ([scoreString compare:@"NA"] == NSOrderedSame) {
                //
                // Unknown score - set to -1
                [match setScore:-1 teamID:team1ID];
                [match setScore:-1 teamID:team2ID];
            }
            else {
                NSArray* scoreArray = [scoreString componentsSeparatedByString:@"-"];
                int score1 = [scoreArray[0] intValue];
                int score2 = [scoreArray[1] intValue];
                [match setScore:score1 teamID:team1ID];
                [match setScore:score2 teamID:team2ID];
            }
            
            //
            // Location
            if ([neutralString containsString:@"yes"]) {
                //
                // Played at a neutral ground
                [match setLocation:venueString];
            }
            else {
                match.homeTeamID = team1ID; // Location is the home team
            }
            [match setSeason:season];
            [model matchIsModified:match];
            [model addMatch:match];
        }
        /*
        if (count == 3500) {
            NSLog(@"Last match parsed key: %@", match.matchKey);
            
            break;
        }
         */
        int ian = 10;
    }
}

- (void)parseLeagueCup {
    NSLog(@"League Cup");
    //
    // Get file, don't download
    NSString* webPageString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:nil dataKey:FileNameLeagueCup];
    NSArray* rows = [webPageString csvRows];
    int count = 0;
    for (NSArray *row in rows){
        ++count;
        if (row.count < 15) {
            break;
        }
        NSString* dateString = row[0];
        if ([dateString containsString:@"Date"]) continue;
        if ([dateString compare:@"NA"] == NSOrderedSame) continue;
        
        NSString* seasonString = row[1];
        int season = [seasonString intValue];
        //if (season < 1990) continue;
        if (season == 1992) break;

        NSString* homeString = row[2];
        NSString* awayString = row[3];
        NSString* scoreString = row[4];
        NSString* roundString = row[7];
        NSString* venueString = row[12];
        
        //
        // Parse Date
        int matchDateNumber = [self parseDateNumber:dateString];
        if (matchDateNumber == 0) continue;
        
        int team1ID = [self addTeam:homeString];
        int team2ID = [self addTeam:awayString];
        
        //
        // Check match
        Match* match = [model getMatch:matchDateNumber teamID1:team1ID teamID2:team2ID];
        if (!match) {
            match = [[Match alloc] init];
            //
            // Set competition and flag
            match.competition = CompetitionLeagueCup;
            if ([roundString containsString:@"semi"]) {
                match.competitionFlag = COMPETITION_SEMI_FINAL_FLAG;
            }
            else if ([roundString containsString:@"final"]) {
                match.competitionFlag = COMPETITION_FINAL_FLAG;
            }
            else {
                match.competitionFlag = [roundString intValue];
            }
            [match setMatchTeamsAndDateNumber:matchDateNumber team1:team1ID team2:team2ID];
            
            //
            // Score
            NSArray* scoreArray = [scoreString componentsSeparatedByString:@"-"];
            int score1 = [scoreArray[0] intValue];
            int score2 = [scoreArray[1] intValue];
            [match setScore:score1 teamID:team1ID];
            [match setScore:score2 teamID:team2ID];
            
            //
            // Location
            if ([venueString compare:@"NA"] == NSOrderedSame) {
                //
                // Location is the home team
                match.homeTeamID = team1ID;
            }
            else {
                //
                // Played at a neutral ground
                [match setLocation:venueString];
            }
            [match setSeason:season];
            [model matchIsModified:match];
            [model addMatch:match];
        }
        /*
         if (count == 3500) {
         NSLog(@"Last match parsed key: %@", match.matchKey);
         
         break;
         }
         */
        int ian = 10;
    }
}

- (void)parseChampionsLeague {
    NSLog(@"Champions League");
    //
    // Get file, don't download
    NSString* webPageString = [fileManager loadPersistedString:dataPath dataName:FilePath dataType:nil dataKey:FileNameChampionsLeague];
    NSArray* rows = [webPageString csvRows];
    int count = 0;
    for (NSArray *row in rows){
        ++count;
        if (row.count < 23) {
            break;
        }
        NSString* dateString = row[0];
        if ([dateString containsString:@"Date"]) continue;
        if ([dateString compare:@"NA"] == NSOrderedSame) continue;
        if ([dateString compare:@"2001-10-16"] == NSOrderedSame) {
            int ian = 10;
        }

        NSString* seasonString = row[1];
        int season = [seasonString intValue];
        if (season < 2000) continue;
        // if (season == 2000) break;

        NSString* roundString = row[2];
        NSString* homeString = row[4];
        NSString* awayString = row[5];
        NSString* scoreString = row[6];
        NSString* pensString = row[9];
        NSString* homeCountryString = row[21];
        NSString* awayCountryString = row[22];
        NSString* venueString = row[3];
/*
        if ([homeString containsString:@"Knatts"]) {
            int ian = 10;
        }
        if ([awayString containsString:@"Knatts"]) {
            int ian = 10;
        }
*/
        //
        // Parse Date
        int matchDateNumber = [self parseDateNumber:dateString];
        if (matchDateNumber == 0) continue;

        int team1ID = [self addTeam:homeString countryString:homeCountryString];
        int team2ID = [self addTeam:awayString countryString:awayCountryString];
        
        //
        // Check match
        Match* match = [model getMatch:matchDateNumber teamID1:team1ID teamID2:team2ID];
        if (!match) {
            match = [[Match alloc] init];
            
            //
            // Set competition and flag
            match.competition = CompetitionEuropeanCup;
            match.competitionFlag = [self getCompetitionFlag:roundString];
            if (match.competitionFlag == 0) {
                int ian = 0;
            }
            [match setMatchTeamsAndDateNumber:matchDateNumber team1:team1ID team2:team2ID];

            //
            // Penalties and match flag
            if ([pensString compare:@"NA"] != NSOrderedSame) {
                if ([pensString containsString:@"coin toss"]) {
                    match.matchFlag = MatchFlagDecidedCoinToss;
                }
                else if ([pensString containsString:@"away goals"]) {
                    match.matchFlag = MatchFlagDecidedAwayGoals;
                }
                else if ([pensString containsString:@"walkover"]) {
                    match.matchFlag = MatchFlagDecidedWalkover;
                }
                else {
                    if ([pensString containsString:@"-"]) {
                        NSArray* scoreArray = [pensString componentsSeparatedByString:@"-"];
                        int score1 = [scoreArray[0] intValue];
                        int score2 = [scoreArray[1] intValue];
                        [match setPenalties:score1 teamID:team1ID];
                        [match setPenalties:score2 teamID:team2ID];
                    }
                    else {
                        int ian = 10;
                    }
                }
            }
            
            //
            // Score
            NSArray* scoreArray = [scoreString componentsSeparatedByString:@"-"];
            int score1 = [scoreArray[0] intValue];
            int score2 = [scoreArray[1] intValue];
            [match setScore:score1 teamID:team1ID];
            [match setScore:score2 teamID:team2ID];
            
            //
            // Location
            if ([venueString compare:@"NA"] == NSOrderedSame) {
                //
                // Venue is not home team, but unknown
                match.homeTeamID = 0;
            }
            else {
                //
                // Played at team1's ground
                match.homeTeamID = team1ID;
            }
            [match setSeason:season];
            [model matchIsModified:match];
            [model addMatch:match];
        }
        /*
         if (count == 3500) {
         NSLog(@"Last match parsed key: %@", match.matchKey);
         
         break;
         }
         */
        int ian = 10;
    }
}

- (void)exportCSVFiles {
    model = [FootballModel getModel];
    [model exportCSVMatches];
}

- (CompetitionType)getCompetition:(int)division season:(int)season{
    
    if (division == 1) {
        if (season >= 1992) {
            return CompetitionPremierLeague;
        }
        return CompetitionFirstDivision;
    }
    if (division == 2) {
        if (season < 1992) {
            return CompetitionSecondDivision;
        }
        if (season < 2004) {
            return CompetitionFirstDivision;
        }
        return CompetitionChampionship;
    }
    if (division == 3) {
        if (season < 1992) {
            return CompetitionThirdDivision;
        }
        if (season < 2004) {
            return CompetitionSecondDivision;
        }
        return CompetitionLeagueOne;
    }
    if (division == 4) {
        if (season < 1992) {
            return CompetitionFourthDivision;
        }
        if (season < 2004) {
            return CompetitionThirdDivision;
        }
        return CompetitionLeagueTwo;
    }

    return CompetitionMinor;
}

- (int)getCompetitionFlag:(NSString*)roundString {
    if ([roundString containsString:@"semi"] ||
        [roundString containsString:@"SF"]) {
        return COMPETITION_SEMI_FINAL_FLAG;
    }
    else if ([roundString containsString:@"final"]) {
        return COMPETITION_FINAL_FLAG;
    }
    else if ([roundString containsString:@"QF"]) {
        return COMPETITION_QUARTER_FINAL_FLAG;
    }
    else if ([roundString containsString:@"R16"]) {
        return COMPETITION_R_16_FLAG;
    }
    else if ([roundString containsString:@"GroupA-prelim"]) {
        return COMPETITION_1_GroupA_FLAG;
    }
    else if ([roundString containsString:@"GroupB-prelim"]) {
        return COMPETITION_1_GroupB_FLAG;
    }
    else if ([roundString containsString:@"GroupC-prelim"]) {
        return COMPETITION_1_GroupC_FLAG;
    }
    else if ([roundString containsString:@"GroupD-prelim"]) {
        return COMPETITION_1_GroupD_FLAG;
    }
    else if ([roundString containsString:@"GroupE-prelim"]) {
        return COMPETITION_1_GroupE_FLAG;
    }
    else if ([roundString containsString:@"GroupF-prelim"]) {
        return COMPETITION_1_GroupF_FLAG;
    }
    else if ([roundString containsString:@"GroupA-inter"]) {
        return COMPETITION_2_GroupA_FLAG;
    }
    else if ([roundString containsString:@"GroupB-inter"]) {
        return COMPETITION_2_GroupB_FLAG;
    }
    else if ([roundString containsString:@"GroupC-inter"]) {
        return COMPETITION_2_GroupC_FLAG;
    }
    else if ([roundString containsString:@"GroupD-inter"]) {
        return COMPETITION_2_GroupD_FLAG;
    }
    else if ([roundString containsString:@"GroupA"]) {
        return COMPETITION_GroupA_FLAG;
    }
    else if ([roundString containsString:@"GroupB"]) {
        return COMPETITION_GroupB_FLAG;
    }
    else if ([roundString containsString:@"GroupC"]) {
        return COMPETITION_GroupC_FLAG;
    }
    else if ([roundString containsString:@"GroupD"]) {
        return COMPETITION_GroupD_FLAG;
    }
    else if ([roundString containsString:@"GroupE"]) {
        return COMPETITION_GroupE_FLAG;
    }
    else if ([roundString containsString:@"GroupF"]) {
        return COMPETITION_GroupF_FLAG;
    }
    else if ([roundString containsString:@"GroupG"]) {
        return COMPETITION_GroupG_FLAG;
    }
    else if ([roundString containsString:@"GroupH"]) {
        return COMPETITION_GroupH_FLAG;
    }
    else if ([roundString containsString:@"prelim"] ||
             [roundString containsString:@"PrelimF"]) {
        return COMPETITION_PRELIM_FLAG;
    }
    else if ([roundString containsString:@"Q-1"]) {
        return COMPETITION_Q_1_FLAG;
    }
    else if ([roundString containsString:@"Q-2"]) {
        return COMPETITION_Q_2_FLAG;
    }
    else if ([roundString containsString:@"Q-3"]) {
        return COMPETITION_Q_3_FLAG;
    }
    else if ([roundString containsString:@"Q-PO"]) {
        return COMPETITION_Q_PO_FLAG;
    }
    else {
        roundString = [roundString extractNumberFromText];
        return [roundString intValue];
    }

}




- (int)addTeam:(NSString*)teamName {
    teamName = [ESDParser modifyTeamName:teamName];
    return [model addTeam:teamName];
}
- (int)addTeam:(NSString*)teamName countryString:(NSString*)countryString {
    teamName = [ESDParser modifyTeamNameEuro:teamName];
    return [model addTeam:teamName countryString:countryString];
}

+ (NSString*)modifyTeamName:(NSString*)originalName {
    //
    // English
    if ([originalName compare:@"Ashford Town (Kent)"] == NSOrderedSame) return @"Ashford Town";
    if ([originalName compare:@"Accrington F.C."] == NSOrderedSame) return @"Accrington Stanley";
    if ([originalName compare:@"Accrington"] == NSOrderedSame) return @"Accrington Stanley";
    if ([originalName compare:@"Barrow AFC"] == NSOrderedSame) return @"Barrow";
    if ([originalName compare:@"Chester"] == NSOrderedSame) return @"Chester City";
    if ([originalName compare:@"Leytonstone/Ilford"] == NSOrderedSame) return @"Leytonstone";
    if ([originalName compare:@"Nuneaton Borough AFC"] == NSOrderedSame) return @"Nuneaton Borough";
    if ([originalName compare:@"Workington Town"] == NSOrderedSame) return @"Workington";
    if ([originalName compare:@"Stevenage Borough"] == NSOrderedSame) return @"Stevenage";
    if ([originalName compare:@"Walsall Town"] == NSOrderedSame) return @"Walsall";
    if ([originalName compare:@"Oswestry"] == NSOrderedSame) return @"Oswestry Town";
    if ([originalName compare:@"Leek"] == NSOrderedSame) return @"Leek Town";
    if ([originalName compare:@"Newark"] == NSOrderedSame) return @"Newark Town";
    if ([originalName compare:@"Witton"] == NSOrderedSame) return @"Witton Albion";
    if ([originalName compare:@"Matlock"] == NSOrderedSame) return @"Matlock Town";
    if ([originalName compare:@"Oswardthistle Rovers"] == NSOrderedSame) return @"Oswaldtwistle Rovers";
    if ([originalName compare:@"Kidderminster"] == NSOrderedSame) return @"Kidderminster Harriers";
    if ([originalName compare:@"Kettering"] == NSOrderedSame) return @"Kettering Town";
    if ([originalName compare:@"Mexborough Athletic"] == NSOrderedSame) return @"Mexborough";
    if ([originalName compare:@"Poole"] == NSOrderedSame) return @"Poole Town";
    if ([originalName compare:@"Vauxhall Motors (Luton)"] == NSOrderedSame) return @"Vauxhall Motors";
    if ([originalName compare:@"Rossendale United"] == NSOrderedSame) return @"Rossendale";
    if ([originalName compare:@"Weston-super-Mare"] == NSOrderedSame) return @"Weston-Super-Mare";
    if ([originalName compare:@"St. Albans City"] == NSOrderedSame) return @"St Albans City";

    
    return originalName;
}

+ (NSString*)modifyTeamNameEuro:(NSString*)originalName {
    
     //
     // Eurpoean
     if ([originalName compare:@"ACF Fiorentina"] == NSOrderedSame) return @"Fiorentina";
     if ([originalName compare:@"Abderden"] == NSOrderedSame) return @"Aberdeen";
     if ([originalName compare:@"AFC Ajax"] == NSOrderedSame) return @"Ajax";
     if ([originalName compare:@"AS Roma"] == NSOrderedSame) return @"Roma";
     if ([originalName compare:@"AS Saint-Etienne"] == NSOrderedSame) return @"Saint-Étienne";
     if ([originalName compare:@"Astana-64"] == NSOrderedSame) return @"Astana";
     if ([originalName compare:@"Austria Wien"] == NSOrderedSame) return @"Austria Vienna";
     if ([originalName compare:@"AZ Alkmaar"] == NSOrderedSame) return @"AZ 67 Alkmaar";
     if ([originalName compare:@"B 1903 Kobenhavn"] == NSOrderedSame) return @"F.C. Copenhagen";
     if ([originalName compare:@"Borussia Monchengladbach"] == NSOrderedSame) return @"Borussia Moenchengladbach";
     if ([originalName compare:@"Crvena Zvezda"] == NSOrderedSame) return @"Red Star Belgrade";
     if ([originalName compare:@"CSKA Moskva"] == NSOrderedSame) return @"CSKA Moscow";
     if ([originalName compare:@"Debreceni VSC"] == NSOrderedSame) return @"Debrecen VSC";
     if ([originalName compare:@"Dinamo Bucuresti"] == NSOrderedSame) return @"Dinamo Bucharest";
     if ([originalName compare:@"Dinamo Kiev"] == NSOrderedSame) return @"Dynamo Kiev";
     if ([originalName compare:@"FC Zurich"] == NSOrderedSame) return @"FC Zürich";
     if ([originalName compare:@"Frankfurter SG Eintracht"] == NSOrderedSame) return @"Eintracht Frankfurt";
     if ([originalName compare:@"Girondins Bordeaux"] == NSOrderedSame) return @"Bordeaux";
     if ([originalName compare:@"Haka Valkeakoski"] == NSOrderedSame) return @"Haka";
     if ([originalName compare:@"Hibernians"] == NSOrderedSame) return @"Hibernian";
     if ([originalName compare:@"Hibernian FC"] == NSOrderedSame) return @"Hibernian";
     if ([originalName compare:@"Internazionale"] == NSOrderedSame) return @"Inter Milan";
     if ([originalName compare:@"Jeunesse Esch"] == NSOrderedSame) return @"Jeunesse d'Esch";
     if ([originalName compare:@"Knattspyrnuf&#233;lag Reykjav&#237;kur"] == NSOrderedSame) return @"Knattspyrnufélag Reykjavíkur";
     //  if ([originalName compare:@"Knattspyrnuf\U00e9lag Reykjav\U00edkur";
     if ([originalName compare:@"Koln"] == NSOrderedSame) return @"Cologne";
     if ([originalName compare:@"Malmo FF"] == NSOrderedSame) return @"Malmo";
     if ([originalName compare:@"Odense BK"] == NSOrderedSame) return @"Odense Boldklub";
     if ([originalName compare:@"Olympique Lyon"] == NSOrderedSame) return @"Olympique Lyonnais";
     if ([originalName compare:@"Olympique Marseille"] == NSOrderedSame) return @"Marseille";
     if ([originalName compare:@"OPS Oulu"] == NSOrderedSame) return @"Oulu Palloseura";
     if ([originalName compare:@"PFC Ludogorets Razgrad"] == NSOrderedSame) return @"Ludogorets Razgrad";
     if ([originalName compare:@"Porto"] == NSOrderedSame) return @"FC Porto";
     if ([originalName compare:@"RSC Anderlecht"] == NSOrderedSame) return @"Anderlecht";
     if ([originalName compare:@"SL Benfica"] == NSOrderedSame) return @"Benfica";
     if ([originalName compare:@"Spartak Moskva"] == NSOrderedSame) return @"Spartak Moscow";
     if ([originalName compare:@"Sparta Praha"] == NSOrderedSame) return @"AC Sparta Praha";
     if ([originalName compare:@"The New Saints"] == NSOrderedSame) return @"Total Network Solutions";
     if ([originalName compare:@"Valencia CF"] == NSOrderedSame) return @"Valencia";
     if ([originalName compare:@"Zalgiris Vilnius"] == NSOrderedSame) return @"FK Zalgiris Vilnius";
 
    if ([originalName compare:@"FC Yerevan"] == NSOrderedSame) return @"Yerevan";
    if ([originalName compare:@"Lusitanos"] == NSOrderedSame) return @"Lusitans";
    
    return originalName;
}

- (int)parseDateNumber:(NSString*)matchDateString {
    //
    // Dates come in as 1959-01-10
    NSString* dateNumberString = [matchDateString stringByReplacingOccurrencesOfString:@"-" withString:@""];
    int dateNumberOut = [dateNumberString intValue];
    int year = (dateNumberOut / 10000);
    if (year < 1870 || year > 2030) {
        NSLog(@"Failed to parse date string: %@", matchDateString);
    }
    
    return dateNumberOut;
}


@end
