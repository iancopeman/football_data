//
//  MatchViewerBridge.m
//  FootballData
//
//  Created by Ian Copeman on 18/10/2023.
//

#import "MatchViewerBridge.h"
#import "FootballModel.h"
#import "FootballData-Swift.h"

@implementation MatchViewerBridge {
    FootballModel* model;
}

//
// Static singleton
static MatchViewerBridge* sharedSingleton;

+ (void)initialize {
    static BOOL initialized = NO;
    if(!initialized)
    {
        initialized = YES;
        sharedSingleton = [[MatchViewerBridge alloc] init];
        sharedSingleton->model = [FootballModel getModel];
    }
}

+ (MatchViewerBridge*)getModelBridge {
    return sharedSingleton;
}

- (NSArray*)getSeasonList {
    return @[@(2020), @(2021), @(2022)];
}

- (NSArray*)getCompetitionList {
    return @[[Match getCompetitionShortName:CompetitionPremierLeague],
             [Match getCompetitionShortName:CompetitionFACup]];
}

- (NSArray*)getFeaturedTeamList {
    return @[@"Spurs", @"Arsenal", @"Liverpool"];
}

- (NSArray*)getMatchList:(NSString*)competitionString teamName:(NSString*)teamName season:(int)season {
    if ([teamName containsString:@"Spurs"]) teamName = @"Tottenham Hotspur";
    int teamID = [model getTeamID:teamName];
    if (teamID == 0) {
        int ian = 10;
        return nil;
    }
    CompetitionType competition = [Match getCompetitionFromShortName:competitionString];
    NSArray* matchArray = [model getAllMatchesInSeason:season competition:competition];
    NSMutableArray* matchList = [NSMutableArray arrayWithCapacity:50];
    for (Match* match in matchArray) {
        if ([match getTeam1ID] == teamID) {
            NSString* matchTitle = [NSString stringWithFormat:@"%@ v %@", teamName, [model getTeamName:[match getTeam2ID]]];
            MatchDetail* matchDetail = [[MatchDetail alloc] initWithTitle:matchTitle matchURL:match.fileURL];
            [matchList addObject:matchDetail];
        }
        else if ([match getTeam2ID] == teamID) {
            NSString* matchTitle = [NSString stringWithFormat:@"%@ v %@", [model getTeamName:[match getTeam1ID]], teamName];
            MatchDetail* matchDetail = [[MatchDetail alloc] initWithTitle:matchTitle matchURL:match.fileURL];
            [matchList addObject:matchDetail];
        }
    }
    
    
   return matchList.count > 0 ? matchList : nil;
}

@end
