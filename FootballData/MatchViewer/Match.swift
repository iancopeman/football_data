//
//  Match.swift
//  FootballData
//
//  Created by Ian Copeman on 18/10/2023.
//

import Foundation

// MARK: - Match
struct Match: Codable {
   // var id: UUID
    var goalMask, matchState: Int
    var location, referee: String
    var teamEvents2, teamEvents1: [TeamEvents]
    var score2, competition, competitionFlag, penaltiesTeam2: Int
    var team2: [String]
    var homeTeamFlag: Int
    var team1: [String]
    var season, penaltiesTeam1: Int
    var teamString2: String
    var timeNumber: Int
    var teamString1: String
    var score1: Int
    var managerKey1, managerKey2: String
    var attendance, dateNumber: Int
}

// MARK: - TeamEvents
struct TeamEvents: Codable, Hashable {
  //  var id: UUID
    var player1ID: String
    var player2ID: String?
    var shirtNumber, eventMatchTime, eventType, position: Int
}
