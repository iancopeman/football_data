//
//  MatchViewerController.swift
//  FootballData
//
//  Created by Ian Copeman on 18/10/2023.
//

import Cocoa
import Foundation
import SwiftUI

@objcMembers
class MatchDetail: NSObject, Identifiable {
    let id = UUID()
    var title: String
    var matchURL: URL
    var match: Match?

    public init(title: String, matchURL: URL) {
        self.title = title
        self.matchURL = matchURL
    }
    
    func getMatch() -> Match? {
        if match != nil {
            return match
        } else {
            match = Bundle.main.decode(Match.self, from: matchURL)
            return match
        }
    }
}

@objc
class SwiftUIViewFactory: NSObject {
    @objc static func makeSwiftUIView(dismissHandler: @escaping (() -> Void)) -> NSViewController {
       // let modelBridge: MatchViewerBridge = MatchViewerBridge.getModel()
        return NSHostingController(rootView: MatchSelectorView(dismiss: dismissHandler))
    }
}

struct MatchSelectorView: View {
    var dismiss: () -> Void = {}
    var modelBridge: MatchViewerBridge
    @State private var seasons: [Int]
    @State private var selectedSeason: Int
    @State private var competitions: [String]
    @State private var selectedCompetition: String
    @State private var teams: [String]
    @State private var selectedTeam: String
    @State private var matches: [MatchDetail]
    @State private var selectedMatch: MatchDetail?
    @State private var selectedMatch2 = Set<UUID>()
    @State private var displayedMatch: Match?

    init (dismiss: @escaping () -> Void = {}) {
        self.dismiss = dismiss
        modelBridge = MatchViewerBridge.getModel()
        let seasonArray = modelBridge.getSeasonList()
        let seasons = (seasonArray as NSArray as? [Int])!
        self.seasons = seasons
        let savedValue = UserDefaults.standard.integer(forKey: "MatchSelectorSeason")
        if (savedValue >= seasons.first! && savedValue <= seasons.last!) {
            self.selectedSeason = savedValue
        }
        else {
            self.selectedSeason = seasons.last!
        }

        let competitionArray = modelBridge.getCompetitionList()
        let competitions = (competitionArray as NSArray as? [String])!
        self.competitions = competitions
        let savedValueString = UserDefaults.standard.string(forKey: "MatchSelectorCompetition")
        self.selectedCompetition = savedValueString ?? competitions.last!
        
        let teamArray = modelBridge.getFeaturedTeamList()
        let teams = (teamArray as NSArray as? [String])!
        self.teams = teams
        let savedValueString2 = UserDefaults.standard.string(forKey: "MatchSelectorTeam")
        self.selectedTeam = savedValueString2 ?? teams.first!
        
        matches = []
    }
    
    @State private var enableLogging = false
    
    var body: some View {
        VStack(spacing: 8) {
            Form {
                Picker("Year:", selection: $selectedSeason) {
                    ForEach(seasons, id: \.self) {
                        Text(String($0))
                    }
                }
                .pickerStyle(.menu)
                
                
                Picker("Competition:", selection: $selectedCompetition) {
                    ForEach(competitions, id: \.self) {
                        Text($0)
                    }
                }
                .pickerStyle(.menu)
                
                Picker("Team:", selection: $selectedTeam) {
                    ForEach(teams, id: \.self) {
                        Text($0)
                    }
                }
                .pickerStyle(.menu)
            }.navigationTitle("Select Match")
            // Toggle("Enable Logging", isOn: $enableLogging)
            Button("Show Matches") {
                saveState()
                let matchArray = modelBridge.getMatchList(selectedCompetition, teamName: selectedTeam, season: Int32(selectedSeason))
                if matchArray.count != 0 {
                    matches = (matchArray as NSArray as? [MatchDetail])!
                    
                }
                
            }
            List(matches, id: \.self, selection: $selectedMatch) {
                    Text($0.title)
            }.frame(height: 300)
            //Text(selectedMatch?.title ?? "-")
            
            MatchView(matchDetail: selectedMatch)
            
            Button("Save changes") {
               // if (selectedMatch != nil) {
                //    displayedMatch = Bundle.main.decode(Match.self, from: selectedMatch!.matchURL)
                 //   let ian = 10
               // }
                saveState()
            }
            Button(action: {
                dismiss()
            }, label: {
                Text("Close")
            })
            Spacer()
        }.frame(width: 800, height: 1200)
        
        
    }
    
    func saveState() {
        UserDefaults.standard.set(self.selectedSeason, forKey: "MatchSelectorSeason")
        UserDefaults.standard.set(self.selectedCompetition, forKey: "MatchSelectorCompetition")
        UserDefaults.standard.set(self.selectedTeam, forKey: "MatchSelectorTeam")
    }
}

class MatchViewerController: NSViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }
    
}

// MARK: - Preview
struct MatchSelectorView_Previews: PreviewProvider {
    static var previews: some View {
        //let modelBridge: MatchViewerBridge = MatchViewerBridge.getModel()
        return MatchSelectorView()
    }
}
