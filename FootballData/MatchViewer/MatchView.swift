//
//  MatchView.swift
//  FootballData
//
//  Created by Ian Copeman on 18/10/2023.
//

import SwiftUI

struct MatchView: View {
    var matchDetail: MatchDetail?
    var match: Match?

    init (matchDetail: MatchDetail?) {
        self.matchDetail = matchDetail
        self.match = matchDetail?.getMatch()
 
    }
    
    var body: some View {
        if let _match = match {
            VStack {
                MatchHeader(match: _match)
                HStack(alignment: .top) {
                    TeamList(title: _match.teamString1, events: _match.teamEvents1)
                    TeamList(title: _match.teamString2, events: _match.teamEvents2)
                }
                Text("Events")
                    .font(.subheadline)
                HStack(alignment: .top) {
                    TeamEventList(title: _match.teamString1, events: _match.teamEvents1)
                    TeamEventList(title: _match.teamString2, events: _match.teamEvents2)
                }
          }

        }
/*
        match.map( {_ in
            VStack {
                Text($0.teamString1)
            }
 */
            /*
            VStack {
               // Text($0.teamString1 + " v " + $0.teamString2)
                /*
                HStack {
                   // match.map({TeamList(title: $0.teamString1, team: $0.team1)})
                   // match.map({TeamList(title: $0.teamString2, team: $0.team2)})
                }
                 */
  //              Text(match?.teamString2 ?? "")
            }
*/
      //  })
    }
}

struct TeamList: View {
    var title: String
    //var team: [String]
    var events: [TeamEvents]
    
    var body: some View {
        VStack {
            
            Text(title)
                .font(.title)
            ForEach (events, id: \.self) {
                if ($0.eventMatchTime == 0) {
                    Text($0.player1ID)
                }
            }
        }
    }
}

struct TeamEventList: View {
    var title: String
    //var team: [String]
    var events: [TeamEvents]
    
    var body: some View {
        VStack {
            
            Text(title)
                .font(.title)
            ForEach (events, id: \.self) {
                if ($0.eventMatchTime != 0) {
                    let eventType = $0.eventType & 0x0F
                    if (eventType == EventSub.rawValue) {
                        Text(String(format: "%d SUB: %@ -> %@",  $0.eventMatchTime, $0.player1ID, $0.player2ID ?? ""))
                    }
                    if (eventType == EventPenalty.rawValue) {
                        Text(String(format: "%d PEN: %@",  $0.eventMatchTime, $0.player1ID))
                    }
                    if (eventType == EventYellowCard.rawValue) {
                        Text(String(format: "%d YELLOW: %@",  $0.eventMatchTime, $0.player1ID))
                    }
                    if (eventType == EventRedCard.rawValue) {
                        Text(String(format: "%d RED: %@",  $0.eventMatchTime, $0.player1ID))
                    }
                    if (eventType == EventGoal.rawValue) {
                        if let assist = $0.player2ID {
                            Text(String(format: "%d GOAL: %@ (Assist: %@)",  $0.eventMatchTime, $0.player1ID, assist))
                        }
                        else {
                            Text(String(format: "%d GOAL: %@",  $0.eventMatchTime, $0.player1ID))
                        }
                    }
                }
            }
        }
    }
}

struct MatchHeader: View {
    var match: Match
    
    var body: some View {
        VStack {
            Text(String(format: "%@ %d : %d %@", match.teamString1, match.score1, match.score2, match.teamString2))
                .font(.title)
            Text(String(format: "DateNumber: %d\t Location: %@\t Attendance: %d", match.dateNumber, match.location, match.attendance))
//            Text(match.teamString1 + " " + String(match.score1) + " : " + String(match.score2) + " " + match.teamString2)
           // Text("DateNumber: " + String(match.dateNumber) + "\tLocation: " + match.location + "\tAttendance: " + String(match.attendance))
        }
    }
}


struct MatchView_Previews: PreviewProvider {
    static var previews: some View {
        let team1 = ["Ian", "Messi", "Gazza" ]
        let team2 = ["Salah", "Maradonna", "Jackie", "Alexander" ]
        var match = Match(goalMask: 0, matchState: 0, location: "Location", referee: "Referee", teamEvents2: [], teamEvents1: [], score2: 1, competition: 2, competitionFlag: 2, penaltiesTeam2: 0, team2: team2, homeTeamFlag: 0, team1: team1, season: 2022, penaltiesTeam1: 0, teamString2: "Team2", timeNumber: 1500, teamString1: "Team1", score1: 1, managerKey1: "Manager1", managerKey2: "Manager2", attendance: 9999, dateNumber: 20220816)
        /*
        let fileURL = URL(fileURLWithPath: "///Volumes/FSNData/FootballData/Devlex/Matches/2020s/2022-23/FAC/Complete/20230107_Portsmouth_Tottenham%20Hotspur.json")
        let match = Bundle.main.decode(Match.self, from:fileURL)
         */
        // MatchView(match: match)
    }
}
