//
//  MatchViewerBridge.h
//  FootballData
//
//  Created by Ian Copeman on 18/10/2023.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MatchViewerBridge : NSObject
+ (MatchViewerBridge*)getModelBridge;
- (NSArray*)getSeasonList;
- (NSArray*)getCompetitionList;
- (NSArray*)getFeaturedTeamList;
- (NSArray*)getMatchList:(NSString*)competitionString teamName:(NSString*)teamName season:(int)season;
@end

NS_ASSUME_NONNULL_END
