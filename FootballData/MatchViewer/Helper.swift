//
//  Helper.swift
//  FootballData
//
//  Created by Ian Copeman on 18/10/2023.
//

import Foundation

extension Bundle {
    func decode<T: Decodable>(_ type: T.Type, from fileURL: URL) -> T {
  /*
        guard let url = self.url(forResource: file, withExtension: nil) else {
            fatalError("Failed to locate \(file) in bundle.")
        }
    */
        guard let data = try? Data(contentsOf: fileURL) else {
            fatalError("Failed to load \(fileURL).")
        }
        
        let decoder = JSONDecoder()
        
        guard let loaded = try? decoder.decode(T.self, from: data) else {
            fatalError("Failed to decode \(fileURL).")
            // return nil
        }
        
        return loaded
    }
}
