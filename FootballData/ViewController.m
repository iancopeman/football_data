//
//  ViewController.m
//  FootballData
//
//  Created by Ian Copeman on 16/10/2020.
//

#import "ViewController.h"
#import "WebPageManager.h"
#import "FootballModel.h"

#import "Footballer.h"
#import "WikipediaPlayers.h"
#import "TeamParser.h"
#import "FootballData-Swift.h"

@interface ViewController () {
    WebPageManager* webPageManager;
    __weak IBOutlet NSTextField *labelDataPath;
    __weak IBOutlet NSTextField *labelBlobPath;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);
        NSString* dataPath = paths.firstObject;
        NSLog(@"%@", dataPath);
    }
    //
    // Get last used data path
    NSString* dataPath  = [[NSUserDefaults standardUserDefaults] objectForKey:@"dataPath"];
    if (dataPath == nil) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);
        dataPath = paths.firstObject;
        [[NSUserDefaults standardUserDefaults] setObject:dataPath forKey:@"dataPath"];
    }
    labelDataPath.stringValue = dataPath;
    
    //
    // Get last used blob path
    NSString* blobPath  = [[NSUserDefaults standardUserDefaults] objectForKey:@"blobPath"];
    if (blobPath == nil) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);
        dataPath = paths.firstObject;
        [[NSUserDefaults standardUserDefaults] setObject:dataPath forKey:@"blobPath"];
    }
    labelBlobPath.stringValue = blobPath;
    

}

- (IBAction)dataPathPush:(id)sender {
    NSOpenPanel* panel = [NSOpenPanel openPanel];
    NSString* dataPath  = [[NSUserDefaults standardUserDefaults] objectForKey:@"dataPath"];
    NSURL* dataURL = [NSURL fileURLWithPath:dataPath];

    panel.directoryURL = dataURL;
    [panel setCanChooseDirectories:YES];
    [panel setCanChooseFiles:NO];
    [panel setAllowsMultipleSelection:NO];
    
    // This method displays the panel and returns immediately.
    // The completion handler is called when the user selects an
    // item or cancels the panel.
    [panel beginWithCompletionHandler:^(NSInteger result){
        if (result == NSModalResponseOK) {
            NSURL* dataPath = [[panel URLs] objectAtIndex:0];
            [self setStoragePath:dataPath.absoluteString];
        }
        
    }];
}

- (IBAction)setStoragePath:(NSString*)dataPath {
    [[NSUserDefaults standardUserDefaults] setObject:dataPath forKey:@"dataPath"];
    if (dataPath == nil) {
        labelDataPath.stringValue = @"-";
    }
    else {
        labelDataPath.stringValue = dataPath;
    }

}

- (IBAction)blobPathPush:(id)sender {
    NSOpenPanel* panel = [NSOpenPanel openPanel];
    NSString* dataPath  = [[NSUserDefaults standardUserDefaults] objectForKey:@"blobPath"];
    NSURL* dataURL = [NSURL fileURLWithPath:dataPath];
    
    panel.directoryURL = dataURL;
    [panel setCanChooseDirectories:YES];
    [panel setCanChooseFiles:NO];
    [panel setAllowsMultipleSelection:NO];
    
    // This method displays the panel and returns immediately.
    // The completion handler is called when the user selects an
    // item or cancels the panel.
    [panel beginWithCompletionHandler:^(NSInteger result){
        if (result == NSModalResponseOK) {
            NSURL* dataPath = [[panel URLs] objectAtIndex:0];
            [self setBlobPath:dataPath.absoluteString];
        }
        
    }];
}

- (IBAction)setBlobPath:(NSString*)dataPath {
    [[NSUserDefaults standardUserDefaults] setObject:dataPath forKey:@"blobPath"];
    if (dataPath == nil) {
        labelBlobPath.stringValue = @"-";
    }
    else {
        labelBlobPath.stringValue = dataPath;
    }
    
}


- (NSURL*)applicationDataDirectory {
    NSFileManager* sharedFM = [NSFileManager defaultManager];
    NSString* test = sharedFM.currentDirectoryPath;
    NSURL* test2 = sharedFM.homeDirectoryForCurrentUser;
    NSArray* possibleURLs = [sharedFM URLsForDirectory:NSUserDirectory
                                             inDomains:NSUserDomainMask];
    NSURL* appSupportDir = nil;
    NSURL* appDirectory = nil;
    
    if ([possibleURLs count] >= 1) {
        // Use the first directory (if multiple are returned)
        appSupportDir = [possibleURLs objectAtIndex:0];
    }
    
    // If a valid app support directory exists, add the
    // app's bundle ID to it to specify the final directory.
    if (appSupportDir) {
        NSString* appBundleID = [[NSBundle mainBundle] bundleIdentifier];
        appDirectory = [appSupportDir URLByAppendingPathComponent:appBundleID];
    }
    
    //
    // Create directory
    NSError *error = nil;
    [[NSFileManager defaultManager] createDirectoryAtURL:appDirectory withIntermediateDirectories:YES attributes: nil error:&error];
    return appDirectory;
}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

- (IBAction)buttonMatchViewerPush:(id)sender {
    NSViewController *vc = [SwiftUIViewFactory makeSwiftUIViewWithDismissHandler:^{
        [self dismissViewController:[[self presentedViewControllers] firstObject]];
        // [[self presentedViewController] dismissViewControllerAnimated:YES completion:nil];
    }];
    [self presentViewControllerAsModalWindow:vc];
}


@end
